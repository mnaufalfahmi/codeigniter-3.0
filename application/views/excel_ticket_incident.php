<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=Ticket Incident.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<style>
    td {
        vertical-align: top !important;
    }
</style>

<table style="margin-bottom: 20px;">
    <thead>
        <tr>
            <th colspan="26">Field Support Management</th>
        </tr>
        <tr>
            <th colspan="26">Report Rawdata - Ticket Incident</th>
        </tr>
    </thead>
</table>

<table border="1">
    <thead>
        <tr>
            <th>No.</th>
            <th>ICONTICK_INCIDENT_ID</th> <!-- ismilling id  -->
            <th>SERVICE_ID</th> <!-- service id  -->
            <th>SERVICE_NAME</th> <!-- class  -->
            <!-- <th style="height: 30px">CUSTOMER_ID</th>
            <th>CUSTOMER_NAME</th>
            <th>ORIGIN</th>
            <th>TERMINATION</th> -->
            <th>PROB_REGION</th> <!-- region -->
            <th>PROB_ID</th> <!-- incident -->
            <th>PROB_DESC</th> <!-- incident desc -->
            <th>PROB_CREATOR</th> <!-- incident desc -->
            <th>LVL1</th> <!-- classification -->
            <th>LVL2</th> <!-- root_cause -->
            <th>LVL3</th> <!-- root cause detail -->
            <th>SERVICE_IMPACT</th> <!-- service impact -->
            <th>NETWORK</th> <!-- network -->
            <th>LINK</th> <!-- network -->
            <th>NOTIFICATION_TRIGER</th> <!-- network -->
            <th>PROB_OPEN_TIMESTAMP</th>
            <th>PROB_CLOSE_TIMESTAMP</th>
            <th>STOPPED_TIME (M)</th>
            <th>DURASI (M)</th> <!-- durations -->
            <th>RAW_TTR (M)</th> <!-- mttr -->
            <th>ANALISIS_TIME (M)</th> <!-- analisis time pnoc -->
            <th>TT_CREATE_TIMESTAMP</th> <!-- ticket created -->
            <th>TT_ATTACH_TIMESTAMP</th> <!-- ticket attach -->
            <th>TT_CLOSE_TIMESTAMP</th> <!-- ticket close -->
            <th>RESPON_TIME (M)</th> <!-- response time -->
        </tr>
    </thead>
    <tbody>
        <?php $no = 1 ?>
        <?php foreach ($data as $row) { ?>
            <tr>
                <td><?php echo $no++ ?></td>
                <td><?php echo $row->ismilling_id ?></td> <!-- ismilling id  -->
                <td><?php echo '`'.$row->service_id ?></td> <!-- service id  -->
                <td><?php echo $row->product ?></td> <!-- class  -->
                <!-- <td></td> 
                <td></td> 
                <td></td> 
                <td></td>  -->
                <td><?php echo $row->region  ?></td> <!-- region -->
                <td><?php echo '`'.$row->incident_no ?></td> <!-- incident -->
                <td style="height: 30px; white-space:nowrap;"><?php echo $row->description  ?></td> <!-- incident desc -->
                <td><?php echo $row->prob_creator  ?></td> <!-- incident desc -->
                <td><?php echo $row->classification ?></td> <!-- classification -->
                <td><?php echo $row->root_cause ?></td> <!-- root_cause -->
                <td><?php echo $row->root_cause_detail ?></td> <!-- root cause detail -->
                <td><?php echo $row->service_impact ?></td> <!-- service impact -->
                <td><?php echo $row->network ?></td> <!-- network -->
                <td><?php echo $row->link ?></td> <!-- network -->
                <td><?php echo $row->type ?></td> <!-- network -->
                <td><?php echo $row->start_incident ?></td>
                <td><?php echo $row->close_incident ?></td>
                <td><?php echo $row->stopclock ?></td>
                <td><?php echo $row->duration ?></td> <!-- durations -->
                <td><?php echo $row->ttr ?></td> <!-- mttr -->
                <td><?php echo $row->analisis_time ?></td> <!-- analisis time pnoc -->
                <td><?php echo $row->start_ticket ?></td> <!-- ticket created -->
                <td><?php echo $row->close_ticket ?></td> <!-- ticket attach -->
                <td><?php echo $row->close_ticket ?></td> <!-- ticket close -->
                <td><?php echo $row->response_time ?></td> <!-- response time -->
            </tr>
        <?php } ?>
    </tbody>
</table>

<!-- <table border="1">
    <thead>
        <tr>
            <th>No.</th>
            <th>Prob Creator</th>
            <th>Incident ID</th>
            <th>Description</th>
            <th>Service ID</th>
            <th>Ticket ID</th>
            <th>Token</th>
            <th>Product</th>
            <th>Root Cause</th>
            <th>Region</th>
            <th>Network</th>
            <th>Root Cause Detail</th>
            <th>Classification</th>
            <th>Type</th>
            <th>Start Incident</th>
            <th>Close Incident</th>
            <th>Start Ticket</th>
            <th>First Process</th>
            <th>Close Ticket</th>
            <th>Analisis Time (S)</th>
            <th>Durasi (M)</th>
            <th>TTR (M)</th>
            <th>Responstime Time (S)</th>
        </tr>
    </thead>
    <tbody>
        <?php // $no = 1 
        ?>
        <?php // foreach ($data as $row) { 
        ?>
            <tr>
                <td><?php // echo $no++ 
                    ?></td>
                <td><?php // echo $row->prob_creator 
                    ?></td>
                <td><?php // echo '`'.$row->incident_id 
                    ?></td>
                <td style="height: 30px; white-space:nowrap;"><?php // echo $row->description 
                                                                ?></td>
                <td><?php // echo '`'.$row->service_id 
                    ?></td>
                <td><?php // echo $row->ismilling_id 
                    ?></td>
                <td><?php // echo $row->token 
                    ?></td>
                <td><?php // echo $row->product 
                    ?></td>
                <td><?php // echo $row->root_cause 
                    ?></td>
                <td><?php // echo $row->region 
                    ?></td>
                <td><?php // echo $row->network 
                    ?></td>
                <td><?php // echo $row->root_cause_detail 
                    ?></td>
                <td><?php // echo $row->classification 
                    ?></td>
                <td><?php // echo $row->type 
                    ?></td>
                <td><?php // echo $row->start_incident 
                    ?></td>
                <td><?php // echo $row->close_incident 
                    ?></td>
                <td><?php // echo $row->start_ticket 
                    ?></td>
                <td><?php // echo $row->second_time 
                    ?></td>
                <td><?php // echo $row->close_ticket 
                    ?></td>
                <td><?php // echo $row->analisis_time 
                    ?></td>
                <td><?php // echo $row->duration 
                    ?></td>
                <td><?php // echo $row->ttr 
                    ?></td>
                <td><?php // echo $row->response_time 
                    ?></td>
            </tr>
        <?php // } 
        ?>
    </tbody>
</table> -->