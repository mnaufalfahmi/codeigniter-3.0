<?php

class Base_controller extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        // if (ENVIRONMENT == 'development') {
        //     $this->output->enable_profiler(TRUE);
        // }
    }

    //DEBUG TOOLS
    public function last_query()
    {
        echo '<br/>';
        echo '<b>Query:</b> ' . $this->db->last_query();
        echo '<br/>';
    }

    public function inspect_variable($var)
    {
        echo '<b>Variable:</b><br/>';
        echo '<pre>';
        print_r($var);
        echo '</pre>';
    }

}

class Front_Controller extends Base_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->template->set_layout('index');
        $this->template->set_theme('default');
    }

    public function load_pagination_config()
    {

        $config['suffix']          = '?' . http_build_query($_GET, '', "&");
        $config['first_url']       = $config['base_url'] . $config['suffix'];
        $config['full_tag_open']   = '<div class="pagination"><ul class="pagination">';
        $config['full_tag_close']  = '</ul></div><!--pagination-->';
        $config['first_link']      = '&laquo; Pertama';
        $config['first_tag_open']  = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link']       = 'Terakhir &raquo;';
        $config['last_tag_open']   = '<li class="next page">';
        $config['last_tag_close']  = '</li>';

        $config['next_link']       = 'Berikutnya &rarr;';
        $config['next_tag_open']   = '<li class="next page">';
        $config['next_tag_close']  = '</li>';

        $config['prev_link']       = '&larr; Sebelumnya';
        $config['prev_tag_open']   = '<li class="prev page">';
        $config['prev_tag_close']  = '</li>';

        $config['cur_tag_open']    = '<li class="active page-item"><a href="">';
        $config['cur_tag_close']   = '</a></li>';

        $config['num_tag_open']    = '<li class="page page-item">';
        $config['num_tag_close']   = '</li>';

        return $config;
    }
}


class User_Controller extends Base_Controller
{
    public $user;
    public $crud = [
        'create' => false,
        'read' => false,
        'update' => false,
        'delete' => false
    ];
    public function __construct()
    {
        parent::__construct();
        $this->check_login();
        // $dataUser = $this->session->userdata('user');
        $dataUser   = unserialize($_COOKIE['user'], ["allowed_classes" => false]);
        $this->user = json_decode($dataUser);
        $roleid     = $this->user->role_id; 
        
        $this->template
            ->set('user', $this->user)
            ->set('menus', $this->menu())
            ->set('notification', $this->notification())
            ->set_layout('index');
        $this->template->set_theme('default');
    }

    public function menu()
    {
        $userMenu = array();
        $menus = array(
            "Dashboard" => array(
                "id" => "dashboard",
                "title" => "Dashboard",
                "url" => base_url('/dashboard'),
                "icon" => "fa fa-dashboard",
            ),
            "Ticket" => array(
                "id" => "ticket",
                "title" => "Ticket",
                "url" => "javascript:;",
                "icon" => "fa fa-ticket",
                "sub_menus" => array(
                    array(
                        "id" => "open-ticket",
                        "title" => "Open Ticket",
                        "url" => site_url('ticket/open_ticket'),
                        "icon" => "O",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "close-ticket",
                        "title" => "Close Ticket",
                        "url" => site_url('ticket/close_ticket'),
                        "icon" => "C",
                        "third_menus" => array()
                    )
                )
            ),
            "Administrator" => array(
                "id" => "administrator",
                "title" => "Administrator",
                "url" => "javascript:;",
                "icon" => "fa fa-user-plus",
                "sub_menus" => array(
                    array(
                        "id" => "sub-user-management",
                        "title" => "User Management",
                        "url" => site_url('administrator/user_management'),
                        "icon" => "M",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "sub-user-log",
                        "title" => "User Change Log",
                        "url" => site_url('administrator/user_log'),
                        "icon" => "UC",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "sub-user-log-online",
                        "title" => "User Log Online",
                        "url" => site_url('administrator/user_log/log_onlines'),
                        "icon" => "LO",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "sub-user-log-users",
                        "title" => "All User Log",
                        "url" => site_url('administrator/user_log/log_user'),
                        "icon" => "LC",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "sub-user-access-management",
                        "title" => "Access Management",
                        "url" => site_url('administrator/access_management'),
                        "icon" => "AM",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "sub-group",
                        "title" => "Group",
                        "url" => site_url('administrator/group'),
                        "icon" => "G",
                        "third_menus" => array()
                    ),
                    // array(
                    //     "id" => "sub-roles-group",
                    //     "title" => "Roles Group ",
                    //     "url" => site_url('administrator/roles_group'),
                    //     "icon" => "RG",
                    //     "third_menus" => array()
                    // ),
                    // array(
                    //     "id" => "sub-user-group",
                    //     "title" => "User Group ",
                    //     "url" => site_url('administrator/user_group'),
                    //     "icon" => "UG",
                    //     "third_menus" => array()
                    // )
                )
            ),
            "Customer" => array(
                "id" => "customer",
                "title" => "Customer",
                "url" => "javascript:;",
                "icon" => "fa fa-users",
                "sub_menus" => array(
                    array(
                        "id" => "customer-list",
                        "title" => "Service List",
                        "url" => site_url('customer'),
                        "icon" => "S",
                        "third_menus" => array()
                    ),
                    // array(
                    //     "id" => "customer-trend",
                    //     "title" => "Trend",
                    //     "url" => site_url('customer_list/trend'),
                    //     "icon" => "T",
                    //     "third_menus" => array()
                    // )
                )
            ),
            "field_support" => array(
                "id" => "field-support",
                "title" => "Field Support",
                "url" => "javascript:;",
                "icon" => "fa fa-question-circle",
                "sub_menus" => array(
                    array(
                        "id" => "fs-region-ar",
                        "title" => "FS Region AR",
                        "url" => "javascript:;",
                        "icon" => "FS",
                        "third_menus" => array(
                            array(
                                "id" => "jkt",
                                "title" => "Jakarta",
                                "url" => site_url('field_support/fs_jkt'),
                                "icon" => "JKT"
                            ),
                            array(
                                "id" => "jbr",
                                "title" => "Jawa Barat",
                                "url" => site_url('field_support/fs_jbr'),
                                "icon" => "JBR"
                            ),
                            array(
                                "id" => "jtg",
                                "title" => "Jawa Tengah",
                                "url" => site_url('field_support/fs_jtg'),
                                "icon" => "JTG"
                            ),
                            array(
                                "id" => "jtm",
                                "title" => "Jawa Timur",
                                "url" => site_url('field_support/fs_jtm'),
                                "icon" => "JTM"
                            ),
                            array(
                                "id" => "bli",
                                "title" => "Bali",
                                "url" => site_url('field_support/fs_bli'),
                                "icon" => "BLI"
                            ),
                            array(
                                "id" => "sbu",
                                "title" => "SBU",
                                "url" => site_url('field_support/fs_sbu'),
                                "icon" => "SBU"
                            ),
                            array(
                                "id" => "sbt",
                                "title" => "SBT",
                                "url" => site_url('field_support/fs_sbt'),
                                "icon" => "SBT"
                            ),
                            array(
                                "id" => "sbs",
                                "title" => "SBS",
                                "url" => site_url('field_support/fs_sbs'),
                                "icon" => "SBS"
                            ),
                            array(
                                "id" => "ibt",
                                "title" => "IBT",
                                "url" => site_url('field_support/fs_ibt'),
                                "icon" => "IBT"
                            ),
                            array(
                                "id" => "kal",
                                "title" => "KAL",
                                "url" => site_url('field_support/fs_kal'),
                                "icon" => "KAL"
                            ),
                        )
                    ),
                    array(
                        "id" => "action-request",
                        "title" => "Action Request",
                        "url" => "javascript:;",
                        "icon" => "AR",
                        "third_menus" => array(
                            array(
                                "id" => "open-ar",
                                "title" => "Open AR",
                                "url" => site_url('field_support/action_open_ar'),
                                "icon" => "OAR"
                            ),
                            array(
                                "id" => "close-request-ar",
                                "title" => "Close Request",
                                "url" => site_url('field_support/action_close_request_ar'),
                                "icon" => "CRA"
                            ),
                            array(
                                "id" => "close-ar",
                                "title" => "Close AR",
                                "url" => site_url('field_support/action_close_ar'),
                                "icon" => "CAR"
                            ),
                        )
                    ),
                )
            ),
            "Incident" => array(
                "id" => "incident",
                "title" => "Incident",
                "url" => "javascript:;",
                "icon" => "fa fa-exclamation-triangle",
                "sub_menus" => array(
                    array(
                        "id" => "open-incident",
                        "title" => "Open Incident",
                        "url" => site_url('incident'),
                        "icon" => "O",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "close-incident",
                        "title" => "Close Incident",
                        "url" => site_url('incident/close_incident'),
                        "icon" => "C",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "ticket-incident",
                        "title" => "Ticket Incident",
                        "url" => site_url('incident/ticket_incident'),
                        "icon" => "TI",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "merge-incident",
                        "title" => "Merge Incident",
                        "url" => site_url('incident/merge'),
                        "icon" => "M",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "log-incident",
                        "title" => "Log Incident",
                        "url" => site_url('incident/log'),
                        "icon" => "L",
                        "third_menus" => array()
                    )
                )
            ),
            "Shift" => array(
                "id" => "shift",
                "title" => "Shift",
                "url" => "javascript:;",
                "icon" => "fa fa-users",
                "sub_menus" => array(
                    array(
                        "id" => "shift",
                        "title" => "Shift",
                        "url" => site_url('shift'),
                        "icon" => "S",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "shift-groups",
                        "title" => "Shift Groups",
                        "url" => site_url('shift/groups'),
                        "icon" => "SG",
                        "third_menus" => array()
                    ),
                    // array(
                    //     "id" => "shift-user-groups",
                    //     "title" => "Shift User Groups",
                    //     "url" => site_url('shift/user'),
                    //     "icon" => "SU",
                    //     "third_menus" => array()
                    // ),
                    array(
                        "id" => "shift-log",
                        "title" => "Shift Log",
                        "url" => site_url('shift/log'),
                        "icon" => "SL",
                        "third_menus" => array()
                    )
                )
            ),
            "Andop" => array(
                "id" => "andop",
                "title" => "Andop",
                "url" => "javascript:;",
                "icon" => "fa fa-rss",
                "sub_menus" => array(
                    array(
                        "id" => "andop-new",
                        "title" => "New",
                        "url" => site_url('andop/new'),
                        "icon" => "N",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "andop-accepted",
                        "title" => "Accepted",
                        "url" => site_url('andop/accepted'),
                        "icon" => "A",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "andop-rejected",
                        "title" => "Rejected",
                        "url" => site_url('andop/rejected'),
                        "icon" => "R",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "andop-closed",
                        "title" => "Closed",
                        "url" => site_url('andop/closed'),
                        "icon" => "C",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "andop-report",
                        "title" => "Report",
                        "url" => site_url('andop/report'),
                        "icon" => "R",
                        "third_menus" => array()
                    ),
                )
            ),
            "broadcast" => array(
                "id" => "broadcast",
                "title" => "Broadcast",
                "url" => "javascript:;",
                "icon" => "fa fa-share",
                "sub_menus" => array(
                    array(
                        "id" => "email-broadcast",
                        "title" => "Email Broadcast",
                        "url" => site_url('broadcast'),
                        "icon" => "B",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "email-recipients",
                        "title" => "Email Recipients",
                        "url" => site_url('broadcast/email_recipients'),
                        "icon" => "E",
                        "third_menus" => array()
                    )
                )
            ),
            "Report" => array(
                "id" => "report",
                "title" => "Report",
                "url" => "javascript:;",
                "icon" => "fa fa-print",
                "sub_menus" => array(
                    array(
                        "id" => "periodic",
                        "title" => "Periodic",
                        "url" => "javascript:;",
                        "icon" => "P",
                        "third_menus" => array(
                            array(
                                "id" => "corporate-mttr",
                                "title" => "Corporate MTTR",
                                "url" => site_url('report/periodik_corporate_mttr'),
                                "icon" => "C"
                            ),
                            array(
                                "id" => "region-mttr",
                                "title" => "Regional MTTR",
                                "url" => site_url('report/periodik_region_mttr'),
                                "icon" => "R"
                            ),
                            array(
                                "id" => "periodic-root-cause",
                                "title" => "Root Cause",
                                "url" => site_url('report/periodik_root_cause'),
                                "icon" => "RC"
                            ),
                            array(
                                "id" => "service-impact",
                                "title" => "Service Impact",
                                "url" => site_url('report/periodik_service_impact'),
                                "icon" => "SI"
                            ),
                            array(
                                "id" => "mod-achievement",
                                "title" => "MOD Achievement",
                                "url" => site_url('report/periodik_mod_achievement'),
                                "icon" => "MA"
                            ),
                            array(
                                "id" => "pnoc-achievement",
                                "title" => "PNOC Achievement",
                                "url" => site_url('report/periodik_pnoc_achievement'),
                                "icon" => "PA"
                            )
                        )
                    ),
                    array(
                        "id" => "trending",
                        "title" => "Trending",
                        "url" => "javascript:;",
                        "icon" => "T",
                        "third_menus" => array(
                            array(
                                "id" => "kpi-achievement",
                                "title" => "KPI Achievement",
                                "url" => site_url('report/trending_kpi_achievement'),
                                "icon" => "KPI"
                            ),
                            array(
                                "id" => "response-time",
                                "title" => "Response Time",
                                "url" => site_url('report/trending_response_time'),
                                "icon" => "RT"
                            ),
                            array(
                                "id" => "analysis-time",
                                "title" => "Analysis Time",
                                "url" => site_url('report/trending_analisis_time'),
                                "icon" => "AT"
                            ),
                            array(
                                "id" => "recovery-time",
                                "title" => "Recovery Time",
                                "url" => site_url('report/trending_recovery_time'),
                                "icon" => "RT"
                            ),
                            array(
                                "id" => "mttr-incident",
                                "title" => "MTTR Incident",
                                "url" => site_url('report/trending_mttr_incident'),
                                "icon" => "MI"
                            ),
                            array(
                                "id" => "root-cause",
                                "title" => "Root Cause",
                                "url" => site_url('report/trending_root_cause'),
                                "icon" => "RC"
                            ),
                        )
                    ),
                    array(
                        "id" => "rawdata",
                        "title" => "RAWDATA",
                        "url" => base_url('report/raw_data'),
                        "icon" => "R",
                        "third_menus" => array()
                    ),
                )
            ),
            "Tracker" => array(
                "id" => "tracker",
                "title" => "Tracker",
                "url" => base_url('tracker'),
                "icon" => "pg pg-map",
            ),
            "Preventive" => array(
                "id" => "preven",
                "title" => "Preventive",
                "url" => "javascript:;",
                "icon" => "pg pg-note",
                "sub_menus" => array(
                    array(
                        "id" => "work_order",
                        "title" => "Work Order",
                        "url" => base_url('preven'),
                        "icon" => "WO",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "work_order",
                        "title" => "Closed WO",
                        "url" => base_url('preven/wo_close'),
                        "icon" => "CW",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "work_order",
                        "title" => "PM Report",
                        "url" => base_url('preven/report'),
                        "icon" => "PMR",
                        "third_menus" => array()
                    ),
                    // /*array(
                    //     "id" => "task",
                    //     "title" => "Task",
                    //     "url" => site_url('broadcast/email_recipients'),
                    //     "icon" => "TS",
                    //     "third_menus" => array()
                    // )*/
                )
            ),
            "Team" => array(
                "id" => "team",
                "title" => "Team",
                "url" => 'javascript:;',
                "icon" => "fa fa-users",
                "sub_menus" => array(
                    array(
                        "id" => "my_team",
                        "title" => "My team",
                        "url" => site_url('team/team'),
                        "icon" => "MT",
                        "third_menus" => array()
                    ),
                    array(
                        "id" => "tracker_team",
                        "title" => "Teamracker",
                        "url" => site_url('team/teamtracker'),
                        "icon" => "T",
                        "third_menus" => array()
                    )
                )
            ),
            "Dashboard2" => array(
                "id" => "dashboard2",
                "title" => "Dashboard 2",
                "url" => 'javascript:;',
                "icon" => "fa fa-dashboard",
                "sub_menus" => array(
                    array(
                        "id" => "profile_wo",
                        "title" => "Chart",
                        "url" => site_url('dashboard2/profile_wo/dashboard'),
                        "icon" => "PW",
                        "third_menus" => array()
                    )
                )
            )
        );

        // get cookie
        $dataUser   = unserialize($_COOKIE['user'], ["allowed_classes" => false]);
        $this->user = json_decode($dataUser);
        $roleid     = $this->user->role_id; 

        // get menu on redis
        $data  = $this->menuRedis($roleid);

        $menuPermission = $data;

        $userMenu = array();

        foreach ($menus as $key => $menu) {
            foreach ($menuPermission as $perm) {
                if ($perm->name == $key) {
                    array_push($userMenu, $menus[$key]);
                }
            }
        }

        return $userMenu;
    }

    public function check_crud($module)
    {
        // get cookie
        $dataUser   = unserialize($_COOKIE['user'], ["allowed_classes" => false]);
        $this->user = json_decode($dataUser);
        $roleid     = $this->user->role_id;
        
        // get menu on redis
        $data = $this->menuRedis($roleid);

        $permission = $data;


        foreach ($permission as $perm) {
            if ($perm->name == $module) {
                $this->crud['create'] = $perm->create_permission;
                $this->crud['read'] = $perm->create_permission;
                $this->crud['update'] = $perm->update_permission;
                $this->crud['delete'] = $perm->delete_permission;
            }
        }

        if (!$this->crud['read']) {
            echo "you can't access this module";
            exit;
        }
    }

    public function check_login()
    {
        error_reporting(E_ERROR | E_PARSE);
        if(!isset($_COOKIE['user_logged_in'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['token'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['username'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['user_id'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['group_code'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['group_name'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['fullname'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['group_id'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['type'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['role_id'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['member'])) {
            redirect('user/signin', 'refresh');
        } elseif(!isset($_COOKIE['user'])) {
            redirect('user/signin', 'refresh');
        }
    }
    
}


class Api_Controller extends Base_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->key = d9d9b483f3b94c962f64424ce03d84ae0d330b0b
        if ($this->input->get('key') != sha1('B1Lin3DEvl0pMenT')) :
            $this->rest->set_error('Invalid key access');
            $this->rest->render();
            die();
        endif;

        header('Access-Control-Allow-Origin: *', false);
        //header('Access-Control-Allow-Origin: https://client.site.com', false);
        //header('Access-Control-Allow-Credentials: true', false);
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS', false);
        header('Access-Control-Allow-Headers: DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type', false);
    }
}
