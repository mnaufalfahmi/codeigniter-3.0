<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model
{
    protected $table_name   = 'users';
    protected $date_format  = 'datetime';

    public function __construct()
    {
        parent::__construct();
    }

    public function _find_user($params){
        return parent::soft_delete(false)->select('*')
            ->join('user_roles ur', 'users.id=ur.user_id')
            ->join('roles r', 'r.id=ur.role_id')
            ->where('ur.role_id', $params['role'])
            ->where('users.deleted', 0)
            ->find_by(['username' => $params['username']]);
    }
}
