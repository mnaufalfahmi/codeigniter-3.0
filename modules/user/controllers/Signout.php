<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Signout extends User_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->offline();
    }


    public function index()
    {
        $this->load->library('user_agent');
        
        // expired active
        $date        = date('Y-m-d H:i:s');
        $date1       = str_replace('-', '/', $date);
        $expiredDate = date('Y-m-d H:i:s',strtotime($date1 . "+5 minutes"));

        $this->online('Logout', json_encode(array('username'=>$_COOKIE['username'],'last_active'=>date('Y-m-d H:i:s'),'expired_active' => $expiredDate,'status'=>'online')));
        
        $this->session->set_userdata('user_logged_in', false);
        $this->session->set_userdata('user', false);

        setcookie("user_logged_in", "", time() - (86400 * 80), '/');
        setcookie("user", "", time() - (86400 * 80), '/');
        setcookie('menu', "", time() - (86400 * 80), '/');
        setcookie('username', "", time() - (86400 * 80), '/');
        setcookie('user_id', "", time() - (86400 * 80), '/');
        setcookie('group_code', "", time() - (86400 * 80), '/');
        setcookie('group_name', "", time() - (86400 * 80), '/');
        setcookie('fullname', "", time() - (86400 * 80), '/');
        setcookie('group_id', "", time() - (86400 * 80), '/');
        setcookie('avatar', "", time() - (86400 * 80), '/');
        setcookie('type', "", time() - (86400 * 80), '/');
        setcookie('role_id', "", time() - (86400 * 80), '/');
        setcookie('mod', "", time() - (86400 * 80), '/');
        setcookie('member', "", time() - (86400 * 80), '/');
        
        $this->session->sess_destroy();

        // $this->check_session($this->session->userdata('user_checkin'));
        redirect(site_url('user/signin'));
        //redirect to login view
    }


    public function check_session($param)
    {
        # code...
        if($param){

            $client = new GuzzleHttp\Client();
            $url    = $this->config->item('api_url') . 'shift/user/log_prosses';

            try {
                $shift_group   = $client->request('POST', $url, [
                    'headers'  => ['x-user-token' => $this->user->token],
                    'form_params' => [
                        'user_id' => $_COOKIE['user_id'],
                        'status'  => 0,
                        ]
                    ]);

                $code_status   = $shift_group->getStatusCode();

                if ($code_status == 200) {

                    // $data_api   = json_decode($shift_group->getBody());
                    // $result     = $data_api->data;

                    // echo json_encode($result);

                    if($this->input->post('status') == 0){

                        $this->session->unset_userdata('user_checkin');
                    }else{

                        $this->session->set_userdata('user_checkin', $this->input->post('user_id'));
                    }

                }
                
            } catch (Exception $e) {
                $this->rest->send_error(405, $e->getMessage());
                exit;
            }

            $this->session->sess_destroy();
            
        }
    }
}
