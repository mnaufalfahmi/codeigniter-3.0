<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class User extends User_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('lib/User_role_model', 'user_role');
        $this->load->model('lib/Role_model', 'role');
        $this->load->model('user/User_model', 'users');
        $this->check_login();
    }

    public function index()
    {
        $this->template->build('dashboard');
    }

    public function help()
    {
        $this->template->build('help');
    }

    public function detail()
    {
        $requestMethod = $this->input->server('REQUEST_METHOD');
        switch ($requestMethod) {
            case 'GET':
                $id = $this->input->get('id');
                $client  = new GuzzleHttp\Client();
                $url = $this->config->item('api_url') . '/user?id='.$id;
                try {
                    $response   = $client->request('GET', $url, [
                        'headers'  => [
                            'x-user-token' => $this->user->token,
                        ]
                    ]);
                    $code = $response->getStatusCode();
                    if ($code == 200) {
                        $data =  json_decode($response->getBody());
                        $users = $data->data;
                        $user['detail']     = $users->detail;
                        $user['roles']      = $users->roles;
                        $user['sub_detail'] = $users->sub_detail;
                        $user['user_group'] = $users->user_group;
                        $this->rest->send($user);
                    }
                } catch (Exception $e) {
                    $this->rest->send_error(405, $e->getMessage());
                    exit;
                }
                
                break;
            default:
                # code...
                break;
        }
    }

    public function update()
    {
        date_default_timezone_set('Asia/Jakarta');
        $access      = [];
        $roleAccess  = [];
        $userID      = $this->input->post('id');
        $userGroupID = $this->input->post('user_group_id');

        $user = [
            'username'    => $this->input->post('username'),
            'email'       => $this->input->post('email'),
            'fullname'    => $this->input->post('fullname'),
            // 'region_code' => $this->input->post('region'),
            'weight'      => $this->input->post('bobot'),
            'surveilance' => $this->input->post('surveilance'),
            'analyst'     => $this->input->post('analyst'),
            'mod'         => $this->input->post('mod'),
            // 'shiftGroup' => $this->input->post('shift_group')
        ];

        $user_log = ['change_date'  => date('Y-m-d H:i:s'),
                     'change_act'   => 'Edit User',
                     'change_by'    => $_COOKIE['username'],
                     'old_email'    => $this->input->post('old_edit_email'),
                     'new_email'    => $this->input->post('email'),
                     'old_fullname' => $this->input->post('old_edit_fullname'),
                     'new_fullname' => $this->input->post('fullname')
                    ];

        // update to table user_groups
        if(!empty($userGroupID))
        {
            // jika user group sudah ada maka ..
            $user_group = ['group_id'   => $this->input->post('group_id'),
                           'modified_on'=> date('Y-m-d H:i:s')
                          ];

            $this->db->where('id',$userGroupID)
                        ->update('user_groups',$user_group);
        }
        else
        {
            // jika user group belum ada maka ..
            $user_group = ['group_id'   => $this->input->post('group_id'),
                           'user_id'    => $userID
                          ];

            $this->db->insert('user_groups',$user_group);
        }
        

        $roles = $this->role->find_all();
        foreach ($roles as $role) {
            $access[$role->name] = $this->input->post($role->name);
            if (empty($access[$role->name])) {
                unset($access[$role->name]);
            }
        }
        foreach ($access as $row) {
            array_push($roleAccess, $row);
        }
        $this->user_role->delete_role($roleAccess, $userID);
        foreach ($roleAccess as $value) {
            $data = $this->user_role->find_by(['user_id' => $userID, 'role_id' => $value]);
            if (empty($data)) {
                $this->user_role->insert(['user_id' => $userID, 'role_id' => $value]);
            }
        }
        
        $this->db->insert('user_act_log', $user_log);

        // update users parent
        if($this->input->post('editparent') == '0')
        {
            // tidak ada parent
            $this->users->update($userID, $user);
        }
        else
        {
            // ada parent
            $no      = 0;
            $dataarr = array();
            $child   = $this->input->post('editchild');
            if($child == null || $child == '')
            {

            }
            else
            {
                foreach($child as $datac)
                {
                    array_push($dataarr,
                    array('parent_id'  => $userID,
                          'id'         => $child[$no]));
                    $no++;
                } 
                $this->db->update_batch('users',$dataarr,'id');
            }
        }

        redirect(base_url('administrator/user_management'), 'refresh');
    }

    public function delete()
    {
        $user_log = ['change_date'  => date('Y-m-d H:i:s'),
                     'change_act'   => 'Delete User',
                     'change_by'    => $_COOKIE['username'],
                     'old_email'    => $this->input->post('delete_email'),
                     'new_email'    => '',
                     'old_fullname' => $this->input->post('delete_fullname'),
                     'new_fullname' => ''
                    ];
        $this->db->where('id',$this->input->post('delete_user_id'));
        $this->db->update('users',array('deleted'=>'1'));
        $this->db->insert('user_act_log',$user_log);
        redirect(base_url('administrator/user_management'), 'refresh');
    }
}
