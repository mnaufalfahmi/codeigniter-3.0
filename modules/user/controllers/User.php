<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class User extends User_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('lib/User_role_model', 'user_role');
        $this->load->model('lib/Role_model', 'role');
        $this->load->model('user/User_model', 'users');
        $this->check_login();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index()
    {
        redirect('dashboard');
        // $this->template->build('dashboard');
    }

    public function help()
    {
        $this->template->build('help');
    }

    public function detail()
    {
        $requestMethod = $this->input->server('REQUEST_METHOD');
        switch ($requestMethod) {
            case 'GET':
                $id = $this->input->get('id');
                $client  = new GuzzleHttp\Client();
                $url = $this->config->item('api_url') . '/user?id=' . $id;
                try {
                    $response   = $client->request('GET', $url, [
                        'headers'  => [
                            'x-user-token' => $this->user->token,
                        ]
                    ]);
                    $code = $response->getStatusCode();
                    if ($code == 200) {
                        $data =  json_decode($response->getBody());
                        $users = $data->data;
                        $user['detail']     = $users->detail;
                        $user['roles']      = $users->roles;
                        $user['sub_detail'] = $users->sub_detail;
                        $user['user_group'] = $users->user_group;
                        $this->rest->send($user);
                        $this->online('Open Detail User Management', json_encode($user));
                    }
                } catch (Exception $e) {
                    $this->rest->send_error(405, $e->getMessage());
                    exit;
                }

                break;
            default:
                # code...
                break;
        }
    }

    public function update()
    {
        $access      = [];
        $roleAccess  = [];
        $userID      = $this->input->post('id');
        $userGroupID = $this->input->post('user_group_id');

        if ($this->input->post('password') == '' || empty($this->input->post('password'))) {
            $password   = $this->input->post('old_password');
            $arrPassLog = ['status'=>'password no changed','password'=>$password];
        } else {
            $password   = $this->input->post('password');
            $arrPassLog = ['status'=>'password has changed','new_password'=>$password, 'old_password'=>$this->input->post('old_password')];
        }

        $user = [
            'username'    => $this->input->post('username'),
            'email'       => $this->input->post('email'),
            'fullname'    => $this->input->post('fullname'),
            'region_code' => $this->input->post('region_code'),
            'weight'      => $this->input->post('bobot'),
            'surveilance' => $this->input->post('surveilance'),
            'analyst'     => $this->input->post('analyst'),
            'mod'         => $this->input->post('mod'),
            'password'    => md5($password)
            // 'shiftGroup' => $this->input->post('shift_group')
        ];

        
        $log = [
            'edit_by'     => $_COOKIE['username'],
            'datetime'    => date('Y-m-d H:i:s'),
            'result_edit' => [
                'username'     => $this->input->post('username'),
                'email'        => $this->input->post('email'),
                'fullname'     => $this->input->post('fullname'),
                'region_code'  => $this->input->post('region_code'),
                'weight'       => $this->input->post('bobot'),
                'surveilance'  => $this->input->post('surveilance'),
                'analyst'      => $this->input->post('analyst'),
                'mod'          => $this->input->post('mod'),
                'credentials'  => $arrPassLog

            ]        
            // 'shiftGroup' => $this->input->post('shift_group')
        ];

        $user_log = [
            'change_date'  => date('Y-m-d H:i:s'),
            'change_act'   => 'Edit User',
            'change_by'    => $_COOKIE['username'],
            'old_email'    => $this->input->post('old_edit_email'),
            'new_email'    => $this->input->post('email'),
            'old_fullname' => $this->input->post('old_edit_fullname'),
            'new_fullname' => $this->input->post('fullname')
        ];

        $this->db->trans_start();
        
        try{
            // update password users
            $this->db->where('id', $userID)
            ->update('users', array('password' => md5($password)));
        }
        catch (Exception $e) {
            // continue;
            $this->rest->send("telah terjadi kesalahan teknis. mohon coba beberapa saat lagi.");
            exit;
        }

        // update to table user_groups
        if (!empty($userGroupID)) {
            // jika user group sudah ada maka ..
            $user_group = [
                'group_id'   => $this->input->post('group_id'),
                'modified_on' => date('Y-m-d H:i:s')
            ];
            try{
            $this->db->where('id', $userGroupID)
                ->update('user_groups', $user_group);
            }
            catch(Exception $e) {
                $this->rest->send("telah terjadi kesalahan teknis. mohon coba beberapa saat lagi. (update user groups)");
                exit;
            }
        } else {
            // jika user group belum ada maka ..

            if ($this->input->post('group_id') != '0' || $this->input->post('group_id') != 0 || !empty($this->input->post('group_id'))) {

                $user_group = [
                    'group_id'   => $this->input->post('group_id'),
                    'user_id'    => $userID
                ];
            
                try{
                    $this->db->insert('user_groups', $user_group);
                }
                catch(Exception $e) {
                    $this->rest->send("telah terjadi kesalahan teknis. mohon coba beberapa saat lagi. (insert user groups)");
                    exit;
                }
            }
        }

        $roles = $this->role->find_all();
        foreach ($roles as $role) {
            $access[$role->name] = $this->input->post($role->name);
            if (empty($access[$role->name])) {
                unset($access[$role->name]);
            }
        }
        foreach ($access as $row) {
            array_push($roleAccess, $row);
        }

        if (count($roleAccess) > 0 || !empty($roleAccess)) {

            $this->user_role->delete_role($roleAccess, $userID);

            foreach ($roleAccess as $value) {
                $data = $this->user_role->find_by(['user_id' => $userID, 'role_id' => $value]);
                if (empty($data)) {
                    try{
                        $this->user_role->insert(['user_id' => $userID, 'role_id' => $value]);
                    }
                    catch(Exception $e) {
                        $this->rest->send("telah terjadi kesalahan teknis. mohon coba beberapa saat lagi. (insert user roles)");
                        exit;
                    }
                }
            }
        }

        // try{
        //     $this->db->insert('user_act_log', $user_log);

        // }
        // catch(Exception $e) {
        //     $this->rest->send("telah terjadi kesalahan teknis. mohon coba beberapa saat lagi. (insert user logs)");
        //     exit;
        // }

        try{   
            $this->users->update($userID, $user);
        }
        catch(Exception $e) {
            $this->rest->send("telah terjadi kesalahan teknis. mohon coba beberapa saat lagi.");
            exit;
        }

        // update users parent
        if ($this->input->post('editparent') == '0') {
            try{   
                // tidak ada parent
                $this->users->update($userID, $user);
            }
            catch(Exception $e) {
                $this->rest->send("telah terjadi kesalahan teknis. mohon coba beberapa saat lagi. (update parents)");
                exit;
            }
        } else {
            // ada parent
            $no      = 0;
            $dataarr = array();
            $child   = $this->input->post('editchild');
            if ($child == null || $child == '') {
            } else {
                foreach ($child as $datac) {
                    array_push(
                        $dataarr,
                        array(
                            'parent_id'  => $userID,
                            'id'         => $child[$no]
                        )
                    );
                    $no++;
                }
                try{
                    $this->db->update_batch('users', $dataarr, 'id');
                }
                catch(Exception $e) {
                    $this->rest->send("telah terjadi kesalahan teknis. mohon coba beberapa saat lagi. (update batch parents)");
                    exit;
                }
            }
        }

        $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->rest->send('telah terjadi kesalahan teknis. mohon coba beberapa saat lagi');
                exit;
            } else {
                $this->online('Edit User Management', json_encode($log));
                $this->db->trans_commit();
                // exit;
                redirect('administrator/user_management', 'refresh');
            }
        // redirect(base_url('administrator/user_management'), 'refresh');
    }

    public function delete()
    {
        $username = $this->input->post('delete_username');
        try{
            $this->db->where('id', $this->input->post('delete_user_id'));
            $this->db->update('users', array('deleted' => '1'));
            $this->online('Delete User Management', json_encode(array('deleted_by'=>$_COOKIE['username'],'has_deleted'=>$username,'datetime'=>date('Y-m-d H:i:s'))));
        } catch (Exception $e) {
            $this->rest->send("telah terjadi kesalahan teknis. mohon coba beberapa saat lagi.");
        }
        redirect(base_url('administrator/user_management'), 'refresh');
    }
}
