<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Signin extends Base_controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function checking()
    {
        if(isset($_COOKIE['user_logged_in']) && isset($_COOKIE['token']) && isset($_COOKIE['username']) && isset($_COOKIE['user_id']) && isset($_COOKIE['user_id']) && isset($_COOKIE['group_code']) &&
        isset($_COOKIE['group_name']) && isset($_COOKIE['fullname']) && isset($_COOKIE['group_id']) && isset($_COOKIE['type']) && isset($_COOKIE['role_id']) && isset($_COOKIE['member']) && isset($_COOKIE['user']) ) 
        {
            redirect('dashboard', 'refresh');
        }
        
    }

    public function index()
    {
        $this->checking();
        $client     = new GuzzleHttp\Client();
        if ($this->input->post()) {
            $params = array(
                "username" => $this->input->post('username'),
                "password" => $this->input->post('password'),
                "role" => $this->input->post('role')
            );

            // $url   = $this->config->item('api_url') . '/user/signin_dev';
            $url   = $this->config->item('api_url') . '/user/signin';
            try {
                $response   = $client->request('POST', $url, [
                    'json' => [
                        'username' => $params['username'],
                        'password' => $params['password'],
                        'role_id' => $params['role']
                    ]
                ]);
                $code = $response->getStatusCode();
                if ($code == 200) {
                    $data =  json_decode($response->getBody());
                    $user = $data->data;
                    // $this->session->set_userdata('user_logged_in', true);
                    // $this->session->set_userdata('user', $user);
                    
                    $datas = json_encode($user);
                    setcookie('user', serialize($datas), time() + (86400 * 80), '/');
                    setcookie('user_logged_in', true, time() + (86400 * 80), '/');
                    
                    $url   = $this->config->item('api_url') . '/role/permission?id=' . $user->role_id;
                    try {
                        $response   = $client->request('GET', $url, [
                            'headers'  => [
                                'x-user-token' => $user->token,
                            ]
                        ]);
                        $code = $response->getStatusCode();
                        if ($code == 200) {
                            $data =  json_decode($response->getBody());
                            $menu = $data->data;
                            
                            // set redis
                            // $this->authRedis($user);

                            // set cookie
                            setcookie('token', $user->token, time() + (86400 * 80), '/');
                            setcookie('username', $this->input->post('username'), time() + (86400 * 80), '/');
                            setcookie('user_id', $user->id, time() + (86400 * 80), '/');
                            setcookie('group_code', $user->group_code, time() + (86400 * 80), '/');
                            setcookie('group_name', $user->group_name, time() + (86400 * 80), '/');
                            setcookie('fullname', $user->fullname, time() + (86400 * 80), '/');
                            setcookie('group_id', $user->group_id, time() + (86400 * 80), '/');
                            setcookie('avatar', $user->avatar, time() + (86400 * 80), '/');
                            setcookie('type', $user->type, time() + (86400 * 80), '/');
                            setcookie('role_id', $user->role_id, time() + (86400 * 80), '/');
                            setcookie('mod', $user->mod, time() + (86400 * 80), '/');
                            setcookie('member', $user->member, time() + (86400 * 80), '/');
                            

                            // $this->session->set_userdata('menu', $menu);
                            // $this->session->set_userdata('username',$this->input->post('username'));
                            // $this->session->set_userdata('user_id', $user->id);
                            // $this->session->set_userdata('group_code', $user->group_code);
                            // $this->session->set_userdata('group_name', $user->group_name);
                            // $this->session->set_userdata('fullname', $user->fullname);
                            // $this->session->set_userdata('group_id', $user->group_id);
                            // $this->session->set_userdata('avatar', $user->avatar);
                            // $this->session->set_userdata('type', $user->type);
                            // $this->session->set_userdata('role_id',$user->role_id);
                            // $this->session->set_userdata('mod',$user->mod);
                            // $this->session->set_userdata('member',$user->member);
                            $role   = $_COOKIE['role_id'];
                            $usernm = $_COOKIE['username'];

                            $this->load->library('user_agent');

                            $this->db->select('user_last_connections.*')->from('user_last_connections')->where('username',$usernm);
                            $checkData = $this->db->get()->row();

                            $this->db->select('user_logs.*')->from('user_logs')->where('remark','logined')->where('cast(created_on as date) =',date('Y-m-d'))->where('username',$usernm);
                            $checkDataLog = $this->db->get()->num_rows();

                            // expired active
                            $date        = date('Y-m-d H:i:s');
                            $date1       = str_replace('-', '/', $date);
                            $expiredDate = date('Y-m-d H:i:s',strtotime($date1 . "+5 minutes"));
                            
                            if(!empty($checkData)) {
                                $this->db->where('username',$usernm)->update('user_last_connections',array('status'=>'online','last_active' => date('Y-m-d H:i:s'),'expired_active' => $expiredDate));
                            } else {
                                $arrLastConnection = array(
                                    'username'       => $usernm,
                                    'last_active'    => date('Y-m-d H:i:s'),
                                    'expired_active' => $expiredDate,
                                    'status'         => 'online',
                                );
                                $this->db->insert('user_last_connections',$arrLastConnection);
                            }

                            $arrLastConnection2 = array(
                                'username'       => $usernm,
                                'last_active'    => date('Y-m-d H:i:s'),
                                'expired_active' => $expiredDate,
                                'status'         => 'online',
                            );
                            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                            $arrLogUser = array(
                                'username'        => $this->input->post('username'),
                                'module'          => 'login',
                                'ip'              => $this->input->ip_address(),
                                'remark'          => 'logined',
                                'browser'         => $this->agent->browser(),
                                'browser_version' => $this->agent->version(),
                                'platform'        => $this->agent->platform(),
                                'full_user_agent' => $_SERVER['HTTP_USER_AGENT'],
                                'url'             => $actual_link,
                                'json'            => json_encode($arrLastConnection2),
                                );
                            
                            // check status jika belum pernah login pada hari tersebut set status 1. jika sudah set 0 (default)
                            if($checkDataLog < 1) {
                                $arrLogUser['status'] = 1;
                            }

                            try {
                                $this->db->insert('user_logs',$arrLogUser);
                            } catch (\Exception $e) {
                                redirect(site_url('user/signin'));
                            }

                            #super_user
                            if($role == '12') 
                            {
                                redirect(site_url('administrator/user_management'));
                            }

                            #MOD & Administrator & BoD & field support
                            elseif($role == '68' || $role == '16' || $role == '70' || $role == '72' || $role == '73')
                            {
                                redirect(site_url('incident'));
                            }

                            #Andop & Customer Loyality & performance
                            elseif($role == '69' || $role == '71' || $role == '75' ) 
                            {
                                redirect(site_url('andop'));
                            }

                            #Obs Infra
                            elseif($role == '74')
                            {
                                redirect(site_url('ticket'));
                            }
                            else
                            {
                                redirect(base_url());
                            }
                            
                        }
                    } catch (Exception $e) {
                        $error = 'email or password incorrect';
                        $this->session->set_flashdata('alert', array('username' => $params['username'], 'type' => 'danger', 'msg' => $error));
                        redirect(site_url('user/signin'), 'refresh');
                    }
                }
            } catch (Exception $e) {
                $error = 'email or password incorrect';
                $this->session->set_flashdata('alert', array('username' => $params['username'], 'type' => 'danger', 'msg' => $error));
                redirect(site_url('user/signin'), 'refresh');
            }
        } else {
            $url   = $this->config->item('api_url') . '/reference/role';
            try {
                $response   = $client->request('GET', $url);
                $code = $response->getStatusCode();
                if ($code == 200) {
                    
                    setcookie("user_logged_in", "", time() - (86400 * 80), '/');
                    setcookie("user", "", time() - (86400 * 80), '/');
                    setcookie('menu', "", time() - (86400 * 80), '/');
                    setcookie('username', "", time() - (86400 * 80), '/');
                    setcookie('user_id', "", time() - (86400 * 80), '/');
                    setcookie('group_code', "", time() - (86400 * 80), '/');
                    setcookie('group_name', "", time() - (86400 * 80), '/');
                    setcookie('fullname', "", time() - (86400 * 80), '/');
                    setcookie('group_id', "", time() - (86400 * 80), '/');
                    setcookie('avatar', "", time() - (86400 * 80), '/');
                    setcookie('type', "", time() - (86400 * 80), '/');
                    setcookie('role_id', "", time() - (86400 * 80), '/');
                    setcookie('mod', "", time() - (86400 * 80), '/');
                    setcookie('member', "", time() - (86400 * 80), '/');

                    $data =  json_decode($response->getBody());
                    $roles = $data->data;
                    $this->template->set('roles', $roles)->set('alert', $this->session->flashdata('alert'))->build('signin');
                }
            } catch (Exception $e) {
                $this->rest->send_error(405, $e->getMessage());
                exit;
            }
        }
    }

    function validasi_role()
    {
        $client = new GuzzleHttp\Client();
        $url    = $this->config->item('api_url') . '/user/signin/validasi_user_role';
        try 
        {
            $response   = $client->request('POST', $url, [
                'json' => [
                    'username' => $this->input->post('username')
                ]
            ]);

            $code = $response->getStatusCode();
                if ($code == 200) 
                {
                    $data =  json_decode($response->getBody());
                    $roles = json_encode($data->data);
                    echo $roles;
                }
        }
        catch (Exception $e) {
            $this->rest->send_error(405, $e->getMessage());
            exit;
        }

    }
}
