<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard extends User_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->notification();
    }

    public function index()
    {
        $data = array(
            'title'          => 'Dashboard',
            'crud'           => $this->crud,
            'status_checkin' => $this->check_checkin(),
        );
        $this->online('Open Dashboard',json_encode($data));

        $this->assetic->addJsDir('themes/default/pages-js/dashboard/dashboard.js', 'dashboad');
        $this->template->set('data', $data)->build('dashboard');
    }

    function get_data()
    {
        $group = $_COOKIE['group_code'];
        $client     = new GuzzleHttp\Client();
        $base_url   = $this->config->item('api_url') . 'dashboard?group=' . $group;

        try {
            $response   = $client->request(
                'GET',
                $base_url,
                [
                    'headers'     => ['x-user-token' => $this->user->token]
                ]
            );

            $code = $response->getStatusCode();
            if ($code == 200) {

                $data =  json_decode($response->getBody());
                $action = json_encode($data->data);
                echo $action;
            }
        } catch (Exception $e) {
            $this->rest->send_error(405, $e->getMessage());
            exit;
        }
    }

    // public function tes()
    // {
    //     # code...
    //     $urut   = ['a', 'a', 'a', 'b', 'c', 'c', 'd', 'd', 'd'];
    //     $hasil  = array_count_values($urut);
    //     $res    = array();
    //     foreach ($hasil as $key => $r) {
    //         $arr = array($key . ',' . $r);
    //         array_push($res, $arr);
    //     }
    //     echo json_encode($res);
    // }

    // public function tes2()
    // {
    //     # code...
    //     $urut   = ['a', 'a', 'a', 'b', 'c', 'c', 'd', 'd', 'd', 'b', 'b'];
    //     $res    = array();
    //     foreach ($urut as $key => $value) {
    //         if($key != 0){
    //             $v1 = $urut[$key];
    //             $v2 = $urut[$key - 1];
    //             if ($v1 == $v2) {
    //                 // $value[$v1] = $v1;
    //                 // array_push($res, array($v1)); 
    //                 echo $v1;
    //             } else {
    //                 // array_push($res, array($v1));
    //                 echo '<br>' . $v1;
    //             }

    //         }
    //     }

    //     // echo json_encode($res);
    // }

}
