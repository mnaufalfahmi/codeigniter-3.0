/* ============================================================
 * DataTables
 * Generate advanced tables with sorting, export options using
 * jQuery DataTables plugin
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */
(function($) {

    'use strict';

    var responsiveHelper = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };

    // Initialize datatable showing a search box at the top right corner
    var initTableWithSearch = function() {
        var table = $('#tableWithSearch');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableWithSearch1 = function() {
        var table = $('#tableWithSearch1');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table1').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableWithSearch2 = function() {
        var table = $('#tableWithSearch2');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table2').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableWithSearch3 = function() {
        var table = $('#tableWithSearch3');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table3').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    // Initialize datatable with ability to add rows dynamically
    var initTableWithDynamicRows = function() {
        var table = $('#tableWithDynamicRows');
        var modal =  $('#modalEditUser');


        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 10
        };


        table.dataTable(settings);

        $('#modal-edit-user').click(function() {
            modal.modal('show')
        });
    }

    var initTableWithDynamicRowsLog = function(){
        var table = $('#tableWithDynamicRowsLog');


        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5
        };


        table.dataTable(settings);

        $('#show-modal').click(function() {
            $('#addNewAppModal').modal('show');
        });

        $('#add-app').click(function() {
            table.dataTable().fnAddData([
                $("#appName").val(),
                $("#appDescription").val(),
                $("#appPrice").val(),
                $("#appNotes").val()
            ]);
            $('#addNewAppModal').modal('hide');

        });
    }

    // Initialize datatable showing export options
    var initTableWithExportOptions = function() {
        var table = $('#tableWithExportOptions');


        var settings = {
            "sDom": "<'exportOptions'T><'table-responsive sm-m-b-15't><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "oTableTools": {
                "sSwfPath": "assets/plugins/jquery-datatable/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [{
                    "sExtends": "csv",
                    "sButtonText": "<i class='pg-grid'></i>",
                }, {
                    "sExtends": "xls",
                    "sButtonText": "<i class='fa fa-file-excel-o'></i>",
                }, {
                    "sExtends": "pdf",
                    "sButtonText": "<i class='fa fa-file-pdf-o'></i>",
                }, {
                    "sExtends": "copy",
                    "sButtonText": "<i class='fa fa-copy'></i>",
                }]
            },
            fnDrawCallback: function(oSettings) {
                $('.export-options-container').append($('.exportOptions'));

                $('#ToolTables_tableWithExportOptions_0').tooltip({
                    title: 'Export as CSV',
                    container: 'body'
                });

                $('#ToolTables_tableWithExportOptions_1').tooltip({
                    title: 'Export as Excel',
                    container: 'body'
                });

                $('#ToolTables_tableWithExportOptions_2').tooltip({
                    title: 'Export as PDF',
                    container: 'body'
                });

                $('#ToolTables_tableWithExportOptions_3').tooltip({
                    title: 'Copy data',
                    container: 'body'
                });
            }
        };


        table.dataTable(settings);

    }

    var initCondensedTable = function() {
        var table = $('#condensedTable');


        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 10
        };

        table.dataTable(settings);
    }

    var initTableOpenTicket = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#open_ticket');

        var settings = {
            "sDom": "Bfrtip",
            "buttons": [
            'colvis'
        ],
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 10,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,
            "searching": false,

         "ajax": {
            url: "ticket/list_open_ticket",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],

        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableCloseTicket = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#close_ticket');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 10,
            "processing": true,
            "serverSide": true,
            "searching": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "ticket/list_close_ticket",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter( this.value, $("#search-table").index(this) );
            // table.fnFilter($(this).val());
        });
    }

    var initTableAndopAccepted = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#andop_accepted');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "andop/list_accepted_andop",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableAndopClosed = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#andop_closed');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "andop/list_closed_andop",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableAndopNew = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#andop_new');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "andop/list_new_andop",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableAndopRejected = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#andop_rejected');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "andop/list_rejected_andop",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableAndopReport = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#andop_report');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "andop/list_report_andop",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableFSJKT = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#fs_jkt');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_fs_jkt",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableFSJBR = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#fs_jbr');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_fs_jbr",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableFSJTG = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#fs_jtg');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_fs_jtg",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableFSJTM = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#fs_jtm');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_fs_jtm",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableFSBLI = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#fs_bli');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_fs_bli",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableFSSBU = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#fs_sbu');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_fs_sbu",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableFSSBT = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#fs_sbt');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_fs_sbt",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableFSSBS = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#fs_sbs');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_fs_sbs",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }
    
    var initTableFSIBT = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#fs_ibt');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_fs_ibt",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableFSKAL = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#fs_kal');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_fs_kal",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableActionOpenAR = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#action_open_ar');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_action_open_ar",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableActionCloseRequestAR = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#action_close_request_ar');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_action_close_request_ar",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }

    var initTableActionCloseAR = function() {
        var url = "http://" + location.host.split(":")[0];
        var table = $('#action_close_ar');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            "oLanguage": {
                "sLengthMenu": "_MENU_ ",
                "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            },
            "iDisplayLength": 5,
            "processing": true,
            "serverSide": true,
            "order": [],
            "scrollX": true,

         "ajax": {
            url: "field_support/list_action_close_ar",
            "type": "POST",
            "data": function (data) {


            }
        },

         //Set column definition initialisation properties.
         "columnDefs": [{
               "targets": [-1], //last column
               "orderable": false, //set not orderable
           },

           ],
        };

        table.dataTable(settings);

        // search box for table
        $('#search-table').keyup(function() {
            table.fnFilter($(this).val());
        });
    }


    initTableWithSearch();
    initTableWithSearch1();
    initTableWithSearch2();
    initTableWithSearch3();
    initTableWithDynamicRows();
    initTableWithExportOptions();
    initTableWithDynamicRowsLog();
    initCondensedTable();
    // initTableOpenTicket();
    // initTableCloseTicket();
    // initTableAndopAccepted();
    // initTableAndopClosed();
    // initTableAndopNew();
    // initTableAndopRejected();
    // initTableAndopReport();
    // initTableFSJKT();
    // initTableFSJBR();
    // initTableFSJTG();
    // initTableFSJTM();
    // initTableFSBLI();
    // initTableFSSBU();
    // initTableFSSBT();
    // initTableFSSBS();
    // initTableFSIBT();
    // initTableFSKAL();
    // initTableActionOpenAR();
    // initTableActionCloseRequestAR();
    // initTableActionCloseAR();

})(window.jQuery);