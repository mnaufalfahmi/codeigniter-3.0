/* ============================================================
 * Demo
 * Try various layout options available in Pages
 * For DEMO purposes only.
 * ============================================================ */
(function($) {

    'use strict';

    var rtlSwitch = $('#rtl-switch').get(0);

    // DEMO MODALS SIZE TOGGLER

    $('#modal-add-user').click(function() {
        $('#modalAddUser').modal('show')
    });

})(window.jQuery);