<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE>wo_detail 14241920200107 Summary Incident</TITLE>
<META name="generator" content="BCL easyConverter SDK 5.0.210">
<STYLE type="text/css">

body {margin-top: 0px;margin-left: 0px;}

#page_1 {position:relative; overflow: hidden;margin: 30px 0px 40px 45px;padding: 0px;border: none;width: 771px;}
#page_1 #id1_1 {border:none;margin: 3px 0px 0px 0px;padding: 0px;border:none;width: 771px;overflow: hidden;}
#page_1 #id1_2 {border:none;margin: 29px 0px 0px 648px;padding: 0px;border:none;width: 123px;overflow: hidden;}

#page_1 #p1dimg1 {position:absolute;top:0px;left:0px;z-index:-1;width:726px;height:453px;}
#page_1 #p1dimg1 #p1img1 {width:726px;height:453px;}




.dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}

.ft0{font: bold 19px 'Helvetica';color: #333333;line-height: 22px;}
.ft1{font: bold 14px 'Helvetica';color: #333333;line-height: 16px;}
.ft2{font: 14px 'Helvetica';color: #333333;line-height: 16px;}
.ft3{font: 13px 'Helvetica';color: #333333;line-height: 16px;}
.ft4{font: 1px 'Helvetica';line-height: 12px;}
.ft5{font: 1px 'Helvetica';line-height: 1px;}
.ft6{font: 14px 'Helvetica';color: #333333;line-height: 20px;}
.ft7{font: bold 13px 'Helvetica';color: #333333;line-height: 16px;}
.ft8{font: 1px 'Helvetica';line-height: 5px;}
.ft9{font: 1px 'Helvetica';line-height: 9px;}
.ft10{font: 13px 'Helvetica';line-height: 16px;}

.p0{text-align: left;margin-top: 0px;margin-bottom: 0px;}
.p1{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p2{text-align: right;padding-right: 10px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p3{text-align: left;padding-left: 12px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p4{text-align: justify;padding-right: 71px;margin-top: 4px;margin-bottom: 0px;}
.p5{text-align: left;margin-top: 12px;margin-bottom: 0px;}
.p6{text-align: left;padding-left: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: left;padding-left: 46px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p8{text-align: center;padding-left: 90px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: left;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: center;padding-left: 93px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p11{text-align: right;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: left;padding-left: 26px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p13{text-align: left;margin-top: 66px;margin-bottom: 0px;}
.p14{text-align: left;padding-left: 11px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: left;margin-top: 4px;margin-bottom: 0px;}

.td0{padding: 0px;margin: 0px;width: 0px;vertical-align: bottom;}
.td1{padding: 0px;margin: 0px;width: 120px;vertical-align: bottom;}
.td2{padding: 0px;margin: 0px;width: 28px;vertical-align: bottom;}
.td3{padding: 0px;margin: 0px;width: 430px;vertical-align: bottom;}
.td4{padding: 0px;margin: 0px;width: 144px;vertical-align: bottom;}
.td5{padding: 0px;margin: 0px;width: 574px;vertical-align: bottom;}
.td6{padding: 0px;margin: 0px;width: 148px;vertical-align: bottom;}
.td7{padding: 0px;margin: 0px;width: 129px;vertical-align: bottom;}
.td8{padding: 0px;margin: 0px;width: 9px;vertical-align: bottom;}
.td9{padding: 0px;margin: 0px;width: 424px;vertical-align: bottom;}
.td10{padding: 0px;margin: 0px;width: 164px;vertical-align: bottom;}
.td11{border-bottom: #dddddd 1px solid;padding: 0px;margin: 0px;width: 129px;vertical-align: bottom;}
.td12{border-bottom: #dddddd 1px solid;padding: 0px;margin: 0px;width: 9px;vertical-align: bottom;}
.td13{border-bottom: #dddddd 1px solid;padding: 0px;margin: 0px;width: 64px;vertical-align: bottom;}
.td14{border-bottom: #dddddd 1px solid;padding: 0px;margin: 0px;width: 360px;vertical-align: bottom;}
.td15{border-bottom: #dddddd 1px solid;padding: 0px;margin: 0px;width: 164px;vertical-align: bottom;}
.td16{padding: 0px;margin: 0px;width: 64px;vertical-align: bottom;}
.td17{padding: 0px;margin: 0px;width: 360px;vertical-align: bottom;}
.td18{padding: 0px;margin: 0px;width: 123px;vertical-align: bottom;}
.td19{padding: 0px;margin: 0px;width: 26px;vertical-align: bottom;}
.td20{padding: 0px;margin: 0px;width: 410px;vertical-align: bottom;}
.td21{padding: 0px;margin: 0px;width: 436px;vertical-align: bottom;}

.tr0{height: 21px;}
.tr1{height: 31px;}
.tr2{height: 10px;}
.tr3{height: 19px;}
.tr4{height: 9px;}
.tr5{height: 12px;}
.tr6{height: 20px;}
.tr7{height: 23px;}
.tr8{height: 5px;}
.tr9{height: 28px;}
.tr10{height: 26px;}
.tr11{height: 27px;}
.tr12{height: 49px;}

.t0{width: 722px;margin-top: 42px;font: 14px 'Helvetica';color: #333333;}
.t1{width: 726px;margin-top: 20px;font: 14px 'Helvetica';color: #333333;}
.t2{width: 559px;margin-top: 12px;font: 14px 'Helvetica';color: #333333;}

</STYLE>
</HEAD>

<BODY>
<DIV id="page_1">



<DIV class="dclr"></DIV>
<DIV id="id1_1">
<P class="p0 ft0">Work Order - Corrective Maintenance <img style="padding-left: 140px;" src="http://portal.iconpln.co.id/uploads/iconpln-desc11.png"></P>
<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD class="tr0 td0"></TD>
	<TD class="tr0 td1"><P class="p1 ft1">Code WO</P></TD>
	<TD class="tr0 td2"><P class="p2 ft2">:</P></TD>
	<TD class="tr0 td3"><P class="p3 ft2">14241920200107</P></TD>
	<TD rowspan=2 class="tr1 td4"><P class="p1 ft3">Printed by: superadmin</P></TD>
</TR>
<TR>
	<TD class="tr2 td0"></TD>
	<TD rowspan=2 class="tr3 td1"><P class="p1 ft1">Created Date</P></TD>
	<TD rowspan=2 class="tr3 td2"><P class="p2 ft2">:</P></TD>
	<TD rowspan=2 class="tr3 td3"><P class="p3 ft2">07 Jan 2020 14:24:51</P></TD>
</TR>
<TR>
	<TD class="tr4 td0"></TD>
	<TD rowspan=2 class="tr0 td4"><P class="p1 ft2">07 Jan 2020 16:51:44</P></TD>
</TR>
<TR>
	<TD class="tr5 td0"></TD>
	<TD class="tr5 td1"><P class="p1 ft4">&nbsp;</P></TD>
	<TD class="tr5 td2"><P class="p1 ft4">&nbsp;</P></TD>
	<TD class="tr5 td3"><P class="p1 ft4">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr3 td0"></TD>
	<TD class="tr3 td1"><P class="p1 ft1">Service Point</P></TD>
	<TD class="tr3 td2"><P class="p2 ft2">:</P></TD>
	<TD class="tr3 td3"><P class="p3 ft2">JKT_jakut_GSPR_01 (jakarta.utaragsp1@gmail.com)</P></TD>
	<TD class="tr3 td4"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr0 td0"></TD>
	<TD class="tr0 td1"><P class="p1 ft1">SBU</P></TD>
	<TD class="tr0 td2"><P class="p2 ft2">:</P></TD>
	<TD class="tr0 td3"><P class="p3 ft2">ROJB</P></TD>
	<TD class="tr0 td4"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td0"></TD>
	<TD class="tr6 td1"><P class="p1 ft1">BaseCamp</P></TD>
	<TD class="tr6 td2"><P class="p2 ft2">:</P></TD>
	<TD class="tr6 td3"><P class="p3 ft2">BC_Jakarta Utara</P></TD>
	<TD class="tr6 td4"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td0"></TD>
	<TD class="tr6 td1"><P class="p1 ft1">Address</P></TD>
	<TD class="tr6 td2"><P class="p2 ft2">:</P></TD>
	<TD colspan=2 class="tr6 td5"><P class="p3 ft2">Jl Bhakti No. 5 RT 04 RW 06 kel. Cilincing kec. Cilincing Jakarta utara,Cilincing ,Jakarta</P></TD>
</TR>
<TR>
	<TD class="tr0 td0"></TD>
	<TD class="tr0 td1"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr0 td2"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr0 td3"><P class="p3 ft2">Utara ,DKI Jakarta ,Indonesia ,14120</P></TD>
	<TD class="tr0 td4"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td0"></TD>
	<TD class="tr6 td1"><P class="p1 ft1">Description WO</P></TD>
	<TD class="tr6 td2"><P class="p2 ft2">:</P></TD>
	<TD colspan=2 class="tr6 td5"><P class="p3 ft2">[IP] - PT. MITRA VISIONER PRATAMA[IP <NOBR>VPN]-Keluhan :</NOBR> down PIC : Sedang ditanyakan</P></TD>
</TR>
<TR>
	<TD class="tr6 td0"></TD>
	<TD class="tr6 td1"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr6 td2"><P class="p1 ft5">&nbsp;</P></TD>
	<TD colspan=2 class="tr6 td5"><P class="p3 ft2">K.Perangkat : Sedang ditanyakan- - Jl Bhakti No. 5 RT 04 RW 06 kel. Cilincing kec.</P></TD>
</TR>
<TR>
	<TD class="tr0 td0"></TD>
	<TD class="tr0 td1"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr0 td2"><P class="p1 ft5">&nbsp;</P></TD>
	<TD colspan=2 class="tr0 td5"><P class="p3 ft2">Cilincing Jakarta utara,Cilincing ,Jakarta Utara ,DKI Jakarta ,Indonesia ,14120</P></TD>
</TR>
<TR>
	<TD class="tr6 td0"></TD>
	<TD class="tr6 td1"><P class="p1 ft1">Prob ID</P></TD>
	<TD class="tr6 td2"><P class="p2 ft2">:</P></TD>
	<TD class="tr6 td3"><P class="p3 ft2">20200107140408000</P></TD>
	<TD class="tr6 td4"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td0"></TD>
	<TD class="tr6 td1"><P class="p1 ft1">No AR</P></TD>
	<TD class="tr6 td2"><P class="p2 ft2">:</P></TD>
	<TD class="tr6 td3"><P class="p3 ft2">20200107140451000</P></TD>
	<TD class="tr6 td4"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr0 td0"></TD>
	<TD colspan=2 class="tr0 td6"><P class="p1 ft1">Incident Description</P></TD>
	<TD class="tr0 td3"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr0 td4"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
</TABLE>
<P class="p4 ft6">[IP] - PT. MITRA VISIONER PRATAMA[IP <NOBR>VPN]-Keluhan :</NOBR> down PIC : Sedang ditanyakan K.Perangkat : Sedang ditanyakan- - Jl Bhakti No. 5 RT 04 RW 06 kel. Cilincing kec. Cilincing Jakarta utara,Cilincing ,Jakarta Utara ,DKI Jakarta ,Indonesia ,14120</P>
<P class="p5 ft0">Status</P>
<TABLE cellpadding=0 cellspacing=0 class="t1">
<TR>
	<TD class="tr7 td7"><P class="p6 ft1">Status</P></TD>
	<TD class="tr7 td8"><P class="p1 ft5">&nbsp;</P></TD>
	<TD colspan=2 class="tr7 td9"><P class="p7 ft1">Date</P></TD>
	<TD class="tr7 td10"><P class="p8 ft7">Duration</P></TD>
</TR>
<TR>
	<TD class="tr8 td11"><P class="p1 ft8">&nbsp;</P></TD>
	<TD class="tr8 td12"><P class="p1 ft8">&nbsp;</P></TD>
	<TD class="tr8 td13"><P class="p1 ft8">&nbsp;</P></TD>
	<TD class="tr8 td14"><P class="p1 ft8">&nbsp;</P></TD>
	<TD class="tr8 td15"><P class="p1 ft8">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr9 td7"><P class="p6 ft2">Preparation</P></TD>
	<TD class="tr9 td8"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr9 td16"><P class="p7 ft2">07</P></TD>
	<TD class="tr9 td17"><P class="p9 ft2">Jan 2020 14:24:51 JKT_jakut_GSPR_01</P></TD>
	<TD class="tr9 td10"><P class="p10 ft3">2:04:06</P></TD>
</TR>
<TR>
	<TD class="tr4 td11"><P class="p1 ft9">&nbsp;</P></TD>
	<TD class="tr4 td12"><P class="p1 ft9">&nbsp;</P></TD>
	<TD class="tr4 td13"><P class="p1 ft9">&nbsp;</P></TD>
	<TD class="tr4 td14"><P class="p1 ft9">&nbsp;</P></TD>
	<TD class="tr4 td15"><P class="p1 ft9">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr10 td7"><P class="p6 ft2">Start Driving</P></TD>
	<TD class="tr10 td8"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr10 td16"><P class="p7 ft2">07</P></TD>
	<TD class="tr10 td17"><P class="p9 ft2">Jan 2020 16:28:57</P></TD>
	<TD class="tr10 td10"><P class="p10 ft3">0:00:04</P></TD>
</TR>
<TR>
	<TD class="tr4 td11"><P class="p1 ft9">&nbsp;</P></TD>
	<TD class="tr4 td12"><P class="p1 ft9">&nbsp;</P></TD>
	<TD class="tr4 td13"><P class="p1 ft9">&nbsp;</P></TD>
	<TD class="tr4 td14"><P class="p1 ft9">&nbsp;</P></TD>
	<TD class="tr4 td15"><P class="p1 ft9">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr11 td7"><P class="p6 ft2">Start Work</P></TD>
	<TD class="tr11 td8"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr11 td16"><P class="p7 ft2">07</P></TD>
	<TD class="tr11 td17"><P class="p9 ft2">Jan 2020 16:29:01</P></TD>
	<TD class="tr11 td10"><P class="p10 ft3">0:22:43</P></TD>
</TR>
<TR>
	<TD class="tr12 td7"><P class="p1 ft1">Total Durasi WO</P></TD>
	<TD class="tr12 td8"><P class="p11 ft2">:</P></TD>
	<TD colspan=2 class="tr12 td9"><P class="p12 ft2">2:26:53</P></TD>
	<TD class="tr12 td10"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td7"><P class="p1 ft1">Total Durasi SBU</P></TD>
	<TD class="tr6 td8"><P class="p11 ft2">:</P></TD>
	<TD colspan=2 class="tr6 td9"><P class="p12 ft2">0:00:00</P></TD>
	<TD class="tr6 td10"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td7"><P class="p1 ft1">Total Durasi Serpo</P></TD>
	<TD class="tr6 td8"><P class="p11 ft2">:</P></TD>
	<TD colspan=2 class="tr6 td9"><P class="p12 ft2">2:26:53</P></TD>
	<TD class="tr6 td10"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
</TABLE>
<P class="p13 ft0">Hashtag</P>
<TABLE cellpadding=0 cellspacing=0 class="t2">
<TR>
	<TD class="tr0 td18"><P class="p1 ft1">Problem History</P></TD>
	<TD class="tr0 td19"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr0 td20"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr3 td18"><P class="p1 ft1">Progress History</P></TD>
	<TD class="tr3 td19"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr3 td20"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr6 td18"><P class="p1 ft2">#fms</P></TD>
	<TD class="tr6 td19"><P class="p14 ft2">:</P></TD>
	<TD class="tr6 td20"><P class="p14 ft2">cek cwdm</P></TD>
</TR>
<TR>
	<TD class="tr6 td18"><P class="p1 ft2">Created By</P></TD>
	<TD class="tr6 td19"><P class="p14 ft2">:</P></TD>
	<TD class="tr6 td20"><P class="p14 ft2">jakarta.utaragsp1@gmail.com <NOBR>2020-01-07</NOBR> 16:29:25</P></TD>
</TR>
<TR>
	<TD class="tr0 td18"><P class="p1 ft2">#fms</P></TD>
	<TD colspan=2 class="tr0 td21"><P class="p14 ft2">: tim jakut 1 motor</P></TD>
</TR>
<TR>
	<TD class="tr6 td18"><P class="p1 ft2">Created By</P></TD>
	<TD class="tr6 td19"><P class="p14 ft2">:</P></TD>
	<TD class="tr6 td20"><P class="p14 ft3">baharudin.yusufrahmatullah@iconpln.co.id <NOBR>2020-01-07</NOBR> 14:25:36</P></TD>
</TR>
<TR>
	<TD class="tr6 td18"><P class="p1 ft1">Replace</P></TD>
	<TD class="tr6 td19"><P class="p1 ft5">&nbsp;</P></TD>
	<TD class="tr6 td20"><P class="p1 ft5">&nbsp;</P></TD>
</TR>
</TABLE>
<P class="p15 ft1">Root Cause Gangguan</P>
</DIV>
<DIV id="id1_2">
<P class="p0 ft10">Page 1 / 1</P>
</DIV>
</DIV>
</BODY>
</HTML>
