<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0 mt-3">
      <?php echo $this->session->flashdata('msg');?>
    <div class="inner">
        <div class="row">
           
        </div>

        <div class="card ">
              <div class="card-header ">
                <div class="card-title">
                  Tickets - Incidents
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-body">
                <table class="table table-hover" id="tableWithSearch">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th>SID</th>
                      <th>Ticket ID</th>
                      <th>Incident ID</th>
                      <th>Description</th>
                      <th>Status</th>
                      <th>Token</th>
                      <th>Timestamp</th>
                      <th class='text-center' width='110px'>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                        <?php $no=1;  ?>
                        <?php foreach ($data['incident'] as $value) :?>
                            <tr>
                              <td><center><?php echo $no++;?></center></td>
                              <td><?php echo $value->service_id;?></td>
                              <td><?php echo $value->ismilling_id;?></td>
                              <td><?php echo $value->incident_id;?></td>
                              <td><?php echo $value->incident_description;?></td>
                              <td><?php echo $value->status;?></td>
                              <td><?php echo $value->token;?></td>
                              <td><?php echo $value->last_mention_time;?></td>
                              <td class='text-center' >
                                <a href="<?php echo site_url('incident/progress/'.$value->id_incident);?>" title="View Incident" target="_blank"><button class="btn btn-complete btn-xs btn-xs ml-1">Progress</button>
                              </td>
                            </tr> 
                        <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>


    </div>
</div>

<div class="modal fade slide-up" id="modalAddRole" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add <span class="semi-bold">Work Order</span></h5>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?php echo site_url('preven/input_wo'); ?>" class="form-horizontal form_save" role="form" >
                        <div class="row">
                            <div class="card card-transparent">
                                <!-- <div class="card-header ">
                                    <div class="card-title">User Details</div>
                                </div> -->
                                <div class="card-body ">
                                    
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" name="title" class="form-control" placeholder="Title" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="date" class="form-control" name="date" placeholder="Select Date" autocomplete="off" required>
                                    </div>

                                    <div class="form-group">
                                        <label>POP</label>
                                        <select class="full-width select2" name="pop" data-init-plugin="select2" required>
                                            <option value="0">-</option>
                                            <?php foreach ($data['pop'] as $pop) { ?>
                                                    <option value="<?= $pop->name; ?>"><?= $pop->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Serpo</label>
                                        <select class="full-width select2" name="serpo" data-init-plugin="select2" required>
                                            <option value="0">-</option>
                                            <?php foreach ($data['sepro'] as $sepro) { ?>
                                                    <option value="<?= $sepro->id_serpo; ?>"><?= $sepro->fullname; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="full-width select2" name="type" data-init-plugin="select2" required>
                                            <option value="ISP">ISP</option>
                                            <option value="OSP">OSP</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="full-width select2" name="status" data-init-plugin="select2" required>
                                            <option value="New">NEW</option>
                                            <option value="On Progress">ON PROGRESS</option>
                                            <option value="Close">CLOSE</option>
                                            <option value="Cancel">CANCEL</option>
                                        </select>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
