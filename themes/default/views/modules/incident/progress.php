<style>
    /* basic positioning */
    .legend {
        list-style: none;
        font-size: 11px;
        margin-left: -46px;
        margin-top: 8px;
    }

    .legend li {
        float: left;
        margin-right: 10px;
    }

    .legend span {
        border: 1px solid #ccc;
        float: left;
        width: 12px;
        height: 12px;
    }

    /* your colors */
    .legend .tickets-open {
        border-left: 15px solid #dadada;
        margin: 6px;
    }

    .legend .tickets-close {
        border-left: 15px solid rgb(76, 175, 80);
        margin: 6px;
    }

    .anyClass {
        height: 500px !important;
        overflow-y: auto;
        overflow-y: scroll;
        /* position: fixed; */
    }

    .auto_scroll {
        height: 150px !important;
        overflow-y: auto;
        overflow-y: scroll;
        /* position: fixed; */
    }

    .progress {
        height: 20px !important;
    }

    .incident_list {
        padding-left: 0px;
        padding-right: 0px;
    }

    .active_incident {
        background: #a8c6fa !important;
    }

    .ttr_incident {
        /* background: #ffc7c7 !important; */
        border-left: 15px solid #FF6362;
        /* red */
    }

    .close_incident {
        /* background: #dadada !important; */
        border-left: 15px solid #dadada;
        /* grey */
    }

    .stop_incident {
        /* background: #fff3bb !important; */
        border-left: 15px solid #FFEB3B;
        /* yellow */
    }

    .ready_incident {
        /* background: #dffcd6 !important; */
        border-left: 15px solid #4CAF50;
        /* clear */
    }

    /* .select2-search, */
    input.form-control,
    .select2-container,
    textarea.form-control {
        /* background: #f0f0f0 !important; */
        border: 1px solid #c2c2c2 !important;
    }

    .input-group>.input-group-append>.btn {
        border: 1px solid #c1c1c1;
    }

    th.big-col {
        width: 20px !important;
    }

    th.act-col {
        width: 30px !important;
    }

    .color-tr {
        background: #dedede;
    }

    .color-tr>th {
        color: #000 !important;
    }

    table.dataTable th {
        padding: 7px !important;
    }

    .dropdown-menu {
        width: 200px !important;
    }

    /* table>tbody>tr {
        cursor: pointer;
    } */

    table.dataTable th,
    table>tfoot>tr {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }

    .table>thead>tr>th {
        line-height: 3 !important;
    }

    pre {
        padding-left: 20px !important;
        padding-right: 20px !important;
    }

    [class^='select2'] {
        border-radius: 5px !important;
    }

    .inner-shadow {
        -webkit-box-shadow: inset 0px 0px 18px 0px rgba(194, 194, 194, 1);
        -moz-box-shadow: inset 0px 0px 18px 0px rgba(194, 194, 194, 1);
        box-shadow: inset 0px 0px 18px 0px rgba(194, 194, 194, 1);
        z-index: 99999;
    }

    #map {
        height: 525px;
        width: 100%;
    }

    .progress,
    .progress-text {
        cursor: pointer;
    }

    /* ---- Timeline elements ---- */
    #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
        color: lightgrey;
        position: relative;
    }

    #progressbar .active {
        color: #000000
    }

    #progressbar .inactive {
        color: #7e7e7e;
    }

    #progressbar li {
        list-style-type: none;
        font-size: 12px;
        width: 12.222%;
        float: left;
        position: relative;
    }

    #progressbar #open_tickets:before {
        font-family: FontAwesome;
        content: "\f09d";
        cursor: pointer;
        ;
    }

    #progressbar #analyst:before {
        font-family: FontAwesome;
        content: "\f007";
        cursor: pointer;
    }

    #progressbar #prepare_team:before {
        font-family: FontAwesome;
        content: "\f0c0";
        cursor: pointer;
    }

    #progressbar #perjalanan:before {
        font-family: FontAwesome;
        content: "\f0d1";
        /* content:"\f1b9"; */
        /* cursor: pointer; */
    }

    #progressbar #recovery_time:before {
        font-family: FontAwesome;
        content: "\f085";
        cursor: pointer;
    }

    #progressbar #recovery_logic:before {
        font-family: FontAwesome;
        content: "\f085";
        cursor: pointer;
    }

    #progressbar #confirm_time:before {
        font-family: FontAwesome;
        content: "\f00c";
        cursor: pointer;
    }

    #progressbar #waiting_process:before {
        font-family: FontAwesome;
        content: "\f252";
        cursor: pointer;
    }

    #progressbar #stopclock:before {
        font-family: FontAwesome;
        content: "\f017";
        cursor: pointer;
    }

    #progressbar #all:before {
        font-family: FontAwesome;
        content: "\f00b";
        cursor: pointer;
    }

    #progressbar li:before {
        width: 50px;
        height: 50px;
        line-height: 45px;
        display: block;
        font-size: 18px;
        color: #ffffff;
        background: lightgray;
        border-radius: 50%;
        margin: 0 auto 10px auto;
        padding: 2px;
        text-align: center;
        z-index: -1 !important;
    }

    #progressbar li:after {
        content: '';
        width: 100%;
        height: 2px;
        background: lightgray;
        position: absolute;
        left: 0;
        top: 25px;
        z-index: -1 !important;
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
        background: #00b169;
    }

    #progressbar li.inactive:before,
    #progressbar li.inactive:after {
        background: #8edb50;
    }

    .kpi_duration {
        display: block;
    }

    .btn_kpi_duration {
        cursor: pointer;
    }

    input[type=time]::-webkit-datetime-edit-ampm-field {
        display: none;
    }

    /* end of timeline  */
</style>


<!-- START JUMBOTRON -->
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active"><?php echo $data['title'] ?></li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pull-right m-t-10">
                        <div class="row">
                            <!-- Notification Request Stop Clock -->
                            <div class="mr-2">
                                <span class="text-danger text-request" style="display:none"><strong>You Have Request Stop Clock Incident </strong></span>
                            </div>
                            <!-- incident summary -->
                            <!-- <div class="mr-2">
                                <select name="filter" class="select2 select-filter">
                                    <option value=""></option>
                                    <option value="assign">My Incident</option>
                                    <option value="token">My Token</option>
                                </select>
                            </div> -->
                            <div class="mr-2">
                                <button class="btn shadow btn-primary btn-xs btn-incident-summary" type="button">
                                    <strong><i class="fa fa-file"></i>&nbsp; Incident Detail</strong>
                                </button>
                            </div>
                            <!-- incident handling  -->
                            <div class="dropdown mr-2">
                                <button class="btn shadow btn-primary dropdown-toggle btn-xs btn-incident-handling" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <strong><i class="fa fa-cog"></i>&nbsp; Incident Handling</strong>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <button type="button" class="btn border-0 btn-close" style="background-color:transparent"><strong><i class="fa fa-power-off"></i>&nbsp; Close</strong></button>
                                    <!-- <button style="display:none; background-color:transparent" class="btn border-0 btn-duration btn-startclock" data-status="open" data-type="1" data-id="<?php // echo $data['incident_detail']['id'] 
                                                                                                                                                                                                ?>"> <strong><i class="fa fa-unlock"></i>&nbsp; Start Clock</strong></button>
                                    <button style="display:none; background-color:transparent" class="btn border-0 btn-duration btn-stopclock" data-status="stop" data-type="0" data-id="<?php // echo $data['incident_detail']['id'] 
                                                                                                                                                                                            ?>"><strong><i class="fa fa-lock"></i>&nbsp; Stop Clock</strong></button> -->
                                    <button style="display:none; background-color:transparent" class="btn border-0 btn-stopclock-incident"><strong><i class="fa fa-lock"></i>&nbsp; Stop Clock</strong></button>
                                    <button type="button" class="btn border-0 btn-merge" data-target="#modalMergeIncident" data-toggle="modal" style="background-color:transparent"><strong><i class="fa fa-save"></i>&nbsp; Merge</strong></button>
                                    <!-- <button type="button" class="btn border-0 btn-request-stopclock" style="background-color:transparent"><strong><i class="fa fa-clock-o"></i>&nbsp; Request Stop Clock</strong></button> -->
                                    <div class="dropdown-divider"></div>
                                    <button type="button" class="btn border-0 btn-attach-incident" style="background-color:transparent"><strong><i class="fa fa-paperclip"></i>&nbsp; Attach File</strong></button>
                                    <button type="button" class="btn border-0 btn-progress-summary" style="background-color:transparent"><strong><i class="fa fa-list"></i>&nbsp; Summary Progress</strong></button>
                                    <button type="button" class="btn border-0 btn-serpo-location" style="background-color:transparent"><strong><i class="fa fa-map-marker"></i>&nbsp; Serpo Location</strong></button>
                                    <button type="button" class="btn border-0 btn-download" style="background-color:transparent"><strong><i class="fa fa-download"></i>&nbsp; Download PDF</strong></button>
                                    <div class="dropdown-divider"></div>
                                    <form class="form_history" action="<?php echo base_url('incident/close_incident') ?>" method="post">
                                        <input type="hidden" name="id" value="<?php echo $data['incident_detail']['id'] ?>">
                                        <button type="submit" class="btn border-0" style="background-color:transparent"> <strong><i class="fa fa-clock-o"></i>&nbsp; History</strong></button>
                                        <button type="button" class="btn border-0 btn-log d-block" style="background-color:transparent"><strong><i class="fa fa-cogs"></i>&nbsp; Log</strong></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- mulai content disini-->
            <div class="row" style="background:#fff">
                <!-- INCIDENT LIST -->
                <div class="col-md-3 incident_list" id="myTable" style="border-right:2px solid #dedede">
                    <p style="padding-top:10px; padding-bottom:5px; padding-left:20px;" class="count_incident">INCIDENT LIST ()</p>
                    <!-- filter incident  -->
                    <div class="input-group" style="padding-bottom:5px; padding-left:5px; padding-right:5px;">
                        <input type="text" class="form-control" placeholder="Search Incident" onkeyup="filterIncident()" id="myInput">
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <!-- select filter incident  -->
                    <div class="filter" style="padding-left:5px; padding-right:5px; margin-bottom:5px">
                        <select name="filter" class="select2 select-filter" data-placeholder=" Select Filter" style="width:100%;">
                            <option value="all">All</option>
                            <option value="assign">My Incident</option>
                            <option value="token" selected>My Token</option>
                        </select>
                    </div>
                    <div class="list-group anyClass scroll-incident-list border-top"></div>
                    <!-- <img src="<?php // echo base_url() 
                                    ?>themes/default/assets/img/Pulse-1s-200px.gif"> -->
                </div>
                <!-- END OF INCIDENT LIST  -->
                <!-- INCIDENT INFO -->
                <div class="col-md-6" style="border-right:2px solid #dedede">
                    <div class="inner-incident-info">
                        <div class="row">
                            <div class="col-md-6">
                                <p class="mt-2 mb-2 total_incident_progress">INCIDENT PROGRESS ()</p>
                            </div>
                            <div class="col-md-6">
                                <p class="mt-2 mb-2 pull-right last_progress">LAST PROGRESS : </p>
                            </div>
                        </div>
                        <div class="input-group" style="padding-bottom:5px;">
                            <input type="text" class="form-control" placeholder="Search Messages" id="myProgress">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <div class="list-group scroll-incident-progress" style="background:#eee; padding-right:10px; padding-left:10px; height:440px; overflow-y: auto; overflow-y: scroll; font-size:12px; padding-bottom:5px;background-image: url(https://www.toptal.com/designers/subtlepatterns/patterns/what-the-hex.png);">
                        </div>
                        <form class="form_create_message">
                            <input type="hidden" name="id" />
                            <input type="hidden" name="user_active_mod" />
                            <div class="token_class"></div>
                            <div class="form-group form_validation">
                                <label for="">Process :</label>
                                <select id="process" class="full-width select2 select-process" name="process" data-placeholder="-- Select validation form --">
                                </select>
                            </div>
                            <div class="form-group">
                                <!-- <label for="">Created Message :</label> -->
                                <p class="input-file"></p>
                                <input type="file" name="userfile" id="attach" class="btn btn-sm btn-info border full-width mb-2" style="display:none" accept="image/*">
                            </div>
                            <div class="form-group preeload" style="display:none">
                                <img src="<?php echo $this->template->get_theme_path(); ?>assets/img/preeload.gif" alt="" style="height:20px; float:left; margin-right:5px">
                                <p>Prosses..</p>
                            </div>
                            <div id="process-mandatory">
                            </div>

                            <div class="form-group" id="incident_close">
                                <div class="input-group">
                                    <textarea rows="7" cols="55" class="form-control custom-control" name="message" id="created_message" placeholder="-- Created Message --"></textarea>
                                    <!-- <label for="attach" class="input-group-addon btn border" style="border-radius:0; background:#eee; border:1px solid #c2c2c2 !important; padding-top:12px">
                                        <i class="fa fa-paperclip fa-lg"></i>
                                    </label>
                                    <span class="input-group-addon btn btn-primary btn-submit-span" style="border-radius:0">
                                        <button type="submit" class="btn btn-submit border-0 text-white" style="background-color:transparent" disabled><strong>Send</strong></button>
                                    </span> -->
                                </div>
                                <!-- <p id="justification"></p> -->
                                <!-- <br> -->
                                <label for="attach" class="input-group-addon btn border" style="border-radius:0; background:#eee; border:1px solid #c2c2c2 !important; padding-top:5px; margin-top:5px">
                                    <i class="fa fa-paperclip fa-lg"></i>
                                </label>&nbsp;
                                <button type="submit" class="input-group-addon btn btn-primary btn-submit-span text-white" style="margin-top:5px"><strong>Send</strong></button>
                            </div>
                        </form>
                        <!-- end of create message -->
                    </div>
                </div>
                <!-- END OF INCIDENT INFO -->
                <!-- STATUS DAN TOKEN INCIDENT PROGRESS -->
                <div class="col-md-3">
                    <!-- header -->
                    <div class="row" style="border-bottom:1px solid #dedede;">
                        <div class="col-md-6" style="border-right:1px solid #dedede; background:#a8c6fa;">
                            <p style="padding-top:10px; font-size:12px">TOKEN</p>
                            <p class="token_incident" style="font-size:12px; line-height:1"></p>
                            <p class="lock_incident" style="font-size:11px; line-height:1"></p>
                        </div>
                        <div class="col-md-6" style="background:#72bb53; color:#fff">
                            <p style="padding-top:10px; font-size:12px">STATUS</p>
                            <p style="font-size:20px; text-transform:uppercase" class="status_incident"></p>
                            <p style="padding-top:10px; font-size:12px">MTTR</p>
                            <p class="mttr_incident"></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p style="padding-top:20px; padding-top:20px; padding-bottom:20px" class="participants">PARTICIPANTS ()</p>
                            <div class="list-group list-incident-participant auto_scroll">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                            <p style="padding-top:20px; padding-top:20px; padding-bottom:20px" class="ticket_attch">TICKET ATTACH ()</p>
                            <div class="list-group list-ticket-attch auto_scroll"></div>
                            <ul class="legend">
                                <li><span class="tickets-open"></span> Ticket Open</li>
                                <li><span class="tickets-close"></span> Ticket Close</li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                            <p style="padding-top:20px; padding-top:20px; padding-bottom:20px">INCIDENT KPI</p>
                            <div class="list-group list-incident-kpi">
                                <!-- <p data-toggle="tooltip" data-placement="top" title="time ticket create to incident create" class="progress-text">Response Time &nbsp;<i class="fa fa-info-circle"></i><span class="float-right">Target : 10 Min</span></p>
                                <div class="progress response_time_new rounded shadow-sm"></div> -->
                                <?php if ($_COOKIE['member'] != 'SERPO') { ?>
                                    <p data-toggle="tooltip" data-placement="top" title="time start to end token" class="progress-text">Analysis Time &nbsp;<i class="fa fa-info-circle"></i><span class="float-right">Target : 10 Min</span></p>
                                    <div class="progress analisis_time_new rounded shadow-sm"></div>
                                <?php } ?>
                                <p data-toggle="tooltip" data-placement="top" title="time start to end token" class="progress-text">Recovery Time &nbsp;<i class="fa fa-info-circle"></i><span class="float-right">Target : 2 Hour</span></p>
                                <div class="progress recovery_time_new rounded shadow-sm"></div>
                                <p data-toggle="tooltip" data-placement="top" title="time incident open to close " class="progress-text">MTTR Time &nbsp;<i class="fa fa-info-circle"></i><span class="float-right">Target : 4 Hour</span></p>
                                <div class="progress mttr_time_new rounded shadow-sm"></div>
                                <!-- <p data-toggle="tooltip" data-placement="top" title="time confirmation from contact center" class="progress-text">Confirmation Time &nbsp;<i class="fa fa-info-circle"></i><span class="float-right">Target : 10 Min</span></p>
                                <div class="progress confirmation_time_new rounded shadow-sm"></div> -->
                            </div>
                        </div>
                    </div>
                    <!-- end of kpi -->

                </div>
                <!-- END OF TTOKEN STATUS  -->
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->

<!-- START MODAL EDIT SHIFT -->
<div class="modal fade slide-up" id="modalCloseIncident" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Close Incident <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form class="form_close" action="" method="post">
                    <input type="hidden" name="id">
                    <div class="modal-body m-t-20">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Timestamp :</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="timestamp" id="datepicker-component" value="<?php echo date("Y-m-d") ?>" placeholder="-- Date --">
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="" value="<?php date_default_timezone_set('Asia/Jakarta');
                                                                                            echo date("H:i:s") ?>" placeholder="-- Time --">
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Reason :</label>
                            <div class="col-sm-10">
                                <textarea name="reason" class="form-control" rows="5" placeholder="-- Reason --"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Additional Reference Data :</label>
                            <div class="col-sm-10">
                                <textarea name="add_reference" class="form-control" rows="5" placeholder="-- Write Site , Brand and equipment reference if you left it blank in below form --"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Configuration Change :</label>
                            <div class="col-sm-10">
                                <select class="full-width select2 select_configuration_change" name="conf_change" data-init-plugin="select2" data-placeholder="-- Selected Configuration Change --">
                                    <option value=""></option>
                                    <option value="yes">Yes</option>
                                    <option value="no" selected>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row configurasi_change_activity" style="display:none">
                            <label class="col-sm-2 col-form-label">Configuration Change Activity :</label>
                            <div class="col-sm-10">
                                <select class="full-width select2" name="conf_change_acv" data-init-plugin="select2" data-placeholder="-- Selected Configuration Change Activity --">
                                    <option value=""></option>
                                    <option value="update">Update</option>
                                    <option value="downgrade">Downgrade</option>
                                    <option value="relocation_port">Relocation Port</option>
                                    <option value="change_device">Change Device</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Physical Inventori Change :</label>
                            <div class="col-sm-10">
                                <div class="checkbox check-success form-check-inline">
                                    <input type="checkbox" name="physical_inv_change1" value="1" id="checkbox1">
                                    <label for="checkbox1">Inside Plant</label>
                                </div>
                                <div class="checkbox check-success form-check-inline">
                                    <input type="checkbox" name="physical_inv_change2" value="2" id="checkbox2">
                                    <label for="checkbox2">OutSite Plant</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" style="display:none">
                            <label class="col-sm-2 col-form-label">Physical Inventori Change value :</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="physical_inv_change">
                            </div>
                        </div>
                        <div class="form-group row inv_change_desc" style="display:none">
                            <label class="col-sm-2 col-form-label">Inventori Change Description :</label>
                            <div class="col-sm-10">
                                <textarea name="inv_change_desc" class="form-control" rows="5" placeholder="-- Write inventory change description --"></textarea>
                            </div>
                        </div>

                        <!-- Detail Incident -->
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label pl-2" style="background:#eee">Incident Detail</label>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Creator :</label>
                            <div class="col-md-10">
                                <input type="text" name="creator" class="form-control" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Timestamp Open :</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="timestamp" id="datepicker-timstamp2" value="<?php echo date("Y-m-d") ?>" placeholder="-- Date --" readonly>
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="" value="<?php date_default_timezone_set('Asia/Jakarta');
                                                                                            echo date("H:i:s") ?>" placeholder="-- Time --" readonly>
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Clasification :</label>
                            <div class="col-md-10">
                                <select class="full-width select2" id="select_classification" name="select_classification" data-placeholder="Select" required>
                                    <option value="0"></option>
                                    <?php foreach ($data['classification'] as $r_data) { ?>
                                        <option value="<?php echo $r_data->id ?>"><?php echo $r_data->classification_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="">Root Cause :</label>
                            <div class="col-md-10">
                                <select class="full-width select2" data-placeholder="Select" id="select_root_cause" name="select_root_cause" required>
                                    <option value="0" selected></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Root Cause Detail</label>
                            <div class="col-md-10">
                                <select class="full-width select2" data-placeholder="Select" id="select_root_cause_detail" name="select_root_cause_detail" required>
                                    <option value="0" selected></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Network</label>
                            <div class="radio radio-success col-md-10">
                                <input required type="radio" value="ip" name="network" id="ip" checked>
                                <label for="ip">IP</label>
                                <input required type="radio" value="tdm" name="network" id="tdm">
                                <label for="tdm">TDM</label>
                                <input required type="radio" value="vsat" name="network" id="vsat">
                                <label for="vsat">VSAT</label>
                                <input required type="radio" value="infra" name="network" id="infra">
                                <label for="infra">INFRA</label>
                                <input required type="radio" value="ms" name="network" id="ms">
                                <label for="ms">MS</label>
                                <input required type="radio" value="mm" name="network" id="mm">
                                <label for="mm">MM</label>
                                <input required type="radio" value="oa" name="network" id="oa">
                                <label for="oa">OA</label>
                                <input required type="radio" value="mspc" name="network" id="mspc">
                                <label for="mspc">MSPC</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Priority</label>
                            <div class="radio radio-success col-md-10">
                                <input type="radio" value="low" name="priority" id="low" checked>
                                <label for="low">Low</label>
                                <input type="radio" value="medium" name="priority" id="medium">
                                <label for="medium">Medium</label>
                                <input type="radio" value="high" name="priority" id="high">
                                <label for="high">High</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Description</label>
                            <div class="col-md-10">
                                <textarea class="form-control" rows="5" name="description"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Link</label>
                            <div class="col-md-10">
                                <select class="full-width select2" data-placeholder="Select" id="link" name="link" required>
                                    <option value="0" selected></option>
                                    <option value="backbone">Backbone</option>
                                    <option value="distribution">Distribution</option>
                                    <option value="access">Access</option>
                                    <option value="retail">Retail</option>
                                </select>
                            </div>
                        </div>
                        <div style="display: none;" id="foc-show">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_region" name="link_start_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_site" name="link_start_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_region" name="link_end_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_site" name="link_end_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- <div style="display: none;" id="div_region"> -->
                        <div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Region</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="region1" name="region1" required>
                                        <option value="0" selected></option>
                                        <?php
                                        foreach ($data['region'] as $row) :
                                            if ($row->status == 0) { ?>
                                                <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php }
                                        endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group row" id="div_site_name"> -->
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Site Name</label>
                            <div class="col-md-10">
                                <select class="full-width select2 select2" id="site_name1" name="site_name1" required>
                                    <option value="0" selected></option>
                                </select>
                            </div>
                        </div>
                        <div style="display: none;" id="foc-show">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_region" name="link_start_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_site" name="link_start_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_region" name="link_end_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_site" name="link_end_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="11">
                            <label for="fname" class="col-md-2 col-form-label">Source Category :</label>
                            <div class="col-md-10">
                                <!-- <label class="m-t-10" id="source_category"></label> -->
                                <!-- <input type="hidden" name="source_category_input" id="source_category_input">
                                <div style="display: none;" id="div_link_upstream_isp"> -->
                                <!-- <select class="full-width select2" data-placeholder="Select" id="link_upstream_isp" name="link_upstream_isp"> -->
                                <select class="full-width select2" data-placeholder="Select" name="source_category_input" id="source_category_input" required>
                                    <option value="FOT">FOT</option>
                                    <option value="FOC">FOC</option>
                                    <option value="PS">PS</option>
                                </select>
                                <!-- </div> -->
                            </div>
                        </div>
                        <div id="1" style="display: none;">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Brand :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="brand1" name="brand1">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">ME :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="me1" name="me1">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="3" style="display: none;">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Broken FO :</label>
                                <div class="col-md-10">
                                    <input type="number" class="form-control" id="fname" placeholder="" name="broken_fo" value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-complete"><i class="fa fa-save"></i>&nbsp; Save</button>
                        <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->



<!-- START MODAL ADD FILE ATTACHMENT  -->
<div class="modal fade slide-up" id="modalAttachFileIncident" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Incident Attachment <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="mt-2">INCIDENT DETAIL</p>
                    <div class="incident-info"></div>
                    <form class="form_attach_file_incident">
                        <div class="form-group" style="display:none">
                            <input type="hidden" name="incident_id" required />
                            <input type="file" class="form-control" name="incident_file" id="attach_incident_file" accept="image/*" />
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" name="incident_file_text" placeholder="-- Choose File --" aria-label="-- Choose File --" aria-describedby="basic-addon2" autocomplete="off">
                            <div class="input-group-append">
                                <label class="btn btn-outline-secondary" for="attach_incident_file"><i class="fa fa-paperclip"></i></label>
                            </div>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-outline-secondary" type="button">Upload</button>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-hover table-striped" id="tb_incident_file_attach" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Uploader</th>
                                    <th>FileName</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->



<!-- START MODAL REQUEST STOP CLOCK  -->
<!-- <div class="modal fade slide-up" id="modalRequestStopClock" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper"> -->
<!-- mod-modal -->
<!-- <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Request Stop Clock <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="data-request-stopclock mt-2">
                        <table class="table table-hover table-striped table-condensed" id="tb_request_stopclock" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:5px">No.</th>
                                    <th style="width:20px">Incident ID</th>
                                    <th style="width:100px">Description</th>
                                    <th style="width:50px">Time Stop Request</th>
                                    <th style="width:50px">Reason</th>
                                    <th style="width:20px">User Requeust</th>
                                    <th style="width:20px">Group</th>
                                    <th style="width:30px">Aksi</th>
                                </tr>
                            </thead> -->
<!-- <tfoot>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                            </tfoot> -->
<!-- </table>
                    </div>
                </div> -->
<!-- modal footer -->
<!-- <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div> -->
<!-- end modal footer -->
<!-- </div> -->
<!-- endof mod-modal -->
<!-- </div> -->
<!-- /.modal-content -->
<!-- </div> -->
<!-- </div> -->
<!-- END MODAL  -->


<!-- <div class="modal fade slide-up" id="modalProgressSummary" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper"> -->
<!-- mod-modal -->
<!-- <div class="modal-content mod-modal-progress">
                <div class="modal-header">
                    <h5>Progress Summary <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class=" container-fluid   container-fixed-lg">
                        <div id="rootwizard" class="m-t-50"> -->
<!-- Nav tabs -->

<!-- <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm" role="tablist" data-init-reponsive-tabs="dropdownfx">
                                <li class="nav-item">
                                    <a id="active-openticket" class="" data-toggle="tab" href="#tab1" data-target="#tab1" role="tab"><i class="fa fa-ticket tab-icon"></i> <b>Open Tickets</b><br><span class="countopenticket"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a id="active-analyst" class="" data-toggle="tab" href="#tab2" data-target="#tab2" role="tab"><i class="fa fa-cogs tab-icon"></i> <b>Analyst</b><br>
                                        <center><span class="countanalyst"></span></center>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="active-orderteam" class="" data-toggle="tab" href="#tab3" data-target="#tab3" role="tab"><i class="fa fa-users tab-icon"></i> <b>Order Team</b><br>
                                        <center><span class="countorderteam"></span></center>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="active-kirimteam" class="" data-toggle="tab" href="#tab4" data-target="#tab4" role="tab"><i class="fa fa-truck tab-icon"></i> <b>Team Berangkat</b><br>
                                        <center><span class="countkirimteam"></span></center>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="active-action" class="" data-toggle="tab" href="#tab5" data-target="#tab5" role="tab"><i class="fa fa-cogs tab-icon"></i> <b>Recovery</b><br>
                                        <center><span class="countaction"></span></center>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="active-confirm" class="" data-toggle="tab" href="#tab6" data-target="#tab6" role="tab"><i class="fa fa-check tab-icon"></i> <b>Confirm</b><br>
                                        <center><span class="countconfirm"></span></center>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a id="active-all" class="" data-toggle="tab" href="#tab7" data-target="#tab7" role="tab"><i class="fa fa-list tab-icon"></i> <b>All</b><br><span class="all"></span>
                                        <center><span class="countall"></span></center>
                                    </a>
                                </li>

                            </ul> -->
<!-- Tab panes -->
<!-- <div class="tab-content" style="background-image: url(https://www.toptal.com/designers/subtlepatterns/patterns/what-the-hex.png);">
                                <div class="tab-pane padding-20 sm-no-padding slide-left" id="tab1">
                                    <div class="row row-same-height">
                                        <div class="openticket" style="width:100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane slide-left padding-20 sm-no-padding" id="tab2">
                                    <div class="row row-same-height">
                                        <div class="analyst" style="width:100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane slide-left padding-20 sm-no-padding" id="tab3">
                                    <div class="row row-same-height">
                                        <div class="orderteam" style="width:100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane slide-left padding-20 sm-no-padding" id="tab4">
                                    <div class="row row-same-height">
                                        <div class="kirimteam" style="width:100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane slide-left padding-20 sm-no-padding" id="tab5">
                                    <div class="row row-same-height">
                                        <div class="action" style="width:100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane slide-left padding-20 sm-no-padding" id="tab6">
                                    <div class="row row-same-height">
                                        <div class="confirm" style="width:100%">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane slide-left padding-20 sm-no-padding" id="tab7">
                                    <div class="row row-same-height">
                                        <div class="dataall" style="width:100%">
                                        </div>
                                    </div>
                                </div> -->
<!-- wizard-footer padding-20 bg-master-light  -->
<!-- </div>
                        </div>
                    </div>
                </div> -->
<!-- modal footer -->
<!-- <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div> -->
<!-- end modal footer -->
<!-- </div> -->
<!-- endof mod-modal -->
<!-- </div> -->
<!-- /.modal-content -->
<!-- </div>
</div> -->

<!-- form validation  -->
<div class="modal fade slide-up" id="modalJustification" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Progress Mention <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form_action_progress">
                        <input type="hidden" name="id">
                        <input type="message" name="token">
                        <h1>Form</h1>
                        <div class="form-group">
                            <label for="">verification :</label>
                            <select class="full-width select2" name="conf_change" data-init-plugin="select2" data-placeholder="-- Selected Configuration Change --">
                                <option value="analyst">Analyst</option>
                                <option value="order_team">Order Team</option>
                                <option value="kirim_team">Kirim Team</option>
                                <option value="action">Action</option>
                                <option value="confirm">Confirm</option>
                            </select>
                        </div>
                    </form>
                </div>
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Send</button>
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<!-- START MODAL TICKET PROGRESS -->
<div class="modal fade slide-up" id="modalTicketProgress" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Ticket Progress <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body m-t-20">
                    <button class="btn btn-xs btn-success shadow btn-cm"><strong>CM</strong></button>
                    <button class="btn btn-xs btn-success shadow btn-autocheck"><strong>Autocheck</strong></button>
                    <button class="btn btn-xs btn-success shadow btn-alarm"><strong>Alarm</strong></button>
                    <button class="btn btn-xs btn-success shadow btn-assets"><strong>Assets</strong></button>
                    <button class="btn btn-xs btn-success shadow btn-progress"><strong>Progress</strong></button>
                    <button class="btn btn-xs btn-success shadow btn-ticket-detail"><strong>Ticket Detail</strong></button>
                    <table class="table table-striped table-condensed table-hover" id="ticket_progress">
                        <thead>
                            <tr>
                                <th style="width:1px">No.</th>
                                <th style="width:20px">User</th>
                                <th style="width:20px">Timestamp</th>
                                <!-- <th style="width:20px">Ticket ID</th>
                                <th style="width:20px">Ismilling ID</th>
                                <th style="width:20px">SID</th> -->
                                <th style="width:100px">Description</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- end modal body -->
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<!-- MODAL Merge Incident -->
<div class="modal fade slide-up" id="modalMergeIncident" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal" style="width:1200px">
                <div class="modal-header">
                    <h5>Merge <span class="semi-bold">Incident</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start modal body -->
                <div class="modal-body m-t-20">
                    <div class="incident_id" style="display:none"></div>
                    <div class="detail_incident">
                        <form action="form_merge">
                            <div class="form-group" style="background:#eee; padding:10px">
                                <label for="" data-toggle="collapse" data-target="#collapseOne" style="cursor:pointer">Incident Detail</label>
                                <span class="pull-right" data-toggle="collapse" data-target="#collapseOne" style="cursor:pointer"><i class="fa fa-chevron-circle-down"></i></span>
                                <div class="collapse" id="collapseOne">
                                    <textarea class="form-control incident_detail mb-2" rows="5" name="incident_detail" readonly></textarea>
                                </div>
                                <select class="full-width select2 select-merge-reason" name="merge_reason" data-placeholder="-- Select Merge Reason --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed" id="merge_incident" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:5px">No.</th>
                                    <th style="width:20px">Open Time</th>
                                    <th style="width:20px">Creator</th>
                                    <th style="width:200px">Description</th>
                                    <!-- <th style="width:5px">Total Ticket</th> -->
                                    <th style="width:5px">Incident ID</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                    <td colspan="6"></td>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>
                <!-- end modal body -->
                <!-- modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-complete btn_merge"><i class="fa fa-save"></i> Save</button>
                    <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- end of modla dialog -->
</div>

<!-- START MODAL TICKET DETAIL-->
<!-- <div class="modal fade slide-up" id="modalTicketDetail" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper"> -->
<!-- mod-modal -->
<!-- <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Ticket detail <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form_ticket_detail mt-3">
                        <div class="form-group row">
                            <label class="col-md-2">Ticket ID</label>
                            <div class="col-md-10">
                                <input type="text" name="ticket_id" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2">Ismilling ID</label>
                            <div class="col-md-10">
                                <input type="text" name="ismilling_id" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2">Service ID</label>
                            <div class="col-md-10">
                                <input type="text" name="service_id" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2">Open Time</label>
                            <div class="col-md-10">
                                <input type="text" name="open_time" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2">Attch Time</label>
                            <div class="col-md-10">
                                <input type="text" name="attach_time" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2">Description</label>
                            <div class="col-md-10">
                                <textarea rows="7" name="description" class="form-control" readonly></textarea>
                            </div>
                        </div>
                    </form>
                </div> -->
<!-- modal footer -->
<!-- <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div> -->
<!-- end modal footer -->
<!-- </div> -->
<!-- endof mod-modal -->
<!-- </div> -->
<!-- /.modal-content -->
<!-- </div> -->
<!-- </div> -->
<!-- END MODAL  -->

<div class="modal fade slide-up" id="modalTicketDetail" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Ticket detail <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>

                <div class=" container-fluid container-fixed-lg bg-white modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- START card -->
                            <div class="card card-transparent">
                                <div class="card-body">
                                    <ul class="nav nav-tabs no-border notification-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="btn btn-default btn-cons btn-rounded btn-notification-style active m-b-10" href="#loadingBar" role="tab" data-toggle="tab" data-type="position-bar">Ticket Detail</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="btn btn-default btn-cons btn-rounded btn-notification-style m-b-10" href="#bouncyFlip" role="tab" data-toggle="tab" data-type="position-flip">CRM Detail</a>
                                        </li>
                                        <!-- <li class="nav-item">
                                            <a class="btn btn-default btn-cons btn-rounded btn-notification-style m-b-10" href="#circleNotification" role="tab" data-toggle="tab" data-type="position-circle">Ticket Progess</a>
                                        </li> -->
                                    </ul>
                                    <div class="config-notification">
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="loadingBar">
                                                <!-- <h4 class="semi-bold">Ticket Detail</h4> -->
                                                <form class="form_ticket_detail">
                                                    <div class="card-body ">
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Ticket ID</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="" name="ticket_id" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Ismilling ID</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="" name="ismilling_id">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Service ID</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="" name="service_id">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Open Time</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="" name="open_time">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Attach Time</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" placeholder="" name="attach_time">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Description</label>
                                                            <div class="col-md-9">
                                                                <textarea rows="5" name="description" class="form-control"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane" id="bouncyFlip">
                                                <!-- <h4 class="semi-bold">CRM</h4> -->
                                                <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                                    <li class="nav-item">
                                                        <a href="#" class="active" data-toggle="tab" data-target="#slide1"><span>Ismilling</span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#" data-toggle="tab" data-target="#slide2"><span>Sisfo MT</span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#" data-toggle="tab" data-target="#slide3"><span>IPDB</span></a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane slide-left active" id="slide1">
                                                        <div class="row">
                                                            <div class="col-lg-12" style="overflow: auto; left: 0px; top: 0px; width: 540px; height: 260px;">
                                                                <iframe id="frameIsmilling" frameborder="0" width="900" height="500" src=""></iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane slide-left" id="slide2">
                                                        <div class="row">
                                                            <div class="col-lg-12">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane slide-left" id="slide3">
                                                        <div class="row">
                                                            <div class="col-lg-12">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="circleNotification">
                                                <!-- <h4 class="semi-bold">Ticket Progress</h4> -->
                                                <div class="table-responsive">
                                                    <table class="table table-hover" id="ticket_progress">
                                                        <thead>
                                                            <tr>
                                                                <th width="5">User</th>
                                                                <th width="5">Time</th>
                                                                <th width="5">Description</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="vertical-align: top">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END card -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- START MODAL REQUEST STOP CLOCK -->
<div class="modal fade slide-up" id="modalStopClock" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Stop Clock <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <form class="form_stop_clock">
                    <input type="hidden" name="id">
                    <input type="hidden" name="type">
                    <input type="hidden" name="status">
                    <div class="modal-body m-t-20">
                        <!-- <div class="form-group">
                            <label>Timestamp :</label>
                            <input type="text" class="form-control" name="timestamp" placeholder="-- Timestamp --" />
                        </div> -->
                        <div class="form-group">
                            <label>Incident Description :</label>
                            <textarea class="form-control" name="description" rows="5" placeholder="-- Incident Description --" readonly></textarea>
                        </div>
                        <!-- <div class="form-group">
                            <label>Duration :</label>
                            <input type="text" class="form-control" name="duration" placeholder="-- Duration --" />
                        </div> -->
                        <div class="form-group">
                            <label>Duration :</label><br>
                            <div style="background-color: white;display: inline-flex;border: 1px solid #ccc;color: #555;">
                                <input required name="request_duration_hours" style="border: none;color: #555;text-align: center;width: 60px;" type="number" min="0" max="23" placeholder="23">:
                                <input required name="request_duration_minutes" style="border: none;color: #555;text-align: center;width: 60px;" type="number" min="0" max="59" placeholder="00">
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label>Request Duration :</label>
                            <input type="time" class="form-control" name="request_duration" placeholder="-- Duration Hours --" required/>
                        </div> -->
                        <div class="form-group">
                            <label for="">Reason type :</label>
                            <select class="full-width select2 select_reason_type" name="reason_type" data-placeholder="-- Select validation form --" required>
                                <option value=""></option>
                                <!-- <option value="hujan">Hujan</option>
                                <option value="hujan">K3</option> -->
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Reason Stop Clock :</label>
                            <textarea type="text" class="form-control" rows="5" name="reason" placeholder="-- Reason Stop Clock --" required></textarea>
                        </div>
                        <div class="form-group">
                            <label>Evidence :</label>
                            <input type="file" class="form-control" name="stopclock_evident" accept="image/*">
                        </div>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-success btn-form-stop" type="button"><i class="fa fa-save"></i>&nbsp; <strong>Stop Clock</strong></button>
                        <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; <strong>Close</strong></button>
                    </div>
                    <!-- end modal footer -->
                </form>
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL SUMMARY -->
<div class="modal fade slide-up" id="modalSummary" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Summary Incident <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body m-t-20">
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Incident ID :</label>
                                <input type="text" class="form-control" id="mod_row_incidentId" placeholder="Enter Incident ID" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Creator :</label>
                                <input type="text" class="form-control" id="mod_row_creator" placeholder="Enter Creator" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Notification Trigger :</label>
                                <input type="text" class="form-control" id="mod_row_notificationTrigger" placeholder="Notification Trigger" readonly>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>State :</label>
                                <input type="text" class="form-control" id="mod_row_state" placeholder="Enter State" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Clasification :</label>
                                <input type="text" class="form-control" id="mod_row_clasification" placeholder="Enter Clasification" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Root Cause :</label>
                                <input type="text" class="form-control" id="mod_row_rootCause" placeholder="Root Cause" readonly>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label>Root Cause Detail :</label>
                                <input type="text" class="form-control" id="mod_row_rootCauseDetail" placeholder="Enter Root Cause Detail" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Hirarchy :</label>
                                <input type="text" class="form-control" id="mod_row_hirarchy" placeholder="Enter Hirarchy" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Link :</label>
                                <input type="text" class="form-control" id="mod_row_link" placeholder="Enter Link" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Equipment :</label>
                                <input type="text" class="form-control" id="mod_equipment" placeholder="Enter Site" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Description :</label>
                            <textarea class="form-control" id="mod_row_description" placeholder="Enter Description" rows="5" readonly></textarea>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label>Link Start Region :</label>
                                <input type="text" class="form-control" id="mod_row_link_start_region" placeholder="Enter Link" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Link End Region:</label>
                                <input type="text" class="form-control" id="mod_row_link_end_region" placeholder="Enter Link" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Link Start Site:</label>
                                <input type="text" class="form-control" id="mod_row_link_start_site" placeholder="Enter Link" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Link end Site :</label>
                                <input type="text" class="form-control" id="mod_row_link_end_site" placeholder="Enter Link" readonly>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label>Region :</label>
                                <input type="text" class="form-control" id="mod_row_region" placeholder="Enter Region" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Site :</label>
                                <input type="text" class="form-control" id="mod_row_site" placeholder="Enter Site" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Source Category :</label>
                                <input type="text" class="form-control" id="mod_source_category" placeholder="Enter Site" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Brand :</label>
                                <input type="text" class="form-control" id="mod_brand" placeholder="Enter Site" readonly>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label>Creation Time :</label>
                                <input type="text" class="form-control" id="mod_row_creationTime" placeholder="Enter Creation Time" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Duration :</label>
                                <input type="text" class="form-control" id="mod_row_duration" placeholder="Enter Duration" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>Total Stop Lock :</label>
                                <input type="text" class="form-control" id="mod_row_stopLock" placeholder="Enter Total Stop Lock" readonly>
                            </div>
                            <div class="form-group col-md-3">
                                <label>TTR :</label>
                                <input type="text" class="form-control" id="mod_row_ttr" placeholder="Enter Total Stop Lock" readonly>
                            </div>
                        </div>

                    </form>
                </div>
                <!-- end modal body -->
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<!-- START MODAL INTEGRATION -->
<div class="modal fade slide-up" id="modalcm" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>CM <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-reponsive mt-2">
                        <table class="table table-hover table-striped table-condensed" id="tb_cm" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>No SO</th>
                                    <th>Service ID</th>
                                    <th>ID So</th>
                                    <th>Name</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                    <td colspan="6"></td>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>

                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->

            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<!-- START MODAL INTEGRATION ALARM-->
<div class="modal fade slide-up" id="modalalarm" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Alarm <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-striped table-condensed" id="tb_alarm" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>ID</th>
                                    <th>TrigID</th>
                                    <th>eventid</th>
                                    <th>zabb</th>
                                    <th>priority</th>
                                    <th>hostid</th>
                                    <th>ipaddr</th>
                                    <th>host_</th>
                                    <th>Description</th>
                                    <th>sid</th>
                                    <th>clock</th>
                                    <th>ack</th>
                                    <th>msg</th>
                                    <th>indate</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                    <td colspan="15"></td>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL INTEGRATION ALARM-->
<div class="modal fade slide-up" id="modallog" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Log Incident Progress <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-striped table-condensed" id="tb_log" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>User</th>
                                    <th>Group</th>
                                    <th>Action</th>
                                    <th>Remarks</th>
                                    <th>Time Log</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                    <td colspan="5"></td>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<!-- START MODAL INTEGRATION -->
<div class="modal fade slide-up" id="modalassets" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Assets <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-reponsive mt-2">
                        <table class="table table-hover table-striped table-condensed" id="tb_assets" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <!-- <th>Row Numb</th> -->
                                    <!-- <th>ID</th> -->
                                    <!-- <th>Code</th> -->
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Pic</th>
                                    <th>Pic Phone</th>
                                    <th>Pic Email</th>
                                    <th>Description</th>
                                    <th>Username</th>
                                    <th>Updated At</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                    <td colspan="10"></td>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>

                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->

            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<!-- START MODAL CM AUTOCHECK-->
<div class="modal fade slide-up" id="modalautocheck" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>CM Autocheck <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe id="frameAutocheck" frameborder="0" width="1200" height="500" src="" style="overflow-x:auto"></iframe>
                </div>
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<!-- START MODAL DISPLAY LIST SERPO COMMENT-->
<div class="modal fade slide-up" id="modalSerpoLocation" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5 class="header-maps">SERPO LOCATION <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <form class="form-maps">
                    <input type="hidden" id="lat" name="lat">
                    <input type="hidden" id="long" name="long">
                </form>
                <div class="modal-body m-t-20">
                    <div id="map"></div>
                </div>
                <!-- end modal body -->
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; <strong>Close</strong></button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL DISPLAY SUMMARY PROGRESS-->
<div class="modal fade slide-up" id="modalSummaryProgress" role="dialog" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal" style="z-index: 9999 !important">
                <div class="modal-header">
                    <h5 class="header-maps">Summary Progress <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start modal body  -->
                <div class="modal-body m-t-20">
                    <!-- time line  -->
                    <ul id="progressbar">
                    </ul>
                    <!-- end of time line  -->

                    <!-- chat progress -->
                    <div class="list-group summary_progress_detail" style="background:#eee; padding-right:10px; padding-left:10px; height:400px; overflow-y: auto; overflow-y: scroll; font-size:12px; padding-bottom:5px;background-image: url(https://www.toptal.com/designers/subtlepatterns/patterns/what-the-hex.png);">
                    </div>
                    <!-- end of chat progress -->
                </div>
                <!-- end modal body -->
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; <strong>Close</strong></button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<!-- <div style="display:none" class="incident_progress" data-status="<?php echo $data['incident_detail']['status'] ?>" data-group="<?php echo $_COOKIE['group_code'] ?>"></div> -->
<div class="sid" data-sid=""></div>
<div class="info_login" data-usergroup="<?php echo $_COOKIE['group_code'] ?>" style="display:none"></div>
<!-- <div class="test pl-3">...</div>  -->
<!-- <button class="btn btn-primary" type="button" disabled>
  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
  Loading...
</button> -->

<script>
    // declare variable
    var id;
    var id_incident;
    var segment;
    var group;

    // set variable
    id_incident = <?php echo $data['incident_detail']['incident_id'] ?>;
    id = <?php echo $data['incident_detail']['id'] ?>;
    segment = <?php echo $this->uri->segment(3) ?>;
    group = <?php echo $data['group'] ?>;
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYxcj9zQErZ6KkleQahg_vuY2cRg5yfEU&callback=fn_progress_maps"></script>