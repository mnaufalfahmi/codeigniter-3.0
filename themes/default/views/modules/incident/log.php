<style>
    table.dataTable th,
    table>tfoot>tr {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }

    .table > thead > tr >th {
        line-height: 3 !important;
    }

    /* pcessing  */
    .dataTables_wrapper .dataTables_processing {
        position: fixed;
        top: 50%;
        left: 50%;
        width: 200px;
        /* height: 20px; */
        margin-bottom: 20px;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        margin-left: -100px;
        margin-top: 0px;
        text-align: center;
        padding: 1em 0;
        z-index: 1;
    }
</style>

<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- mulai content table-->
            <div class="row">
                <div class="col-md-12">
                    <!-- start card  -->
                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="card-title">
                                <?php echo $data['title'] ?>
                            </div>
                        </div>
                        <!-- card body -->
                        <div class="card-body">
                            <!-- start table responsive -->
                            <!-- <div class="table-responsive"> -->
                            <table class="table table-striped table-hover table-condensed" id="log_incident" style="width:100%">
                                <thead>
                                    <tr>
                                        <th data-toggle="tooltip" title="No">No.</th>
                                        <th data-toggle="tooltip" title="Incident ID" >Incident ID</th>
                                        <th data-toggle="tooltip" title="Username" >Username</th>
                                        <th data-toggle="tooltip" title="Action">Action Type</th>
                                        <th data-toggle="tooltip" title="Remark">Remark</th>
                                        <th data-toggle="tooltip" title="Time Log">Time Log</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- </div> -->
                            <!-- end table responsive -->

                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>

<!-- test -->