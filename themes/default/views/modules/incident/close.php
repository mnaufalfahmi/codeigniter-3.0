<style>

    .color-tr {
        background: #dedede;
    }

    .color-tr>th {
        color: #000 !important;
    }

    table>tbody>tr {
        cursor: pointer;
    }

    table.dataTable th,
    table>tfoot>tr {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }

    .table > thead > tr >th {
        line-height: 3 !important;
    }

    /* pcessing  */
    .dataTables_wrapper .dataTables_processing {
        position: fixed;
        top: 50%;
        left: 50%;
        width: 200px;
        /* height: 20px; */
        margin-bottom: 20px;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        margin-left: -100px;
        margin-top: 0px;
        text-align: center;
        padding: 1em 0;
        z-index: 1;
    }

    .modal_filter .modal_filter_body {
        height: 180px !important;
    }
    
    .progress-color {
        background: #3F51B5 !important;
    }
</style>
<!-- MULAI KODE DISINI -->
<!-- START JUMBOTRON -->
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <!-- mulai content table-->
        <div class="row">
            <div class="col-md-12">
                <!-- start card  -->
                <div class="card" style="width: 100%;">
                    <div class="card-header">
                        <div class="card-title">
                            <?php echo $data['title'] ?>
                        </div>
                    </div>
                    <!-- card body -->
                    <div class="card-body">
                        <!-- start table responsive -->
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-condensed" id="incident_close" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Incident ID</th>
                                        <th>Service ID</th>
                                        <th>Ismilling ID</th>
                                        <th>Open Time</th>
                                        <th>Region</th>
                                        <th>Close Time</th>
                                        <th>TTR</th>
                                        <th>Description</th>
                                        <th>Summary</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- end table responsive -->

                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->
            </div>
        </div>
        <!-- akhir content table -->
    </div>
</div>
</div>
<!-- END JUMBOTRON -->

<!-- START MODAL DETAIL ROW DATA -->
<div class="modal fade slide-up" id="modalLogDetail" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal" style="width:1200px">
                <div class="modal-header">
                    <ul class="nav nav-tabs nav-tabs-fillup d-none d-md-flex d-lg-flex d-xl-flex" data-init-reponsive-tabs="dropdownfx">
                        <li class="nav-item">
                            <a href="#" class="active show" data-toggle="tab" data-target="#slide1"><span>Incident Detail</span></a>
                        </li>
                        <li class="nav-item">
                            <a href="#" data-toggle="tab" data-target="#slide2" class=""><span>Tiket Attact</span></a>
                        </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
                </div>
                <!-- start form -->
                <div class="modal-body m-t-20">

                    <div class="tab-content">
                        <div class="tab-pane slide-left active" id="slide1">
                            <form class="form_summary">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="">Incident ID :</label>
                                        <input type="text" class="form-control" id="mod_row_incidentId" placeholder="Enter Incident ID" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Creator :</label>
                                        <input type="text" class="form-control" id="mod_row_creator" placeholder="Enter Creator" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Notification Trigger :</label>
                                        <input type="text" class="form-control" id="mod_row_notificationTrigger" placeholder="Notification Trigger" readonly>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="">State :</label>
                                        <input type="text" class="form-control" id="mod_row_state" placeholder="Enter State" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Clasification :</label>
                                        <input type="text" class="form-control" id="mod_row_clasification" placeholder="Enter Clasification" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Root Cause :</label>
                                        <input type="text" class="form-control" id="mod_row_rootCause" placeholder="Root Cause" readonly>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="">Root Cause Detail :</label>
                                        <input type="text" class="form-control" id="mod_row_rootCauseDetail" placeholder="Enter Root Cause Detail" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Hirarchy :</label>
                                        <input type="text" class="form-control" id="mod_row_hirarchy" placeholder="Enter Hirarchy" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Link :</label>
                                        <input type="text" class="form-control" id="mod_row_link" placeholder="Enter Link" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Equipment :</label>
                                        <input type="text" class="form-control" id="mod_equipment" placeholder="Enter Site" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">Description :</label>
                                    <textarea class="form-control" id="mod_row_description" placeholder="Enter Description" rows="5" readonly></textarea>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="">Link Start Region :</label>
                                        <input type="text" class="form-control" id="mod_row_link_start_region" placeholder="Enter Link" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Link End Region:</label>
                                        <input type="text" class="form-control" id="mod_row_link_end_region" placeholder="Enter Link" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Link Start Site:</label>
                                        <input type="text" class="form-control" id="mod_row_link_start_site" placeholder="Enter Link" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Link end Site :</label>
                                        <input type="text" class="form-control" id="mod_row_link_end_site" placeholder="Enter Link" readonly>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="">Region :</label>
                                        <input type="text" class="form-control" id="mod_row_region" placeholder="Enter Region" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Site :</label>
                                        <input type="text" class="form-control" id="mod_row_site" placeholder="Enter Site" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Source Category :</label>
                                        <input type="text" class="form-control" id="mod_source_category" placeholder="Enter Site" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Brand :</label>
                                        <input type="text" class="form-control" id="mod_brand" placeholder="Enter Site" readonly>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="">Creation Time :</label>
                                        <input type="text" class="form-control" id="mod_row_creationTime" placeholder="Enter Creation Time" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Duration :</label>
                                        <input type="text" class="form-control" id="mod_row_duration" placeholder="Enter Duration" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Total Stop Lock :</label>
                                        <input type="text" class="form-control" id="mod_row_stopLock" placeholder="Enter Total Stop Lock" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">TTR :</label>
                                        <input type="text" class="form-control" id="mod_row_ttr" placeholder="Enter Total Stop Lock" readonly>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <!-- slide 2 -->
                        <div class="tab-pane slide-left" id="slide2">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-condensed" id="detail_incident_ticket" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Service ID</th>
                                            <th>Ticket ID</th>
                                            <th>Open Time</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end body -->
                <!-- start footer -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end footer -->

            </div>
            <!-- end of mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL DETAIL ROW DATA -->


<!-- START MODAL CRM INFO -->
<div class="modal fade slide-up" id="modalCrm" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>CRM info <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body m-t-20">
                    <iframe id="frameIsmilling" frameborder="0" width="900" height="500" src=""></iframe>
                </div>
                <!-- end modal body -->
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL TICKET PROGRESS -->
<div class="modal fade slide-up" id="modalTicketProgress" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Ticket Progress <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body m-t-20">
                    <table class="table table-striped table-hover" id="ticket_progress" style="width:100%">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>User</th>
                                <th>Timestamp</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- end modal body -->
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL  -->
<div class="modal fade slide-up modal_filter" id="modalFilter" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Filter</h5>
                </div>
                <div class="modal-body m-t-20 modal_filter_body">
                    <form class="form_filter">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Start date </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="datepicker-component" name="start_date" placeholder="-- Input Start Date --" autocomplete="off" required>
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">End date </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="datepicker-component" name="end_date" placeholder="-- Input End Date --" autocomplete="off" required>
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Service ID </label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="service_id" placeholder="-- Input Service ID--">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Incident ID </label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="ismilling_id" placeholder="-- Input Incident ID --">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <textarea class="form-control" name="description" placeholder="-- Input Description --"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Inventory Change</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <select class="full-width select2" name="inventory_change" data-init-plugin="select2" data-placeholder="-- Select User --">
                                        <option value=""></option>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Region</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <select class="full-width select2 select_region" name="region" data-init-plugin="select2" data-placeholder="-- Select User --">
                                    </select>
                                </div>
                            </div>
                        </div> -->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-complete" type="submit"><i class="fa fa-save"></i>&nbsp; Apply</button>
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp;Close</button>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->



<!-- AKHIR KODE DISNI -->
<script>
    <?php
    $id = $this->input->post('id');
    if (!isset($id)) {
        $id = 0;
    } else {
        $id = $this->input->post('id');
    }
    ?>
    id = <?php echo $id ?>

</script>