<style>
    th.big-col {
        width: 5% !important;
    }

    th.big-col1 {
        width: 30% !important;
    }

    th.big-col2 {
        width: 15% !important;
    }

    .progress-color {
        background: #3F51B5 !important;
    }

    .close-color {
        background: #4CAF50 !important;
    }

    .manage-color {
        background: #673AB7 !important;
    }

    .color-tr>th {
        color: #000 !important;
    }

    table>tbody>tr {
        cursor: pointer;
    }

    .card .card-body {
        padding-top: 10px;
    }

    table#incident_open {
        border-spacing: 1px;
        border-collapse: separate;
    }

    .btn-filter {
        background: rgb(248, 248, 248) !important;
        border-color: rgb(166, 166, 166) !important;
        color: #000 !important;
    }

    .table.table-condensed thead tr th{
        padding-left: 5px !important;
    }

    .dataTables_wrapper .dataTables_processing {
        position: fixed;
        top: 50%;
        left: 50%;
        width: 200px;
        /* height: 20px; */
        margin-bottom: 20px;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        margin-left: -100px;
        margin-top: 0px;
        text-align: center;
        padding: 1em 0;
        z-index: 1;
    }

</style>
<!-- START JUMBOTRON -->
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- mulai content table-->
            <div class="row">
                <div class="col-md-12">
                    <!-- start card  -->
                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="card-title">
                                <?php echo $data['title'] ?>
                            </div>
                        </div>
                        <!-- card body -->
                        <div class="card-body">
                            <!-- start table responsive -->
                            <!-- <div class="table-responsive"> -->
                            <table class="table table-hover table-condensed" id="incident_open" style="width:100%; border-spacing: 2px 1px; ">
                                <thead>
                                    <tr>
                                        <th data-toggle="tooltip" title="No">No.</th>
                                        <th data-toggle="tooltip" title="ID">ID</th>
                                        <th data-toggle="tooltip" title="Incident ID">ID Incident</th>
                                        <th data-toggle="tooltip" title="Open Time">Open Time</th>
                                        <th data-toggle="tooltip" title="Description">Description</th>
                                        <th data-toggle="tooltip" title="Assign By">Assign To</th>
                                        <th data-toggle="tooltip" title="Token">Token</th>
                                        <th data-toggle="tooltip" title="Lock By">Lock By</th>
                                        <th data-toggle="tooltip" title="Priority">Priority</th>
                                        <th data-toggle="tooltip" title="Region">Region</th>
                                        <th data-toggle="tooltip" title="State">State</th>
                                        <th data-toggle="tooltip" title="Total Ticket">Ticket</th>
                                        <th data-toggle="tooltip" title="Analisis Time">Analysis Time</th>
                                        <th data-toggle="tooltip" title="Recovery Time">Recovery Time</th>
                                        <th data-toggle="tooltip" title="Last Comment">Last Comment</th>
                                        <th data-toggle="tooltip" title="Action" class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <!-- <tfoot>
                                    <tr>
                                        <td colspan="13"></td>
                                    </tr>
                                </tfoot> -->
                            </table>
                            <!-- </div> -->
                            <!-- end table responsive -->

                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->


<!-- START MODAL ASSIGN -->
<div class="modal fade slide-up" id="modalAssign" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Assign Incident<span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form class="form_assign">
                    <input type="hidden" name="id">
                    <input type="hidden" name="creator">
                    <div class="modal-body m-t-20">
                        <!-- start form -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Incident ID :</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="incident_id" placeholder="Incident ID" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description :</label>
                            <div class="col-sm-10">
                                <textarea name="description" class="form-control" placeholder="Description" rows="10" readonly></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Assign To :</label>
                            <div class="col-sm-10">
                                <select class="full-width select2" id="select_user" name="creator" data-init-plugin="select2" data-placeholder="-- Select User --">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-complete"><i class="fa fa-save"></i> Save</button>
                        <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL ASSIGN -->

<!-- START MODAL DETAIL ROW DATA -->
<div class="modal fade slide-up" id="modalLogDetail" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal" style="width:1200px">
                <div class="modal-header">
                    <ul class="nav nav-tabs nav-tabs-fillup d-none d-md-flex d-lg-flex d-xl-flex" data-init-reponsive-tabs="dropdownfx">
                        <li class="nav-item">
                            <a href="#" class="active show" data-toggle="tab" data-target="#slide1"><span>Incident Detail</span></a>
                        </li>
                        <li class="nav-item">
                            <a href="#" data-toggle="tab" data-target="#slide2" class=""><span>Ticket Attach</span></a>
                        </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i></button>
                </div>
                <!-- start form -->
                <div class="modal-body m-t-20">

                    <div class="tab-content">
                        <div class="tab-pane slide-left active" id="slide1">
                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="">Incident ID :</label>
                                        <input type="text" class="form-control" id="mod_row_incidentId" placeholder="Enter Incident ID" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Creator :</label>
                                        <input type="text" class="form-control" id="mod_row_creator" placeholder="Enter Creator" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Notification Trigger :</label>
                                        <input type="text" class="form-control" id="mod_row_notificationTrigger" placeholder="Notification Trigger" readonly>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="">State :</label>
                                        <input type="text" class="form-control" id="mod_row_state" placeholder="Enter State" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Clasification :</label>
                                        <input type="text" class="form-control" id="mod_row_clasification" placeholder="Enter Clasification" readonly>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Root Cause :</label>
                                        <input type="text" class="form-control" id="mod_row_rootCause" placeholder="Root Cause" readonly>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="">Root Cause Detail :</label>
                                        <input type="text" class="form-control" id="mod_row_rootCauseDetail" placeholder="Enter Root Cause Detail" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Hirarchy :</label>
                                        <input type="text" class="form-control" id="mod_row_hirarchy" placeholder="Enter Hirarchy" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Link :</label>
                                        <input type="text" class="form-control" id="mod_row_link" placeholder="Enter Link" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Equipment :</label>
                                        <input type="text" class="form-control" id="mod_equipment" placeholder="Enter Site" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">Description :</label>
                                    <textarea class="form-control" id="mod_row_description" placeholder="Enter Description" rows="5" readonly></textarea>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="">Link Start Region :</label>
                                        <input type="text" class="form-control" id="mod_row_link_start_region" placeholder="Enter Link" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Link End Region:</label>
                                        <input type="text" class="form-control" id="mod_row_link_end_region" placeholder="Enter Link" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Link Start Site:</label>
                                        <input type="text" class="form-control" id="mod_row_link_start_site" placeholder="Enter Link" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Link end Site :</label>
                                        <input type="text" class="form-control" id="mod_row_link_end_site" placeholder="Enter Link" readonly>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="">Region :</label>
                                        <input type="text" class="form-control" id="mod_row_region" placeholder="Enter Region" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Site :</label>
                                        <input type="text" class="form-control" id="mod_row_site" placeholder="Enter Site" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Source Category :</label>
                                        <input type="text" class="form-control" id="mod_source_category" placeholder="Enter Site" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Brand :</label>
                                        <input type="text" class="form-control" id="mod_brand" placeholder="Enter Site" readonly>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="">Creation Time :</label>
                                        <input type="text" class="form-control" id="mod_row_creationTime" placeholder="Enter Creation Time" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Duration :</label>
                                        <input type="text" class="form-control" id="mod_row_duration" placeholder="Enter Duration" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Total Stop Lock :</label>
                                        <input type="text" class="form-control" id="mod_row_stopLock" placeholder="Enter Total Stop Lock" readonly>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">TTR :</label>
                                        <input type="text" class="form-control" id="mod_row_ttr" placeholder="Enter Total Stop Lock" readonly>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <!-- slide 2 -->
                        <div class="tab-pane slide-left" id="slide2">
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed" id="detail_incident_ticket" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="width:1px">No.</th>
                                            <th style="width:20px">Service ID</th>
                                            <th style="width:20px">Ticket ID</th>
                                            <th style="width:20px">Open Time</th>
                                            <th style="width:20px">Bobot</th>
                                            <th style="width:100px">Description</th>
                                            <th style="width:10px">Action</th>
                                        </tr>
                                    </thead>
                                    <!-- <tfoot>
                                        <tr>
                                            <td colspan="6"></td>
                                        </tr>
                                    </tfoot> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end body -->
                <!-- start footer -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-complete btn-edit" type="button" data-toggle="modal" data-target="#modalEditIncident"><i class="fa fa-pencil"></i>&nbsp; Edit incident</button>
                    <button class="btn btn-sm btn-complete btn-merge" type="button" data-toggle="modal" data-target="#modalMergeIncident"><i class="fa fa-save"></i>&nbsp; Merge Incident</button>
                    <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end footer -->

            </div>
            <!-- end of mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL DETAIL ROW DATA -->


<!-- MODAL EDIT Incident -->
<div class="modal fade slide-up" id="modalEditIncident" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal" style="width:1200px">
                <div class="modal-header">
                    <h5>Edit <span class="semi-bold">Incident</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form class="form_edit" action="<?php echo base_url('incident/update_incident') ?>" method="post">
                    <input type="hidden" name="id">
                    <div class="modal-body m-t-20">
                        <!-- start form -->
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Creator :</label>
                            <div class="col-md-10">
                                <input type="text" name="creator" class="form-control" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Timestamp Open :</label>
                            <div class="col-md-10">
                                <!-- 16/07/2019 12:51:00 -->
                                <input type="text" name="timestamp" class="form-control" value="" id="timestamp-open" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Clasification :</label>
                            <div class="col-md-10">
                                <select class="full-width select2" id="select_classification" name="select_classification" data-placeholder="Select">
                                    <option value="0"></option>
                                    <?php foreach ($data['classification'] as $r_data) { ?>
                                        <option value="<?php echo $r_data->id ?>"><?php echo $r_data->classification_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="">Root Cause :</label>
                            <div class="col-md-10">
                                <select class="full-width select2" data-placeholder="Select" id="select_root_cause" name="select_root_cause">
                                    <option value="0" selected></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Root Cause Detail</label>
                            <div class="col-md-10">
                                <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="select_root_cause_detail" name="select_root_cause_detail">
                                    <option value="0" selected></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Network</label>
                            <div class="radio radio-success col-md-10">
                                <input type="radio" value="ip" name="network" id="ip" checked>
                                <label for="ip">IP</label>
                                <input type="radio" value="tdm" name="network" id="tdm">
                                <label for="tdm">TDM</label>
                                <input type="radio" value="vsat" name="network" id="vsat">
                                <label for="vsat">VSAT</label>
                                <input type="radio" value="infra" name="network" id="infra">
                                <label for="infra">INFRA</label>
                                <input type="radio" value="ms" name="network" id="ms">
                                <label for="ms">MS</label>
                                <input type="radio" value="mm" name="network" id="mm">
                                <label for="mm">MM</label>
                                <input type="radio" value="oa" name="network" id="oa">
                                <label for="oa">OA</label>
                                <input type="radio" value="mspc" name="network" id="mspc">
                                <label for="mspc">MSPC</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Priority</label>
                            <div class="radio radio-success col-md-10">
                                <input type="radio" value="low" name="priority" id="low" checked>
                                <label for="low">Low</label>
                                <input type="radio" value="medium" name="priority" id="medium">
                                <label for="medium">Medium</label>
                                <input type="radio" value="high" name="priority" id="high">
                                <label for="high">High</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Description</label>
                            <div class="col-md-10">
                                <textarea class="form-control" rows="5" name="description"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Link</label>
                            <div class="col-md-10">
                                <select class="full-width select2" data-placeholder="Select" id="link" name="link">
                                    <option value="0" selected></option>
                                    <option value="backbone">Backbone</option>
                                    <option value="distribution">Distribution</option>
                                    <option value="access">Access</option>
                                    <option value="retail">Retail</option>
                                </select>
                            </div>
                        </div>
                        <div style="display: none;" id="foc-show">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_region" name="link_start_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_site" name="link_start_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_region" name="link_end_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_site" name="link_end_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div style="display: none;" id="div_region">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Region</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="region1" name="region1">
                                        <option value="0" selected></option>
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="div_site_name">
                            <label for="fname" class="col-md-2 col-form-label">Site Name</label>
                            <div class="col-md-10">
                                <select class="full-width select2" data-placeholder="Select" id="site_name1" name="site_name1">
                                    <option value="0" selected></option>
                                </select>
                            </div>
                        </div>
                        <div style="display: none;" id="foc-show">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_region" name="link_start_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_site" name="link_start_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_region" name="link_end_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_site" name="link_end_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="11">
                            <label for="fname" class="col-md-2 col-form-label">Source Category :</label>
                            <div class="col-md-10">
                                <!-- <label class="m-t-10" id="source_category"></label>
                                <input type="hidden" name="source_category_input" id="source_category_input"> -->
                                <!-- <div style="display: none;" id="div_link_upstream_isp"> -->
                                <div id="div_link_upstream_isp">
                                    <!-- <select class="full-width select2" data-placeholder="Select" id="link_upstream_isp" name="link_upstream_isp" required> -->
                                    <select class="full-width select2" data-placeholder="Select" id="link_upstream_isp" name="source_category_input" required>
                                        <option value="FOT">FOT</option>
                                        <option value="FOC">FOC</option>
                                        <option value="PS">PS</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="1" style="display: none;">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Brand :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="brand1" name="brand1">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">ME :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="me1" name="me1">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="3" style="display: none;">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Broken FO :</label>
                                <div class="col-md-10">
                                    <input type="number" class="form-control" id="fname" placeholder="" name="broken_fo" value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-complete" type="submit"><i class="fa fa-save"></i> Save</button>
                        <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL EDIT Incident -->

<!-- MODAL Merge Incident -->
<div class="modal fade slide-up" id="modalMergeIncident" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal" style="width:1200px">
                <div class="modal-header">
                    <h5>Select <span class="semi-bold">Incident</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start modal body -->
                <div class="modal-body m-t-20">
                    <div class="incident_id" style="display:none"></div>
                    <div class="detail_incident">
                        <form action="form_merge">
                            <div class="form-group" style="background:#eee; padding:10px">
                                <label for="" data-toggle="collapse" data-target="#collapseOne" style="cursor:pointer">Incident Detail</label>
                                <span class="pull-right" data-toggle="collapse" data-target="#collapseOne" style="cursor:pointer"><i class="fa fa-chevron-circle-down"></i></span>
                                <div class="collapse" id="collapseOne">
                                    <textarea class="form-control incident_detail mb-2" rows="5" name="incident_detail" readonly></textarea>
                                </div>
                                <select class="full-width select2 select-merge-reason" name="merge_reason" data-placeholder="-- Select Merge Reason --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed" id="merge_incident" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:5px">No.</th>
                                    <th style="width:20px">Open Time</th>
                                    <th style="width:20px">Creator</th>
                                    <th style="width:200px">Description</th>
                                    <!-- <th style="width:5px">Total Ticket</th> -->
                                    <th style="width:5px">Incident ID</th>
                                </tr>
                            </thead>
                            <!-- <tfoot>
                                <tr>
                                    <td colspan="6"></td>
                                </tr>
                            </tfoot> -->
                        </table>
                    </div>
                </div>
                <!-- end modal body -->
                <!-- modal footer -->
                <div class="modal-footer">
                    <button class="btn btn-sm btn-complete btn_merge"><i class="fa fa-save"></i> Save</button>
                    <button class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- end of modla dialog -->
</div>
<!-- END MODAL EDIT Incident -->

<!-- MODAL CONFIRMATION  -->
<div class="modal fade slide-up" id="modalUnlock" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are you sure want to unlock this incident ?</span>
                    </div>
                    <div class="card-body">
                        <form class="form_confirm">
                            <input type="hidden" name="id">
                            <input type="hidden" name="lock_by">
                            <input type="hidden" name="option">
                            <button type="submit" class="btn btn-danger">Yes</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- end modal -->


<!-- MODAL CONFIRMATION  -->
<div class="modal fade slide-up" id="modalUnlockFail" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span class="text-confirm">Your Account Cannot Unlock This Incident</span>
                    </div>
                    <div class="card-body">
                        <form class="#">
                            <input type="hidden" name="id">
                            <input type="hidden" name="lock_by">
                            <button type="button" class="btn btn-info" data-dismiss="modal">Ok</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- end modal -->

<!-- START MODAL CLOSE INCIDENT -->
<div class="modal fade slide-up" id="modalCloseIncident" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Close Incident <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form class="form_close" action="" method="post">
                    <input type="hidden" name="id">
                    <div class="modal-body m-t-20">
                        <!-- <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Last Stop Clock :</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="last_stop_clock" readonly>
                            </div>
                        </div> -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Timestamp :</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="timestamp" id="datepicker-timstamp1" value="<?php echo date("Y-m-d") ?>" placeholder="-- Date --" readonly>
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="time" value="" placeholder="-- Time --" readonly>
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Reason :</label>
                            <div class="col-sm-10">
                                <textarea name="reason" class="form-control" rows="5" placeholder="-- Reason --"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Additional Reference Data :</label>
                            <div class="col-sm-10">
                                <textarea name="add_reference" class="form-control" rows="5" placeholder="-- Write Site , Brand and equipment reference if you left it blank in below form --"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Configurasi Change :</label>
                            <div class="col-sm-10">
                                <select class="full-width select2 select_configuration_change" name="conf_change" data-init-plugin="select2" data-placeholder="-- Selected Configuration Change --">
                                    <option value=""></option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row configurasi_change_activity" style="display:none">
                            <label class="col-sm-2 col-form-label">Configurasi Change Activity :</label>
                            <div class="col-sm-10">
                                <select class="full-width select2" name="conf_change_acv" data-init-plugin="select2" data-placeholder="-- Selected Configuration Change Activity --">
                                    <option value=""></option>
                                    <option value="update">Update</option>
                                    <option value="downgrade">Downgrade</option>
                                    <option value="relocation_port">Relocation Port</option>
                                    <option value="change_device">Change Device</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Physical Inventori Change :</label>
                            <div class="col-sm-10">
                                <div class="checkbox check-success form-check-inline">
                                    <input type="checkbox" name="physical_inv_change1" value="1" id="checkbox1">
                                    <label for="checkbox1">Inside Plant</label>
                                </div>
                                <div class="checkbox check-success form-check-inline">
                                    <input type="checkbox" name="physical_inv_change2" value="2" id="checkbox2">
                                    <label for="checkbox2">OutSite Plant</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" style="display:none">
                            <label class="col-sm-2 col-form-label">Physical Inventori Change value :</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="physical_inv_change">
                            </div>
                        </div>
                        <div class="form-group row inv_change_desc" style="display:none">
                            <label class="col-sm-2 col-form-label">Inventori Change Description :</label>
                            <div class="col-sm-10">
                                <textarea name="inv_change_desc" class="form-control" rows="5" placeholder="-- Write inventory change description --"></textarea>
                            </div>
                        </div>
                        <!-- incident detail  -->
                        <!-- Detail Incident -->
                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label pl-2" style="background:#eee">Incident Detail</label>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Creator :</label>
                            <div class="col-md-10">
                                <input type="text" name="creator" class="form-control" value="" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Timestamp Open :</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="timestamp" id="datepicker-timstamp2" value="<?php echo date("Y-m-d") ?>" placeholder="-- Date --" readonly>
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="" value="<?php date_default_timezone_set('Asia/Jakarta');
                                                                                                                echo date("H:i:s") ?>" placeholder="-- Time --" readonly>
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Clasification :</label>
                            <div class="col-md-10">
                                <select class="full-width select2" id="select_classification_close" name="select_classification" data-placeholder="Select" required>
                                    <option value="0"></option>
                                    <?php foreach ($data['classification'] as $r_data) { ?>
                                        <option value="<?php echo $r_data->id ?>"><?php echo $r_data->classification_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label" for="">Root Cause :</label>
                            <div class="col-md-10">
                                <select class="full-width select2" id="select_root_cause_close" data-placeholder="Select" name="select_root_cause" required>
                                    <option value="0" selected></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Root Cause Detail</label>
                            <div class="col-md-10">
                                <select class="full-width select2" id="select_root_cause_detail_close" data-placeholder="Select" name="select_root_cause_detail" required>
                                    <option value="0" selected></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Network</label>
                            <div class="col-md-10">
                                <div class="form-check form-check-inline">
                                    <input required class="form-check-input ip" type="radio" name="network" id="ip_close" value="ip">
                                    <label class="form-check-label" for="ip_close">IP</label>
                                    <input required class="form-check-input tdm" type="radio" name="network" id="tdm_close" value="tdm">
                                    <label class="form-check-label" for="tdm_close">TDM</label>
                                    <input required class="form-check-input vsat" type="radio" name="network" id="vsat_close" value="vsat">
                                    <label class="form-check-label" for="vsat_close">VSAT</label>
                                    <input required class="form-check-input infra" type="radio" name="network" id="infra_close" value="infra">
                                    <label class="form-check-label" for="infra_close">INFRA</label>
                                    <input required class="form-check-input ms" type="radio" name="network" id="ms_close" value="ms">
                                    <label class="form-check-label" for="ms_close">MS</label>
                                    <input required class="form-check-input mm" type="radio" name="network" id="mm_close" value="mm">
                                    <label class="form-check-label" for="mm_close">MM</label>
                                    <input required class="form-check-input oa" type="radio" name="network" id="oa_close" value="oa">
                                    <label class="form-check-label" for="oa_close">OA</label>
                                    <input required class="form-check-input mspc" type="radio" name="network" id="mspc_close" value="mspc">
                                    <label class="form-check-label" for="mspc_close">MSPC</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Priority</label>
                            <div class="col-md-10">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input low" type="radio" name="priority" id="low_close" value="low">
                                    <label class="form-check-label" for="low_close">Low</label>
                                    <input class="form-check-input medium" type="radio" name="priority" id="medium_close" value="medium">
                                    <label class="form-check-label" for="medium_close">Medium</label>
                                    <input class="form-check-input high" type="radio" name="priority" id="high_close" value="high">
                                    <label class="form-check-label" for="high_close">High</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Description</label>
                            <div class="col-md-10">
                                <textarea class="form-control" rows="5" name="description"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Link</label>
                            <div class="col-md-10">
                                <select class="full-width select2" data-placeholder="Select" id="link_close" name="link" required>
                                    <option value="0" selected></option>
                                    <option value="backbone">Backbone</option>
                                    <option value="distribution">Distribution</option>
                                    <option value="access">Access</option>
                                    <option value="retail">Retail</option>
                                </select>
                            </div>
                        </div>
                        <div style="display: none;" id="foc-show_close">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_region_close" name="link_start_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_site_close" name="link_start_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_region_close" name="link_end_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_site_close" name="link_end_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- <div style="display: none;" id="div_region_close"> -->
                        <div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Region</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="region1_close" name="region1" requied>
                                    <!-- <select class="full-width select2" data-placeholder="Select" name="region1" requied> -->
                                        <option value="0" selected></option>
                                        <?php
                                        foreach ($data['region'] as $row) : 
                                            if($row->status == 0) { ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php } endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group row" id="div_site_name_close"> -->
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Site Name</label>
                            <div class="col-md-10">
                                <!-- <select class="full-width select2 select2" id="site_name1_close" name="site_name1" required> -->
                                <select class="full-width select2 select2" id="site_name1_close" name="site_name1" required>
                                    <option value="0" selected></option>
                                </select>
                            </div>
                        </div>
                        <div style="display: none;" id="foc-show_close">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_region_close" name="link_start_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link Start Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_start_site_close" name="link_start_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Region :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_region_close" name="link_end_region">
                                        <?php
                                        foreach ($data['region'] as $row) : ?>
                                            <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Link End Site :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="link_end_site_close" name="link_end_site">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="11_close">
                            <label for="fname" class="col-md-2 col-form-label">Source Category </label>
                            <div class="col-md-10">
                                <!-- <label class="m-t-10" id="source_category"></label>
                                <input type="hidden" name="source_category_input" id="source_category_input"> -->
                                <div id="div_link_upstream_isp_close">
                                    <!-- <select class="full-width select2" data-placeholder="Select" id="link_upstream_isp_close" name="link_upstream_isp" required> -->
                                    <select class="full-width select2" data-placeholder="Select" id="link_upstream_isp_close" name="source_category_input" required>
                                        <option value="FOT">FOT</option>
                                        <option value="FOC">FOC</option>
                                        <option value="PS">PS</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="1_close" style="display: none;">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Brand :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="brand1_close" name="brand1">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">ME :</label>
                                <div class="col-md-10">
                                    <select class="full-width select2" data-placeholder="Select" id="me1_close" name="me1">
                                        <option value="0" selected></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="3_close" style="display: none;">
                            <div class="form-group row">
                                <label for="fname" class="col-md-2 col-form-label">Broken FO :</label>
                                <div class="col-md-10">
                                    <input type="number" class="form-control" id="fname_close" placeholder="" name="broken_fo" value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-complete"><i class="fa fa-save"></i>&nbsp; Save</button>
                        <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL CRM INFO -->
<div class="modal fade slide-up" id="modalCrm" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>CRM info <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body m-t-20">
                    <iframe id="frameIsmilling" frameborder="0" width="900" height="500" src=""></iframe>
                </div>
                <!-- end modal body -->
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL TICKET PROGRESS -->
<div class="modal fade slide-up" id="modalTicketProgress" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Ticket Progress <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body m-t-20">
                    <table class="table table-striped table-hover" id="ticket_progress">
                        <thead>
                            <tr>
                                <th style="width:5px">No.</th>
                                <th style="width:20px">User</th>
                                <th style="width:20px">Timestamp</th>
                                <th style="width:100px">Description</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- end modal body -->
                <!-- modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                </div>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<div class="modal fade slide-up" id="modalFilterIncident" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Filter Incident <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <form action="#" class="form_submit">
                    <div class="modal-body m-t-20">
                        <table class="table table-striped table-hover table-condensed" id="incident_filter" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                                    <th>Incident</th>
                                    <th>Start Time</th>
                                    <th>Description</th>
                                    <th>Token</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-success"> Apply</button>
                    </div>
                </form>
                <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<div class="info_login" data-usergroup="<?php echo $_COOKIE['group_id'] ?>" data-username="<?php echo $_COOKIE['username'] ?>" style="display:none"></div>