<style>
    table.dataTable th,
    table>tfoot>tr {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }

    .table > thead > tr >th {
        line-height: 3 !important;
    }
</style>

<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- mulai content table-->
            <div class="row">
                <div class="col-md-12">
                    <!-- start card  -->
                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="card-title">
                                <?php echo $data['title'] ?>
                            </div>
                        </div>
                        <!-- card body -->
                        <div class="card-body">
                            <!-- start table responsive -->
                            <!-- <div class="table-responsive"> -->
                            <table class="table table-hover table-condensed" id="merge_incident" style="width:100%">
                                <thead>
                                    <tr>
                                        <th data-toggle="tooltip" title="No" style="width:25px">No.</th>
                                        <th data-toggle="tooltip" title="Incident ID" style="width:50px">Incident ID</th>
                                        <th data-toggle="tooltip" title="Ticket ID" style="width:50px">Ticket ID</th>
                                        <th data-toggle="tooltip" title="Open Time" style="width:70px">Open Time</th>
                                        <th data-toggle="tooltip" title="Region" style="width:30px">Region</th>
                                        <th data-toggle="tooltip" title="State" style="width:30px">State</th>
                                        <th data-toggle="tooltip" title="Description" style="width:400px">Description</th>
                                        <th data-toggle="tooltip" title="Reference" >Reference</th>
                                        <th data-toggle="tooltip" title="Un Merge" style="width:50px">Un Merge</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- </div> -->
                            <!-- end table responsive -->

                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>

<!-- MODAL CONFIRMATION  -->
<div class="modal fade slide-up" id="modalUnlock" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are you sure want to Unmerge this incident ?</span>
                    </div>
                    <div class="card-body">
                        <form class="form_confirm">
                            <input type="hidden" name="id">
                            <input type="hidden" name="lock_by">
                            <button type="submit" class="btn btn-danger">Yes</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">No</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- end modal -->