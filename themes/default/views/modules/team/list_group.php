<!--     <link href="<?php echo base_url('themes/default/assets/plugins/bootstrap-suggest') ?>/dist/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url('themes/default/assets/plugins/bootstrap-suggest') ?>/dist/responsive.dataTables.min.css" rel="stylesheet">
    

    <link href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css" rel="stylesheet"> 
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>  -->

<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
      <?php echo $this->session->flashdata('msg');?>
    <div class="inner">
        <div class="row">
           
        </div>

        <div class="card ">
              <div class="card-header ">
                <div class="card-title">
                  <div class="card-title" style="padding-top: 10px">Data Group</div>

                </div>
                <div class="pull-right">
                  <!-- <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                  </div> -->
                </div>
                <div class="clearfix"></div>
              </div>
              <!-- <div class="card-body">
                <table class="table table-hover" id="tableWithSearch">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th>Group</th>
                      <th>Fullname</th>
                      <th>Checkin</th>
                      <th>SBU</th>
                      <th>Created Shift</th>
                    </tr>
                  </thead>
                  <tbody>
                        <?php $no=1;  ?>
                        <?php foreach ($data['list_group'] as $value) :?>
                            <tr>
                              <td><center><?php echo $no++;?></center></td>
                              <td><?php echo $value->group;?></td>
                              <td><?php echo $value->fullname;?></td>
                              <td><?php echo $value->checkin;?></td>
                              <td><?php echo $value->sbu;?></td>
                              <td><?php echo $value->created_shift;?></td>
                            </tr> 
                        <?php endforeach; ?>
                  </tbody>
                </table>
              </div> -->

          <div class=" container-fluid   container-fixed-lg">
            <div class="row">
              <div class="col-lg-12">
                <div class="sm-m-l-5 sm-m-r-5">
                  <div class="card-group horizontal" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php $i=1;  ?>
                    <?php foreach ($data['parent_group'] as $key) :?>
                      <div class="card card-default m-b-0">
                        <div class="card-header " role="tab" id="headingOne">
                          <h4 class="card-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne">
                               <?php echo $key->group;?>
                              </a>
                            </h4>
                        </div>
                        <div id="<?php echo $i; ?>" class="collapse show" role="tabcard" aria-labelledby="headingOne">
                          <div class="card-header ">
                            <div class="pull-right">
                              <div class="col-xs-12">
                                <input type="text" id="search-table<?php echo $i; ?>" class="form-control pull-right" placeholder="Search">
                              </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                          <div class="card-body">
                            <table class="table table-hover" id="tableWithSearch<?php echo $i; ?>">
                              <thead>
                                <tr>
                                  <th><center>No</center></th>
                                  <th>Group</th>
                                  <th>Fullname</th>
                                  <th>Checkin</th>
                                  <th>SBU</th>
                                  <th>Created Shift</th>
                                </tr>
                              </thead>
                              <tbody>
                                    <?php $no=1;  ?>
                                    <?php foreach ($data['list_group'] as $value) :?>
                                      <?php if ($value->group==$key->group) :?>    
                                        <tr>
                                          <td><center><?php echo $no++;?></center></td>
                                          <td><?php echo $value->group;?></td>
                                          <td><?php echo $value->fullname;?></td>
                                          <?php if ($value->checkin==1) :?>             
                                            <td><a href="<?php echo base_url();?>team/maps/<?php echo $value->id; ?>"><button class="btn btn-complete btn-xs btn-xs ml-1">Check in</button></td>
                                          <?php elseif($value->checkin==null):  ?>
                                            <td><span class="label label-warning">Unknow</span>
                                          <?php elseif($value->checkin==0):  ?>
                                            <td><span class="label label-danger">Check out</span></td>
                                          <?php endif; ?>
                                          <td><?php echo $value->sbu;?></td>
                                          <td><?php echo $value->created_shift;?></td>
                                        </tr> 
                                      <?php endif; ?>
                                    <?php endforeach; ?>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
              <!--END CARD-->
            </div>


    </div>
</div>

<!-- <script type="text/javascript">
  $(document).ready(function() {
        table = $('#example1').DataTable( {

            // dataTable
            processing: true,
            //serverSide: true,
            //iDisplayLength: 25,
            //aLengthMenu: [[10,25,50],[10,25,50]],
            //filter jumlah data diatas
            lengthChange: false,
            //show entries dibawah---
            //bLengthChange : false,
            //sDom : '<"top">rt<"bottom"flp><"clear">',
            //show entries dibawah---
            pagingType: "simple",
            deferRender:    true,            
            //scrollY:        "300px",
            scrollX:        false,
            scrollCollapse: true,
            /*columnDefs: [
                { width: '20%', targets: 0 },
            ],*/
            //fnFormatNumber: false,
            fixedColumns: true,
            paging:   true
            // initComplete: function () {
            //     this.api().row( 1000 ).scrollTo();
            // }
        } );

    } );
</script>

<script src="<?php echo base_url('themes/default/assets/plugins/bootstrap-suggest') ?>/dist/datatables.min.js"></script>
<script src="<?php echo base_url('themes/default/assets/plugins/bootstrap-suggest') ?>/dist/dataTables.bootstrap4.min.js"></script> -->
