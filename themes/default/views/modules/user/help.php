<div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">Manual FMS
                </div>
              </div>
              <div class="card-body sortable">
                <div class="row">
                  <div class="col-lg-6 sortable-column ui-sortable">
                    <div class="card card-default bg-danger" data-pages="card" style="position: relative; opacity: 1; left: 0px; top: 0px;">
                      <div class="card-header ui-sortable-handle">
                        <!-- <div class="card-title">Draggable Portlet
                        </div> -->
                        <div class="card-controls">
                          <ul>
                            <li><a href="#" class="card-collapse" data-toggle="collapse"><i class="card-icon card-icon-collapse"></i></a>
                            </li>
                            <li><a href="#" class="card-refresh" data-toggle="refresh"><i class="card-icon card-icon-refresh"></i></a>
                            </li>
                            <li><a href="#" class="card-close" data-toggle="close"><i class="card-icon card-icon-close"></i></a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="card-body">
                        <h3 class="text-white">
								<span class="semi-bold">Document</span> Manual</h3>
                        <p class="text-white">When it comes to digital design, the lines between functionality, aesthetics, and psychology are inseparably blurred. Without the constraints of the physical world, there’s no natural form to fall back on, and every bit of constraint and affordance must be introduced intentionally. Good design makes a product useful. A product is bought to be used. It has to satisfy certain criteria, not only functional, but also psychological and aesthetic. </p>
                      </div>
                    </div>
                    
                  </div>
                  <div class="col-lg-6 sortable-column ui-sortable">
                    <div class="card card-default bg-primary" data-pages="card">
                      <div class="card-header ui-sortable-handle">
                        <!-- <div class="card-title">Draggable Portlet
                        </div> -->
                        <div class="card-controls">
                          <ul>
                            <li><a href="#" class="card-collapse text-white" data-toggle="collapse"><i class="card-icon card-icon-collapse"></i></a>
                            </li>
                            <li><a href="#" class="card-refresh text-white" data-toggle="refresh"><i class="card-icon card-icon-refresh"></i></a>
                            </li>
                            <li><a href="#" class="card-close text-white" data-toggle="close"><i class="card-icon card-icon-close"></i></a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="card-body">
                        <h3 class="text-white">
								<span class="semi-bold">Document</span> Implementasi</h3>
                        <p class="text-white">When it comes to digital design, the lines between functionality, aesthetics, and psychology are inseparably blurred. Without the constraints of the physical world, there’s no natural form to fall back on, and every bit of constraint and affordance must be introduced intentionally. Good design makes a product useful. A product is bought to be used. It has to satisfy certain criteria, not only functional, but also psychological and aesthetic. </p>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>