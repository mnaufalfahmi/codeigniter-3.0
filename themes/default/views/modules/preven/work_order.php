<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0 mt-3">
      <?php echo $this->session->flashdata('msg');?>
    <div class="inner">
        <div class="row">
           
        </div>

        <div class="card ">
              <div class="card-header ">
                <div class="card-title">
                  <?php if($$_COOKIE['role_id'] != 72){ ?>
                  <?php if($this->uri->segment(2) == "wo_close"){?>
                  Closed Work Order
                  <?php }else{ ?>
                  <button class="btn btn-primary btn-cons m-b-10" data-target="#modalAddRole" data-toggle="modal" type="submit"><span class="bold"><i class="fa fa-plus"></i> Add Work Order</span><i class="fs-14 sl-user-follow"></i></button>
                  <?php }} ?>
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-body">
                <table class="table table-hover" id="tableWithSearch">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th width='70px'>Tanggal</th>
                      <th>Title</th>
                      <th class='text-center' >Type</th>
                      <th>POP</th>
                      <th>Serpo</th>
                      <th class='text-center'  width="80px">Status</th>
                      <th class='text-center' width='110px'>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                        <?php $no=1;  ?>
                        <?php foreach ($data['list_wo'] as $value) :?>
                            <tr>
                              <td><center><?php echo $no++;?></center></td>
                              <td><?php echo date('Y-m-d', strtotime($value->date));?></td>
                              <td><?php echo $value->title;?></td>
                              <td class='text-left' ><?php echo $value->type;?></td>
                              <td><?php echo $value->pop;?></td>
                              <td><?php echo $value->name;?></td>
                              <?php if ($value->status=='On Progress') :?>             
                                <td class='text-center'><span class="label label-warning"><?php echo strtoupper($value->status);?></span></td>
                              <?php elseif($value->status=='New'):  ?>
                                <td class='text-center'><span class="label label-inverse"><?php echo strtoupper($value->status);?></span></td>
                              <?php elseif($value->status=='Cancel'):  ?>
                                <td class='text-center'><span class="label label-danger"><?php echo strtoupper($value->status);?></span></td>
                              <?php elseif($value->status=='Close'):  ?>
                                <td class='text-center'><span class="label label-success"><?php echo strtoupper($value->status);?></span></td>
                              <?php endif; ?>
                             <td class='text-center' >
                                <a href="<?php echo site_url('preven/detail_wo/'.$value->id);?>" title="View Task List"><button class="btn btn-complete btn-xs btn-xs ml-1">Detail</button>
                                <a href="<?php echo site_url('preven/download/'.$value->id);?>" title="Download WO"><button class="btn btn-complete btn-xs btn-xs ml-1"><i class="fa fa-download"></i></button>
                              <?php if($this->uri->segment(2) != "wo_close"){?><a href="<?php echo site_url('preven/delete_wo/'.$value->id);?>" title="Delete WO" onclick="return confirm('Are you sure you want to delete?')" title="Delete WO"><button class="btn btn-danger btn-xs ml-1 actionDefine"><i class="fa fa-trash"></i></button><?php } ?>
                              </td>
                            </tr> 
                        <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>


    </div>
</div>

<div class="modal fade slide-up" id="modalAddRole" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add <span class="semi-bold">Work Order</span></h5>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?php echo site_url('preven/input_wo'); ?>" class="form-horizontal form_save" role="form" >
                        <div class="row">
                            <div class="card card-transparent">
                                <!-- <div class="card-header ">
                                    <div class="card-title">User Details</div>
                                </div> -->
                                <div class="card-body ">
                                    
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" name="title" class="form-control" placeholder="Title" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="date" class="form-control" name="date" placeholder="Select Date" autocomplete="off" required>
                                    </div>

                                    <div class="form-group">
                                        <label>POP</label>
                                        <select class="full-width select2" name="pop" data-init-plugin="select2" required>
                                            <option value="0">-</option>
                                            <?php foreach ($data['pop'] as $pop) { ?>
                                                    <option value="<?= $pop->name; ?>"><?= $pop->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Serpo</label>
                                        <select class="full-width select2" name="serpo" data-init-plugin="select2" required>
                                            <option value="0">-</option>
                                            <?php foreach ($data['sepro'] as $sepro) { ?>
                                                    <option value="<?= $sepro->id_serpo; ?>"><?= $sepro->fullname; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="full-width select2" name="type" data-init-plugin="select2" required>
                                            <option value="ISP">ISP</option>
                                            <option value="OSP">OSP</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="full-width select2" name="status" data-init-plugin="select2" required>
                                            <option value="New">NEW</option>
                                            <option value="On Progress">ON PROGRESS</option>
                                            <option value="Close">CLOSE</option>
                                            <option value="Cancel">CANCEL</option>
                                        </select>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
