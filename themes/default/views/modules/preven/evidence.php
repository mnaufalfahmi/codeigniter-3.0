<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0 mt-3">
      <?php echo $this->session->flashdata('msg');?>
    <div class="inner">
        <div class="row">
           
        </div>

        <div class="card ">
              <div class="card-header ">
                <div class="row">
                    <div class="col-6">
                        <table style="font-size:12px;line-height:110%">
                        <?php $task = $data['task']; $detail = $data['detail']; ?>
                        <tr>
                            <td>NO WO</td>
                            <td class='px-2'>:</td>
                            <td>ICON+/PM/<?php echo $detail->id;?></td>
                        </tr>
                        <tr>
                            <td>DATE</td>
                            <td class='px-2'>:</td>
                            <td><?php echo date('d M Y', strtotime($detail->date));?></td>
                        </tr>
                        <tr>
                            <td class='pr-2'>TITLE</td>
                            <td class='px-2'>:</td>
                            <td><?php echo $detail->title;?></td>
                        </tr>
                        <tr>
                            <td>POP</td>
                            <td class='px-2'>:</td>
                            <td><?php echo $detail->pop;?></td>
                        </tr>
                        <tr>
                            <td>TASK</td>
                            <td class='px-2'>:</td>
                            <td><?php echo $task->title;?></td>
                        </tr>
                        </table>
                    </div>
                    <div class="col-6 text-right">
                        <a href="<?php echo site_url('preven/detail_wo/'.$detail->id); ?>" class='btn btn-secondary'><i class='fa fa-chevron-left'></i> Back to Tasks</a>
                    </div>
                </div>
              </div>
              <div class="card-body">
                <div class="mx-2 mt-3">
                <h4><strong>Task Evidence</strong></h4>
                <div class="row">
                    <?php foreach($data['evidence'] as $row):?>
                        <div class="col-3 px-3">
                            <div class="border p-2 rounded">
                                <div class='row'>
                                    <div class='col-12'><a href="<?php echo 'http://10.14.22.85/fms-api/uploads/task_evidence/'.$row->photo;?>" title="Click to download" target="blank"><img src="<?php echo 'http://10.14.22.85/fms-api/uploads/task_evidence/'.$row->photo;?>" class='w-100 rounded'/></a></div>
                                </div>
                                <div class="pt-1 row">
                                    <div class='col-12'><small style='font-size:10px' class='label bg-<?php echo ($row->type == "Before")?'primary':'success';?> text-white'><?php echo strtoupper($row->type);?></small></div>
                                </div>
                                <div class="pt-1"><?php echo $row->caption;?></div>
                                <div class=""><small style='font-size:10px'>Upload at <?php echo date('d M Y H:i:s', strtotime($row->created_on));?></small></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                </div>
              </div>
            </div>
    </div>
</div>


