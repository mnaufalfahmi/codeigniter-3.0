<script src="<?php echo base_url();?>/themes/default/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0 mt-3">
      <?php echo $this->session->flashdata('msg');?>
    <div class="inner">
        <div class="row">
           
        </div>

        <div class="card ">
              <div class="card-header ">
                <div class="card-title">
                  Preventive Maintenance Report
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                    
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-body">
                <form method="get" action="" class="col-md-4">
                  <div class="form-group">
                    <label>Date Start</label>
                    <div class="input-group date">
                      <input type="text" class="form-control" id="datepicker-start" name="start" value="<?php echo ($data['start'])?$data['start']:date('Y-m-d');?>" required>
                      <div class="input-group-append ">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Date End</label>
                    <div class="input-group date">
                      <input type="text" class="form-control" id="datepicker-end" name="end" value="<?php echo ($data['end'])?$data['end']:date('Y-m-d');?>" required>
                      <div class="input-group-append ">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>


    </div>
</div>
<script>
    $('#datepicker-start').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#datepicker-end').datepicker({
        format: 'yyyy-mm-dd',
    });
    </script>