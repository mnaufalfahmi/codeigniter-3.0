<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0 mt-3">
      <?php echo $this->session->flashdata('msg');?>
    <div class="inner">
        <div class="row">
           
        </div>

        <div class="card ">
              <div class="card-header ">
                <div class="row">
                  <div class="col-6">
                    <table style="font-size:12px;line-height:110%">
                      <?php $detail = $data['detail'];?>
                      <tr>
                        <td>NO WO</td>
                        <td class='px-2'>:</td>
                        <td>ICON+/PM/<?php echo $detail->id;?></td>
                      </tr>
                      <tr>
                        <td>DATE</td>
                        <td class='px-2'>:</td>
                        <td><?php echo date('d M Y', strtotime($detail->date));?></td>
                      </tr>
                      <tr>
                        <td class='pr-2'>TITLE</td>
                        <td class='px-2'>:</td>
                        <td><?php echo $detail->title;?></td>
                      </tr>
                      <tr>
                        <td>POP</td>
                        <td class='px-2'>:</td>
                        <td><?php echo $detail->pop;?></td>
                      </tr>
                    </table>
                    <br>
                  </div>
                  <div class="col-6 text-right">
                     <a href="<?php echo site_url('preven/wo/'); ?>" class='btn btn-secondary'><i class='fa fa-chevron-left'></i> Back to Workorder</a>
                  </div>
                </div>
                <div class="card-title">
                  <?php if($detail->status != 'Close'){ ?> <button class="btn btn-primary btn-cons m-b-10" data-target="#modalAddTask" data-toggle="modal" type="submit"><i class="fa fa-plus"></i> New Task </button><?php } ?>
                </div>
                <div class="pull-right">
                  <div class="col-xs-12">
                    <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-body">
                <table class="table table-hover" id="">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th>Activity</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Hasil</th>
                      <th>Remark</th>
                      <th>Approval</th>
                      <th class='text-center'>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                        <?php $no=1;  ?>
                        <?php foreach ($data['list_task'] as $value) :?>
                            <tr>
                              <td width="30px"><center><?php echo $no++;?></center></td>
                              <td><?php echo $value->title;?></td>
                              <?php if ($value->status=='On Progress') :?>             
                                <td class="text-center"><span class="label label-warning"><?php echo strtoupper($value->status);?></span></td>
                              <?php elseif($value->status=='New'):  ?>
                                <td class="text-center"><span class="label label-inverse"><?php echo strtoupper($value->status);?></span></td>
                              <?php elseif($value->status=='Cancel'):  ?>
                                <td class="text-center"><span class="label label-danger"><?php echo strtoupper($value->status);?></span></td>
                              <?php elseif($value->status=='Close'):  ?>
                                <td class="text-center"><span class="label label-success"><?php echo strtoupper($value->status);?></span></td>
                              <?php endif; ?>
                              <td class="text-center"><?php echo ($value->result)?$value->result:'-';?></td>
                              <td class="text-left"><?php echo ($value->remark)?$value->remark:'-';?></td>
                              <td width="100px"><button id="btnApproval" data-id="<?php echo $value->id; ?>" data-title="<?php echo $value->title; ?>" data-result="<?php echo $value->result; ?>" class="btnApproval btn btn-xs btn-neutral" data-target="#modalApproval" data-toggle="modal" ><?php echo ($value->approval)?'<b>'.strtoupper($value->approval).'</b>':"<i class='fa fa-check-square-o'></i> Approval";?></button></td>
                              <td width="150px" class='text-center'>
                                <a href="<?php echo site_url('preven/detail_task/'.$value->id);?>"  title="View Evidence"><button class="btn btn-primary btn-xs ml-1"><i class="fa fa-camera"></i> View</i></button>
                                <?php if($detail->status != 'Close'){ ?><a href="<?php echo site_url('preven/delete_task/'.$value->id.'/'.$detail->id);?>"  onclick="return confirm('Are you sure you want to delete?')" title="Delete Task"><button class="btn btn-danger btn-xs ml-1 actionDefine"><i class="fa fa-trash"></i></button><?php } ?>
                              </td>
                            </tr> 
                        <?php endforeach; ?>
                  </tbody>
                </table>
                <div class="py-2 text-center">
                    <?php if($detail->status != 'Close'){ ?><a href="<?php echo site_url('preven/close/'.$data['id_wo']);?>" class="btn btn-success" onclick="return confirm('Apakah Anda yakin akan melakukan Close WO? Mohon pastikan semua task sudah close dan approve')">Close Workorder</a><?php } ?>
                </div>
              </div>
            </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalAddTask" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>New <span class="semi-bold">Task</span></h5>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?php echo site_url('preven/input_task'); ?>" class="form-horizontal form_save" role="form" >
                        <div class="row">
                            <div class="card card-transparent">
                                <!-- <div class="card-header ">
                                    <div class="card-title">User Details</div>
                                </div> -->
                                <div class="card-body ">
                                    
                                    <input type="hidden" name="id_wo" class="form-control" placeholder="Title" value="<?php echo $data['id_wo']; ?>" required>

                                    <div class="form-group">
                                        <label>Activity</label>
                                        <!-- <input type="text" name="title" class="form-control" placeholder="Title"> -->
                                        <select class="full-width select2" name="title" data-init-plugin="select2" required>
                                           <?php foreach ($data['activity'] as $act) { ?>
                                                    <option value="<?= $act->name; ?>"><?= $act->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <!-- <div class="form-group">
                                        <label>Asset</label>
                                        <select class="full-width select2" name="asset" data-init-plugin="select2">
                                            <option value="0">-</option>
                                            <?php foreach ($data['asset'] as $asset) { ?>
                                                    <option value="<?= $asset->popname; ?>"><?= $asset->popname; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div> -->

                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="full-width select2" name="status" data-init-plugin="select2">
                                            <option value="New">New</option>
                                            <option value="On Progress">On Progress</option>
                                            <option value="Close">Close</option>
                                            <option value="Cancel">Cancel</option>
                                        </select>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade slide-up" id="modalApproval" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Approval <span class="semi-bold">Task</span></h5>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?php echo site_url('preven/approval_task/'.$data['id_wo']); ?>" class="form-horizontal form_save" role="form" >
                        <div class="row">
                            <div class="card card-transparent">
                                <div class="card-body ">
                                    
                                    <input type="hidden" id="task_id" name="task_id" class="form-control" placeholder="Title" value="" required>

                                    <div class="form-group">
                                        <label>Task Name</label>
                                        <input type="text" readonly class="form-control" name="task_title" id="task_title" />
                                    </div>

                                    <div class="form-group">
                                        <label>Result</label>
                                        <select class="full-width form-control" name="task_result" required>
                                            <option value="OK">OK</option>
                                            <option value="NOK">NOK</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Remark</label>
                                        <textarea class="form-control" name="task_remark"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Approval</label>
                                        <select class="full-width form-control" name="task_approval" required>
                                            <option value="Approved">Approve</option>
                                            <option value="Not Approved">Not Approve</option>
                                        </select>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){
  $(".btnApproval").click(function(){
    var id = $(this).data('id');
    var title = $(this).data('title');
    var result = $(this).data('result');
    $('#task_id').val(id);
    $('#task_title').val(title);
    $('#task_result').val(result);
  })
})
</script>