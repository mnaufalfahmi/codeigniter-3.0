<style>
    .table-dashboard2 > thead > tr > th {
        line-height: 0 !important;
    }
    .table-dashboard > tfoot > tr > td {
        line-height: 0 !important;
    }
    .table-dashboard > tbody > tr > td {
        line-height: 0.5 !important;
    }
</style>
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <div class="row">
                <div class="col-lg-5">
                    <!-- START WIDGET widget_tableWidgetBasic-->
                    <div
                        class="widget-11-2 card no-border card-condensed no-margin widget-loader-circle full-height d-flex flex-column">
                        <div class="card-header  top-right">
                            <div class="card-controls">
                                <ul>
                                    <li>
                                        <a data-toggle="refresh" class="card-refresh text-black" href="#">
                                            <i class="card-icon card-icon-refresh"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="padding-25">
                            <div class="pull-left">
                                <h3 class="text-default no-margin">INCIDENT ASSIGNMENT</h3>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="auto-overflow container">
                            <table class="table table-condensed table-hover table-dashboard" id="assigment_incident">
                                <thead>
                                    <tr>
                                        <th width="200px">Nama</th>
                                        <th>Open</th>
                                        <th>All</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot id="assigment_total">
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- END WIDGET -->
                </div>

                <div class="col-lg-3">
                    <div class="widget-10 card no-border bg-white no-margin widget-loader-bar">
                        <div class="card-header  top-left top-right ">
                            <div class="card-title text-black hint-text">
                                <span class="font-montserrat fs-11 all-caps">Count Incident
                                </span>
                            </div>
                            <div class="card-controls">
                                <ul>
                                    <li>
                                        <a data-toggle="refresh" class="card-refresh text-black" href="#">
                                            <i class="card-icon card-icon-refresh"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body p-t-40">
                            <div class="row">
                                <div class="col-md-6" style="background-color:#81daf6">
                                    <h4 class="no-margin p-b-5 text-white semi-bold">Open</h4>
                                </div>
                                <div class="col-md-6" style="background-color:#81daf6">
                                    <h4 class="no-margin p-b-5 text-right text-white semi-bold" id="count_open"></h4>
                                </div>
                                <div class="col-md-6" style="background-color:#fdd073">
                                    <h4 class="no-margin p-b-5 text-white semi-bold">Stopclock</h4>
                                </div>
                                <div class="col-md-6" style="background-color:#fdd073">
                                    <h4 class="no-margin p-b-5 text-right text-white semi-bold" id="count_stop"></h4>
                                </div>
                                
                                <hr>
                                
                                <div class="col-md-6">
                                    <h4 class="no-margin p-b-5 text-default semi-bold">Total</h4>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="no-margin p-b-5 text-right semi-bold" id="count_total"></h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <!-- start open incident -->
                    <div class="widget-15 card no-border bg-white no-margin widget-loader-bar">
                        <div class="card-header  top-left top-right ">
                            <div class="card-title text-black hint-text">
                                <span class="font-montserrat fs-11 all-caps">Open Incident</span>
                            </div>
                            <div class="card-controls">
                                <ul>
                                    <li>
                                        <a data-toggle="refresh" class="card-refresh text-black" href="#">
                                            <i class="card-icon card-icon-refresh"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body p-t-40">
                            <div class="text-center">
                                <span class="h1 text-success" id="open_incident"></span>
                            </div>
                            
                            <hr>

                            <div class="row" id="network_incident">
                                
                            </div>
                        </div>
                    </div>
                    <!-- end open incident -->

                    <br>
                    <!-- START stop clock incident -->
                    <div class="widget-15 card no-border bg-white no-margin widget-loader-bar">
                        <div class="card-header  top-left top-right ">
                            <div class="card-title text-black hint-text">
                                <span class="font-montserrat fs-11 all-caps">Stop Clock Incident</span>
                            </div>
                            <div class="card-controls">
                                <ul>
                                    <li>
                                        <a data-toggle="refresh" class="card-refresh text-black" href="#">
                                            <i class="card-icon card-icon-refresh"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body p-t-40">
                            <div class="text-center">
                                <span class="h1 text-info text-warning" id="stop_clock"></span>
                            </div>
                                    
                            <hr>

                            <div class="row" id="network_incident_stop">
                                
                            </div>
                        </div>
                    </div>
                    <!-- END stop clock incident -->

                    <br>

                    <!-- START open ticket -->
                    <div class="widget-15 card no-border bg-primary no-margin widget-loader-bar">
                        <div class="card-header  top-left top-right ">
                            <div class="card-title text-black hint-text">
                                <span class="font-montserrat fs-11 all-caps text-white">Open Ticket</span>
                            </div>
                            <div class="card-controls">
                                <ul>
                                    <li>
                                        <a data-toggle="refresh" class="card-refresh text-black" href="#">
                                            <i class="card-icon card-icon-refresh"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body p-t-40">
                            <div class="text-center text-white">
                                <span class="h1 text-white" id="open_ticket">
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- END open ticket -->
                </div>

                <div class="col-lg-4">
                    <div class="widget-12 card no-border bg-white no-margin widget-loader-bar">
                        <div class="card-header  top-left top-right ">
                            <div class="card-title text-black hint-text">
                                <span class="font-montserrat fs-11 all-caps">OPEN INCIDENTS PER REGIONAL
                                </span>
                            </div>
                            <div class="card-controls">
                                <ul>
                                    <li>
                                        <a data-toggle="refresh" class="card-refresh text-black" href="#">
                                            <i class="card-icon card-icon-refresh"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body p-t-40">
                            <div class="widget-11-2-table">
                                <table class="table table-condensed table-hover table-dashboard table-dashboard2">
                                    <thead>
                                        <tr>
                                            <th width="240px">Nama</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="incident_regional">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <br>
                    <!-- start open incident pernetwork -->
                    <div class="widget-15 card no-border bg-white no-margin widget-loader-bar">
                        <div class="card-header  top-left top-right ">
                            <div class="card-title text-black hint-text">
                                <span class="font-montserrat fs-11 all-caps">Open Incident Per Network</span>
                            </div>
                            <div class="card-controls">
                                <ul>
                                    <li>
                                        <a data-toggle="refresh" class="card-refresh text-black" href="#">
                                            <i class="card-icon card-icon-refresh"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body p-t-40">
                           <div class="widget-11-2-table">
                                <table class="table table-condensed table-hover table-dashboard table-dashboard2">
                                    <thead>
                                        <tr>
                                            <th width="240px">Nama</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody id="network">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- end open incident per network-->
                    <br>
                    <div class="widget-15 card no-border bg-white no-margin widget-loader-bar">
                        <div class="card-header  top-left top-right ">
                            <div class="card-title text-black hint-text">
                                <span class="font-montserrat fs-11 all-caps">Ticket Priority</span>
                            </div>
                            <div class="card-controls">
                                <ul>
                                    <li>
                                        <a data-toggle="refresh" class="card-refresh text-black" href="#">
                                            <i class="card-icon card-icon-refresh"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body p-t-40">
                           <div class="widget-11-2-table">
                                <table class="table table-hover table-dashboard2">
                                    <thead>
                                        <tr>
                                            <th>Description</th>
                                            <th width="100px">Bobot</th>
                                        </tr>
                                    </thead>
                                    <tbody id="bobot" style="cursor: pointer;">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>