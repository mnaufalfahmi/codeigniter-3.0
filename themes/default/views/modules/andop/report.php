<style type="text/css">
@media only screen and (min-width: 800px){
 .mod-modal {
  width: 1100px;
  height:580px;
}
}
.table.table-condensed tbody tr td {
        /* padding-top: 1px;
        padding-bottom: 1px; */
      }
      #btn{
        padding-top: 0px;
        padding-bottom: 0px;
      }
    </style>

    <div class="jumbotron" >
      <div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
          <!-- START BREADCRUMB -->
          <div class="row">
           
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header m-t-10">
                  <div class="card-title">
                    Andop Report
                  </div>
                  <br>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <!-- table -->
                    <table class="table table-hover table-condensed" id="andop_report" style="width:100%">
                      <thead>
                        <tr>
                          <th width="4%">No</th>
                          <th width="10%">Create Time</th>
                          <th width="8%">Creator</th>
                          <th width="9%">Job Number</th>
                          <th width="25%">Title</th>
                          <th width="10%">Status</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- START MODAL  -->
    <div class="modal fade slide-up" id="modalFilter" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
          <div class="modal-content mod-modal">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <h5>Filter</h5>
            </div>
            <div class="modal-body text-center m-t-20">
              <form action="">
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Create Time </label>
                  <div class="col-sm-10">
                    <div class="input-group date">
                      <input type="text" class="form-control"
                      id="datepicker-component">
                      <div class="input-group-append ">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Job Number </label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Title </label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Execution Time</label>
                  <div class="col-sm-10">
                    <div class="input-group">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-complete">Apply</button>
                <button data-dismiss="modal" class="btn btn-default">Cancel</button>
              </div>
            </form>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>


    <div class="modal fade slide-up" id="modaladdandop" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content-wrapper">
          <div class="modal-content mod-modal">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <h5>Add New <span class="semi-bold">Andop</span></h5>
            </div>
            <div class="modal-body">

              <form enctype="multipart/form-data" id="form-work" class="form-horizontal form_save" role="form" autocomplete="off" action="<?php echo base_url()?>andop/addAndop" method="POST">
                <div class="row">
                  <div class="col-lg-8">
                    <div class="card card-transparent">
                     <div class="card-header ">
                       <div class="card-title">Andop Detail</div>
                     </div>
                     <div class="card-body ">
                      <div class="form-group row">
                        <label class="col-md-3 control-label">Andop Type</label>
                        <div class="col-md-9">
                          <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="andop_type" name="andop_type">
                            <option value="0" selected>select andop type</option>
                            <option value="IMPROVEMENT">IMPROVEMENT</option>
                            <option value="PROJECT">PROJECT</option>
                            <option value="MAINTENANCE">MAINTENANCE</option>
                            <option value="ACTIVATION">ACTIVATION</option>
                          </select>
                          <input type="hidden" name="status_andop" id="status_andop" value="<?php echo $data['status']; ?>">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 control-label">Title</label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" placeholder="Title" name="andop_title">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 control-label">Description</label>
                        <div class="col-md-9">
                          <textarea class="form-control" name="andop_description" placeholder="Description"></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 control-label">Duration</label>
                        <div class="col-md-2">
                          <div class="form-group row">
                           <input type="number" class="form-control" placeholder="Duration" name="andop_duration_hours" value="0">
                         </div>
                       </div>
                       <div class="col-md-2">
                        <label class="col-md-12 control-label">Hours</label>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group row">
                         <input type="number" class="form-control" placeholder="Hours" name="andop_duration_minutes" value="0">
                       </div>
                     </div>
                     <div class="col-md-2">
                      <label class="col-md-12 control-label">Minutes</label>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Duration Prediction</label>
                    <div class="col-md-2">
                      <div class="form-group row">
                       <input type="number" class="form-control" placeholder="Duration Prediction" name="andop_downtime_hours" value="0">
                     </div>
                   </div>
                   <div class="col-md-2">
                    <label class="col-md-12 control-label">Hours</label>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group row">
                     <input type="number" class="form-control" placeholder="Hours Prediction" name="andop_downtime_minutes" value="0">
                   </div>
                 </div>
                 <div class="col-md-2">
                  <label class="col-md-12 control-label">Minutes</label>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Execution Time</label>
                <div class="col-md-4">
                  <div class="form-group row">
                    <div class="input-group date">
                     <input id="datepicker" type="text" class="form-control" placeholder="Execution Time" name="andop_execution_time_date">
                     <div class="input-group-append ">
                      <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group row">
                  <input type="text" name="andop_execution_time_hours" id="andop_execution_time_hours" class="form-control">
                      <!-- <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="andop_execution_time_hours" name="andop_execution_time_hours">
                        <option selected></option>
                        <option>03:30:00</option>
                        <option>04:30:00</option>
                        <option>05:30:00</option>
                        <option>06:30:00</option>
                      </select> -->
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 control-label">Location</label>
                  <div class="col-md-9">
                    <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="andop_location" name="andop_location">
                      <option value="0" selected>select location</option>
                      <?php
                      foreach($data['location'] as $row): ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 control-label">Severity</label>
                  <div class="radio radio-success col-md-9">
                    <input type="radio" value="Emergency" name="andop_severity" id="Emergency">
                    <label for="Emergency">Emergency</label>
                    <input type="radio" value="Major" name="andop_severity" id="Major">
                    <label for="Major">Major</label>
                    <input type="radio" value="Minor" name="andop_severity" id="Minor">
                    <label for="Minor">Minor</label>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 control-label">Your Name</label>
                  <div class="col-md-9">
                    <input readonly type="text" class="form-control" placeholder="your name" value="<?php echo $_COOKIE['fullname']?>">
                    <input type="hidden" name="andop_user_id">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 control-label">Your Phone Number</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="Phone Number" name="andop_phone_user_id">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 control-label">Your Email</label>
                  <div class="col-md-9">
                    <input type="email" class="form-control" placeholder="Email" name="andop_email_user_id" value="<?php echo $_COOKIE['username']; ?>@iconpln.co.id">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">Field Support</div>
              </div>
              <div class="card-body">
                <button id="add-button-fs" type="button" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add</button>
                <div id="field-support">
                  <div class="row">
                    <div style="width: 130px;padding-top: 10px;" class="col-xs-4">
                      <input type="text" id="andop_name_field_support" name="andop_name_field_support[]" class="form-control" placeholder="name" style="font-size:12px">
                    </div>
                    <div style="width: 130px;padding-top: 10px;" class="col-xs-4">
                      <input type="text" id="andop_mobile_field_support" name="andop_mobile_field_support[]" class="form-control" placeholder="mobile" style="font-size:12px" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                    </div>
                    <div style="padding-top: 15px;" class="col-xs-4">
                      <button title="" data-toggle="tooltip" type="button" class="btn btn-xs tip" data-original-title="This field is mandatory" aria-describedby="tooltip527904"><i class="fa fa-exclamation-circle"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">File Attachment</div>
              </div>
              <div class="card-body">
                <button type="button" id="add-button-fa" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add</button>
                <div id="file-attachment">
                  <div class="row">
                    <div style="width: 259px;padding-top: 10px;" class="col-xs-4">
                      <input type="file" id="andop_file_attachment" name="andop_file_attachment" class="form-control">
                    </div>
                    <div style="padding-top: 15px;" class="col-xs-4">
                      <button title="" data-toggle="tooltip" type="button" class="btn btn-xs tip" data-original-title="This field is mandatory" aria-describedby="tooltip527904"><i class="fa fa-exclamation-circle"></i></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">Affected Services</div>
              </div>
              <div class="card-body">
                <button id="button_attach" type="button" data-target="#modalattachotherticket" data-toggle="modal" class="btn btn-primary btn-xs"><i class="fa fa-file"></i> Attach Services</button>
                <table class="table table-hover table-condensed" id="affected_services" style="font-family: sans-serif, Arial, Verdana, 'Trebuchet MS', 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';font-size: 13px;">
                  <tr>
                    <th style="width:4%">..</th>
                    <th style="width:10%">SID</th>
                    <th style="width:10%">Service</th>
                    <th style="width:11%">Customer</th>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button class="btn btn-sm btn-complete" type="submit"><i class="fa fa-file"></i>&nbsp; Save</button>
        <button data-dismiss="modal" class="btn btn-sm btn-default" type="button"><i class="fa fa-times-circle"></i>&nbsp; Cancel</button>
      </div>
    </form>
  </div>
</div>
</div>
</div>





<div class="modal fade slide-up" id="modalattachotherticket" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content-wrapper">
      <div class="modal-content mod-modal">
        <div class="modal-header clearfix text-left">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
          <h5>Affected <span class="semi-bold">Services</span></h5>
        </div>
        <div class="modal-body">

          <form id="form-work" class="form-horizontal" role="form" autocomplete="off">
            <div class="row">
              <div class="card card-transparent">
                <div class="card-header ">
                  <div class="card-title">User Details</div>
                </div>
                <div class="card-body ">
                  <table class="table table-hover table-condensed" id="attach_affected_service">
                    <thead>
                      <tr>
                        <th width="3%">No</th>
                        <th width="10%">Service ID</th>
                        <th width="10%">Service</th>
                        <th width="10%">Origin</th>
                        <th width="10%">Termination</th>
                        <th width="10%">Customer Name</th>
                      </tr>  
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-complete">Attach</button>
          <button data-dismiss="modal" class="btn btn-default">Cancel</button>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
</div>

<div class="modal fade slide-up" id="modalAndopDetail" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content-wrapper">
      <div class="modal-content mod-modal">
        <div class="modal-header clearfix text-left">
          <button id="removed" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
          <h5>Andop <span class="semi-bold">Detail Report</span></h5>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-lg-12">
              <div class="card card-transparent">
                <div class="card-header ">
                  <div class="card-title">Andop Detail</div>
                </div>
                <div class="card-body ">
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Report Status</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="status" placeholder="" name="status">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label class="col-md-3 control-label">Report Creator</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="creator" placeholder="" name="creator">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Report Submit</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="report_submit" placeholder="" name="report_submit">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Report Validation</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="report_validation" placeholder="" name="report_validation">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Number Job</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="number_job" placeholder="" name="number_job" required>
                      <input type="hidden" name="andop_unique" id="andop_unique">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Title</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="title" placeholder="" name="title">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Description</label>
                    <div class="col-md-9">
                      <textarea class="form-control" rows="5" id="description" name="description"></textarea>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Actual Start Time</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="actual_start" placeholder="" name="actual_start">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Actual End Time</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="actual_end" placeholder="" name="actual_end">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Actual Downtime Time</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="actual_downtime" placeholder="" name="actual_downtime">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Trouble</label>
                    <div class="col-md-9">
                      <textarea class="form-control" rows="5" id="trouble" name="trouble"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Attachment</label>
                    <div class="col-md-9" id="attachment">
                      
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <div class="modal-footer">
          
          <div id="andopStatus" style="display: none;">
          </div>
          <!-- data-dismiss="modal" -->
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade slide-up" id="modalreport" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content-wrapper">
      <div class="modal-content mod-modal">
        <div class="modal-header clearfix text-left">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
          <h5>Report New <span class="semi-bold">Andop</span></h5>
        </div>
        <div class="modal-body">

          <form enctype="multipart/form-data" id="form-work" class="form-horizontal form_report" role="form" autocomplete="off" action="<?php echo base_url()?>andop/andopReport" method="POST">
            <div class="row">
              <div class="col-lg-12">
                <div class="card card-transparent">
                 <div class="card-header ">
                   <div class="card-title">Add Report</div>
                 </div>
                 <div class="card-body ">

                  <div class="form-group row">
                    <label class="col-md-3 control-label">Title</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" placeholder="Title" name="report_andop_title" id="report_andop_title">
                      <input type="hidden" name="id_unique_report" id="id_unique_report">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-3 control-label">Number Job</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" placeholder="Number Job" name="report_andop_number_job" id="report_andop_number_job">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-md-3 control-label">Actual Start Time</label>
                    <div class="col-md-4">
                      <div class="form-group row">
                        <div class="input-group date">
                          <input id="reportdatepicker-start" type="text" class="form-control" placeholder="Actual Start Time" name="report_andop_actual_date_start">
                         <div class="input-group-append ">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group row">
                      <input type="text" name="report_andop_actual_minutes_start" id="report_andop_actual_minutes_start" class="form-control">
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 control-label">Actual End Time</label>
                    <div class="col-md-4">
                      <div class="form-group row">
                        <div class="input-group date">
                          <input id="reportdatepicker-end" type="text" class="form-control" placeholder="Actual End Time" name="report_andop_actual_date_end">
                         <div class="input-group-append ">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group row">
                      <input type="text" name="report_andop_actual_minutes_end" id="report_andop_actual_minutes_end" class="form-control">
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 control-label">Actual Downtime</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" placeholder="Actual Downtime" name="report_andop_actual_downtime" id="report_andop_actual_downtime">
                    </div>
                  </div>

                <div class="form-group row">
                  <label class="col-md-3 control-label">Trouble</label>
                  <div class="col-md-9">
                    <textarea class="form-control" name="report_andop_trouble" id="report_andop_trouble" placeholder="trouble"></textarea>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-md-3 control-label">Attachment</label>
                  <div class="col-md-9">
                   <input type="file" class="form-control" name="report_andop_file" id="report_andop_file">
                  </div>
                </div>
                
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button class="btn btn-sm btn-complete" type="submit"><i class="fa fa-file"></i>&nbsp; Save</button>
        <button data-dismiss="modal" class="btn btn-sm btn-default" type="button"><i class="fa fa-times-circle"></i>&nbsp; Cancel</button>
      </div>
    </form>
  </div>
</div>
</div>
</div>


<div class="modal fade slide-up" id="modaledit" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content-wrapper">
      <div class="modal-content mod-modal">
        <div class="modal-header clearfix text-left">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
          <h5>Edit New <span class="semi-bold">Andop</span></h5>
        </div>
        <div class="modal-body">

          <form enctype="multipart/form-data" id="form-work" class="form-horizontal form_edit" role="form" autocomplete="off" action="<?php echo base_url()?>andop/andopEdit" method="POST">
            <div class="row">
              <div class="col-lg-8">
                <div class="card card-transparent">
                 <div class="card-header ">
                   <div class="card-title">Andop Detail</div>
                 </div>
                 <div class="card-body ">
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Andop Type</label>
                    <div class="col-md-9">
                      <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="edit_andop_type" name="edit_andop_type">
                        <option value="ada">select andop type</option>
                        <option value="IMPROVEMENT">IMPROVEMENT</option>
                        <option value="PROJECT">PROJECT</option>
                        <option value="MAINTENANCE">MAINTENANCE</option>
                        <option value="ACTIVATION">ACTIVATION</option>
                      </select>
                      <input type="hidden" name="id_unique" id="id_unique">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Title</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" placeholder="Title" name="edit_andop_title" id="edit_andop_title">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Description</label>
                    <div class="col-md-9">
                      <textarea class="form-control" name="edit_andop_description" id="edit_andop_description" placeholder="Description"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 control-label">Duration</label>
                    <div class="col-md-2">
                      <div class="form-group row">
                       <input type="number" class="form-control" placeholder="Duration" id="edit_andop_duration_hours" name="edit_andop_duration_hours" value="0">
                     </div>
                   </div>
                   <div class="col-md-2">
                    <label class="col-md-12 control-label">Hours</label>
                  </div>
                  <div class="col-md-2">
                    <div class="form-group row">
                     <input type="number" class="form-control" placeholder="Hours" id="edit_andop_duration_minutes" name="edit_andop_duration_minutes" value="0">
                   </div>
                 </div>
                 <div class="col-md-2">
                  <label class="col-md-12 control-label">Minutes</label>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 control-label">Duration Prediction</label>
                <div class="col-md-2">
                  <div class="form-group row">
                   <input type="number" class="form-control" placeholder="Duration Prediction" name="edit_andop_downtime_hours" id="edit_andop_downtime_hours" value="0">
                 </div>
               </div>
               <div class="col-md-2">
                <label class="col-md-12 control-label">Hours</label>
              </div>
              <div class="col-md-2">
                <div class="form-group row">
                 <input type="number" class="form-control" placeholder="Hours Prediction" name="edit_andop_downtime_minutes" id="edit_andop_downtime_minutes" value="0">
               </div>
             </div>
             <div class="col-md-2">
              <label class="col-md-12 control-label">Minutes</label>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 control-label">Execution Time</label>
            <div class="col-md-4">
              <div class="form-group row">
                <div class="input-group date">
                 <input id="editdatepicker" type="text" class="form-control" placeholder="Execution Time" name="edit_andop_execution_time_date" id="edit_andop_execution_time_date">
                 <div class="input-group-append ">
                  <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <div class="form-group row">
              <input type="text" id="edit_andop_execution_time_hours" name="edit_andop_execution_time_hours" id="andop_execution_time_hours" class="form-control">
                      <!-- <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="andop_execution_time_hours" name="andop_execution_time_hours">
                        <option selected></option>
                        <option>03:30:00</option>
                        <option>04:30:00</option>
                        <option>05:30:00</option>
                        <option>06:30:00</option>
                      </select> -->
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 control-label">Location</label>
                  <div class="col-md-9">
                    <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="edit_andop_location" name="edit_andop_location">
                      <option value="0" >select location</option>
                      <?php
                      foreach($data['location'] as $row): ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 control-label">Severity</label>
                  <div class="radio radio-success col-md-9">
                    <input type="radio" value="Emergency" name="edit_andop_severity" id="edit_Emergency" class='radio1'>
                    <label for="edit_Emergency">Emergency</label>
                    <input type="radio" value="Major" name="edit_andop_severity" id="edit_Major" class='radio1'>
                    <label for="edit_Major">Major</label>
                    <input type="radio" value="Minor" name="edit_andop_severity" id="edit_Minor" class='radio1'>
                    <label for="edit_Minor">Minor</label>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 control-label">Your Name</label>
                  <div class="col-md-9">
                    <input readonly type="text" class="form-control" placeholder="your name" id="edit_andop_fullname">
                    <input type="hidden" name="edit_andop_user_id" id="edit_andop_user_id">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 control-label">Your Phone Number</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="Phone Number" name="edit_andop_phone_user_id" id="edit_andop_phone_user_id">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-md-3 control-label">Your Email</label>
                  <div class="col-md-9">
                    <input type="email" class="form-control" placeholder="Email" id="edit_andop_email_user_id" name="edit_andop_email_user_id">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">Field Support</div>
              </div>
              <div class="card-body">
                <button id="add-button-fs" type="button" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add</button>
                <div id="edit-field-support">

                </div>
              </div>
            </div>

            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">File Attachment</div>
              </div>
              <div class="card-body">
                <button type="button" id="add-button-fa" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add</button>
                <div id="edit-file-attachment">

                </div>
              </div>
            </div>

            <div class="card card-transparent">
              <div class="card-header ">
                <div class="card-title">Affected Services</div>
              </div>
              <div class="card-body">
                <button id="button_attach" type="button" data-target="#modalattachotherticket" data-toggle="modal" class="btn btn-primary btn-xs"><i class="fa fa-file"></i> Attach Services</button>
                <table class="table table-hover table-condensed" id="edit_affected_services" style="font-family: sans-serif, Arial, Verdana, 'Trebuchet MS', 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';font-size: 13px;">
                  <tr>
                    <th style="width:4%">..</th>
                    <th style="width:10%">SID</th>
                    <th style="width:10%">Service</th>
                    <th style="width:11%">Customer</th>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button class="btn btn-sm btn-complete" type="submit"><i class="fa fa-file"></i>&nbsp; Edit</button>
        <button data-dismiss="modal" class="btn btn-sm btn-default" type="button"><i class="fa fa-times-circle"></i>&nbsp; Cancel</button>
      </div>
    </form>
  </div>
</div>
</div>
</div>

<!-- END MODAL  -->
<script>
  where_status = "<?php echo $data['status']; ?>";
</script>
