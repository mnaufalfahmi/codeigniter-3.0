<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
        <div class="row">
            
        </div>

        <div class="card">
            <div class="card-header m-t-10">
                <div class="card-title">
                    Customer List
                </div>
                <br>
            </div>
            <div class="card-body">
                <table class="table table-striped" id="customer-table" style="width:100%">
                    <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Service ID</th>
                            <th>Service Name</th>
                            <th>Origin</th>
                            <th>Termination</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- show data -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalDeleteCustomer" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are you sure want to delete this user ?</span>
                    </div>
                    <div class="card-body">
                        <a id="delete"><button class="btn btn-danger">Delete</button></a>
                        <button class="btn btn-info" id="cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- end modal -->