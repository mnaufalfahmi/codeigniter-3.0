<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
      <?php echo $this->session->flashdata('msg');?>
    <div class="inner">
        <div class="row">
          <div class="col-md-12 ">
                <!-- START card -->
                <div class="card card-transparent">
                  <div class="card-body">
                  <?php echo form_open('dashboard2/Profile_wo/dashboard_filter', 'id="form-personal" role="form" autocomplete="off"'); ?>
                      <div class="row clearfix">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                          <div class="input-daterange input-group" id="datepicker-range">
                            <input type="text" class="input-sm form-control" name="start_date">
                            <div class="input-group-addon">to</div>
                            <input type="text" class="input-sm form-control" name="end_date">
                          </div>
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-primary" type="submit">filter</button>
                        </div>
                         <div class="col-md-3">
                        </div>
                      </div>
                    <?php echo form_close(); ?>
                  </div>
                </div>
                <!-- END card -->
              </div>
           <div class="col-md-6">

    <!--START CHART 1-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                <canvas id="bar-chart" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart"), getChartJs('bar'));
                        });

                        function getChartJs(type) {
                            var config = null;

                                config = {
                                    type: 'bar',
                            data: {
                              labels: [
                                            <?php foreach ($data['po'] as $value) :?>
                                                <?php echo '"'.$value->region_code.'"';?>,
                                            <?php endforeach; ?>
                                        ],
                              datasets: [
                                {
                                  label: "Jumlah",
                                  backgroundColor: [
                                                        <?php foreach ($data['po'] as $value) :?>
                                                            "#"+Math.floor(Math.random()*16777215).toString(16),
                                                        <?php endforeach; ?>
                                                    ],
                                  data: [
                                            <?php foreach ($data['po'] as $value) :?>
                                                <?php if($value->count!=0): echo ($value->count); else: echo 0; endif;?>,
                                            <?php endforeach; ?>
                                        ]
                                }
                              ]
                            },
                            options: {
                              legend: { display: false },
                              title: {
                                display: true,
                                text: 'PROFILEWORK ORDER (WO) GANGGUAN'
                              }
                            }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--END CHART 1-->

    <!--START CHART 3-->
        <div class="card ">
              <div class=" container-fluid   container-fixed-lg">
                <div class="card-body">
                     <canvas id="pie-chart2" height="150"></canvas>
                        <script type="text/javascript">
                            $(function () {
                                new Chart(document.getElementById("pie-chart2"), getChartJs3('pie'));
                            });

                            function getChartJs3(type) {
                                var config = null;

                                    config = {
                                        type: 'pie',
                                data: {
                                  labels: [
                                                <?php foreach ($data['cat_gangguan'] as $value) :?>
                                                    <?php echo '"'.$value->source_category.'"';?>,
                                                <?php endforeach; ?>
                                            ],
                                  datasets: [
                                    {
                                      label: "Jumlah",
                                      backgroundColor: [
                                                            <?php foreach ($data['cat_gangguan'] as $value) :?>
                                                                "#"+Math.floor(Math.random()*16777215).toString(16),
                                                            <?php endforeach; ?>
                                                        ],
                                      data: [
                                                <?php foreach ($data['cat_gangguan'] as $value) :?>
                                                    <?php if($value->count!=0): echo (round(($value->count/$data['total_catgang'])*100,1))+'%'; else: echo 0; endif;?>,
                                                <?php endforeach; ?>
                                            ]
                                    }
                                  ]
                                },
                                options: {
                                  legend: { position : 'top' },
                                  title: {
                                    display: true,
                                    text: 'CATEGORY GANGGUAN (%)'
                                  }
                                }
                                    }
                                
                                return config;
                            }
                        </script>
                  </div>
              </div>
            </div>
    <!--START CHART 3-->

    <!--START CHART 5-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="bar-chart-grouped5" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart-grouped5"), getChartJs5('bar-chart-grouped'));
                            console.log('a')
                        });

                        function getChartJs5(type) {
                            var config = null;

                                config = {
                                    type: 'bar',
                                    data: {
                                      labels: [
                                              <?php foreach ($data['po'] as $value) :?>
                                                  <?php echo '"'.$value->region_code.'"';?>,
                                              <?php endforeach; ?>
                                            ],
                                      datasets: [
                                          <?php foreach ($data['cat_gangguan'] as $value) :?>
                                                {
                                                  label: <?php echo '"'.$value->source_category.'"';?>,
                                                  backgroundColor: "#"+Math.floor(Math.random()*16777215).toString(16),
                                                  data: [
                                                    <?php $temp_region=''; ?>
                                                      <?php foreach ($data['region_category'] as $key) :?>
                                                        <?php if ($key->source_category==$value->source_category) :?>
                                                          <?php if($key->count!=0): echo $key->count; else: echo 0; endif;?>,
                                                        <?php endif; ?>
                                                      <?php endforeach; ?>
                                                    ]
                                                },
                                          <?php endforeach; ?>
                                      ]
                                    },
                                    options: {
                                      legend: { display: true },
                                      title: {
                                        display: true,
                                        text: 'CATEGORY GANGGUAN PER SBU REGIONAL'
                                      },
                                      scales: {
                                          xAxes: [{
                                              stacked: true
                                          }],
                                          yAxes: [{
                                              stacked: true
                                          }]
                                      }
                                    }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 5-->

    <!--START CHART 7-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="bar-chart7" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart7"), getChartJs7('bar'));
                        });

                        function getChartJs7(type) {
                            var config = null;

                                config = {
                                    type: 'horizontalBar',
                                    data: {
                                      labels: ["Ormas", "Material FOT", "Lalu lintas padat", "Material FOC", "Akses POP"],
                                      datasets: [
                                        {
                                          label: "Jumlah",
                                          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                                          data: [2478,5267,734,784,433]
                                        }
                                      ]
                                    },
                                    options: {
                                      legend: { display: false },  
                                      title: {
                                        display: true,
                                        text: 'KENDALA RECOVERY GANGGUAN TOTAL'
                                      }
                                    }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 7-->

    <!--START CHART 9-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="line-chart9" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("line-chart9"), getChartJs9('line-chart'));
                        });

                        function getChartJs9(type) {
                            var config = null;

                                config = {
                                    type: 'line',
                                      data: {
                                      labels: [ <?php foreach ($data['bulan_gangguan'] as $value) :?>
                                                  <?php echo '"'.$value->bulan.'"';?>,
                                                  <?php break 1; ?>
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                        <?php foreach ($data['bulan_gangguan'] as $value) :?>
                                                {
                                                  label: <?php echo '"'.$value->source_category.'"';?>,
                                                  fill: false,
                                                  backgroundColor: "#"+Math.floor(Math.random()*16777215).toString(16),
                                                  data: [
                                                          <?php if($value->count!=0): echo $value->count; else: echo 0; endif;?>
                                                        ]
                                                },
                                        <?php endforeach; ?>
                                      ]
                                      },
                                      options: {
                                        title: {
                                          display: true,
                                          text: 'Jumlah Gangguan ALL (POP & Lastmile)'
                                        },
                                        scales: {
                                          yAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                      }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 9-->

    <!--START CHART 11-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="line-chart11" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("line-chart11"), getChartJs11('line-chart'));
                        });

                        function getChartJs11(type) {
                            var config = null;

                                config = {
                                    type: 'line',
                                      data: {
                                      labels: [ <?php foreach ($data['pop_gangguan'] as $value) :?>
                                                  <?php echo '"'.$value->bulan.'"';?>,
                                                  <?php break 1; ?>
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                        <?php foreach ($data['pop_gangguan'] as $value) :?>
                                                {
                                                  label: <?php echo '"'.$value->source_category.'"';?>,
                                                  fill: false,
                                                  backgroundColor: "#"+Math.floor(Math.random()*16777215).toString(16),
                                                  data: [
                                                          <?php if($value->count!=0): echo $value->count; else: echo 0; endif;?>
                                                        ]
                                                },
                                        <?php endforeach; ?>
                                      ]
                                      },
                                      options: {
                                        title: {
                                          display: true,
                                          text: 'Jumlah Gangguan Pada POP'
                                        },
                                        scales: {
                                          yAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                      }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 11-->

    <!--START CHART 13-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="line-chart13" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("line-chart13"), getChartJs13('line-chart'));
                        });

                        function getChartJs13(type) {
                            var config = null;

                                config = {
                                    type: 'line',
                                      data: {
                                      labels: [ <?php foreach ($data['lastmile_gangguan'] as $value) :?>
                                                  <?php echo '"'.$value->bulan.'"';?>,
                                                  <?php break 1; ?>
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                        <?php foreach ($data['lastmile_gangguan'] as $value) :?>
                                                {
                                                  label: <?php echo '"'.$value->source_category.'"';?>,
                                                  fill: false,
                                                  backgroundColor: "#"+Math.floor(Math.random()*16777215).toString(16),
                                                  data: [
                                                          <?php if($value->count!=0): echo $value->count; else: echo 0; endif;?>
                                                        ]
                                                },
                                        <?php endforeach; ?>
                                      ]
                                      },
                                      options: {
                                        title: {
                                          display: true,
                                          text: 'Jumlah Gangguan Pada Lastmile'
                                        },
                                        scales: {
                                          yAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                      }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 13-->

    <!--START CHART 15-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="bar-chart15" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart15"), getChartJs15('bar'));
                        });

                        function getChartJs15(type) {
                            var config = null;

                                config = {
                                    type: 'horizontalBar',
                                    data: {
                                      labels: [ <?php foreach ($data['root_foc'] as $value) :?>
                                                  <?php echo '"'.$value->root_cause_name.'"';?>,
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                                  {
                                                    label: "Jumlah",
                                                    backgroundColor: [
                                                          <?php foreach ($data['root_foc'] as $value) :?>
                                                              "#"+Math.floor(Math.random()*16777215).toString(16),
                                                          <?php endforeach; ?>
                                                      ],
                                                    data: [
                                                          <?php foreach ($data['root_foc'] as $value) :?>
                                                              <?php if($value->jml!=0): echo $value->jml; else: echo 0; endif;?>,
                                                          <?php endforeach; ?>
                                                    ]
                                                  },
                                      ]
                                    },
                                    options: {
                                      legend: { display: false },  
                                      title: {
                                        display: true,
                                        text: 'Root Cause Gangguan FOC'
                                      },
                                        scales: {
                                          xAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                    }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 15-->

    <!--START CHART 17-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="bar-chart17" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart17"), getChartJs17('bar'));
                        });

                        function getChartJs17(type) {
                            var config = null;

                                config = {
                                    type: 'horizontalBar',
                                    data: {
                                      labels: [ <?php foreach ($data['root_power_supply_lastmile'] as $value) :?>
                                                  <?php echo '"'.$value->root_cause_name.'"';?>,
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                                  {
                                                    label: "Jumlah",
                                                    backgroundColor: [
                                                          <?php foreach ($data['root_power_supply_lastmile'] as $value) :?>
                                                              "#"+Math.floor(Math.random()*16777215).toString(16),
                                                          <?php endforeach; ?>
                                                      ],
                                                    data: [
                                                          <?php foreach ($data['root_power_supply_lastmile'] as $value) :?>
                                                              <?php if($value->jml!=0): echo $value->jml; else: echo 0; endif;?>,
                                                          <?php endforeach; ?>
                                                    ]
                                                  },
                                      ]
                                    },
                                    options: {
                                      legend: { display: false },  
                                      title: {
                                        display: true,
                                        text: 'Root Cause Gangguan Power Supply CPE Lastmile'
                                      },
                                        scales: {
                                          xAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                    }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 17-->

    <!--START CHART 19-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="bar-chart19" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart19"), getChartJs19('bar'));
                        });

                        function getChartJs19(type) {
                            var config = null;

                                config = {
                                    type: 'horizontalBar',
                                    data: {
                                      labels: [ <?php foreach ($data['root_fot_lastmile'] as $value) :?>
                                                  <?php echo '"'.$value->root_cause_name.'"';?>,
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                                  {
                                                    label: "Jumlah",
                                                    backgroundColor: [
                                                          <?php foreach ($data['root_fot_lastmile'] as $value) :?>
                                                              "#"+Math.floor(Math.random()*16777215).toString(16),
                                                          <?php endforeach; ?>
                                                      ],
                                                    data: [
                                                          <?php foreach ($data['root_fot_lastmile'] as $value) :?>
                                                              <?php if($value->jml!=0): echo $value->jml; else: echo 0; endif;?>,
                                                          <?php endforeach; ?>
                                                    ]
                                                  },
                                      ]
                                    },
                                    options: {
                                      legend: { display: false },  
                                      title: {
                                        display: true,
                                        text: 'Root Cause Perangkat Lastmile'
                                      },
                                        scales: {
                                          xAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                    }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 19-->
    </div>

    <div class="col-md-6">
    <!--START CHART 2-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="pie-chart" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("pie-chart"), getChartJs1('pie'));
                        });

                        function getChartJs1(type) {
                            var config = null;

                                config = {
                                    type: 'pie',
                            data: {
                              labels: [
                                            <?php foreach ($data['po'] as $value) :?>
                                                <?php echo '"'.$value->region_code.'"';?>,
                                            <?php endforeach; ?>
                                        ],
                              datasets: [
                                {
                                  label: "Jumlah",
                                  backgroundColor: [
                                                        <?php foreach ($data['po'] as $value) :?>
                                                            "#"+Math.floor(Math.random()*16777215).toString(16),
                                                        <?php endforeach; ?>
                                                    ],
                                  data: [
                                            <?php foreach ($data['po'] as $value) :?>
                                                <?php if($value->count!=0): echo (round(($value->count/$data['total'])*100,1))+'%'; else: echo 0; endif;?>,
                                            <?php endforeach; ?>
                                        ]
                                }
                              ]
                            },
                            options: {
                              legend: { position : 'right' },
                              title: {
                                display: true,
                                text: 'PROFILEWORK ORDER (WO) GANGGUAN (%)'
                              }
                            }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 2-->

    <!--START CHART 4-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="bar-chart-grouped" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart-grouped"), getChartJs4('bar-chart-grouped'));
                        });

                        function getChartJs4(type) {
                            var config = null;

                                config = {
                                    type: 'bar',
                                    data: {
                                      labels: [ <?php foreach ($data['bulan_gangguan'] as $value) :?>
                                                  <?php echo '"'.$value->bulan.'"';?>,
                                                <?php break 1; ?>
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                            <?php foreach ($data['bulan_gangguan'] as $value) :?>
                                                  {
                                                    label: <?php echo '"'.$value->source_category.'"';?>,
                                                    backgroundColor: "#"+Math.floor(Math.random()*16777215).toString(16),
                                                    data: [<?php if($value->count!=0): echo $value->count; else: echo 0; endif;?>]
                                                  },
                                            <?php endforeach; ?>
                                      ]
                                    },
                                    options: {
                                      title: {
                                        display: true,
                                        text: 'CATEGORY GANGGUAN (Bulan)'
                                      },
                                      scales: {
                                          yAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                      }
                                    }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 4-->

    <!--START CHART 6-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="bar-chart-grouped6" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart-grouped6"), getChartJs6('bar-chart-grouped'));
                        });

                        function getChartJs6(type) {
                            var config = null;

                                config = {
                                    type: 'horizontalBar',
                                    data: {
                                      labels: ["POP_1", "POP_2"],
                                      datasets: [
                                        {
                                          label: "FOT",
                                          backgroundColor: "#3e95cd",
                                          data: [133, 500]
                                        }, {
                                          label: "FOC",
                                          backgroundColor: "#8e5ea2",
                                          data: [408, 200]
                                        },
                                        {
                                          label: "PS",
                                          backgroundColor: "#E9967A",
                                          data: [420, 300]
                                        }
                                      ]
                                    },
                                    options: {
                                      title: {
                                        display: true,
                                        text: 'TOP 10 : POP TERMINASI'
                                      },
                                      scales: {
                                          xAxes: [{
                                              stacked: true
                                          }],
                                          yAxes: [{
                                              stacked: true
                                          }]
                                      }
                                    }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 6-->

    <!--START CHART 8-->
        <div class="card ">
              <div class=" container-fluid   container-fixed-lg">
                <div class="card-body">
                     <canvas id="pie-chart8" height="150"></canvas>
                        <script type="text/javascript">
                            $(function () {
                                new Chart(document.getElementById("pie-chart8"), getChartJs8('pie'));
                            });

                            function getChartJs8(type) {
                                var config = null;

                                  config = {
                                      type: 'pie',
                              data: {
                                labels: [
                                              <?php foreach ($data['po'] as $value) :?>
                                                  <?php echo '"'.$value->region_code.'"';?>,
                                              <?php endforeach; ?>
                                          ],
                                datasets: [
                                  {
                                    label: "Jumlah",
                                    backgroundColor: [
                                                          <?php foreach ($data['po'] as $value) :?>
                                                              "#"+Math.floor(Math.random()*16777215).toString(16),
                                                          <?php endforeach; ?>
                                                      ],
                                    data: [
                                              <?php foreach ($data['po'] as $value) :?>
                                                  <?php if($value->count!=0): echo (round(($value->count/$data['total'])*100,1))+'%'; else: echo 0; endif;?>,
                                              <?php endforeach; ?>
                                          ]
                                  }
                                ]
                              },
                              options: {
                                legend: { position : 'right' },
                                title: {
                                  display: true,
                                  text: 'KENDALA RECOVERY GANGGUAN (%)'
                                }
                              }
                                  }
                              
                              return config;
                          }
                        </script>
                  </div>
              </div>
            </div>
    <!--START CHART 8-->

    <!--START CHART 10-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="line-chart10" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("line-chart10"), getChartJs10('line-chart'));
                        });

                        function getChartJs10(type) {
                            var config = null;

                                config = {
                                    type: 'line',
                                      data: {
                                      labels: [ <?php foreach ($data['durasi_gangguan'] as $value) :?>
                                                  <?php echo '"'.$value->bulan.'"';?>,
                                                  <?php break 1; ?>
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                        <?php foreach ($data['durasi_gangguan'] as $value) :?>
                                                {
                                                  label: <?php echo '"'.$value->source_category.'"';?>,
                                                  fill: false,
                                                  backgroundColor: "#"+Math.floor(Math.random()*16777215).toString(16),
                                                  data: [
                                                          <?php if($value->durasi!=0): echo $value->durasi; else: echo 0; endif;?>
                                                        ]
                                                },
                                        <?php endforeach; ?>
                                      ]
                                      },
                                      options: {
                                        title: {
                                          display: true,
                                          text: 'POP & Lastmile (Durasi - Menit)'
                                        },
                                        scales: {
                                          yAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                      }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 10-->

    <!--START CHART 12-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="line-chart12" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("line-chart12"), getChartJs12('line-chart'));
                        });

                        function getChartJs12(type) {
                            var config = null;

                                 config = {
                                    type: 'line',
                                      data: {
                                      labels: [ <?php foreach ($data['pop_durasi_gangguan'] as $value) :?>
                                                  <?php echo '"'.$value->bulan.'"';?>,
                                                  <?php break 1; ?>
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                        <?php foreach ($data['pop_durasi_gangguan'] as $value) :?>
                                                {
                                                  label: <?php echo '"'.$value->source_category.'"';?>,
                                                  fill: false,
                                                  backgroundColor: "#"+Math.floor(Math.random()*16777215).toString(16),
                                                  data: [
                                                          <?php if($value->durasi!=0): echo $value->durasi; else: echo 0; endif;?>
                                                        ]
                                                },
                                        <?php endforeach; ?>
                                      ]
                                      },
                                      options: {
                                        title: {
                                          display: true,
                                          text: 'Gangguan POP (Durasi - Menit)'
                                        },
                                        scales: {
                                          yAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                      }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 12-->

    <!--START CHART 14-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="line-chart14" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("line-chart14"), getChartJs14('line-chart'));
                        });

                        function getChartJs14(type) {
                            var config = null;

                                config = {
                                    type: 'line',
                                      data: {
                                      labels: [ <?php foreach ($data['lastmile_durasi_gangguan'] as $value) :?>
                                                  <?php echo '"'.$value->bulan.'"';?>,
                                                  <?php break 1; ?>
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                        <?php foreach ($data['lastmile_durasi_gangguan'] as $value) :?>
                                                {
                                                  label: <?php echo '"'.$value->source_category.'"';?>,
                                                  fill: false,
                                                  backgroundColor: "#"+Math.floor(Math.random()*16777215).toString(16),
                                                  data: [
                                                          <?php if($value->durasi!=0): echo $value->durasi; else: echo 0; endif;?>
                                                        ]
                                                },
                                        <?php endforeach; ?>
                                      ]
                                      },
                                      options: {
                                        title: {
                                          display: true,
                                          text: 'Gangguan Lastmile (Durasi - Menit)'
                                        },
                                        scales: {
                                          yAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                      }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 14-->

    <!--START CHART 16-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="bar-chart16" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart16"), getChartJs16('bar'));
                        });

                        function getChartJs16(type) {
                            var config = null;

                                config = {
                                    type: 'horizontalBar',
                                    data: {
                                      labels: [ <?php foreach ($data['root_power_supply_pop'] as $value) :?>
                                                  <?php echo '"'.$value->root_cause_name.'"';?>,
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                                  {
                                                    label: "Jumlah",
                                                    backgroundColor: [
                                                          <?php foreach ($data['root_power_supply_pop'] as $value) :?>
                                                              "#"+Math.floor(Math.random()*16777215).toString(16),
                                                          <?php endforeach; ?>
                                                      ],
                                                    data: [
                                                          <?php foreach ($data['root_power_supply_pop'] as $value) :?>
                                                              <?php if($value->jml!=0): echo $value->jml; else: echo 0; endif;?>,
                                                          <?php endforeach; ?>
                                                    ]
                                                  },
                                      ]
                                    },
                                    options: {
                                      legend: { display: false },  
                                      title: {
                                        display: true,
                                        text: 'Root Cause Gangguan Power Supply POP'
                                      },
                                        scales: {
                                          xAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                    }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 16-->

    <!--START CHART 18-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="bar-chart18" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart18"), getChartJs18('bar'));
                        });

                        function getChartJs18(type) {
                            var config = null;

                                config = {
                                    type: 'horizontalBar',
                                    data: {
                                      labels: [ <?php foreach ($data['root_fot_pop'] as $value) :?>
                                                  <?php echo '"'.$value->root_cause_name.'"';?>,
                                                <?php endforeach; ?>
                                              ],
                                      datasets: [
                                                  {
                                                    label: "Jumlah",
                                                    backgroundColor: [
                                                          <?php foreach ($data['root_fot_pop'] as $value) :?>
                                                              "#"+Math.floor(Math.random()*16777215).toString(16),
                                                          <?php endforeach; ?>
                                                      ],
                                                    data: [
                                                          <?php foreach ($data['root_fot_pop'] as $value) :?>
                                                              <?php if($value->jml!=0): echo $value->jml; else: echo 0; endif;?>,
                                                          <?php endforeach; ?>
                                                    ]
                                                  },
                                      ]
                                    },
                                    options: {
                                      legend: { display: false },  
                                      title: {
                                        display: true,
                                        text: 'Root Cause Perangkat POP'
                                      },
                                        scales: {
                                          xAxes: [{
                                              ticks: {
                                                  beginAtZero: true
                                              }
                                          }]
                                        }
                                    }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 18-->

    <!--START CHART 20-->
        <div class="card ">
          <div class=" container-fluid   container-fixed-lg">
            <div class="card-body">
                 <canvas id="bar-chart-grouped20" height="150"></canvas>
                    <script type="text/javascript">
                        $(function () {
                            new Chart(document.getElementById("bar-chart-grouped20"), getChartJs20('bar-chart-grouped'));
                        });

                        function getChartJs20(type) {
                            var config = null;

                                config = {
                                    type: 'horizontalBar',
                                    data: {
                                      labels: ["POP_1", "POP_2"],
                                      datasets: [
                                        {
                                          label: "FOT",
                                          backgroundColor: "#3e95cd",
                                          data: [133, 500]
                                        }, {
                                          label: "FOC",
                                          backgroundColor: "#8e5ea2",
                                          data: [408, 200]
                                        },
                                        {
                                          label: "PS",
                                          backgroundColor: "#E9967A",
                                          data: [420, 300]
                                        }
                                      ]
                                    },
                                    options: {
                                      title: {
                                        display: true,
                                        text: 'Site & Gangguan Power Suppy POP'
                                      },
                                      scales: {
                                          xAxes: [{
                                              stacked: true
                                          }],
                                          yAxes: [{
                                              stacked: true
                                          }]
                                      }
                                    }
                                }
                            
                            return config;
                        }
                    </script>
              </div>
          </div>
        </div>
    <!--START CHART 20-->
    </div>
    




    </div>
</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>



