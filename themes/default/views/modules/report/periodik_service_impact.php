<?php
      $date     = date('Y-m-d');
      $date1    = str_replace('-', '/', $date);
      $datefrom = date('Y-m-d',strtotime($date1 . "-6 days"));
      $dateto   = date('Y-m-d');
      $datepnoc = date('m-Y');
?>

<!-- START JUMBOTRON -->
<div class="jumbotron">
  <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <div class="row">
        
      </div>
      
      <!-- mulai content table-->
      <div class="row">
        <div class="col-md-12">
          <!-- start card  -->
          <div class="card" style="width: 100%;">
            <div class="card-header">
              <div class="card-title">
                Service Impact Incident
              </div>
              <!-- <div class="pull-right">
                <div class="col-xs-12">
                  <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
                </div>
              </div> -->
            </div>
            <!-- card body -->
            <div class="card-body">
              
              <div class="table-responsive">
                <table class="table table-hover table-condensed" id="table-service-impact">
                  <thead>
                    <tr>
                      <th style="width:50px">No.</th>
                      <th>Incident ID</th>
                      <th>Description</th>
                      <th>Cause</th>
                      <th>Hirarchy</th>
                      <th>Notification</th>
                      <th>TTR</th>
                      <th>Service</th>
                    </tr>
                  </thead>
                  <tbody id="table">
                  </tbody>
                </table>
              </div>
              
            </div>
            <!-- end card body -->
          </div>
          <!-- end card -->
        </div>
      </div>
      <!-- akhir content table -->
    </div>
  </div>
</div>
<!-- END JUMBOTRON -->


<!-- START MODAL FILTER -->
<div class="modal fade slide-up" id="modalSelector" role="dialog" aria-hidden="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content-wrapper">
      <!-- mod-modal -->
      <div class="modal-content mod-modal">
        <div class="modal-header">
          <h5>Filter <span class="semi-bold"></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
        </div>
        <!-- start form -->
        <form class="form_filter">
          <div class="modal-body m-t-20">
            <!-- start fied form -->
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Start Date :</label>
              <div class="col-sm-10">
                <div class="input-group date">
                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $datefrom; ?>">
                  <div class="input-group-append ">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">End Date :</label>
              <div class="col-sm-10">
                <div class="input-group date">
                  <input type="text" class="form-control" id="end_date" name="end_date" value="<?php echo $dateto; ?>">
                  <div class="input-group-append ">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Network Type :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="network_type" name="network_type">
                  <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="ip">IP</option>
                  <option value="tdm">TDM</option>
                  <option value="vsat">VSAT</option>
                  <option value="infra">INFRA</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Network Hirarchy :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="network_hierarchy" name="network_hierarchy" >
                  <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="backbone">Backbone</option>
                  <option value="distribution">Distribution</option>
                  <option value="access">Access</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Incident Clasification :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="classification" name="classification">
                  <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="not_incident">Not Incident</option>
                  <option value="other">Other</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Notification Type :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="notification" name="notification">
                <option value=""></option>
                  <option value="all">All</option>
                  <option value="proaktive" selected>Proactive</option>
                  <option value="reactive">Reactive</option>
                </select>
              </div>
            </div>
            <!-- end of fields form -->
          </div>
          <!-- end modal body -->
          <!-- modal footer -->
          <div class="modal-footer">
            <button class="btn btn-sm btn-complete" type="submit">Apply</button>
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
          </div>
          <!-- end modal footer -->
        </form>
        <!-- end form -->
      </div>
      <!-- endof mod-modal -->
    </div>
    <!-- /.modal-content -->
  </div>
</div>
<!-- END MODAL  -->

