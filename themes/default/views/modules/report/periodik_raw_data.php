<?php
$date     = date('Y-m-d');
$date1    = str_replace('-', '/', $date);
$datefrom = date('Y-m-d', strtotime($date1 . "-6 days"));
$dateto   = date('Y-m-d');
$datepnoc = date('m-Y');
?>

<!-- START JUMBOTRON -->
<div class="jumbotron">
  <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <div class="row">

      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="card" style="width: 100%;">
            <div class="card-header">
              <div class="card-title">
                Rawdata
              </div>
            </div>
            <div class="card-body">
              <form class="form-horizontal form_rawdata" action="<?php echo base_url() ?>report/rawdata_incident" method="POST">
                <h5>1. Report Type:</h5>
                <div class="row">
                  <div class="col-md-2">
                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="rawdata_incident" name="rawdata" class="custom-control-input" value="rawdata_incident">
                      <label class="custom-control-label" for="rawdata_incident">Rawdata Incident PNOC</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="rawdata_incident_sbu" name="rawdata" class="custom-control-input" value="rawdata_incident_sbu">
                      <label class="custom-control-label" for="rawdata_incident_sbu">Rawdata Incident SBU</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="summary_gangguan" name="rawdata" class="custom-control-input" value="summary_gangguan">
                      <label class="custom-control-label" for="summary_gangguan">Summary Gangguan Terlama</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="open_incident" name="rawdata" class="custom-control-input" value="open_incident">
                      <label class="custom-control-label" for="open_incident">Open Incident</label>
                    </div>
                  </div>

                  <div class="col-md-2">

                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="inventory_change" name="rawdata" class="custom-control-input" value="inventory_change">
                      <label class="custom-control-label" for="inventory_change">Inventory Change</label>
                    </div>

                    <br>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="PNOC" name="rawdata" class="custom-control-input" value="pnoc_presence">
                      <label class="custom-control-label" for="PNOC">PNOC Presence</label>
                    </div>

                    <br>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="kpi" name="rawdata" class="custom-control-input" value="rawdata_incident_kpi">
                      <label class="custom-control-label" for="kpi">KPI Incidents</label>
                    </div>

                    <br>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="resume_incident" name="rawdata" class="custom-control-input" value="resume_incident">
                      <label class="custom-control-label" for="resume_incident">Resume Incident</label>
                    </div>

                  </div>

                  <div class="col-md-2">
                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="detail_kpi" name="rawdata" class="custom-control-input" value="rawdata_incident_kpi_process">
                      <label class="custom-control-label" for="detail_kpi">Detail KPI Incidents</label>
                    </div>
                    <br>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="rawdata_ticket_incident" name="rawdata" class="custom-control-input" value="rawdata_ticket_incident">
                      <label class="custom-control-label" for="rawdata_ticket_incident">Ticket Incidents</label>
                    </div>
                    <br>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input required type="radio" id="service_incident" name="rawdata" class="custom-control-input" value="service_incident">
                      <label class="custom-control-label" for="service_incident">Service Incident</label>
                    </div>
                  </div>

                  <!-- <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="kpi" name="rawdata" class="custom-control-input" value="rawdata_incident_kpi">
                    <label class="custom-control-label" for="kpi">KPI Incidents</label>
                  </div> -->


                  <!-- <div class="col-md-3">
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="kpi" name="rawdata" class="custom-control-input" value="rawdata_incident_kpi">
                    <label class="custom-control-label" for="kpi">KPI Incidents</label>
                  </div>
                  <br>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="sbu" name="rawdata" class="custom-control-input" value="rawdata_incident_group_sbu">
                    <label class="custom-control-label" for="sbu">KPI Incidents SBU</label>
                  </div>
                  <br>
                  <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="serpo" name="rawdata" class="custom-control-input" value="rawdata_incident_group_serpo">
                    <label class="custom-control-label" for="serpo">KPI Incidents SERPO</label>
                  </div>
                </div> -->

                </div>

                <div id="data_periode">
                  <h5>2. Data Periode:</h5>
                </div>
                <div id="pnoc" style="display: none;">
                  <h5>2. Mount:</h5>
                  <div class="col-md-2">
                    <div class="input-group date">
                      <input type="text" class="form-control" id="datepicker-component" value="<?= $datepnoc ?>" name="month">
                      <div class="input-group-append ">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row" id="form-input">
                  <div class="col-md-2" id="from">

                    <p>from:</p>
                    <div class="input-group date">
                      <input type="text" class="form-control" id="datepicker-start" name="start_date" value="<?= $datefrom ?>">
                      <div class="input-group-append ">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2" id="to">
                    <p>to:</p>
                    <div class="input-group date">
                      <input type="text" class="form-control" id="datepicker-end" name="end_date" value="<?= $dateto ?>">
                      <div class="input-group-append ">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                  </div>
                </div>
                <br>
                <button type="submit" class="btn btn-complete export">Export</button>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END JUMBOTRON -->