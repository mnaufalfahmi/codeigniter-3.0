<?php
      $date     = date('Y-m-d');
      $date1    = str_replace('-', '/', $date);
      $datefrom = date('Y-m-d',strtotime($date1 . "-6 days"));
      $dateto   = date('Y-m-d');
      $datepnoc = date('m-Y');
?>

<!-- START JUMBOTRON -->
<div class="jumbotron">
  <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <div class="row">
        
      </div>
      
      <!-- mulai content table-->
      <div class="row">
        <div class="col-md-12">
          <!-- start card  -->
          <div class="card" style="width: 100%;">
            <div class="card-header">
              <div class="card-title">
                Regional MTTR
              </div>
              <!-- <button class="btn btn-primary btn-sm" style="float: right;" data-toggle="modal" data-target="#modalSelector"><i class="fa fa-filter"></i></button> -->
            </div>
            <!-- card body -->
            <div class="card-body">
              
              <div class="table-responsive">
                <table class="table table-hover table-condensed" id="table-region-mttr" style="width:100%">
                  <thead>
                    <tr>
                      <th style="width:50px">No.</th>
                      <th>Region</th>
                      <th>FOT</th>
                      <th>FOT</th>
                      <th>PS</th>
                    </tr>
                  </thead>
                  <tbody id="table">
                  </tbody>
                </table>
              </div>
              
            </div>
            <!-- end card body -->
          </div>
          <!-- <div class="card" style="width: 100%;">
            <div class="card-header">
              <div class="card-title">
                Regional MTTR
              </div>
            </div>
            <div class="card-body">
              
              <div id="container" ></div>
              
            </div>
          </div> -->
          <!-- end card -->
        </div>
      </div>
      <!-- akhir content table -->
    </div>
  </div>
</div>
<!-- END JUMBOTRON -->


<!-- START MODAL FILTER -->
<div class="modal fade slide-up" id="modalSelector" role="dialog" aria-hidden="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content-wrapper">
      <!-- mod-modal -->
      <div class="modal-content mod-modal">
        <div class="modal-header">
          <h5>Filter <span class="semi-bold"></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
        </div>
        <!-- start form -->
        <form class="form_filter">
          <div class="modal-body m-t-20">
            
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Start Date :</label>
              <div class="col-sm-10">
                <div class="input-group date">
                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $datefrom; ?>">
                  <div class="input-group-append ">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">End Date :</label>
              <div class="col-sm-10">
                <div class="input-group date">
                  <input type="text" class="form-control" id="end_date" name="end_date" value="<?php echo $dateto; ?>">
                  <div class="input-group-append ">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">MTTR Result :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="mttr_result" name="mttr_result">
                  <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="achieved">Achieved</option>
                  <option value="failed">Failed</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Notification Type :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="type" name="type">
                <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="proaktive" selected>Proactive</option>
                  <option value="reactive">Reactive</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Service Impact :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="service_impact" name="service_impact">
                  <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="yes">Yes</option>
                  <option value="not">No</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Third Party :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="third_party" name="third_party">
                  <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="yes">Yes</option>
                  <option value="not">No</option>
                </select>
              </div>
            </div>
            
          </div>
          <!-- end modal body -->
          <!-- modal footer -->
          <div class="modal-footer">
            <button class="btn btn-sm btn-complete" type="submit">Apply</button>
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
          </div>
          <!-- end modal footer -->
        </form>
        <!-- end form -->
      </div>
      <!-- endof mod-modal -->
    </div>
    <!-- /.modal-content -->
  </div>
</div>
<!-- END MODAL  -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>