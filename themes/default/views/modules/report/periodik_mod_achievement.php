<?php
  $date     = date('Y-m-d');
  $date1    = str_replace('-', '/', $date);
  $datefrom = date('Y-m-d',strtotime($date1 . "-6 days"));
  $dateto   = date('Y-m-d');
  $datepnoc = date('m-Y');
?>

<!-- START JUMBOTRON -->
<div class="jumbotron">
  <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <div class="row">
        
      </div>
      
      <!-- mulai content table-->
      <div class="row">
        <div class="col-md-12">
          <!-- start card  -->
          <div class="card" style="width: 100%;">
            <div class="card-header">
              <div class="card-title">
                MOD Achievement
              </div>
            </div>
            <!-- card body -->
            <div class="card-body">
              
              <div class="table-responsive">
                <table class="table table-hover table-condensed" id="mod-ach" style="width:100%">
                  <thead>
                    <tr>
                      <th rowspan="2" style="width:50px">No.</th>
                      <th rowspan="2">MOD Name </th>
                      <th rowspan="2">Respon Time</th>
                      <th rowspan="2">Analisis Time</th>
                      <th colspan="2">Good Closure </th>
                      <th colspan="2">Selft Closure </th>
                      <th colspan="2">Close Closure </th>
                      <th rowspan="2">Unattach Ticket</th>
                      <th rowspan="2">Unattach ICR</th>
                    </tr>
                    <tr>
                        <th>Number</th>
                        <th>Ratio(%)</th>
                        <th>Number</th>
                        <th>Ratio(%)</th>
                        <th>Number</th>
                        <th>Ratio(%)</th>
                    </tr>
                  </thead>
                  <tbody id="table">
                  </tbody>
                </table>
              </div>
              
            </div>
            <!-- end card body -->
          </div>
          <!-- end card -->
        </div>
      </div>
      <!-- akhir content table -->
    </div>
  </div>
</div>
<!-- END JUMBOTRON -->


<!-- START MODAL FILTER -->
<div class="modal fade slide-up" id="modalSelector" role="dialog" aria-hidden="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content-wrapper">
      <!-- mod-modal -->
      <div class="modal-content mod-modal">
        <div class="modal-header">
        <h5>Filter <span class="semi-bold"></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
        </div>
        <!-- start form -->
        <form class="form_filter">
          <div class="modal-body m-t-20">
            
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Start Date :</label>
              <div class="col-sm-10">
                <div class="input-group date">
                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $datefrom; ?>">
                  <div class="input-group-append ">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">End Date :</label>
              <div class="col-sm-10">
                <div class="input-group date">
                  <input type="text" class="form-control" id="end_date" name="end_date" value="<?php echo $dateto; ?>">
                  <div class="input-group-append ">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Shift :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" name="shift" id="shift">
                  <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="11">Pagi</option>
                  <option value="12">Siang</option>
                  <option value="13">Malam</option>
                </select>
              </div>
            </div>  
          </div>
          <!-- end modal body -->
          <!-- modal footer -->
          <div class="modal-footer">
            <button class="btn btn-sm btn-complete">Apply</button>
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
          </div>
          <!-- end modal footer -->
        </form>
        <!-- end form -->
      </div>
      <!-- endof mod-modal -->
    </div>
    <!-- /.modal-content -->
  </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL VIEW PERSONNEL -->
<div class="modal fade slide-up" id="modalViewPersonnel" role="dialog" aria-hidden="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content-wrapper">
      <!-- mod-modal -->
      <div class="modal-content mod-modal">
        <div class="modal-header">
        <h5>Personnel <span class="semi-bold">List</span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
        </div>
          <!-- start modal body -->
          <div class="modal-body m-t-20">

            <div class="table-responsive">
              <table class="table table-hover table-condensed" id="personil-list">
                <thead>
                  <tr>
                    <th style="width:20px">No.</th>
                    <th style="width:30px">Shift Date</th>
                    <th style="width:30px">Shift Turn</th>
                    <th style="width:30px">Full name</th>
                    <th style="width:30px">MOD</th>
                  </tr>
                </thead>
              </table>
            </div>  
            
            
          </div>
          <!-- end modal body -->
          <!-- modal footer -->
          <div class="modal-footer">
            <!-- <button class="btn btn-sm btn-complete">Apply</button>
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button> -->
          </div>
          <!-- end modal footer -->
        
      </div>
      <!-- endof mod-modal -->
    </div>
    <!-- /.modal-content -->
  </div>
</div>
<!-- END MODAL  -->
