<?php
      $date     = date('Y-m-d');
      $date1    = str_replace('-', '/', $date);
      $datefrom = date('Y-m-d',strtotime($date1 . "-6 days"));
      $dateto   = date('Y-m-d');
      $dateyear = date('Y');
?>

<!-- START JUMBOTRON -->
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <div class="row">
                
            </div>

            <!-- mulai content table-->
            <div class="row">
                <div class="col-md-12">
                    <!-- start card -->
                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="card-title"> MTTR Achievement </div>
                            <button class="btn btn-primary btn-sm" style="float: right;" data-toggle="modal" data-target="#modalSelector"><i class="fa fa-filter"></i></button>
                           
                        </div>
                        <!-- card body -->
                        <div class="card-body">
                            <!-- start row -->
                            <div class="row">
                                <!-- col-md-6 -->
                                <div class="col-md-4">
                                    <h6>MTTR Achievement</h6>
                                    <div class="table-responsive">
                                        <table class="table table-condensed table-hover" id="mttr_achievement">
                                            <thead>
                                                <tr>
                                                    <th>Week</th>
                                                    <th>Achievement</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table-mttr">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- end col-md-6 -->

                                <div class="col-md-8">
                                    <div id="container" style="height: 300px; width: 100%;"></div>
                                </div>

                            </div>
                            <!-- end row -->

                            <div class="row">
                                <!-- col-md-6 -->
                                <div class="col-md-4">
                                    <h6>Service Problem</h6>
                                    <div class="table-responsive">
                                        <table class="table table-condensed table-hover" id="service_problem">
                                            <thead>
                                                <tr>
                                                    <th>Week</th>
                                                    <th>Problem</th>
                                                </tr>
                                            </thead>
                                            <tbody id="table-service">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div id="container2" style="height: 300px; width: 100%;"></div>
                                </div>
                                <!-- end col-md-6 -->
                            </div>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->

<!-- START MODAL FILTER -->
<div
    class="modal fade slide-up"
    id="modalSelector"
    role="dialog"
    aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Filter
                        <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form class="form_filter">
                    <div class="modal-body m-t-20">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Year :</label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input
                                        type="text"
                                        class="form-control"
                                        id="year"
                                        name="year"
                                        value="<?php echo $dateyear; ?>">
                                    <div class="input-group-append ">
                                        <span class="input-group-text">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Analisis</label>
                            <div class="radio radio-success col-md-9">
                                <input type="radio" value="WA" name="analisis" id="weekly_average" checked="checked">
                                <label for="weekly_average">Weekly Average</label>
                                <input type="radio" value="YTD" name="analisis" id="year_to_date">
                                <label for="year_to_date">Year to Date</label>
                            </div>
                        </div>

                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-complete" type="submit">Apply</button>
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL -->


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>