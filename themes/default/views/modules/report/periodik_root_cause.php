<?php
      $date     = date('Y-m-d');
      $date1    = str_replace('-', '/', $date);
      $datefrom = date('Y-m-d',strtotime($date1 . "-6 days"));
      $dateto   = date('Y-m-d');
      $datepnoc = date('m-Y');
?>

<!-- START JUMBOTRON -->
<div class="jumbotron">
  <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <div class="row">
       
      </div>
      
      <!-- mulai content table-->
      <div class="row">
        <div class="col-md-12">
          <!-- start card  -->
          <div class="card" style="width: 100%;">
            <div class="card-header">
              <div class="card-title">
                Regional Detail Clasification
              </div>
            </div>
            <!-- card body -->
            <div class="card-body">
              
              <div class="table-responsive">
                <table class="table table-hover table-condensed" id="table-root-cause" style="width:100%">
                  <thead>
                    <tr>
                      <th style="width:50px">No.</th>
                      <th>Detail Clasification</th>
                      <th>JKT</th>
                      <th>JBR</th>
                      <th>JTG</th>
                      <th>JTM</th>
                      <th>BLI</th>
                      <th>SBU</th>
                      <th>SBT</th>
                      <th>SBS</th>
                      <th>IBT</th>
                      <th>OPS</th>
                      <th>CORP</th>
                    </tr>
                  </thead>
                  <tbody id="table">
                  </tbody>
                </table>
              </div>
              
            </div>
            <!-- end card body -->
          </div>
          <!-- end card -->
        </div>
      </div>
      <!-- akhir content table -->
    </div>
  </div>
</div>
<!-- END JUMBOTRON -->


<!-- START MODAL FILTER -->
<div class="modal fade slide-up" id="modalSelector" role="dialog" aria-hidden="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content-wrapper">
      <!-- mod-modal -->
      <div class="modal-content mod-modal">
        <div class="modal-header">
          <h5>Filter <span class="semi-bold"></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
        </div>
        <!-- start form -->
        <form class="form_filter">
          <div class="modal-body m-t-20">
            
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Start Date :</label>
              <div class="col-sm-10">
                <div class="input-group date">
                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $datefrom; ?>">
                  <div class="input-group-append ">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">End Date :</label>
              <div class="col-sm-10">
                <div class="input-group date">
                  <input type="text" class="form-control" id="end_date" name="end_date" value="<?php echo $dateto; ?>">
                  <div class="input-group-append ">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">MTTR Result :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" name="mttr_result" id="mttr_result">
                  <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="achieved">Achieved</option>
                  <option value="failed">Failed</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Notification Type :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" name="type" id="type">
                  <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="proaktive" selected>Proactive</option>
                  <option value="reactive">Reactive</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Service Impact :</label>
              <div class="col-sm-10">
                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" name="service_impact" id="service_impact">
                  <option value=""></option>
                  <option value="all" selected>All</option>
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
                                    <label for="fname" class="col-md-2 col-form-label">Analisis</label>
                                    <div class="radio radio-success col-md-9">
                                        <input type="radio" value="count" name="analisis" id="Count" checked>
                                        <label for="Count">Count</label>
                                        <input type="radio" value="mttr" name="analisis" id="MTTR">
                                        <label for="MTTR">MTTR</label>
                                        <input type="radio" value="ticket" name="analisis" id="Ticket">
                                        <label for="Ticket">Ticket</label>
                                        <input type="radio" value="service" name="analisis" id="Service">
                                        <label for="Service">Service</label>
                                        
                                    </div>
                                </div>
            
            
          </div>
          <!-- end modal body -->
          <!-- modal footer -->
          <div class="modal-footer">
            <button class="btn btn-sm btn-complete" type="submit">Apply</button>
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
          </div>
          <!-- end modal footer -->
        </form>
        <!-- end form -->
      </div>
      <!-- endof mod-modal -->
    </div>
    <!-- /.modal-content -->
  </div>
</div>
<!-- END MODAL  -->
