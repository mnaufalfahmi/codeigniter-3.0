<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

<?php
      $date     = date('Y-m-d');
      $date1    = str_replace('-', '/', $date);
      $datefrom = date('Y-m-d',strtotime($date1 . "-6 days"));
      $dateto   = date('Y-m-d');
      $dateyear = date('Y');
?>

<!-- START JUMBOTRON -->
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <div class="row">
                
            </div>

            <!-- mulai content table-->
            <div class="row">
                <div class="col-md-12">
                    <!-- start card -->
                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="card-title">
                                Root Cause
                            </div>
                        </div>
                        <!-- card body -->
                        <div class="card-body">

                            <!-- tabel data -->
                            <div class="table-responsive">
                                <table
                                    class="table table-hover table-condensed"
                                    id="trending_root_cause"
                                    style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Detail Clasification</th>
                                            <th>Jan</th>
                                            <th>Feb</th>
                                            <th>Mar</th>
                                            <th>Apr</th>
                                            <th>May</th>
                                            <th>Jun</th>
                                            <th>Jul</th>
                                            <th>Aug</th>
                                            <th>Sep</th>
                                            <th>Okt</th>
                                            <th>Nov</th>
                                            <th>Dec</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table">
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->

<!-- START MODAL FILTER -->
<div
    class="modal fade slide-up"
    id="modalSelector"
    role="dialog"
    aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Filter
                        <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form class="form_filter">
                    <div class="modal-body m-t-20">

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Year :</label>
                            <div class="col-sm-10">
                                <input
                                    type="number"
                                    class="form-control"
                                    value="<?php echo $dateyear; ?>"
                                    name="year"
                                    id="year">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">MTTR Result :</label>
                            <div class="col-sm-10">
                                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" name="mttr_result" id="mttr_result">
                                    <option value=""></option>
                                    <option value="all" selected="selected">All</option>
                                    <option value="achived">Achived</option>
                                    <option value="failed">Failed</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="">Notification Type :</label>
                        <div class="col-sm-10">
                            <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="type" name="type">
                            <option value=""></option>
                            <option value="all">All</option>
                            <option value="proaktive" selected>Proactive</option>
                            <option value="reactive">Reactive</option>
                            </select>
                        </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Service Impact :</label>
                            <div class="col-sm-10">
                                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" name="service_impact" id="service_impact">
                                    <option value=""></option>
                                    <option value="all" selected="selected">All</option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Analisis :</label>
                            <div class="radio radio-success col-md-10">
                                <input type="radio" value="count" name="analisis" id="count" checked="checked">
                                <label for="count">Count</label>
                                <input type="radio" value="impact" name="analisis" id="impact">
                                <label for="impact">Impact</label>
                            </div>
                        </div>

                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-complete" type="submit">Apply</button>
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL -->

<script>
    baseurl = "<?php echo base_url('report/trending_list_root_cause') ?>"
</script>