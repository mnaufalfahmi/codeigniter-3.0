<?php
      $date     = date('Y-m-d');
      $date1    = str_replace('-', '/', $date);
      $datefrom = date('Y-m-d',strtotime($date1 . "-6 days"));
      $dateto   = date('Y-m-d');
      $dateyear = date('Y');
?>

<!-- START JUMBOTRON -->
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <div class="row">
                
            </div>

            <!-- mulai content table-->
            <div class="row">
                <div class="col-md-12">
                    <!-- start card -->
                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="card-title">
                                Response Time
                            </div>
                            
                        </div>
                        <!-- card body -->
                        <div class="card-body">

                            <!-- tabel data -->
                            <div class="table-responsive">
                                <table
                                    class="table table-hover table-condensed"
                                    id="trending_response_time"
                                    style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Week</th>
                                            <th>Response Time</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table">
                                    </tbody>
                                </table>
                            </div>
                            <!-- end of table data -->

                        </div>
                        <!-- end card body -->
                    </div>

                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="card-title">
                                Response Time
                            </div>
                            <!-- <div class="pull-right"> <div class="col-xs-12"> <input type="text"
                            id="search-table" class="form-control pull-right" placeholder="Search"> </div>
                            </div> -->
                        </div>
                        <!-- card body -->
                        <div class="card-body">

                            <!-- tabel data -->
                            <div id="container"></div>
                            <!-- end of table data -->

                        </div>
                        <!-- end card body -->
                    </div>

                    <!-- end card -->
                </div>
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->

<!-- START MODAL FILTER -->
<div
    class="modal fade slide-up"
    id="modalSelector"
    role="dialog"
    aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Selector
                        <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form class="form_filter">
                    <div class="modal-body m-t-20">

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Year :</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" value="<?php echo $dateyear;?>" name="year" id="year">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Ticket Clasification :</label>
                            <div class="col-sm-10">
                                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" name="ticket_classification" id="ticket_classification">
                                    <option value=""></option>
                                    <option value="all" selected="selected">All</option>
                                    <option value="ip">IP</option>
                                    <option value="tdm">TDM</option>
                                    <option value="vsat">VSAT</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="">Incident Clasification :</label>
                            <div class="col-sm-10">
                                <select class="full-width" data-init-plugin="select2" data-placeholder="Select" name="incident_classification" id="incident_classification">
                                    <option value=""></option>
                                    <option value="all" selected="selected">All</option>
                                    <option value="not incident">Not Incident</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-md-2 col-form-label">Analisis</label>
                            <div class="radio radio-success col-md-9">
                                <input
                                    type="radio"
                                    value="WA"
                                    name="analisis"
                                    id="weekly_average"
                                    checked="checked">
                                <label for="weekly_average">Weekly Average</label>
                                <input type="radio" value="YTD" name="analisis" id="year_to_date">
                                <label for="year_to_date">Year to Date</label>
                            </div>
                        </div>

                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-complete" type="submit">Apply</button>
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL -->

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>