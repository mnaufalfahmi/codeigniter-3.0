<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<style type="text/css">
#container {
  min-width: 310px;
  max-width: 1200px;
  height: 300px;
  margin: 0 auto
}
</style>

<?php
      $date     = date('Y-m-d');
      $date1    = str_replace('-', '/', $date);
      $datefrom = date('Y-m-d',strtotime($date1 . "-6 days"));
      $dateto   = date('Y-m-d');
      $datepnoc = date('m-Y');
?>

<!-- START JUMBOTRON -->
<div class="jumbotron">
  <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
      <!-- START BREADCRUMB -->
      <div class="row">
        <div class="col-md-10">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?php echo $data['title'] ?></li>
          </ol>
        </div>
        <!-- END BREADCRUMB -->
        <div class="col-md-2">
          <!-- button filter -->
          <div class="pull-right m-t-10">
            <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#modalSelector"><strong>Selector</strong></button>
          </div>                                
          <!-- end of button filter  -->
        </div>
      </div>
      
      <!-- mulai content table-->
      <div class="row">
        <div class="col-md-12">
          <!-- start card  -->
          <div class="card" style="width: 100%;">
            <div class="card-header">
              <div class="card-title">
                Analisis Time
              </div>
            </div>
            <!-- card body -->
            <div class="card-body">

              <div class="table-responsive">
                <table class="table table-hover table-condensed" id="utilization" style="width:100%">
                  <thead>
                    <tr>
                      <th width="5">Week</th>
                      <th width="5">Analisis Time </th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>

              <br>
              <br>
              <br>
              <div id="container"></div>
            </div>

            <!-- end card body -->
          </div>
          <!-- end card -->
        </div>
      </div>
      <!-- akhir content table -->
    </div>
  </div>
</div>
<!-- END JUMBOTRON -->


<!-- START MODAL FILTER -->
<div class="modal fade slide-up" id="modalSelector" role="dialog" aria-hidden="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content-wrapper">
      <!-- mod-modal -->
      <div class="modal-content mod-modal">
        <div class="modal-header">
          <h5>Selector <span class="semi-bold"></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
          </button>
        </div>
        <!-- start form -->
        <form>
          <div class="modal-body m-t-20">

            <div class="form-group row">
              <label class="col-sm-2 col-form-label" for="">Start Date :</label>
              <div class="col-sm-10">
                <div class="input-group date">
                  <input type="text" class="form-control" id="datepicker-component">
                  <div class="input-group-append ">
                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Region :</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="text" class="form-control" id="region">
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Contract :</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="text" class="form-control" id="region">
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Basecamp :</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <input type="text" class="form-control" id="region">
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="fname" class="col-md-2 col-form-label">Analisis :</label>
              <div class="radio radio-success col-md-10">
                <input type="radio" value="yes" name="low" id="low" checked>
                <label for="low">Weekly Average</label>
                <input type="radio" value="medium" name="low" id="medium">
                <label for="medium">Year to Date</label>
              </div>
            </div>

            
          </div>
          <!-- end modal body -->
          <!-- modal footer -->
          <div class="modal-footer">
            <button class="btn btn-sm btn-complete">Apply</button>
            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
          </div>
          <!-- end modal footer -->
        </form>
        <!-- end form -->
      </div>
      <!-- endof mod-modal -->
    </div>
    <!-- /.modal-content -->
  </div>
</div>
<!-- END MODAL  -->

<script>
  baseurl = "<?php echo base_url('report/get_trending_serpo_utilization') ?>"
</script>