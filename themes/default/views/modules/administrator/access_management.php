<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
        <div class="row">
            
        </div>

        <div class="card">
            <div class="card-header m-t-10">
                <div class="card-title">
                    Access Management 
                </div>
                <br>
            </div>
            <div class="card-body">
                <div class="card card-transparent ">
                   
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-fillup1">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-striped" id="access-management" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Role Code</th>
                                                <th>Description</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- show data -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalAddRole" tabindex="-1" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add <span class="semi-bold">Role</span></h5>
                </div>
                <div class="modal-body">
                    <form id="form-work" class="form-horizontal" role="form" method="post" action="<?php echo base_url('/lib/role') ?>" autocomplete="off">
                        <div class="row">
                            <div class="card card-transparent">
                              
                                <div class="card-body ">
                                    <div class="form-group">
                                        <label>Role Name</label>
                                        <!-- <span class="help">e.g. "Mona Lisa Portrait"</span> -->
                                        <input type="text" class="form-control" name="role_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Role Description</label>
                                        <!-- <span class="help">e.g. "Mona Lisa Portrait"</span> -->
                                        <input type="text" class="form-control" name="description" required>
                                    </div>
                                    <table class="table table-hover" id="basicTable">
                                        <thead>
                                            <tr>
                                                <th style="width:1%" class="text-center">
                                                    <div class="checkbox text-center">
                                                        <input type="checkbox" value="3" id="check_all">
                                                        <label for="check_all" class="no-padding no-margin"></label>
                                                    </div>
                                                </th>
                                                <th style="width:20%">Module</th>
                                                <th style="width:5%">Read</th>
                                                <th style="width:5%">Create</th>
                                                <th style="width:5%">Update</th>
                                                <th style="width:5%">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($data['modules'] as $module) { ?>
                                            <tr>
                                                <td class="v-align-middle">
                                                    <div class="checkbox text-center">
                                                        <input type="checkbox" value="<?= $module->id ?>" id="module_id_<?= $module->name ?>" name="module_id_<?= $module->name ?>">
                                                        <label for="module_id_<?= $module->name ?>" class="no-padding no-margin"></label>
                                                    </div>
                                                </td>
                                                <td class="v-align-middle">
                                                    <?= $module->name ?>
                                                </td>
                                                <td class="v-align-middle">
                                                    <div class="checkbox ">
                                                        <input type="checkbox" value="t" id="read_<?= $module->name ?>" name="read_<?= $module->name ?>">
                                                        <label for="read_<?= $module->name ?>"></label>
                                                    </div>
                                                </td>
                                                <td class="v-align-middle">
                                                    <div class="checkbox ">
                                                        <input type="checkbox" value="t" id="create_<?= $module->name ?>" name="create_<?= $module->name ?>">
                                                        <label for="create_<?= $module->name ?>"></label>
                                                    </div>
                                                </td>
                                                <td class="v-align-middle">
                                                    <div class="checkbox ">
                                                        <input type="checkbox" value="t" id="update_<?= $module->name ?>" name="update_<?= $module->name ?>">
                                                        <label for="update_<?= $module->name ?>"></label>
                                                    </div>
                                                </td>
                                                <td class="v-align-middle">
                                                    <div class="checkbox ">
                                                        <input type="checkbox" value="t" id="delete_<?= $module->name ?>" name="delete_<?= $module->name ?>">
                                                        <label for="delete_<?= $module->name ?>"></label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalEditRole" tabindex="-1" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Edit <span class="semi-bold">Role</span></h5>
                </div>
                <div class="modal-body">
                    <form id="form-work" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('/lib/role/update') ?>" autocomplete="off">
                        <div class="row">
                            <div class="card card-transparent">
                                <!-- <div class="card-header ">
                                    <div class="card-title">Role Details</div>
                                </div> -->
                                <div class="card-body ">
                                    <div class="form-group">
                                        <label>Role Name</label>
                                        <!-- <span class="help">e.g. "Mona Lisa Portrait"</span> -->
                                        <input type="text" class="form-control hidden" id="edit_role_id" name="edit_role_id" required>
                                        <input type="text" class="form-control" id="edit_role_name" name="edit_role_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Role Description</label>
                                        <!-- <span class="help">e.g. "Mona Lisa Portrait"</span> -->
                                        <input type="text" class="form-control" id="edit_description" name="edit_description" required>
                                    </div>
                                    <table class="table table-hover" id="basicTable">
                                        <thead>
                                            <tr>
                                                <th style="width:1%" class="text-center">
                                                    <div class="checkbox text-center">
                                                        <input type="checkbox" value="3" id="check_all">
                                                        <label for="check_all" class="no-padding no-margin"></label>
                                                    </div>
                                                </th>
                                                <th style="width:20%">Module</th>
                                                <th style="width:5%">Read</th>
                                                <th style="width:5%">Create</th>
                                                <th style="width:5%">Update</th>
                                                <th style="width:5%">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($data['modules'] as $module) { ?>
                                            <tr>
                                                <td class="v-align-middle">
                                                    <div class="checkbox text-center">
                                                        <input type="checkbox" value="<?= $module->id ?>" id="edit_module_id_<?= $module->name ?>" name="edit_module_id_<?= $module->name ?>">
                                                        <label for="edit_module_id_<?= $module->name ?>" class="no-padding no-margin"></label>
                                                    </div>
                                                </td>
                                                <td class="v-align-middle">
                                                    <?= $module->name ?>
                                                </td>
                                                <td class="v-align-middle">
                                                    <div class="checkbox ">
                                                        <input type="checkbox" value="t" id="edit_read_<?= $module->name ?>" name="edit_read_<?= $module->name ?>">
                                                        <label for="edit_read_<?= $module->name ?>"></label>
                                                    </div>
                                                </td>
                                                <td class="v-align-middle">
                                                    <div class="checkbox ">
                                                        <input type="checkbox" value="t" id="edit_create_<?= $module->name ?>" name="edit_create_<?= $module->name ?>">
                                                        <label for="edit_create_<?= $module->name ?>"></label>
                                                    </div>
                                                </td>
                                                <td class="v-align-middle">
                                                    <div class="checkbox ">
                                                        <input type="checkbox" value="t" id="edit_update_<?= $module->name ?>" name="edit_update_<?= $module->name ?>">
                                                        <label for="edit_update_<?= $module->name ?>"></label>
                                                    </div>
                                                </td>
                                                <td class="v-align-middle">
                                                    <div class="checkbox ">
                                                        <input type="checkbox" value="t" id="edit_delete_<?= $module->name ?>" name="edit_delete_<?= $module->name ?>">
                                                        <label for="edit_delete_<?= $module->name ?>"></label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalDeleteRole" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are you sure want to delete this role ?</span>
                    </div>
                    <div class="card-body">
                        <a id="delete"><button class="btn btn-danger">Delete</button></a>
                        <button class="btn btn-info" id="cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>