<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
        <div class="row">
            <div class="col-md-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active"><?=$data['title'] ?></li>
                </ol>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="card card-transparent ">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-fillup1">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-striped" id="access-group" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Name Sub Group</th>
                                                <th>Name Group Induk</th>
                                                <th>Type</th>
                                                <th>..</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- show data -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalAddRole" tabindex="-1" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add <span class="semi-bold">Roles Group</span></h5>
                </div>
                <div class="modal-body">
                    <form id="form-work" class="form-horizontal form_save" role="form" >
                        <div class="row">
                            <div class="card card-transparent">
                                <!-- <div class="card-header ">
                                    <div class="card-title">User Details</div>
                                </div> -->
                                <div class="card-body ">  
                                    <div class="form-group">
                                        <label>Group Name</label>
                                        <select class="full-width select2" name="reference_group_id" data-init-plugin="select2" id="reference_group_id">
                                            <option value="0">-</option>
                                            <?php foreach ($data['modules'] as $modules) { ?>
                                                <option value="<?= $modules->id ?>"><?= $modules->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Name Sub Group</label>
                                        <input type="text" name="name" class="form-control full-width">
                                    </div>
                                    <div class="form-group">
                                        <label>Code Sub Group</label>
                                        <input type="text" name="group_code" class="form-control full-width">
                                    </div>
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="full-width select2" name="type" data-init-plugin="select2">
                                            <option value="0">-</option>
                                            <option value="ANALYSE">ANALYSE</option>
                                            <option value="CONTACTCENTER">CONTACTCENTER</option>
                                            <option value="RECOVERY">RECOVERY</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalEditRole" tabindex="-1" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Edit <span class="semi-bold">Roles Group</span></h5>
                </div>
                <div class="modal-body">
                    <form id="form-work" class="form-horizontal form_edit" >
                        <div class="row">
                            <div class="card card-transparent">
                                <!-- <div class="card-header ">
                                    <div class="card-title">Role Details</div>
                                </div> -->
                                <div class="card-body ">
                                  <div class="form-group">
                                    <label>Group Name</label>
                                    <select class="full-width select2" name="edit_reference_group_id" data-init-plugin="select2" id="edit_reference_group_id">
                                        <option value="0">-</option>
                                        <?php foreach ($data['modules'] as $modules) { ?>
                                            <option value="<?= $modules->id ?>"><?= $modules->name ?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="hidden" name="id" id="id">
                                </div>
                                <div class="form-group">
                                    <label>Name Sub Group</label>
                                    <input type="text" name="edit_name" id="edit_name" class="form-control full-width">
                                </div>
                                <div class="form-group">
                                    <label>Code Sub Group</label>
                                    <input type="text" name="edit_group_code" id="edit_group_code" class="form-control full-width">
                                </div>
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="full-width select2" name="edit_type" id="edit_type" data-init-plugin="select2">
                                        <option value="0">-</option>
                                        <option value="ANALYSE">ANALYSE</option>
                                        <option value="CONTACTCENTER">CONTACTCENTER</option>
                                        <option value="RECOVERY">RECOVERY</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade slide-up" id="modalDeleteRole" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are you sure want to delete this role ?</span>
                    </div>
                    <div class="card-body">
                        <a id="delete"><button class="btn btn-danger">Delete</button></a>
                        <button class="btn btn-info" id="cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>