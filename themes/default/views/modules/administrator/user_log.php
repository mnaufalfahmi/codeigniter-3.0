<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
        <div class="row">
            
        </div>

        <div class="card">
            <div class="card-header m-t-10">
                <div class="card-title">
                    User Change Log 
                </div>
                <br>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover" id="user-management" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Change Date</th>
                            <th>Change Act</th>
                            <th>Change By</th>
                            <th>Old Email</th>
                            <th>New Email</th>
                            <th>Old Fullname</th>
                            <th>New Fullname</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- show data -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalAddUser" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add <span class="semi-bold">User</span></h5>
                </div>
                <div class="modal-body">

                    <form id="form-work" class="form-horizontal" method="post" action="<?= base_url('/administrator/user_management/user') ?>" role="form" autocomplete="off">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">User Details</div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="form-group row">
                                            <label for="username" class="col-md-3 control-label">Username</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="username" placeholder="" name="username" required>
                                                <p class="text-danger hidden" id="username-danger">username already exist</p>
                                                <p class="text-danger hidden" id="username-length-danger">username must be more than 6 characters</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Email</label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" id="email" placeholder="" name="email" required>
                                                <p class="text-danger hidden" id="email-danger">email already exist</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fullname" class="col-md-3 control-label">Fullname</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="fullname" placeholder="" name="fullname" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="bobot" class="col-md-3 control-label">Bobot</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="bobot" placeholder="" name="bobot">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="region" class="col-md-3 control-label">Region</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="select-region" name="region">
                                                    <?php foreach ($data['region'] as $region) { ?>
                                                    <option value="<?= $region->region_code ?>"><?= $region->region_name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="surveilance" class="col-md-3 control-label">Surveilance</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="select-surveilance" name="surveilance">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                    <option value="VSAT">VSAT</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="analyst" class="col-md-3 control-label">Analyst</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="select-analyst" name="analyst">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="mod" class="col-md-3 control-label">MOD</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="select-mod" name="mod">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="mod" class="col-md-3 control-label">Parent</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="select-parent" name="parent">
                                                    <option value="">-</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="display:none" id="child">
                                            <label for="mod" class="col-md-3 control-label">Child</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="select-child" name="child">
                                                    <option value="0">-</option>
                                                    <?php foreach ($data['child'] as $child) { ?>
                                                        <option value="<?= $child->id ?>"><?= $child->fullname ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group row">
                                            <label for="shift_group" class="col-md-3 control-label">Shift Group</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="fname" placeholder="" name="shift_group">
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">Roles</div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="checkbox check-success">
                                            <input type="checkbox" value="1" id="check_all" onclick="checkAll()">
                                            <label for="check_all">Check All</label>
                                        </div>
                                        <?php foreach ($roles as $role) { ?>
                                        <div class="checkbox check-success">
                                            <input type="checkbox" value="<?= $role->id ?>" name="<?= $role->name ?>" id="<?= $role->name ?>">
                                            <label for="<?= $role->name ?>"><?= $role->name ?></label>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Create User</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade slide-up" id="modalEditUser" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Edit <span class="semi-bold">User</span></h5>
                </div>
                <div class="modal-body">
                    
                    <form id="form-work" class="form-horizontal" role="form" method="post" action="<?php echo base_url('user/update') ?>" autocomplete="off">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">User Details</div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="form-group row">
                                            <label for="username" class="col-md-3 control-label">Username</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control hidden" id="user_id" placeholder="" name="id">
                                                <input type="text" class="form-control" id="edit_username" placeholder="" name="username" required>
                                                <p class="text-danger hidden" id="edit_username-danger">username already exist</p>
                                                <p class="text-danger hidden" id="edit_username-length-danger">username must be more than 6 characters</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Email</label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" id="edit_email" placeholder="" name="email" required>
                                                <p class="text-danger hidden" id="edit_email-danger">email already exist</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fullname" class="col-md-3 control-label">Fullname</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="edit_fullname" placeholder="" name="fullname" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="bobot" class="col-md-3 control-label">Bobot</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="edit_bobot" placeholder="" name="bobot">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="region" class="col-md-3 control-label">Region</label>
                                            <div class="col-md-9">
                                                <select class="full-width edit_region" data-init-plugin="select2" id="edit_select-region" name="region">
                                                    <!-- <option>--</option> -->
                                                    <?php foreach ($data['region'] as $region) { ?>
                                                        <option value="<?= $region->region_code ?>"><?= $region->region_name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="surveilance" class="col-md-3 control-label">Surveilance</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="edit_select-surveilance" name="surveilance">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                    <option value="VSAT">VSAT</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="analyst" class="col-md-3 control-label">Analyst</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="edit_select-analyst" name="analyst">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="mod" class="col-md-3 control-label">MOD</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="edit_select-mod" name="mod">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="edit-parent">
                                            <label for="mod" class="col-md-3 control-label">Parent</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="edit-select-parent" name="editparent">
                                                    <option value="">-</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="display:none" id="edit-child">
                                            <label for="mod" class="col-md-3 control-label">Child</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" multiple="multiple" id="edit-select-child" name="editchild">
                                                    <option value="0">-</option>
                                                    <?php foreach ($data['child'] as $child) { ?>
                                                        <option value="<?= $child->id ?>"><?= $child->fullname ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group row">
                                            <label for="shift_group" class="col-md-3 control-label">Shift Group</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="edit_shift_group" placeholder="" name="shift_group">
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">Roles</div>
                                    </div>
                                    <div class="card-body ">
                                        <fieldset>
                                            <div class="checkbox check-success">
                                                <input type="checkbox" id="check_all" onClick="checkAll()">
                                                <label for="check_all">Check All</label>
                                            </div>
                                            <?php foreach ($roles as $role) { ?>
                                            <div class="checkbox check-success">
                                                <input type="checkbox" value="<?= $role->id ?>" name="<?= $role->name ?>" id="edit_<?= $role->name ?>">
                                                <label for="edit_<?= $role->name ?>"><?= $role->name ?></label>
                                            </div>
                                            <?php } ?>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalDeleteUser" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are you sure want to delete this user ?</span>
                    </div>
                    <div class="card-body">
                        <a id="delete"><button class="btn btn-danger">Delete</button></a>
                        <button class="btn btn-info" id="cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- end modal -->