<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
        <div class="row">

        </div>

        <div class="card">
            <div class="card-header m-t-10">
                <div class="card-title">
                    User Management
                </div><br>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover" id="user-management" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Full Name</th>
                            <th>Email</th>
                            <th>Group</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- show data -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalAddUser" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add <span class="semi-bold">User</span></h5>
                </div>
                <div class="modal-body">

                    <form id="form-work" class="form-horizontal" method="post" action="<?= base_url('/administrator/user_management/user') ?>" role="form" autocomplete="off">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">User Details</div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="form-group row">
                                            <label for="username" class="col-md-3 control-label">Username</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="username" placeholder="" name="username" required>
                                                <p class="text-danger hidden" id="username-danger">username already exist</p>
                                                <p class="text-danger hidden" id="username-length-danger">username must be more than 6 characters</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Email</label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" id="email" placeholder="" name="email" required>
                                                <p class="text-danger hidden" id="email-danger">email already exist</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fullname" class="col-md-3 control-label">Fullname</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="fullname" placeholder="" name="fullname" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="bobot" class="col-md-3 control-label">Bobot</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="bobot" placeholder="" name="bobot">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="surveilance" class="col-md-3 control-label">Surveilance</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="select-surveilance" name="surveilance">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                    <option value="VSAT">VSAT</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="analyst" class="col-md-3 control-label">Analyst</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="select-analyst" name="analyst">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="mod" class="col-md-3 control-label">MOD</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="select-mod" name="mod">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="mod" class="col-md-3 control-label">REGION</label>
                                            <div class="col-md-9">
                                                <select required class="full-width select_region" data-init-plugin="select2" name="region_code">
                                                    <option value="">-</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="mod" class="col-md-3 control-label">Parent</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="select-parent" name="parent">
                                                    <option value="">-</option>
                                                    <option value="1">Yes</option>
                                                    <option selected value="0" selected>No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="display:none" id="child">
                                            <label for="mod" class="col-md-3 control-label">Child</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" multiple="multiple" id="select-child" name="child[]">
                                                    <option value="0">-</option>
                                                    <?php foreach ($data['child'] as $child) { ?>
                                                        <option value="<?= $child->id ?>"><?= $child->fullname ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="group" class="col-md-3 control-label">Group</label>
                                            <div class="col-md-9">
                                                <select required class="full-width select2 select-group" name="group_id" data-init-plugin="select2">
                                                    <option value="">-</option>
                                                    <?php foreach ($data['modules'] as $modules) { ?>
                                                        <option value="<?= $modules->id ?>"><?= $modules->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="field_password" style="display:none">
                                            <label for="group" class="col-md-3 control-label">Password</label>
                                            <div class="col-md-9">
                                                <input type="password" class="form-control" id="password" placeholder="" name="password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">Roles</div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="checkbox check-success">
                                            <input type="checkbox" value="1" id="check_all" onclick="checkAll()">
                                            <label for="check_all">Check All</label>
                                        </div>
                                        <?php foreach ($roles as $role) { ?>
                                            <div class="checkbox check-success">
                                                <input type="checkbox" value="<?= $role->id ?>" name="<?= $role->name ?>" id="<?= $role->name ?>">
                                                <label for="<?= $role->name ?>"><?= $role->name ?></label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button id="submit-add" type="submit" class="btn btn-primary">Create User</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade slide-up" id="modalEditUser" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Edit <span class="semi-bold">User</span></h5>
                </div>
                <div class="modal-body">

                    <form id="form-work" class="form-horizontal" role="form" method="post" action="<?php echo base_url('user/update') ?>" autocomplete="off">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">User Details</div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="form-group row">
                                            <label for="username" class="col-md-3 control-label">Username</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control hidden" id="user_id" placeholder="" name="id">
                                                <input type="text" class="form-control" id="edit_username" placeholder="" name="username" required>
                                                <p class="text-danger hidden" id="edit_username-danger">username already exist</p>
                                                <p class="text-danger hidden" id="edit_username-length-danger">username must be more than 6 characters</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Email</label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" id="edit_email" placeholder="" name="email" required>
                                                <input type="hidden" name="old_edit_fullname" id="old_edit_fullname">
                                                <input type="hidden" name="old_edit_email" id="old_edit_email">
                                                <input type="hidden" name="old_password" id="old_password">
                                                <p class="text-danger hidden" id="edit_email-danger">email already exist</p>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fullname" class="col-md-3 control-label">Fullname</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="edit_fullname" placeholder="" name="fullname" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="bobot" class="col-md-3 control-label">Bobot</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="edit_bobot" placeholder="" name="bobot">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="surveilance" class="col-md-3 control-label">Surveilance</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="edit_select-surveilance" name="surveilance">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                    <option value="VSAT">VSAT</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="analyst" class="col-md-3 control-label">Analyst</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="edit_select-analyst" name="analyst">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="mod" class="col-md-3 control-label">MOD</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="edit_select-mod" name="mod">
                                                    <option value="">-</option>
                                                    <option value="IP">IP</option>
                                                    <option value="TDM">TDM</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="mod" class="col-md-3 control-label">REGION</label>
                                            <div class="col-md-9">
                                                <select required class="full-width select_region" data-init-plugin="select2" name="region_code">
                                                    <option value="">-</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="edit-parent">
                                            <label for="mod" class="col-md-3 control-label">Parent</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" id="edit-select-parent" name="editparent">
                                                    <option value="">-</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="display:none" id="edit-child">
                                            <label for="mod" class="col-md-3 control-label">Child</label>
                                            <div class="col-md-9">
                                                <select class="full-width" data-init-plugin="select2" multiple="multiple" id="edit-select-child" name="editchild[]">
                                                    <option value="0">-</option>
                                                    <?php foreach ($data['child'] as $child) { ?>
                                                        <option value="<?= $child->id ?>"><?= $child->fullname ?></option>
                                                    <?php } ?>
                                                </select>
                                                <div style="display:none">
                                                    <select class="full-width" data-init-plugin="select2" multiple="multiple" id="edit-select-child2" name="editchild2[]">
                                                        <option value="0">-</option>
                                                        <?php foreach ($data['child'] as $child) { ?>
                                                            <option value="<?= $child->id ?>"><?= $child->fullname ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="group" class="col-md-3 control-label">Group</label>
                                            <div class="col-md-9">
                                                <select required class="full-width select2" name="group_id" data-init-plugin="select2" id="group_id">
                                                    <option value="">-</option>
                                                    <?php foreach ($data['modules'] as $modules) { ?>
                                                        <option value="<?= $modules->id ?>"><?= $modules->name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="user_group_id" id="user_group_id">
                                        <div class="form-group row" id="field_password_edit">
                                            <label for="group" class="col-md-3 control-label">Password</label>
                                            <div class="col-md-9">
                                                <input type="password" class="form-control" id="edit_password" placeholder="" name="password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">Roles</div>
                                    </div>
                                    <div class="card-body ">
                                        <fieldset>
                                            <div class="checkbox check-success">
                                                <input type="checkbox" id="check_all" onClick="checkAll()">
                                                <label for="check_all">Check All</label>
                                            </div>
                                            <?php foreach ($roles as $role) { ?>
                                                <div class="checkbox check-success">
                                                    <input type="checkbox" value="<?= $role->id ?>" name="<?= $role->name ?>" id="edit_<?= $role->name ?>">
                                                    <label for="edit_<?= $role->name ?>"><?= $role->name ?></label>
                                                </div>
                                            <?php } ?>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button id="submit-edit" type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalDeleteUser2" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Delete <span class="semi-bold">User</span></h5>
                </div>
                <div class="card mod-card" style="margin-bottom:0px">
                    <div class="card-header confirm_user">
                        <span>Are you sure want to delete this user ?</span>
                    </div>
                    <div class="card-body">
                        <form id="form-work" class="form-horizontal" role="form" method="post" action="<?php echo base_url('user/delete') ?>">
                            <input type="hidden" name="delete_user_id" id="id">
                            <input type="hidden" name="delete_email" id="delete_email">
                            <input type="hidden" name="delete_username" id="delete_username">
                            <input type="hidden" name="delete_fullname" id="delete_fullname">
                            <button class="btn btn-complete" type="submit">Delete</button>
                            <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>