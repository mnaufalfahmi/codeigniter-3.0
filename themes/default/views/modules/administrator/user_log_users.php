<style>
    pre {
  width: 100%;
  min-height: 30rem;
  font-family: "Lucida Console", Monaco, monospace;
  font-size: 0.8rem;
  line-height: 1.2;
}
.modal_filter .modal_filter_body {
        height: 180px !important;
}
</style>
<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
        <div class="row">
            
        </div>

        <div class="card">
            <div class="card-header m-t-10">
                <div class="card-title">
                    <?php echo $data['title']; ?>
                </div>
                <br>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover" id="user-log-users" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Created</th>
                            <th>Username</th>
                            <th>Module</th>
                            <th>Remark</th>
                            <th>IP</th>
                            <th>Browser</th>
                            <th>Browser Version</th>
                            <th>URL</th>
                            <th>...</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- show data -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalLogUser" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" >
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>JSON Log <span class="semi-bold">User</span></h5>
                </div>
                <div class="modal-body">
                    <br>
                    <pre id="json"></pre>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- START MODAL  -->
<div class="modal fade slide-up modal_filter" id="modalFilter" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Filter</h5>
                </div>
                <div class="modal-body text-center m-t-20 modal_filter_body">
                    <form class="form_filter">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Start date </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text" class="form-control"
                                    id="datepicker-start" name="start_date">
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">End date </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text" class="form-control"
                                    id="datepicker-end" name="end_date">
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Modules </label>
                            <div class="col-sm-10">
                                <select class="full-width select2" data-init-plugin="select2" data-placeholder="modules" id="modules" name="modules">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button id="reset" class="btn btn-sm btn-default" type="reset"><i class="fa fa-filter"></i>&nbsp; Clear Filter</button>
                    <button type="submit" class="btn btn-sm btn-complete" type="submit"><i class="fa fa-filter"></i>&nbsp; Apply</button>
                    <button type="button" data-dismiss="modal" class="btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp;Close</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->