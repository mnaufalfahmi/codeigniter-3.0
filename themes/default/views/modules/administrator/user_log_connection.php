<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
        <div class="row">
            
        </div>

        <div class="card">
            <div class="card-header m-t-10">
                <div class="card-title">
                    User Log Connection
                </div>
                <br>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover" id="user-log-connection" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Date</th>
                            <th>IP</th>
                            <th>Remark</th>
                            <th>Connections</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- show data -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

