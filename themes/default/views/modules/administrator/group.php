<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
    <div class="inner">
        <div class="row">

        </div>

        <div class="card">
            <div class="card-header m-t-10">
                <div class="card-title">
                    Group Management
                </div>
                <br>
            </div>
            <div class="card-body">
                <div class="card card-transparent ">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-fillup1">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-striped" id="access-group" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Group Code</th>
                                                <th>Group Name</th>
                                                <th>Type</th>
                                                <th>Initial</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- show data -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalAddRole" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Add <span class="semi-bold">Group</span></h5>
                </div>
                <div class="modal-body">
                    <form id="form-work" class="form-horizontal form_save" role="form">
                        <div class="row">
                            <div class="card card-transparent">
                                <!-- <div class="card-header ">
                                    <div class="card-title">User Details</div>
                                </div> -->
                                <div class="card-body ">

                                    <div class="form-group">
                                        <label>Group Name</label>
                                        <input type="text" name="group_name" class="form-control" placeholder="Group Name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Group Code</label>
                                        <input type="text" name="group_code" class="form-control" placeholder="Group Code" onkeypress="allowAlphaNumericSpace(event)" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="full-width select2" name="type" data-init-plugin="select2" required>
                                            <!-- <option>-</option> -->
                                            <?php if ($_COOKIE['member'] == 'NOC' || $_COOKIE['member'] == 'CONTACTCENTER') { ?>
                                                <option value="ANALYSE">ANALYSE</option>
                                                <option value="CONTACTCENTER">CONTACTCENTER</option>
                                                <option value="RECOVERY">RECOVERY</option>
                                            <?php } else { ?>
                                                <option value="RECOVERY" selected>RECOVERY</option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Parent</label>
                                        <select class="full-width select2" name="parent_id" data-init-plugin="select2">
                                            <!-- <option>-</option> -->
                                            <?php foreach ($data['modules'] as $modules) { ?>
                                                <?php if ($_COOKIE['member'] == 'NOC' || $_COOKIE['member'] == 'CONTACTCENTER') { ?>
                                                    <option value="<?= $modules->id ?>"><?= $modules->name ?></option>
                                                <?php } else { ?>
                                                    <option value="<?= $modules->id ?>" selected><?= $modules->name ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Member</label>
                                        <select class="full-width select2" name="member" data-init-plugin="select2" required>
                                            <!-- <option>-</option> -->
                                            <?php if ($_COOKIE['member'] == 'NOC' || $_COOKIE['member'] == 'CONTACTCENTER') { ?>
                                                <option value="SBU">SBU</option>
                                                <option value="CONTACTCENTER">CONTACTCENTER</option>
                                                <option value="NOC">NOC</option>
                                                <option value="SERPO">SERPO</option>
                                            <?php } else { ?>
                                                <option value="SERPO" selected>SERPO</option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Status Mention</label>
                                        <select class="full-width select2" name="status_mention" data-init-plugin="select2" required>
                                            <!-- <option>-</option>     -->
                                            <option value="t">Active</option>
                                            <option value="f" selected>InActive</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Initial</label>
                                        <input type="text" name="initial" class="form-control" placeholder="Nama Singkat Group">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalEditRole" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal" style="height:530px;">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Edit <span class="semi-bold">Group</span></h5>
                </div>
                <div class="modal-body">
                    <form id="form-work" class="form-horizontal form_edit">
                        <div class="row">
                            <div class="card card-transparent">
                                <!-- <div class="card-header ">
                                    <div class="card-title">Role Details</div>
                                </div> -->
                                <div class="card-body ">

                                    <input type="hidden" name="group_id" id="group_id">
                                    <div class="form-group">
                                        <label>Group Name</label>
                                        <input type="text" name="edit_group_name" class="form-control" placeholder="Group Name" id="edit_group_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Group Code</label>
                                        <input type="text" name="edit_group_code" class="form-control" placeholder="Group Code" id="edit_group_code" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="full-width select2" name="edit_type" data-init-plugin="select2" id="edit_type" required>
                                            <!-- <option>-</option> -->
                                            <?php if ($_COOKIE['member'] == 'NOC' || $_COOKIE['member'] == 'CONTACTCENTER') { ?>
                                                <option value="ANALYSE">ANALYSE</option>
                                                <option value="CONTACTCENTER">CONTACTCENTER</option>
                                                <option value="RECOVERY">RECOVERY</option>
                                            <?php } else { ?>
                                                <option value="RECOVERY" selected>RECOVERY</option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Parent</label>
                                        <select class="full-width select2" name="edit_parent_id" id="edit_parent_id" data-init-plugin="select2">
                                            <!-- <option>-</option> -->
                                            <?php foreach ($data['modules'] as $modules) { ?>
                                                <?php if ($_COOKIE['member'] == 'NOC' || $_COOKIE['member'] == 'CONTACTCENTER') { ?>
                                                    <option value="<?= $modules->id ?>"><?= $modules->name ?></option>
                                                <?php } else { ?>
                                                    <option value="<?= $modules->id ?>" selected><?= $modules->name ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Member</label>
                                        <select class="full-width select2" name="edit_member" id="edit_member" data-init-plugin="select2" required>
                                            <?php if ($_COOKIE['member'] == 'NOC' || $_COOKIE['member'] == 'CONTACTCENTER') { ?>
                                                <!-- <option>-</option> -->
                                                <option value="SBU">SBU</option>
                                                <option value="CONTACTCENTER">CONTACTCENTER</option>
                                                <option value="NOC">NOC</option>
                                                <option value="SERPO">SERPO</option>
                                            <?php } else { ?>
                                                <option value="SERPO" selected>SERPO</option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Status Mention</label>
                                        <select class="full-width select2" name="edit_status_mention" id="edit_status_mention"  data-init-plugin="select2" required>
                                            <!-- <option>-</option> -->
                                            <option value="t">Active</option>
                                            <option value="f" selected>InActive</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Initial</label>
                                        <input type="text" id="edit_initial" name="edit_initial" class="form-control" placeholder="Nama Singkat Group">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalDeleteRole" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are you sure want to delete this role ?</span>
                    </div>
                    <div class="card-body">
                        <a id="delete"><button class="btn btn-danger">Delete</button></a>
                        <button class="btn btn-info" id="cancel">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>