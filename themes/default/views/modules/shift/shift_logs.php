<style>
    .modal .modal-body {
        height: 180px !important;
    }

    .color-tr {
        background: #dedede;
    }

    table.dataTable th,
    table>tfoot>tr {
        padding-top: 0px !important;
        padding-bottom: 0px !important;
    }

    table>thead>tr>th {
        line-height: 3 !important;
    }
</style>

<!-- START JUMBOTRON -->
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">

            <!-- mulai content table-->
            <div class="row">
                <div class="col-md-12">
                    <!-- start card  -->
                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="card-title">
                                <?php echo $data['title'] ?>
                            </div>
                        </div>
                        <!-- card body -->
                        <div class="card-body">
                            <table class="table table-striped table-hover table-condensed" id="shift_logs" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Username</th>
                                        <th>Groups</th>
                                        <th>Mod</th>
                                        <th>Shift Date</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Shift Name</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->

<!-- START MODAL ADD SHIFT -->
<div class="modal fade slide-up" id="modalAddLog" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Add User <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form action="<?php echo base_url('shift/user/add_user_shift_group') ?>" method="post">
                    <div class="modal-body m-t-20">
                        <!-- start form -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">User :</label>
                            <div class="col-sm-10">
                                <select class="full-width select-user select2" name="user_id" data-init-plugin="select2" data-placeholder="-- Selected User --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Shift Name :</label>
                            <div class="col-sm-10">
                                <select class="full-width select-shift select2" name="shift_group_id" data-init-plugin="select2" data-placeholder="-- Selected Days --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-complete">Apply</button>
                        <button type="button" class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<!-- START MODAL FILTER SHIFT -->
<div class="modal fade slide-up" id="modalFilter" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Filter</h5>
                </div>
                <div class="modal-body text-center m-t-20">
                    <form class="form_filter">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Start date </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="datepicker-start" name="start_date" autocomplete="off">
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">End date </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text" class="form-control" id="datepicker-end" name="end_date" autocomplete="off">
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-complete" id="apply" type="submit"><i class="fa fa-save"></i>&nbsp; Apply</button>
                    <button data-dismiss="modal" class="btn btn-default"><i class="fa fa-times"></i>&nbsp; Cancel</button>
                </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- MODAL CONFIRMATION  -->
<div class="modal fade slide-up" id="modalDeleted" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are You Sure Delete This Data...?</span>
                    </div>
                    <div class="card-body">
                        <form class="form_deleled" action="<?php echo base_url() ?>shift/user/delete_user_shift" method="post">
                            <input type="hidden" name="id">
                            <button type="Submit" class="btn btn-success btn-sm"><strong>Yes</strong></button>
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><strong>Cancel</strong></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<script>
    var start_date = "<?php echo date('Y-m-d') ?>";
    var end_date = "<?php echo date('Y-m-d') ?>";
</script>