<style>
    th.big-col {
        width: 30px !important;
    }

    .modal .modal-body {
        height: 180px !important;
    }

    .color-tr {
        background: #dedede;
    }
</style>

<!-- START JUMBOTRON -->
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- mulai content table-->
            <div class="row">
                <div class="col-md-12">
                    <!-- start card  -->
                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="card-title">
                                <?php echo $data['title'] ?>
                            </div>
                        </div>
                        <!-- card body -->
                        <div class="card-body">
                            <!-- start table responsive -->
                            <!-- <div class="table-responsive"> -->
                            <table class="table table-hover table-condensed" id="shift_groups" style="width:100%">
                                <thead>
                                    <tr class="color-tr">
                                        <th>No.</th>
                                        <th>Mod</th>
                                        <th>Mod Name</th>
                                        <th>User Mod</th>
                                        <th>Group</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- </div> -->
                            <!-- end table responsive -->
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->

<!-- START MODAL ADD SHIFT -->
<div class="modal fade slide-up" id="modalAddGroups" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Add Groups <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form action="<?php echo base_url('shift/groups/prosses') ?>" method="post">
                    <input type="hidden" name="prosses" value="add">
                    <div class="modal-body m-t-20">
                        <!-- start form -->
                        <div class="form-group row">
                            <div class="col">
                                <label class="col-form-label">MOD :</label>
                                <select class="full-width select-mod select2" name="user_mod" data-init-plugin="select2" data-placeholder="-- Selected User --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="col">
                                <label class="col-form-label">User :</label>
                                <select class="full-width select-user select2" name="user_group[]" multiple="multiple" data-init-plugin="select2" data-placeholder="-- Selected User --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col">
                                <label class="col-form-label">GROUP NAME :</label>
                                <select class="full-width select2" name="group_name" data-init-plugin="select2" data-placeholder="-- Selected Group --" required>
                                    <option value=""></option>
                                    <option value="group1">Group 1</option>
                                    <option value="group2">Group 2</option>
                                    <option value="group3">Group 3</option>
                                    <option value="group4">Group 4</option>
                                </select>
                                <!-- <input type="text" class="form-control" name="group_name" placeholder="-- Entry Group Name --" required> -->
                            </div>
                        </div>

                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-complete">Apply</button>
                        <button type="button" class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL EDIT SHIFT -->
<div class="modal fade slide-up" id="modalEditGroups" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Edit Groups <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form class="form_edit" action="<?php echo base_url('shift/groups/prosses') ?>" method="post">
                    <input type="hidden" name="prosses" value="update">
                    <input type="hidden" name="old_group_name">
                    <input type="hidden" name="id">
                    <div class="modal-body m-t-20">
                        <!-- start form -->
                        <div class="form-group row">
                            <div class="col">
                                <label class="col-form-label">MOD :</label>
                                <select class="full-width select-mod select2" name="user_mod" data-init-plugin="select2" data-placeholder="-- Selected User --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                            <div class="col">
                                <label class="col-form-label">User :</label>
                                <select class="full-width select-user select2" name="user_group" data-init-plugin="select2" data-placeholder="-- Selected User --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col">
                                <label class="col-form-label">GROUP NAME :</label>
                                <input type="text" class="form-control" name="group_name" placeholder="-- Entry Group Name --">
                            </div>
                        </div>

                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-complete">Apply</button>
                        <button type="button" class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- MODAL CONFIRMATION  -->
<div class="modal fade slide-up" id="modalDeleted" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are You Sure Delete This Data...?</span>
                    </div>
                    <div class="card-body">
                        <form class="form_deleted" action="<?php echo base_url() ?>shift/groups/prosses" method="post">
                            <input type="hidden" name="id">
                            <input type="hidden" name="prosses" value="delete">
                            <button type="Submit" class="btn btn-success btn-sm"><strong>Yes</strong></button>
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><strong>Cancel</strong></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>