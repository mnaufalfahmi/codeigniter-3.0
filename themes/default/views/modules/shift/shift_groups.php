<style>
    th.big-col{
    width:30px !important;
    }
    .modal .modal-body {
        height: 100px !important;
    }

    .color-tr{
        background: #dedede;
    }

</style>

<?php 

    function puluhan($param){

        if($param < 10){

            return '0'.$param;
        }else{

            return $param;
        }

    }

?>
<!-- START JUMBOTRON -->
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- mulai content table-->
            <div class="row">
                <div class="col-md-12">
                    <!-- start card  -->
                    <div class="card shadow-sm" style="width: 100%;">
                        <div class="card-header"></div>
                        <!-- card body -->
                        <div class="card-body">
                            <!-- start table responsive -->
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed" id="shift_groups" style="width:100%">
                                    <thead>
                                        <tr class="color-tr">
                                            <th class="big-col">No.</th>
                                            <th>User</th>
                                            <th>Shift Date</th>
                                            <!-- <th>Shift Name</th> -->
                                            <th>Start Time</th>
                                            <th>End Time</th>
                                            <th class="text-center" style="width:70px">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr class="color-tr">
                                            <td colspan="6"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- end table responsive -->
                            
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->


<!-- START MODAL ADD SHIFT -->
<div class="modal fade slide-up" id="modalAddUser" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Add User <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form action="<?php echo base_url('shift/user/add_user_shift_group') ?>" method="post">
                    <div class="modal-body m-t-20">
                        <!-- start form -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">User :</label>
                            <div class="col-sm-10">
                                <select class="full-width select-user select2" name="user_id" data-init-plugin="select2" data-placeholder="-- Selected User --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Shift Name :</label>
                            <div class="col-sm-10">
                                <select class="full-width select-shift select2" name="shift_group_id" data-init-plugin="select2" data-placeholder="-- Selected Days --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-complete">Apply</button>
                        <button type="button" class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL EDIT SHIFT -->
<div class="modal fade slide-up" id="modalEditUser" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Edit User <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form class="form_edit" action="<?php echo base_url('shift/user/update_user_shift_group') ?>" method="post">
                <input type="hidden" name="id">
                    <div class="modal-body m-t-20">
                        <!-- start form -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">User :</label>
                            <div class="col-sm-10">
                                <select class="full-width select-user select2" name="user_id" data-init-plugin="select2" data-placeholder="-- Selected User --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Shift Name :</label>
                            <div class="col-sm-10">
                                <select class="full-width select-shift select2" name="shift_group_id" data-init-plugin="select2" data-placeholder="-- Selected Days --" required>
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-sm btn-complete">Apply</button>
                        <button type="button" class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<!-- MODAL CONFIRMATION  -->
<div class="modal fade slide-up" id="modalDeleted" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are You Sure Delete This Data...?</span>
                    </div>
                    <div class="card-body">
                        <form class="form_deleled" action="<?php echo base_url() ?>shift/user/delete_user_shift" method="post">
                            <input type="hidden" name="id">
                            <button type="Submit" class="btn btn-success btn-sm"><strong>Yes</strong></button>
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><strong>Cancel</strong></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>