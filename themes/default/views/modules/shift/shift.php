<style>
    th.big-col{
    width:30px !important;
    }
    .modal .modal-body {
        height: 180px !important;
    }

    .color-tr{
        background: #dedede;
    }

</style>

<?php 

    function puluhan($param){

        if($param < 10){

            return '0'.$param;
        }else{

            return $param;
        }

    }

?>
<!-- START JUMBOTRON -->
<div class="jumbotron">
    <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- mulai content table-->
            <div class="row">
                <div class="col-md-12">
                    <!-- start card  -->
                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            <div class="card-title">
                                <?php echo $data['title'] ?>
                            </div>
                        </div>
                        <!-- card body -->
                        <div class="card-body">
                            <!-- start table responsive -->
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed" id="shift_groups" style="width:100%">
                                    <thead>
                                        <tr class="color-tr">
                                            <th class="big-col">No.</th>
                                            <th>Shift Name</th>
                                            <th>Start Time</th>
                                            <th>End Time</th>
                                            <th class="text-center" style="width:70px">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- end table responsive -->
                            
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <!-- akhir content table -->
        </div>
    </div>
</div>
<!-- END JUMBOTRON -->

<!-- START MODAL ADD SHIFT -->
<div class="modal fade slide-up" id="modalAddShift" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Add Shift <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form action="<?php echo base_url('shift/add_shift_group') ?>" method="post">
                    <div class="modal-body m-t-20">
                        <!-- start form -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Shift Name :</label>
                            <div class="col-sm-10">
                                <select class="full-width select2" name="day" data-init-plugin="select2" data-placeholder="-- Selected Days --" required>
                                    <option value=""></option>
                                    <option value="Pagi">Pagi</option>
                                    <option value="Siang">Siang</option>
                                    <option value="Malam">Malam</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Start Time :</label>
                            <div class="col-sm-10">
                                <div class="form-row">
                                    <div class="col">
                                        <select class="full-width select2" name="start_hour" data-placeholder="-- Selected Hours --" required>
                                            <option value=""></option>
                                            <option value="00">00</option>
                                            <?php for($i=1;$i<25;$i++){ ?>
                                            <option value="<?php echo puluhan($i)?>"><?php echo puluhan($i) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select class="full-width select2" name="start_minute" data-placeholder="-- Selected Minutes --" required>
                                            <option value=""></option>
                                            <option value="00">00</option>
                                            <?php for($i=1;$i<60;$i++){ ?>
                                            <option value="<?php echo puluhan($i)?>"><?php echo puluhan($i) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">End Time :</label>
                            <div class="col-sm-10">
                                <div class="form-row">
                                    <div class="col">
                                        <select class="full-width select2" name="end_hour" data-placeholder="-- Selected Hours --" required>
                                            <option value=""></option>
                                            <option value="00">00</option>
                                            <?php for($i=1;$i<25;$i++){ ?>
                                            <option value="<?php echo puluhan($i)?>"><?php echo puluhan($i) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select class="full-width select2" name="end_minute" data-placeholder="-- Selected Minutes --" required>
                                            <option value=""></option>
                                            <option value="00">00</option>
                                            <?php for($i=1;$i<60;$i++){ ?>
                                            <option value="<?php echo puluhan($i)?>"><?php echo puluhan($i) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-complete">Apply</button>
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- START MODAL UPDATE SHIFT -->
<div class="modal fade slide-up" id="modalEditShift" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Edit Shift <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <!-- start form -->
                <form class="form_edit" action="<?php echo base_url('shift/update_shift_group') ?>" method="post">
                <input type="hidden" name="id">
                    <div class="modal-body m-t-20">
                        <!-- start form -->
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Shif Name :</label>
                            <div class="col-sm-10">
                                <select class="full-width select2 select_day" name="day" data-init-plugin="select2" data-placeholder="-- Selected Days --" required>
                                    <option value=""></option>
                                    <option value="Pagi">Pagi</option>
                                    <option value="Siang">Siang</option>
                                    <option value="Malam">Malam</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Start Time :</label>
                            <div class="col-sm-10">
                                <div class="form-row">
                                    <div class="col">
                                        <select class="full-width select2 select_start_hour" name="start_hour" data-placeholder="-- Selected Hours --" required>
                                            <option value=""></option>
                                            <option value="00">00</option>
                                            <?php for($i=1;$i<25;$i++){ ?>
                                            <option value="<?php echo puluhan($i)?>"><?php echo puluhan($i) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select class="full-width select2 select_start_minute" name="start_minute" data-placeholder="-- Selected Minutes --" required>
                                            <option value=""></option>
                                            <option value="00">00</option>
                                            <?php for($i=1;$i<60;$i++){ ?>
                                            <option value="<?php echo puluhan($i)?>"><?php echo puluhan($i) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">End Time :</label>
                            <div class="col-sm-10">
                                <div class="form-row">
                                    <div class="col">
                                        <select class="full-width select2 select_end_hour" name="end_hour" data-placeholder="-- Selected Hours --" required>
                                            <option value=""></option>
                                            <option value="00">00</option>
                                            <?php for($i=1;$i<25;$i++){ ?>
                                            <option value="<?php echo puluhan($i)?>"><?php echo puluhan($i) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select class="full-width select2 select_end_minute" name="end_minute" data-placeholder="-- Selected Minutes --" required>
                                            <option value=""></option>
                                            <option value="00">00</option>
                                            <?php for($i=1;$i<60;$i++){ ?>
                                            <option value="<?php echo puluhan($i)?>"><?php echo puluhan($i) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-complete">Apply</button>
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
                    </div>
                    <!-- end modal footer -->
                </form>
                <!-- end form -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->


<!-- MODAL CONFIRMATION  -->
<div class="modal fade slide-up" id="modalDeleted" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Are You Sure Delete This Data...?</span>
                    </div>
                    <div class="card-body">
                        <form class="form_deleted" action="<?php echo base_url() ?>shift/deleted_shift_group" method="post">
                            <input type="hidden" name="id">
                            <button type="Submit" class="btn btn-success btn-sm"><strong>Yes</strong></button>
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><strong>Cancel</strong></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
