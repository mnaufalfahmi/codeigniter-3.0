<style type="text/css">
@media only screen and (min-width: 800px){
 .mod-modal {
  width: 900px;
  height:580px;
}
}
/*.table.table-condensed tbody tr td {
         padding-top: 1px;
        padding-bottom: 1px; 
    }*/
    #btn{
      padding-top: 0px;
      padding-bottom: 0px;
  }
</style>
<div class="jumbotron" >
    <div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <div class="row">
               
          </div>

          <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header m-t-10">
                  <div class="card-title">
                    Close Tickets 
                  </div>
                  <br>
                </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-condensed" id="close_ticket" style="width:100%">
                                <thead>
                                    <tr>
                                        <th width="3%">No</th>
                                        <th width="10%">Close Time</th>
                                        <th width="8%">Ticket ID FSM+</th>
                                        <th width="8%">Ticket ID CRM</th>
                                        <th width="10%">Service ID</th>
                                        <th width="6%">Class</th>
                                        <th width="40%">Description</th>
                                        <th width="5%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="table">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
</div>
<!-- START MODAL  -->
<div class="modal fade slide-up" id="modalFilter" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Filter</h5>
                </div>
                <div class="modal-body text-center m-t-20">
                    <form class="form_filter">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Start date </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text" class="form-control"
                                    id="datepicker-start" name="start_date">
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">End date </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text" class="form-control"
                                    id="datepicker-end" name="end_date">
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Service ID </label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="service_id">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">CC ID </label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="ismilling_id">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description ID</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <textarea class="form-control" name="description"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-complete" id="apply" type="submit">Apply</button>
                        <button data-dismiss="modal" class="btn btn-default">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<div class="modal fade slide-up" id="modalTicketDetail" tabindex="-1" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Ticket <span class="semi-bold">Detail</span></h5>
                </div>
                
                    <div class=" container-fluid bg-white modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- START card -->
                                <div class="card card-transparent">
                                <div class="card-body">
                                    <ul class="nav nav-tabs no-border notification-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="btn btn-default btn-cons btn-rounded btn-notification-style active m-b-10" href="#loadingBar" role="tab" data-toggle="tab" data-type="position-bar">Ticket Detail</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-default btn-cons btn-rounded btn-notification-style m-b-10" href="#bouncyFlip" role="tab" data-toggle="tab" data-type="position-flip">CRM Detail</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-default btn-cons btn-rounded btn-notification-style m-b-10" href="#circleNotification" role="tab" data-toggle="tab" data-type="position-circle">Ticket Progess</a>
                                    </li>
                                    </ul>
                                    <div class="config-notification">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="loadingBar">
                                        <!-- <h4 class="semi-bold">Ticket Detail</h4> -->
                                        <form>
                                                    <div class="card-body ">
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Ticket ID</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="ticket_id" placeholder="" name="ticket_id" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Ismilling ID</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="ismilling_id" placeholder="" name="ismilling_id">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Service ID</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="service_id" placeholder="" name="service_id">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Open Time</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="open_time" placeholder="" name="open_time">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Attach Time</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="attach_time" placeholder="" name="attach_time">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Description</label>
                                                            <div class="col-md-9">
                                                                <textarea rows="5" id="description" name="detail_description" class="form-control"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                        </div>
                                        <div class="tab-pane" id="bouncyFlip">
                                        <!-- <h4 class="semi-bold">CRM</h4> -->
                                                    <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                                        <li class="nav-item">
                                                        <a href="#" class="active" data-toggle="tab" data-target="#slide1"><span>Ismilling</span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#" data-toggle="tab" data-target="#slide2"><span>Sisfo MT</span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#" data-toggle="tab" data-target="#slide3"><span>IPDB</span></a>
                                                    </li>
                                                    </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane slide-left active" id="slide1">
                                                        <div class="row">
                                                            <div class="col-lg-12" style="overflow: auto; left: 0px; top: 0px; width: 540px; height: 260px;">
                                                            <iframe id="frameIsmilling" frameborder="0" width="900" height="500" src=""></iframe>                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane slide-left" id="slide2">
                                                    <div class="row">
                                                        <div class="col-lg-12">

                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="tab-pane slide-left" id="slide3">
                                                        <div class="row">
                                                            <div class="col-lg-12">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="tab-pane" id="circleNotification">
                                        <!-- <h4 class="semi-bold">Ticket Progress</h4> -->
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="ticket_progress" >
                                                <thead>
                                                    <tr>
                                                        <th width="5">User</th>
                                                        <th width="5">Time</th>
                                                        <th width="5">Description</th>
                                                    </tr>
                                                </thead>
                                                <tbody style="vertical-align: top">
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                <!-- END card -->
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-complete btn-summary" type="button"><i class="fa fa-file"></i>Summary Incident</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up" id="modalTicketDetail" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Incident <span class="semi-bold">Summary</span></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card card-transparent">
                                <div class="card-header ">
                                    <div class="card-title">Incident Details</div>
                                </div>
                                <form>
                                    <div class="card-body ">
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Incident ID</label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" id="id_incident" placeholder="" name="id_incident" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Creator</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="creator" placeholder="" name="creator">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Notification Trigger</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="notification" placeholder="" name="notification">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">State</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="state" placeholder="" name="state">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Priority</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="priority" placeholder="" name="priority">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Classification</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="classification" placeholder="" name="classification">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Root Cause</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="root_cause" placeholder="" name="root_cause">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Root Cause Details</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="root_cause_detail" placeholder="" name="root_cause_detail">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Description</label>
                                            <div class="col-md-9">
                                                <textarea rows="5" id="description" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Link</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="linkk" placeholder="" name="linkk">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Hierarchy</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="hierarchy" placeholder="" name="hierarchy">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Region</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="region" placeholder="" name="region">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Site</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="site" placeholder="" name="site">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Creation Time</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="create_time" placeholder="" name="create_time">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Duration</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="duration" placeholder="" name="duration">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Total Stop Clock</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="total_stop_clock" placeholder="" name="total_stop_clock">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">TTR</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="ttr" placeholder="" name="ttr">
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <div id="accordion">
                                    <!-- incident progress -->
                                    <div class="card mb-0">
                                        <div class="card-header card-summary text-left" id="headingOne">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#colIncProgress" aria-expanded="true" aria-controls="collapseOne">
                                                Incident Progress
                                            </button>
                                        </div>
                                        <div id="colIncProgress" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">                                                                    
                                                <table class="table table-hover table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th>User</th>
                                                            <th>Progres Time</th>
                                                            <th>Description</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>User</td>
                                                            <td>Progres Time</td>
                                                            <td>Description</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end incident progress -->
                                    <!-- file attachment -->
                                    <div class="card mb-0">
                                        <div class="card-header card-summary text-left" id="headingOne">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#colFileAttach" aria-expanded="true" aria-controls="collapseOne">
                                                File Attachment
                                            </button>
                                        </div>
                                        <div id="colFileAttach" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">                                                                    
                                                <table class="table table-hover table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:10px">...</th>
                                                            <th>Timestamp</th>
                                                            <th>Uploader</th>
                                                            <th>Filename</th>
                                                            <th>Description</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>...</td>
                                                            <td>Timestamp</td>
                                                            <td>Uploader</td>
                                                            <td>Filename</td>
                                                            <td>Description</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end incident progress -->
                                    <!-- ticket -->
                                    <div class="card mb-0">
                                        <div class="card-header card-summary text-left" id="headingOne">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#colTicket" aria-expanded="true" aria-controls="collapseOne">
                                                Ticket
                                            </button>
                                        </div>
                                        <div id="colTicket" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">                                                                    
                                                <table class="table table-hover table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:10px">...</th>
                                                            <th>Timestamp</th>
                                                            <th>Uploader</th>
                                                            <th>Filename</th>
                                                            <th>Description</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>...</td>
                                                            <td>Timestamp</td>
                                                            <td>Uploader</td>
                                                            <td>Filename</td>
                                                            <td>Description</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end ticket -->
                                    <!-- service affect -->
                                    <div class="card mb-0">
                                        <div class="card-header card-summary text-left" id="headingOne">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#colService" aria-expanded="true" aria-controls="collapseOne">
                                                Service Affect
                                            </button>
                                        </div>
                                        <div id="colService" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">                                                                    
                                                <table class="table table-hover table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th>...</th>
                                                            <th>Service ID</th>
                                                            <th>service Name</th>
                                                            <th>Customer</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>...</td>
                                                            <td>Service ID</td>
                                                            <td>service Name</td>
                                                            <td>Customer</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end service affect -->

                                    <!-- service affect -->
                                    <div class="card mb-0">
                                        <div class="card-header card-summary text-left" id="headingOne">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#colAR" aria-expanded="true" aria-controls="collapseOne">
                                                Action Request
                                            </button>
                                        </div>
                                        <div id="colAR" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">                                                                    
                                                <table class="table table-hover table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <th>...</th>
                                                            <th>Time Creaton</th>
                                                            <th>Destination</th>
                                                            <th>Request</th>
                                                            <th>State</th>
                                                            <th>Remark Closing</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>...</td>
                                                            <td>Time Creaton</td>
                                                            <td>Destination</td>
                                                            <td>Request</td>
                                                            <td>State</td>
                                                            <td>Remark Closing</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end service affect -->

                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var url_string = window.location.href;
    var url = new URL(url_string);
    var service_id = url.searchParams.get("service_id");

</script>