<style type="text/css">
@media only screen and (min-width: 800px){
 .mod-modal {
  width: 1100px;
  height:580px;
}
}
.table.table-condensed tbody tr td {
   padding-top: 1px;
   padding-bottom: 1px; 
}
#btn{
  padding-top: 0px;
  padding-bottom: 0px;
}

</style>
<div class="jumbotron" >
    <div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <div class="row">
                
          </div>
            <!-- open ticket  -->
          <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header m-t-10">
                  <div class="card-title">
                    Open Tickets 
                  </div>
                  <br>
                </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-condensed" id="open_ticket" style="width:100%" >
                                <thead>
                                    <tr>
                                        <th width="3%">No</th>
                                        <th width="10%">Ticket ID FSM+</th>
                                        <th width="8%">Ticket ID CRM</th>
                                        <th width="10%">Open Time</th>
                                        <th width="6%">Class</th>
                                        <th width="20%">Description</th>
                                        <th width="8%">Service ID</th>
                                        <th width="5%">Lock By</th>
                                        <th width="5%">Bobot</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- start modal -->

<!-- attach tickets -->
<div class="modal fade slide-up" id="modalTicketAttach" tabindex="-1" role="dialog" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button onClick="removedAttach()" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Attach <span class="semi-bold">Incident</span></h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card card-transparent">
                                <div class="card-header ">
                                    <div class="card-title">Select Incident</div>
                                </div>
                                <!-- <form class="form-horizontal form_attach" role="form" autocomplete="off" action="<?php echo base_url()?>ticket/updateIncidents" method="POST"> -->
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-condensed" id="listIncident">
                                                <thead>
                                                    <tr>
                                                        <th width="15">Open Time</th>
                                                        <th width="15">Creator</th>
                                                        <th width="15">Description</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- </form> -->
                                    <div class="card-body ">

                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">Attach Ticket List </div>
                                    </div>
                                    <div class="card-body ">
                                        <form id="form-work" class="form-horizontal form_attach" role="form" autocomplete="off" action="<?php echo base_url()?>ticket/updateIncidents" method="POST">
                                            <div class="table-responsive">
                                                <button id="attachIncident" data-target="#modalAttachIncident" data-toggle="modal" class="btn btn-primary btn-xs" type="button">Attach Other Ticket</button>
                                                <table class="table table-hover table-condensed" id="listAttachTickets" style="font-family: sans-serif, Arial, Verdana, 'Trebuchet MS', 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';font-size: 13px;">
                                                    <thead>
                                                        <tr>
                                                            <th width="3%">...</th>
                                                            <th width="7%">Service ID</th>
                                                            <th width="9%">Open Time</th>
                                                            <th width="9%">Description</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody style="vertical-align: top">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <input type="hidden" name="incident" id="incident">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-sm btn-complete" type="submit"><i class="fa fa-file"></i>&nbsp; Save</button>
                            <button onClick="removedAttach()" data-dismiss="modal" class="btn btn-sm btn-default remove" type="button"><i class="fa fa-times-circle"></i>&nbsp; Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade slide-up" id="modalAttachIncident" tabindex="-1" role="dialog" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content-wrapper">
                <div class="modal-content mod-modal">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h5>Attached Other <span class="semi-bold">Ticket</span></h5>
                    </div>
                    <div class="modal-body">
                        <form id="form-work" class="form-horizontal" role="form" autocomplete="off">
                            <div class="row">
                                <div class="card card-transparent">
                                    <div class="card-header ">
                                        <div class="card-title">User Details</div>
                                    </div>
                                    <div class="card-body ">

                                      <table class="table table-hover table-condensed" id="addAttachTickets">
                                        <thead>
                                            <tr>
                                                <th width="5">No</th>
                                                <th width="5">Open Time</th>
                                                <th width="5">Description</th>
                                                <th width="5">Service ID</th>
                                                <!-- <th width="5">Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody style="vertical-align: top">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <!-- <button class="btn btn-sm btn-complete" type="button"><i class="fa fa-file"></i>&nbsp; Attach</button> -->
                    <button data-dismiss="modal" class="btn btn-sm btn-default remove" type="button"><i class="fa fa-times-circle"></i>&nbsp; Back</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- end -->

<div class="modal fade slide-up" id="modalTicketAnalyze" tabindex="-1" role="dialog" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Analyze <span class="semi-bold">Ticket</span></h5>
                </div>

                <div class=" container-fluid bg-white modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- START card -->
                                <div class="card card-transparent">
                                <div class="card-body">
                                    <ul class="nav nav-tabs no-border notification-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="btn btn-default btn-cons btn-rounded btn-notification-style active m-b-10" href="#loadingBar9" role="tab" data-toggle="tab" data-type="position-bar">Analyze Ticket</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-default btn-cons btn-rounded btn-notification-style m-b-10" href="#bouncyFlip9" role="tab" data-toggle="tab" data-type="position-flip">CRM Detail</a>
                                    </li>
                                    </ul>
                                    <div class="config-notification">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="loadingBar9">
                                        <form>
                                    <div class="card-body ">
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 control-label">Description</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="description_analyze" placeholder="" name="description_analyze" required>
                                            </div>
                                        </div>
                                        <!-- <div class="card-header ">
                                            <div class="card-title">
                                                Possible Root Cause
                                            </div>
                                        </div> -->
                                        <button class="btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#modalAddIncident" id="default_define">Default Define</button>
                                        <div class="table-responsive">
                                            <table class="table table-hover table-condensed" id="analyze_possible">
                                                <thead>
                                                    <tr>
                                                        <!-- <th width="5">..</th> -->
                                                        <th width="41px">Classification</th>
                                                        <th width="57px">Root Cause</th>
                                                        <th width="44px">Region</th>
                                                        <th width="3px">%</th>
                                                        <th width="4px">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                    </div>
                                </form>
                                        </div>
                                        <div class="tab-pane" id="bouncyFlip9">
                                        <div class="card card-transparent">
                                            <div id="accordion">
                                                <!-- incident progress -->
                                                <div class="card mb-0">
                                                    <div class="card-header card-summary text-left" id="headingOne">
                                                        <button class="btn btn-link" data-toggle="collapse" data-target="#colIncProgress" aria-expanded="true" aria-controls="collapseOne">
                                                            Ismilling
                                                        </button>
                                                    </div>
                                                    <div id="colIncProgress" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                                                                                            
                                                            <div class="col-lg-12" style="overflow: auto; width: auto; height: 114px; left: 0px;">
                                                            <iframe id="frameIsmilling-Analyze" frameborder="0" width="900" height="500" src=""></iframe>                                                
                                                            </div>
                                                    </div>
                                                </div>
                                                <!-- end incident progress -->
                                                <!-- file attachment -->
                                                <div class="card mb-0">
                                                    <div class="card-header card-summary text-left" id="headingOne">
                                                        <button class="btn btn-link" data-toggle="collapse" data-target="#colFileAttach" aria-expanded="true" aria-controls="collapseOne">
                                                            Sisfo MT
                                                        </button>
                                                    </div>
                                                    <div id="colFileAttach" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                        <div class="card-body">                                                                    

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end incident progress -->
                                                <!-- ticket -->
                                                <div class="card mb-0">
                                                    <div class="card-header card-summary text-left" id="headingOne">
                                                        <button class="btn btn-link" data-toggle="collapse" data-target="#colTicket" aria-expanded="true" aria-controls="collapseOne">
                                                            IPDB
                                                        </button>
                                                    </div>
                                                    <div id="colTicket" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                        <div class="card-body">                                                                    

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end ticket -->
                                                <!-- service affect -->
                                                <!-- <div class="card mb-0">
                                                    <div class="card-header card-summary text-left" id="headingOne">
                                                        <button class="btn btn-link" data-toggle="collapse" data-target="#colService" aria-expanded="true" aria-controls="collapseOne">
                                                            Related Andop
                                                        </button>
                                                    </div>
                                                    <div id="colService" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                        <div class="card-body">                                                                    
                                                            <table class="table table-hover table-condensed" id="related_andop">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="5">Title</th>
                                                                        <th width="5">Execution</th>
                                                                        <th width="5">Downtime</th>
                                                                        <th width="5">Location</th>
                                                                        <th width="5">Creator</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                <!-- END card -->
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <!-- <button class="btn btn-sm btn-complete btn-summary" type="button"><i class="fa fa-file"></i>Summary Incident</button> -->
                </div>

        </div>
    </div>
</div>
</div>

<div class="modal fade slide-up" id="modalAddIncident" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button onClick="removed()" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Define <span class="semi-bold">Incident</span></h5>
                </div>
                <div class="modal-body">

                    <form id="form-work" class="form-horizontal form_save" role="form" autocomplete="off" method="POST">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="card card-transparent">
                                    <div class="card-body ">
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 col-form-label">Creator</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="creator" placeholder="" name="creator" value="<?= $_COOKIE['username']; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fname" class="col-md-3 col-form-label">Timestamp Open</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="timestamp" placeholder="" name="timestamp" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                          <label class="col-sm-3 col-form-label" for="">Clasification :</label>
                                          <div class="col-sm-9">
                                            <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="select_classification" name="select_classification" required>
                                              <option value=""></option>
                                              <?php
                                              foreach($data['classification'] as $row): ?>
                                                <option value="<?php echo $row->id; ?>"><?php echo $row->classification_name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label" for="">Root Cause :</label>
                                    <div class="col-sm-9">
                                        <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="select_root_cause" name="select_root_cause" required>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-md-3 col-form-label">Root Cause Detail</label>
                                    <div class="col-md-9">
                                        <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="select_root_cause_detail" name="select_root_cause_detail">
                                           
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-md-3 col-form-label">Network</label>
                                    <div class="radio radio-success col-md-9">
                                        <input required type="radio" value="ip" name="network" id="ip">
                                        <label for="ip">IP</label>
                                        <input required type="radio" value="tdm" name="network" id="tdm">
                                        <label for="tdm">TDM</label>
                                        <input required type="radio" value="vsat" name="network" id="vsat">
                                        <label for="vsat">VSAT</label>
                                        <input required type="radio" value="infra" name="network" id="infra">
                                        <label for="infra">INFRA</label>
                                        <input required type="radio" value="ms" name="network" id="ms">
                                        <label for="ms">MS</label>
                                        <input required type="radio" value="mm" name="network" id="mm">
                                        <label for="mm">MM</label>
                                        <input required type="radio" value="oa" name="network" id="oa">
                                        <label for="oa">OA</label>
                                        <input required type="radio" value="mspc" name="network" id="mspc">
                                        <label for="mspc">MSPC</label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-md-3 col-form-label">Priority</label>
                                    <div class="radio radio-success col-md-9">
                                        <input type="radio" value="low" name="priority" id="low">
                                        <label for="low">Low</label>
                                        <input type="radio" value="medium" name="priority" id="medium">
                                        <label for="medium">Medium</label>
                                        <input type="radio" value="high" name="priority" id="high">
                                        <label for="high">High</label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-md-3 col-form-label">Description</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="description" id="definedescription" required style="height: 114px;"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-md-3 col-form-label">Link</label>
                                    <div class="col-md-9">
                                        <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="link" name="link" required>
                                            
                                            <option value="backbone">Backbone</option>
                                            <option value="distribution">Distribution</option>
                                            <option selected value="Access">Access</option>
                                            <option value="retail">Retail</option>
                                        </select>
                                    </div>
                                </div>

                                <div style="display: none;" id="div_region">
                                    <div class="form-group row">
                                        <label for="fname" class="col-md-3 col-form-label">Region</label>
                                        <div class="col-md-9">
                                            <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="region1" name="region1">
                                                
                                                <?php
                                                  foreach($data['region'] as $row): ?>
                                                    <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" id="div_site_name">
                                    <label for="fname" class="col-md-3 col-form-label">Site Name</label>
                                    <div class="col-md-9">
                                        <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="site_name1" name="site_name1">
                                            
                                        </select>
                                    </div>
                                </div>
                                <div style="display: none;" id="foc-show">
                                    <div class="form-group row">
                                        <label for="fname" class="col-md-3 col-form-label">Link Start Region :</label>
                                        <div class="col-md-9">
                                            <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="link_start_region" name="link_start_region">
                                                
                                                <?php
                                                  foreach($data['region'] as $row): ?>
                                                    <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-md-3 col-form-label">Link Start Site :</label>
                                        <div class="col-md-9">
                                            <select class="full-width" data-init-plugin="select2" data-placeholder="Select" id="link_start_site" name="link_start_site">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-md-3 col-form-label">Link End Region :</label>
                                        <div class="col-md-9">
                                            <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="link_end_region" name="link_end_region">
                                                
                                                <?php
                                                  foreach($data['region'] as $row): ?>
                                                    <option value="<?php echo $row->region_code; ?>"><?php echo $row->description; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-md-3 col-form-label">Link End Site :</label>
                                        <div class="col-md-9">
                                            <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="link_end_site" name="link_end_site">
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" id="11">
                                    <label for="fname" class="col-md-3 col-form-label">Source Category :</label>
                                    <div class="col-md-9">
                                        <label class="m-t-10" id="source_category"></label>
                                        <input type="hidden" name="source_category_input" id="source_category_input">
                                        <div style="display: none;" id="div_link_upstream_isp">
                                            <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="link_upstream_isp" name="link_upstream_isp">
                                                
                                                <option value="1">FOT</option>
                                                <option value="2">FOC</option>
                                                <option value="3">PS</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="1" style="display: none;">
                                    <div class="form-group row">
                                        <label for="fname" class="col-md-3 col-form-label">Brand :</label>
                                        <div class="col-md-9">
                                            <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="brand1" name="brand1">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-md-3 col-form-label">ME :</label>
                                        <div class="col-md-9">
                                            <select class="full-width select2" data-init-plugin="select2" data-placeholder="Select" id="me1" name="me1">
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="3" style="display: none;">
                                    <div class="form-group row">
                                        <label for="fname" class="col-md-3 col-form-label">Broken FO :</label>
                                        <div class="col-md-9">
                                            <input type="number" class="form-control" id="broken_fo" placeholder="" name="broken_fo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card card-transparent">
                            <div class="card-header ">
                                <div class="card-title">Attach Ticket List</div>
                            </div>
                            <div class="card-body">
                                <button id="attachotherticket" data-target="#modalattachotherticket" data-toggle="modal" class="btn btn-primary btn-xs" type="button">Attach Other Ticket</button>
                                <table class="table table-hover table-condensed" id="define_attach_ticket" style="font-family: sans-serif, Arial, Verdana, 'Trebuchet MS', 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';font-size: 13px;">
                                    <thead>
                                        <tr>
                                            <th style="width:5%">..</th>
                                            <th style="width:15%">Service ID</th>
                                            <th style="width:22%">Open Time</th>
                                        </tr>
                                    </thead>
                                    <tbody style="vertical-align: top" id="table">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-complete" type="submit"><i class="fa fa-file"></i>&nbsp; Save</button>
                <button data-dismiss="modal" onClick="removed()" class="btn btn-sm btn-default" type="button"><i class="fa fa-times-circle"></i>&nbsp; Cancel</button>
                <!-- data-dismiss="modal" -->
            </div>
        </form>
    </div>
</div>
</div>
</div>

<div class="modal fade slide-up" id="modalattachotherticket" role="dialog" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Attach Other <span class="semi-bold">Ticket</span></h5>
                </div>
                <div class="modal-body">
                    <form id="form-work" class="form-horizontal" role="form" autocomplete="off">
                        <div class="row">
                            <div class="card card-transparent">
                                <div class="card-header ">
                                    <!-- <div class="card-title">User Details</div> -->
                                </div>
                                <div class="card-body ">

                                  <table class="table table-hover table-condensed" id="add_attach_ticket">
                                    <thead>
                                        <tr>
                                            <th width="5">No</th>
                                            <th width="5">Open Time</th>
                                            <th width="5">Description</th>
                                            <th width="5">Service ID</th>
                                            <th width="5">Ismilling ID</th>
                                        </tr>
                                    </thead>
                                    <tbody style="vertical-align: top">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-sm btn-complete" type="button"><i class="fa fa-file"></i>&nbsp; Attach</button> -->
                <button data-dismiss="modal" class="btn btn-sm btn-default" type="button"><i class="fa fa-times-circle"></i>&nbsp; Back</button>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
</div>

<!-- START MODAL  -->
<div class="modal fade slide-up" id="modalFilter" tabindex="-1" role="dialog" aria-hidden="false"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Filter <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                <div class="modal-body text-center m-t-20">
                    <form  class="form-horizontal" >
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Start date </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text" class="form-control start_date"
                                    id="datepicker-start" name="FilterStartDate">
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">End date </label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <input type="text" class="form-control end_date"
                                    id="datepicker-end" name="FilterEndDate">
                                    <div class="input-group-append ">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Service ID </label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="text" class="form-control service_id" name="FilterServiceId">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">CC ID </label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input type="text" class="form-control cc_id" name="FilterCCID">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Description ID</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <textarea class="form-control description" name="FilterDescription"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-complete apply" id="apply" type="button">Apply</button>
                        <button data-dismiss="modal" class="btn btn-default">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<div class="modal fade slide-up" id="modalTicketDetail" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content mod-modal">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Ticket <span class="semi-bold">Detail</span></h5>
                </div>
                
                    <div class=" container-fluid bg-white modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- START card -->
                                <div class="card card-transparent">
                                <div class="card-body">
                                    <ul class="nav nav-tabs no-border notification-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="btn btn-default btn-cons btn-rounded btn-notification-style active m-b-10" href="#loadingBar" role="tab" data-toggle="tab" data-type="position-bar">Ticket Detail</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-default btn-cons btn-rounded btn-notification-style m-b-10" href="#bouncyFlip" role="tab" data-toggle="tab" data-type="position-flip">CRM Detail</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-default btn-cons btn-rounded btn-notification-style m-b-10" href="#circleNotification" role="tab" data-toggle="tab" data-type="position-circle">Ticket Progess</a>
                                    </li>
                                    </ul>
                                    <div class="config-notification">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="loadingBar">
                                        <!-- <h4 class="semi-bold">Ticket Detail</h4> -->
                                        <form>
                                                    <div class="card-body ">
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Ticket ID</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="ticket_id" placeholder="" name="ticket_id" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Ismilling ID</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="ismilling_id" placeholder="" name="ismilling_id">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Service ID</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="service_id" placeholder="" name="service_id">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Open Time</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="open_time" placeholder="" name="open_time">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Attach Time</label>
                                                            <div class="col-md-9">
                                                                <input type="text" class="form-control" id="attach_time" placeholder="" name="attach_time">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="fname" class="col-md-3 control-label">Description</label>
                                                            <div class="col-md-9">
                                                                <textarea rows="5" id="description" name="detail_description" class="form-control"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                        </div>
                                        <div class="tab-pane" id="bouncyFlip">
                                        <!-- <h4 class="semi-bold">CRM</h4> -->
                                                    <ul class="nav nav-tabs nav-tabs-fillup" data-init-reponsive-tabs="dropdownfx">
                                                        <li class="nav-item">
                                                        <a href="#" class="active" data-toggle="tab" data-target="#slide1"><span>Ismilling</span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#" data-toggle="tab" data-target="#slide2"><span>Sisfo MT</span></a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="#" data-toggle="tab" data-target="#slide3"><span>IPDB</span></a>
                                                    </li>
                                                    </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane slide-left active" id="slide1">
                                                        <div class="row">
                                                            <div class="col-lg-12" style="overflow: auto; left: 0px; top: 0px; width: 540px; height: 260px;">
                                                            <iframe id="frameIsmilling" frameborder="0" width="900" height="500" src=""></iframe>                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane slide-left" id="slide2">
                                                    <div class="row">
                                                        <div class="col-lg-12">

                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="tab-pane slide-left" id="slide3">
                                                        <div class="row">
                                                            <div class="col-lg-12">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="tab-pane" id="circleNotification">
                                        <!-- <h4 class="semi-bold">Ticket Progress</h4> -->
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="ticket_progress" >
                                                <thead>
                                                    <tr>
                                                        <th width="5">User</th>
                                                        <th width="5">Time</th>
                                                        <th width="5">Description</th>
                                                    </tr>
                                                </thead>
                                                <tbody style="vertical-align: top">
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                <!-- END card -->
                            </div>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-complete btn-summary" type="button"><i class="fa fa-file"></i>Summary Incident</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- summary incidents -->
<div class="modal fade slide-up" id="modalLogDetail" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal" style="width:1200px">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Incident <span class="semi-bold">Summary</span></h5>
                </div>
                <!-- start form -->
                <div class=" container-fluid bg-white modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- START card -->
                                <div class="card card-transparent">
                                <div class="card-body">
                                    <ul class="nav nav-tabs no-border notification-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="btn btn-default btn-cons btn-rounded btn-notification-style active m-b-10" href="#loadingBar2" role="tab" data-toggle="tab" data-type="position-bar">Incident Detail</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-default btn-cons btn-rounded btn-notification-style m-b-10" href="#bouncyFlip2" role="tab" data-toggle="tab" data-type="position-flip">Ticket Attach</a>
                                    </li>
                                    </ul>
                                    <div class="config-notification">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="loadingBar2">
                                        <form>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="">Incident ID :</label>
                                                    <input type="text" class="form-control" id="mod_row_incidentId" placeholder="Enter Incident ID" readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">Creator :</label>
                                                    <input type="text" class="form-control" id="mod_row_creator" placeholder="Enter Creator" readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">Notification Trigger :</label>
                                                    <input type="text" class="form-control" id="mod_row_notificationTrigger" placeholder="Notification Trigger" readonly>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="">State :</label>
                                                    <input type="text" class="form-control" id="mod_row_state" placeholder="Enter State" readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">Clasification :</label>
                                                    <input type="text" class="form-control" id="mod_row_clasification" placeholder="Enter Clasification" readonly>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">Root Cause :</label>
                                                    <input type="text" class="form-control" id="mod_row_rootCause" placeholder="Root Cause" readonly>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label for="">Root Cause Detail :</label>
                                                    <input type="text" class="form-control" id="mod_row_rootCauseDetail" placeholder="Enter Root Cause Detail" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Hirarchy :</label>
                                                    <input type="text" class="form-control" id="mod_row_hirarchy" placeholder="Enter Hirarchy" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Link :</label>
                                                    <input type="text" class="form-control" id="mod_row_link" placeholder="Enter Link" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Equipment :</label>
                                                    <input type="text" class="form-control" id="mod_equipment" placeholder="Enter Site" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="">Description :</label>
                                                <textarea class="form-control" id="mod_row_description" placeholder="Enter Description" rows="5" readonly></textarea>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label for="">Link Start Region :</label>
                                                    <input type="text" class="form-control" id="mod_row_link_start_region" placeholder="Enter Link" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Link End Region:</label>
                                                    <input type="text" class="form-control" id="mod_row_link_end_region" placeholder="Enter Link" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Link Start Site:</label>
                                                    <input type="text" class="form-control" id="mod_row_link_start_site" placeholder="Enter Link" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Link end Site :</label>
                                                    <input type="text" class="form-control" id="mod_row_link_end_site" placeholder="Enter Link" readonly>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label for="">Region :</label>
                                                    <input type="text" class="form-control" id="mod_row_region" placeholder="Enter Region" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Site :</label>
                                                    <input type="text" class="form-control" id="mod_row_site" placeholder="Enter Site" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Source Category :</label>
                                                    <input type="text" class="form-control" id="mod_source_category" placeholder="Enter Site" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Brand :</label>
                                                    <input type="text" class="form-control" id="mod_brand" placeholder="Enter Site" readonly>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-3">
                                                    <label for="">Creation Time :</label>
                                                    <input type="text" class="form-control" id="mod_row_creationTime" placeholder="Enter Creation Time" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Duration :</label>
                                                    <input type="text" class="form-control" id="mod_row_duration" placeholder="Enter Duration" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">Total Stop Lock :</label>
                                                    <input type="text" class="form-control" id="mod_row_stopLock" placeholder="Enter Total Stop Lock" readonly>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="">TTR :</label>
                                                    <input type="text" class="form-control" id="mod_row_ttr" placeholder="Enter Total Stop Lock" readonly>
                                                </div>
                                            </div>
                                            
                                        </form>
                                        </div>
                                        <div class="tab-pane" id="bouncyFlip2">
                                            <div class="table-responsive">
                                            <table class="table table-hover table-condensed" id="detail_incident_ticket" style="width:100%">
                                                <thead>
                                                    <tr class="color-tr">
                                                        <th style="width:1%">No.</th>
                                                        <th style="width:1%">Service ID</th>
                                                        <th style="width:1%">Ticket ID</th>
                                                        <th style="width:1%">Open Time</th>
                                                        <th style="width:95%">Description</th>
                                                        <th style="width:1%">Action</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr class="color-tr">
                                                        <td colspan="6"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                <!-- END card -->
                            </div>
                        </div>
                    </div>

                    <!-- end body -->
                    <!-- start footer -->
                    <div class="modal-footer">
                        <button class="btn btn-sm btn-default" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                    </div>
                    <!-- end footer -->
                
            </div>
            <!-- end of mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>


<!-- crm -->
<div class="modal fade slide-up" id="modalCrm" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>CRM info <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                    <div class="modal-body m-t-20">
                        <iframe id="frameIsmillingSummary" frameborder="0" width="900" height="500" src=""></iframe>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                    </div>
                    <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<!-- START MODAL TICKET PROGRESS -->
<div class="modal fade slide-up" id="modalTicketProgress" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <!-- mod-modal -->
            <div class="modal-content mod-modal">
                <div class="modal-header">
                    <h5>Ticket Progress <span class="semi-bold"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                </div>
                    <div class="modal-body m-t-20">
                        <table class="table table-striped table-hover table-condensed" id="ticket_progress2">
                            <thead>
                                <tr class="color-tr">
                                    <th width="1">No.</th>
                                    <th width="1">User</th>
                                    <th width="1">Timestamp</th>
                                    <th width="1">Description</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr class="color-tr">
                                    <td colspan="4"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- end modal body -->
                    <!-- modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
                    </div>
                    <!-- end modal footer -->
            </div>
            <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<!-- END MODAL  -->

<div class="modal fade slide-up" id="modalpopupNotUser" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Forbidden</span></h5>
                </div>
                <div class="card mod-card" style="margin-bottom:0px">
                    <div class="card-header">
                        <span>This ticket is currently locked !</span>
                    </div>
                    <div class="card-body">
                        
                        <button data-dismiss="modal" class="btn btn-default" type="button" id="">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<div class="modal fade slide-up" id="modalpopupUnlock" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Unlock <span class="semi-bold">Ticket</span></h5>
                </div>
                <div class="card mod-card" style="margin-bottom:0px">
                    <div class="card-header">
                        <span>are you sure want to unlock ticket?</span>
                    </div>
                    <div class="card-body">
                        <button class="btn btn-complete" type="button" id="yesUnlock">Yes</button>
                        <button class="btn btn-default" type="button" id="noUnlock">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<div class="modal fade slide-up" id="modalpopupIncident" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5>Attention </h5>
                </div>
                <div class="card mod-card" style="margin-bottom:0px">
                    <div class="card-header">
                        <span>Please Select Data Incidents!</span>
                    </div>
                    <div class="card-body">
                        <!-- <button id="attachIncident" data-target="#modalAttachIncident" data-toggle="modal" class="btn btn-primary btn-xs" type="button">Attach Other Ticket</button> -->
                        <button class="btn btn-default" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<div class="modal fade slide-up" id="modalpopupTicket" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content modal-delete-user ">
                <div class="card mod-card">
                    <div class="card-header">
                        <span>Data Tickets is Mandatory!</span>
                    </div>
                    <div class="card-body">
                        <button id="attachIncident" data-target="#modalAttachIncident" data-toggle="modal" class="btn btn-primary btn-xs" type="button">Attach Other Ticket</button>
                        <button class="btn btn-default btn-xs" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

<script>
    listTickets = "<?php echo base_url('ticket/list_open_ticket') ?>"
    progress = "<?php echo base_url('ticket/get_ticket_progress') ?>"

    analyze_possible = "<?php echo base_url('ticket/get_ticket_table_analyze_possible') ?>"
    analyze_open_incident = "<?php echo base_url('ticket/get_ticket_table_analyze_open_incident') ?>"
    related_andop = "<?php echo base_url('ticket/get_ticket_table_analyze_related_andop') ?>"
    define_attach_ticket = "<?php echo base_url('ticket/get_ticket_table_define_attach_ticket') ?>"

    incident = "<?php echo base_url('incident/get_incident_open') ?>"
    session_username = "<?= $_COOKIE['username']; ?>"

</script>