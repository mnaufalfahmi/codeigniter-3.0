<!DOCTYPE html>
<html lang="en">

<head profile="http://www.w3.org/2005/10/profile">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta charset="utf-8" />
  <title>Field Support Management</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
  <link rel="icon" type="image/x-icon" href="<?php echo $this->template->get_theme_path(); ?>assets/img/logo_icon.png" />

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <meta content="" name="description" />
  <meta content="" name="author" />

  <?php $this->assetic->writeCssLinks(); ?>

  <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"> -->
  <!-- <link href="//cdn.datatables.net/colvis/1.1.0/css/dataTables.colVis.css" rel="stylesheet"> -->
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css"> -->
  <!-- <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet"> -->

</head>
<style>
  .table tbody tr td {
    padding: 2px;
  }

  .toolbar {
    float: right;
  }

  .toolbar-left {
    float: left;
  }

  @media only screen and (min-width: 800px) {
    .mod-modal {
      width: 900px;
      /* overflow-y: scroll;
      height:580px; */
    }
  }

  @media only screen and (min-width: 800px) {
    .mod-modal-progress {
      width: 1038px;
      /* overflow-y: scroll;
      height:580px; */
    }
  }

  .modal-body {
    overflow-y: scroll !important;
    height: 500px !important;
  }

  .cs-wrapper {
    display: inherit;
  }

  .modal-delete-user {
    height: 100px;
  }

  .form-horizontal .form-group {
    padding-top: 1px;
    padding-bottom: 1px;
    border-bottom: 0px;
    margin-bottom: 2px;
  }

  .add-user {
    position: absolute;
  }

  .card .card-summary {
    padding-top: 3px;
    padding-bottom: 3px;
    min-height: 0px;
  }

  .mod-card .card-header {
    margin: 0 auto;
  }

  .mod-card .card-body {
    margin: 0 auto;
  }

  button.buttons-colvis {
    padding: 1px 7px 2px;
    font-size: 1.1em;
    margin-bottom: 0px;
    border: 1px solid #f5f5f5;
    background-color: #f5f5f5;
  }

  div.dt-buttons {
    position: relative;
    float: left;
    display: flex;
    align-items: center;
  }

  button.active {
    background-color: #fff !important;
    background-image: none !important;
    box-shadow: none !important;
    border: none !important;
  }

  button.buttons-columnVisibility {
    background-color: #fff !important;
    border: none !important;
  }

  .button-row {
    display: flex;
  }

  .button-row>button {
    flex: 1;
  }

  .add-broadcast-modal {
    width: 900px;
  }

  .form-check-label {
    margin-right: 50px;
  }

  td.details-control {
    background: url('<?php echo base_url() ?>themes/default/assets/plugins/datatables-responsive/img/details_open.png') no-repeat center center !important;
    cursor: pointer;
  }

  tr.shown td.details-control {
    background: url('<?php echo base_url() ?>themes/default/assets/plugins/datatables-responsive/img/details_close.png') no-repeat center center !important;
  }

  .modal .modal-header {
    padding-top: 0px;
    box-shadow: 0px 0px 10px #dedede;
  }

  .modal .modal-footer {
    box-shadow: 0px 0px 10px #dedede;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-right: 10px;
  }

  /* .modal-footer{
      padding: 0px !important;
      } */

  /* /table selected/ */
  .table tbody tr.selected td {
    background: #B0BED9 !important;
  }

  .table.table-hover tbody tr.selected:hover td {
    background: #B0BED9 !important;
  }

  table>thead>tr>th,
  table>tfoot>tr>td,
  table>tfoot>tr>th {
    background: #3F51B5 !important;
    color: #fff !important;
  }

  /* table>tfoot>tr>td {
    color: #000 !important;
    background: #dedede;
  } */

  table.dataTable th,
  table>tfoot>tr {
    padding-top: 0px !important;
    padding-bottom: 0px !important;
  }

  table>thead>tr>th {
    line-height: 3 !important;
  }

  .speech-bubble {
    position: relative;
    background: #dcf8c6;
    border-radius: 0.4em;

    padding-left: 10px;
  }

  .speech-bubble:after {
    content: '';
    position: absolute;
    right: 0;
    top: 76%;
    width: 0;
    height: 0;
    border: 32px solid transparent;
    border-left-color: #dcf8c6;
    border-right: 0;
    border-top: 0;
    margin-top: -55px;
    margin-right: -28px;
  }

  .speech-bubble2 {
    position: relative;
    background: #ffff;
    border-radius: .4em;
  }

  .speech-bubble2:after {
    content: '';
    position: absolute;
    left: 12px;
    top: 50%;
    width: 0;
    height: 0;
    border: 32px solid transparent;
    border-right-color: #ffff;
    border-left: 0;
    border-top: 0;
    margin-top: -16px;
    margin-left: -32px;
  }

  .speech-bubble-front {
    position: relative;
    background: #dcf8c6;
    border-radius: 0.4em;

    padding-left: 10px;
  }

  .speech-bubble-front:after {
    content: '';
    position: absolute;
    right: 21px;
    top: 90%;
    width: 0;
    height: 0;
    border: 11px solid transparent;
    border-left-color: #dcf8c6;
    border-right: 0;
    border-top: 0;
    margin-top: -55px;
    margin-right: -28px;
  }

  .speech-bubble3 {
    position: relative;
    background: #ffff;
    border-radius: .4em;
  }

  .speech-bubble3:after {
    content: '';
    position: absolute;
    left: 24px;
    top: 50%;
    width: 0;
    height: 0;
    border: 11px solid transparent;
    border-right-color: #ffff;
    border-left: 0;
    border-top: 0;
    margin-top: -16px;
    margin-left: -32px;
  }

  .circle {
    background: red;
    border-radius: 50%;
    height: 20px;
    width: 20px;
    position: absolute;
    top: 10px;
    right: -11px;
  }

  /* .notif_stopclock,
  .notif_stopclock::before,
  .notif_stopclock::after {
    background: red;
    border-radius: 50%;
    height: 20px;
    width: 20px;
    position: absolute;
    top: 32px;
    right: 1180px;
  } */

  .circle-content {
    color: #fff;
    float: left;
    line-height: 1;
    margin-top: -0.5em;
    padding-top: 50%;
    text-align: center;
    width: 100%;
    font-size: 9px;
    font-weight: bold;
  }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>public/css/message.css">
<script src="<?php echo base_url() ?>public/js/message.js"></script>
<script src="<?php echo base_url() ?>public/js/jquery.min.js"></script>
<script src="<?php echo base_url() ?>public/websocket/fancywebsocket.js"></script>

<body>

  <body class="fixed-header dashboard">
    <!-- SIDEBAR -->
    <nav class="page-sidebar" data-pages="sidebar">

      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header m-b-10">
        Field Support Management
      </div>
      <!-- END SIDEBAR MENU HEADER-->

      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
          <?php foreach ($menus as $menu) { ?>
            <li id="<?php echo $menu['id'] ?>">
              <a href="<?php echo $menu['url'] ?>">
                <span class="title"><?php echo $menu['title'] ?></span>
                <span <?php if (!empty($menu['sub_menus'])) {
                        echo "class='arrow'";
                      } ?>></span></a>
              <span class="icon-thumbnail"><i class="<?php echo $menu['icon'] ?>"></i></span>
              <?php if (!empty($menu['sub_menus'])) { ?>
                <ul class="sub-menu">
                  <?php foreach ($menu['sub_menus'] as $second) { ?>
                    <li id="<?php echo $second['id'] ?>">
                      <a href="<?php echo $second['url'] ?>">
                        <span class="title"><?php echo $second['title'] ?></span>
                        <span <?php if (!empty($second['third_menus'])) {
                                echo "class='arrow'";
                              } ?>></span></a>
                      </a>
                      <span class="icon-thumbnail"><?php echo $second['icon'] ?></span>
                      <?php if (!empty($second['third_menus'])) { ?>
                        <ul class="sub-menu">
                          <?php foreach ($second['third_menus'] as $third) { ?>
                            <li id="<?php echo $third['id'] ?>">
                              <a href="<?php echo $third['url'] ?>"><?php echo $third['title'] ?></a>
                              <span class="icon-thumbnail"><?php echo $third['icon'] ?></span>
                            </li>
                          <?php } ?>
                        </ul>
                      <?php } ?>
                    </li>
                  <?php } ?>
                </ul>
              <?php } ?>
            </li>
          <?php } ?>

        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>


    <!-- START PAGE-CONTAINER -->
    <div class="page-container ">
      <!-- START HEADER -->
      <div class="header shadow-sm">
        <!-- START MOBILE SIDEBAR TOGGLE -->
        <a href="#" class="btn-link toggle-sidebar d-lg-none pg pg-menu" data-toggle="sidebar">
        </a>
        <!-- END MOBILE SIDEBAR TOGGLE -->
        <div class="">
          <div class="brand inline">
            <h5>FIELD SUPPORT MANAGEMENT</h5>
            <!-- <img src="assets/img/logo.png" alt="logo" data-src="assets/img/logo.png" data-src-retina="assets/img/logo_2x.png" width="78" height="22"> -->
          </div>
          <!-- START REQUEST NOTIFICATION -->
          <?php $data_arr = array('NOC', 'SBU', 'CONTACTCENTER'); ?>
          <?php if (in_array($_COOKIE['member'], $data_arr)) { ?>
            <?php if ($_COOKIE['role_id'] == 12 || $_COOKIE['role_id'] == 68 ){ ?>
            <ul class="d-lg-inline-block notification-list no-margin d-lg-inline-block b-grey b-l no-style p-r-10">
              <li class="p-r-10 inline">
                <div class="dropdown">
                  <span style="cursor: pointer" class="shadow-sm btn-listrequest" data-toggle="tooltip" title="Request StopClock"><i class="fa fa-envelope-o fa-lg"></i>
                    <!-- &nbsp;
                    Stopclock -->
                    <div class="circle notif_stopclock">
                      <div class="circle-content total-request">...</div>
                    </div>
                  </span>
                </div>
              </li>
            </ul>
            <?php } ?>
          <?php } ?>

          <!-- START NOTIFICATION LIST -->
          <ul class="d-lg-inline-block notification-list no-margin d-lg-inline-block b-grey b-l b-r no-style p-r-20">
            <li class="p-r-10 inline">
              <div class="dropdown">
                <!-- <a href="javascript:;" id="notification-center" class="header-icon pg pg-world" data-toggle="dropdown"> -->
                <!-- <span class="bubble"></span> -->
                <!-- </a> -->
                <span data-toggle="dropdown">
                  <!-- <button class="btn btn-xs btn-success" id="notification-center" data-toggle="tooltip" title="Incident Progress"><i class="fa fa-bell-o"></i> -->
                  <span style="cursor: pointer" class="shadow-sm" id="notification-center" data-toggle="tooltip" title="Incident Progress"><i class="fa fa-bell-o fa-lg"></i>
                    <!-- &nbsp;Incident -->
                    <div class="circle notif_token">
                      <div class="circle-content count-token">...</div>
                    </div>
                  </span>
                </span>
                <div class="dropdown-menu notification-toggle" role="menu" aria-labelledby="notification-center">
                  <div class="notification-panel">
                    <div class="notification-body scrollable">
                      <div id="form-notification">
                        <form id="notification">
                        </form>
                      </div>
                    </div>

                    <div class="notification-footer text-center" id="footer-notification">

                    </div>
                  </div>
                </div>
              </div>
            </li>
            <!-- <li class="p-r-10 inline">
              <a href="#" class="header-icon pg pg-link"></a>
            </li>
            <li class="p-r-10 inline">
              <a href="#" class="header-icon pg pg-thumbs"></a>
            </li> -->
          </ul>
          <!-- END NOTIFICATIONS LIST -->
          <!-- <a href="#" class="search-link d-lg-inline-block d-none" data-toggle="search"><i class="pg-search"></i>Type anywhere to <span class="bold">search</span></a> -->
        </div>
        <div class="d-flex align-items-center">
          <!-- START User Info-->
          <div class="pull-left p-r-10 fs-14 font-heading d-lg-block d-none b-r b-grey">
            <!-- <p class="text-time" style="vertical-align:middle; display:table-cell"><strong></strong></p> -->
            <p class="text-time2" style="vertical-align:middle; display:table-cell"><strong></strong></p>
            <p style="display:none" class="input-time2"></p>
          </div>
          <?php //if ($this->session->userdata('member') == 'NOC') { 
          ?>
          <!-- <div class="pull-left p-r-10 fs-14 font-heading d-lg-block d-none b-r b-grey ml-3">
              <button class="btn btn-xs btn-info shadow-sm btn-listrequest mr-2"><strong><i class="fa fa-bell fa-lg"></i>&nbsp; Request Stop Clock</strong>
                &nbsp;
                <span class="total-request bg-danger text-white rounded-circle pr-2 pl-2" style="border:1px solid #fff; position:absolute; margin-top:7px"><strong></strong></span>
              </button>
            </div> -->
          <?php //} 
          ?>
          <div class="pull-left p-r-10 fs-14 font-heading d-lg-block b-r b-grey ml-3">
            <button class="btn btn-xs btn-info shadow-sm btn-claim-mod"><strong><i class="fa fa-user-circle fa-lg"></i>&nbsp; I'M MOD</strong></button>
            <!-- <button style="display:none" class="btn btn-xs btn-info shadow-sm btn-claim-mod-"><strong><i class="fa fa-user-circle fa-lg"></i>&nbsp; I'M MOD</strong></button> -->
          </div>
          <div class="pull-left p-r-10 fs-14 font-heading d-lg-block b-r b-grey ml-3">
            <!-- <button style="display:none" id="btn-checkin" class="btn btn-xs btn-danger shadow-sm btn-mod" data-flex="1" data-userid="<?php  // echo $this->session->userdata('user_id') 
                                                                                                                                          ?>" data-username="<?php  // echo ucwords($this->session->userdata('username')) 
                                                                                                                                                              ?>"><strong><i class="fa fa-power-off fa-lg"></i>&nbsp; Check Out</strong></button>
            <button style="display:none" id="btn-checkout" class="btn btn-xs btn-success shadow-sm btn-mod" data-flex="0" data-userid="<?php // echo $this->session->userdata('user_id') 
                                                                                                                                        ?>"><strong><i class="fa fa-check-circle fa-lg"></i>&nbsp; Check In</strong></button> -->
            <?php if ($data['status_checkin'] == 1) { ?>
              <input class="mycheckin" type="checkbox" checked data-toggle="toggle" data-size="mini" data-onstyle="success" data-on="Check in" data-off="Check out">
            <?php } else { ?>
              <input class="mycheckin" type="checkbox" data-toggle="toggle" data-size="mini" data-onstyle="success" data-on="Check in" data-off="Check out">
            <?php } ?>
            <!-- <span class="checkbox_text">Check In</span>
            <input type="checkbox" class="mycheckin" data-init-plugin="switchery" data-size="small" checked="checked" /> -->
          </div>
          <div class="pull-left p-r-10 fs-14 font-heading d-lg-block d-none ml-3">
            <span class="semi-bold d-block"><?= ucwords($_COOKIE['username']); ?></span>
            <span class="semi-bold" style="color:#989898; font-size:10px"><?= $_COOKIE['group_name']; ?></span>
          </div>
          <div class="dropdown pull-right d-lg-block">
            <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="thumbnail-wrapper d32 circular inline">
                <?php
                // $path = $this->template->get_theme_path() . "/assets/img/profiles/" . $this->session->userdata('avatar');
                $path = $this->template->get_theme_path() . "/assets/img/profiles/" . $_COOKIE['avatar'];
                if ($_COOKIE['avatar'] == 'default.png' || $_COOKIE['avatar'] == null || empty($_COOKIE['avatar'])) { ?>
                  <img src="<?php echo $this->template->get_theme_path(); ?>assets/img/profiles/default.png" alt="" data-src="<?php echo $this->template->get_theme_path(); ?>assets/img/profiles/default.png" data-src-retina="<?php echo $this->template->get_theme_path(); ?>assets/img/profiles/default.png" width="32" height="32">

                <?php
                } else {
                ?>
                  <img src="<?php echo $path ?>" alt="" data-src="<?php echo $path ?>" data-src-retina="<?php echo $path ?>" width="32" height="32">
                <?php } ?>

              </span>
            </button>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
              <a href="#" class="dropdown-item"><i class="pg-settings_small"></i> Settings</a>
              <a href="#" class="dropdown-item"><i class="pg-outdent"></i> Feedback</a>
              <a href="#" class="dropdown-item"><i class="pg-signals"></i> Help</a>
              <a href="<?= base_url() ?>user/signout" class="clearfix bg-master-lighter dropdown-item">
                <span class="pull-left">Logout</span>
                <span class="pull-right"><i class="pg-power"></i></span>
              </a>
            </div>
          </div>
          <!-- END User Info-->
          <!-- <a href="#" class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
        </div>
      </div>
      <!-- END HEADER -->

      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <!-- MULAI KODE DISINI -->
          <?php echo $template['body'] ?>
          <!-- AKHIR KODE DISINI -->
        </div>
        <!-- END PAGE CONTENT -->


        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <div class=" container-fluid  container-fixed-lg footer">
          <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">Copyright &copy; <?= date('Y') ?> </span>
              <span class="font-montserrat">Icon Plus</span>.
              <span class="hint-text">All rights reserved. </span>
              <!-- <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> <span class="muted">|</span> <a href="#" class="m-l-10">Privacy Policy</a></span> -->
            </p>
            <p class="small no-margin pull-right sm-pull-reset">

            </p>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->

    <!-- modal progress bar -->
    <div class="modal fade slide-up" id="modalprogressbar" role="dialog" aria-hidden="false" data-backdrop="false" data-keyboard="false">
      <img class="image-responsive-height demo-mw-50" src="<?php echo $this->template->get_theme_path(); ?>assets/img/demo/progress.svg" alt="Progress" style="margin-top: 300px;margin-left: 650px;">
    </div>

    <div class="modal fade slide-up" id="modalerror" role="dialog" aria-hidden="false">
      <div class="modal-dialog">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <h5>Attention </h5>
            </div>
            <div class="card mod-card" style="margin-bottom:0px">
              <div class="card-header">
                <span class="content_alert">something error please try again!</span>
              </div>
              <div class="card-body">
                <!-- <button id="attachIncident" data-target="#modalAttachIncident" data-toggle="modal" class="btn btn-primary btn-xs" type="button">Attach Other Ticket</button> -->
                <button class="btn btn-default" id="ok">Ok</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- START MODAL REQUEST STOP CLOCK  -->
    <div class="modal fade slide-up" id="modalRequestStopClock" role="dialog" aria-hidden="false">
      <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
          <!-- mod-modal -->
          <div class="modal-content mod-modal">
            <div class="modal-header">
              <h5>Request Stop Clock <span class="semi-bold"></span></h5>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
            </div>
            <div class="modal-body">
              <div class="data-request-stopclock mt-2">
                <table class="table table-hover table-striped table-condensed" id="tb_request_stopclock" style="width:100%">
                  <thead>
                    <tr>
                      <th style="width:5px">No.</th>
                      <th style="width:20px">Incident ID</th>
                      <th style="width:100px">Description</th>
                      <th style="width:50px">Time Stop Request</th>
                      <th style="width:50px">Reason</th>
                      <th style="width:50px">Reason Type</th>
                      <th style="width:20px">User Requeust</th>
                      <th style="width:20px">Group</th>
                      <th style="width:20px">File Name</th>
                      <th style="width:30px">Aksi</th>
                    </tr>
                  </thead>
                  <!-- <tfoot>
                    <tr>
                      <td colspan="8"></td>
                    </tr>
                  </tfoot> -->
                </table>
              </div>
            </div>
            <!-- modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-danger" type="button" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
            </div>
            <!-- end modal footer -->
          </div>
          <!-- endof mod-modal -->
        </div>
        <!-- /.modal-content -->
      </div>
    </div>
    <!-- END MODAL  -->


    <!-- Modal Confirmation MOD  -->
    <div class="modal fade slide-up" id="modalConfirm" role="dialog" aria-hidden="false" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog">
        <div class="modal-content-wrapper">
          <div class="modal-content">
            <div class="modal-header clearfix text-left">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
              </button>
              <h5>Mod <span class="semi-bold">Confirmation</span></h5>
            </div>
            <div class="card mod-card" style="margin-bottom:0px">
              <div class="card-header">
                <span class="content_message">Are you sure want to be MOD ?</span>
              </div>
              <div class="card-body">
                <form id="form-work" class="form-horizontal form_create_mod" role="form">
                  <button class="btn btn-complete btn-yes" type="submit">Yes</button>
                  <button class="btn btn-default btn-no" data-dismiss="modal">No</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
    </div>

    <audio id="myAudio">
      <source src="<?= base_url('themes/default/assets/audio/eventually.mp3') ?>" type="audio/mpeg" allow="autoplay" muted="muted">
    </audio>

    <script>
      var source;
      var stream;
      var source_kpi;
      var status_checkin;
      baseurlapi = "<?php echo $this->config->item('api_url') ?>";
      baseurl = "<?php echo base_url() ?>"
      create = "<?php echo $data['crud']['create'] ?>";
      read = "<?php echo $data['crud']['read'] ?>";
      del = "<?php echo $data['crud']['delete'] ?>";
      update = "<?php echo $data['crud']['update'] ?>";
      groupcode = "<?php echo $_COOKIE['group_code'] ?>";
      username = "<?php echo $_COOKIE['username'] ?>";
      path = "<?php echo $this->template->get_theme_path(); ?>";
      mod = "<?= (isset($_COOKIE['mod']) ? $_COOKIE['mod'] : '') ?>";
      type = "<?php echo $_COOKIE['type'] ?>";
      member = "<?php echo $_COOKIE['member'] ?>";
      userid = "<?php echo $_COOKIE['user_id'] ?>";
      sess_checkin = "<?= (isset($_COOKIE['user_checkin']) ? $_COOKIE['user_checkin'] : '') ?>";
      group_id = <?php echo $_COOKIE['group_id'] ?>;
      role = "<?php echo $_COOKIE['role_id'] ?>";

      // get cookies
      function getCookie(name) {
          // Split cookie string and get all individual name=value pairs in an array
          var cookieArr = document.cookie.split(";");

          // Loop through the array elements
          for (var i = 0; i < cookieArr.length; i++) {
              var cookiePair = cookieArr[i].split("=");

              /* Removing whitespace at the beginning of the cookie name
              and compare it with the given string */
              if (name == cookiePair[0].trim()) {
                  // Decode the cookie value and return
                  return decodeURIComponent(cookiePair[1]);
              }
          }

          // Return null if not found
          return null;
      }

    </script>

    <script>
      function activeClass(menu, first, second = null) {
        $('#' + menu).addClass('active')
        $('#' + menu + ' .arrow').addClass('open')
        $('#' + first).addClass('active')
        $('#' + second).addClass('active')
        $('#' + first).find('.sub-menu').attr('style', 'display:block')
      }
    </script>

    <?php $this->assetic->writeJsScripts(); ?>

    <script>
      function play() {
        // play audio
        var x = document.getElementById("myAudio");
        x.play();
      }
      var Server;
      Server = new FancyWebSocket('ws://10.14.22.85:9300');
      // Server = new FancyWebSocket('ws://127.0.0.1:9300');
      Server.connect();

      // buat yang di icon notif
      Server.bind('notification', function(data) {

        // buat validasi siapa saja yang dapat menerima notifikasinya
        // $('#notification').empty();
        // $('#footer-notification').empty();

        // buat fungsi angka
        // $('.count-token').empty();
        // $('.count-token').html(data.total_not_read);

        // for (var i = 0; i < data.notification.length; i++) {

        //     if (data.notification[i].is_read == '1') {
        //       var html = `<div class="notification-item clearfix" id="unread">
        //       <div class="heading">
        //         <a href="#" id="cs" class="text-danger pull-left toggle-more-details">
        //           <div class="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
        //           <img width="30" height="30" data-src-retina="` + path + `assets/img/profiles/contact-center.png" data-src="` + path + `assets/img/profiles/contact-center.png" alt="" src="` + path + `assets/img/profiles/contact-center.png">
        //         </div>
        //           <span class="bold">` + data.notification[i].group_code + `</span>
        //           <span class="fs-12 m-l-10">` + data.notification[i].progress_fullname + `</span>
        //         </a>
        //         <div class="pull-right">
        //           <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
        //             <div><i class="fa fa-angle-left"></i>
        //             </div>
        //           </div>
        //           <span class=" time">` + data.notification[i].progress_created_on + `</span>
        //         </div>
        //         <div class="more-details">
        //           <div class="more-details-inner">
        //             <h5 class="semi-bold fs-16">“` + data.notification[i].progress_message + `.”</h5>
        //             <p class="small hint-text">
        //               Commented on incidents <b><a onClick="is_read(` + data.notification[i].id + `)" href="` + baseurl + `incident/progress/` + data.notification[i].incident_id + `">` + data.notification[i].description + `.
        //             </a></b></p>
        //           </div>
        //         </div>
        //       </div>
        //       ` + statment_read(data.notification[i].is_read) + `

        //         <a href="#" class="mark" onClick="is_read(` + data.notification[i].id + `)"></a>
        //       </div>
        //     </div>`
        //     } else {
        //         var html = `<div class="notification-item unread clearfix" id="unread">
        //         <div class="heading">
        //           <a href="#" id="cs" class="text-danger pull-left toggle-more-details">
        //             <div class="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
        //             <img width="30" height="30" data-src-retina="` + path + `assets/img/profiles/contact-center.png" data-src="` + path + `assets/img/profiles/contact-center.png" alt="" src="` + path + `assets/img/profiles/contact-center.png">
        //           </div>
        //             <span class="bold">` + data.notification[i].group_code + `</span>
        //             <span class="fs-12 m-l-10">` + data.notification[i].progress_fullname + `</span>
        //           </a>
        //           <div class="pull-right">
        //             <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
        //               <div><i class="fa fa-angle-left"></i>
        //               </div>
        //             </div>
        //             <span class=" time">` + data.notification[i].progress_created_on + `</span>
        //           </div>
        //           <div class="more-details">
        //             <div class="more-details-inner">
        //               <h5 class="semi-bold fs-16">“` + data.notification[i].progress_message + `.”</h5>
        //               <p class="small hint-text">
        //                 Commented on incidents <b><a onClick="is_read(` + data.notification[i].id + `)" href="` + baseurl + `incident/progress/` + data.notification[i].incident_id + `">` + data.notification[i].description + `.
        //               </a></b></p>
        //             </div>
        //           </div>
        //         </div>
        //         ` + statment_read(data.notification[i].is_read) + `

        //           <a href="#" class="mark" onClick="is_read(` + data.notification[i].id + `)"></a>
        //         </div>
        //       </div>`
        //     }

        //     // if (data.notification[i].mention_to == groupcode) {
        //       if (data.notification[i].is_read == '1') {
        //           $('#notification').append(html);
        //           $('#footer-notification').html(`<a href="#" class="">notifications</a>
        //           <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
        //             <i class="pg-refresh_new"></i>
        //           </a>`);
        //       } else {
        //           $('#notification').append(html);
        //           $('#footer-notification').html(`<a href="#" class="">notifications</a>
        //           <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
        //             <i class="pg-refresh_new"></i>
        //           </a>`);
        //       }
        //     // } else {
        //     //   $('#footer-notification').html(`<a href="#" class="">none notifications</a>
        //     //   <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
        //     //     <i class="pg-refresh_new"></i>
        //     //   </a>`);
        //     // }
        // }

      });

      // buat yand di popup notif
      Server.bind('notification2', function(data) {
        dhtmlx.message({
          'text': `<a style="color:black;" href="http://10.14.22.85/fms-dashboard/incident/progress/` + data.incident.id + `"><b>` + data.data.name + ` | ` + data.data.username + `</b> membalas pesan di incident ` + data.incident.incident_id + `</a>`,
          'expire': 0
        });notification
        play();

        // buat validasi siapa saja yang dapat menerima notifikasinya
        if (group_id == parseInt(data.group_id_notification)) {
          $('#notification').empty();
          $('#footer-notification').empty();

          // buat fungsi angka
          $('.count-token').empty();
          $('.count-token').html(data.total_not_read);

          for (var i = 0; i < data.notification.length; i++) {

            if (data.notification[i].is_read == '1') {
              var html = `<div class="notification-item clearfix" id="unread">
              <div class="heading">
                <a href="#" id="cs" class="text-danger pull-left toggle-more-details">
                  <div class="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
                  <img width="30" height="30" data-src-retina="` + path + `assets/img/profiles/contact-center.png" data-src="` + path + `assets/img/profiles/contact-center.png" alt="" src="` + path + `assets/img/profiles/contact-center.png">
                </div>
                  <span class="bold">` + data.notification[i].group_code + `</span>
                  <span class="fs-12 m-l-10">` + data.notification[i].progress_fullname + `</span>
                </a>
                <div class="pull-right">
                  <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
                    <div><i class="fa fa-angle-left"></i>
                    </div>
                  </div>
                  <span class=" time">` + data.notification[i].progress_created_on + `</span>
                </div>
                <div class="more-details">
                  <div class="more-details-inner">
                    <h5 class="semi-bold fs-16">“` + data.notification[i].progress_message + `.”</h5>
                    <p class="small hint-text">
                      Commented on incidents <b><a onClick="is_read(` + data.notification[i].id + `)" href="` + baseurl + `incident/progress/` + data.notification[i].incident_id + `">` + data.notification[i].description + `.
                    </a></b></p>
                  </div>
                </div>
              </div>
              ` + statment_read(data.notification[i].is_read) + `
              
                <a href="#" class="mark" onClick="is_read(` + data.notification[i].id + `)"></a>
              </div>
            </div>`
            } else {
              var html = `<div class="notification-item unread clearfix" id="unread">
                <div class="heading">
                  <a href="#" id="cs" class="text-danger pull-left toggle-more-details">
                    <div class="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
                    <img width="30" height="30" data-src-retina="` + path + `assets/img/profiles/contact-center.png" data-src="` + path + `assets/img/profiles/contact-center.png" alt="" src="` + path + `assets/img/profiles/contact-center.png">
                  </div>
                    <span class="bold">` + data.notification[i].group_code + `</span>
                    <span class="fs-12 m-l-10">` + data.notification[i].progress_fullname + `</span>
                  </a>
                  <div class="pull-right">
                    <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
                      <div><i class="fa fa-angle-left"></i>
                      </div>
                    </div>
                    <span class=" time">` + data.notification[i].progress_created_on + `</span>
                  </div>
                  <div class="more-details">
                    <div class="more-details-inner">
                      <h5 class="semi-bold fs-16">“` + data.notification[i].progress_message + `.”</h5>
                      <p class="small hint-text">
                        Commented on incidents <b><a onClick="is_read(` + data.notification[i].id + `)" href="` + baseurl + `incident/progress/` + data.notification[i].incident_id + `">` + data.notification[i].description + `.
                      </a></b></p>
                    </div>
                  </div>
                </div>
                ` + statment_read(data.notification[i].is_read) + `
                
                  <a href="#" class="mark" onClick="is_read(` + data.notification[i].id + `)"></a>
                </div>
              </div>`
            }

            // if (data.notification[i].mention_to == groupcode) {
            if (data.notification[i].is_read == '1') {
              $('#notification').append(html);
              $('#footer-notification').html(`<a href="#" class="">notifications</a>
                  <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                    <i class="pg-refresh_new"></i>
                  </a>`);
            } else {
              $('#notification').append(html);
              $('#footer-notification').html(`<a href="#" class="">notifications</a>
                  <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                    <i class="pg-refresh_new"></i>
                  </a>`);
            }
            // } else {
            //   $('#footer-notification').html(`<a href="#" class="">none notifications</a>
            //   <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
            //     <i class="pg-refresh_new"></i>
            //   </a>`);
            // }
          }
          // buat tambah ke notifikasi
        }

      });
    </script>

    <script type="text/javascript">
      getTimezoneName()

      function updateTime() {
        var indonesiaTime = new Date().toLocaleString("en-US", {
          timeZone: "Asia/Jakarta"
        });
        indonesiaTime = new Date(indonesiaTime);
        var today = new Date();
        var bb = today.getMonth();
        var yy = today.getFullYear();
        var dd = today.getDate();
        var hh = today.getHours();
        var mm = today.getMinutes();
        var ss = today.getSeconds();

        var h = indonesiaTime.getHours().toLocaleString();
        var m = indonesiaTime.getMinutes().toLocaleString();
        var s = indonesiaTime.getSeconds().toLocaleString();

        var tahun = indonesiaTime.getFullYear();
        var bulan = indonesiaTime.getMonth().toLocaleString();
        var hari = indonesiaTime.getDay().toLocaleString();
        var tanggal = indonesiaTime.getDate().toLocaleString();

        var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        var months_number = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        h = (h < 10) ? "0" + h : h;
        m = (m < 10) ? "0" + m : m;
        s = (s < 10) ? "0" + s : s;

        hh = (hh < 10) ? "0" + hh : hh;
        mm = (mm < 10) ? "0" + mm : mm;
        ss = (ss < 10) ? "0" + ss : ss;

        var bulan_client = (bb < 10) ? "0" + bb : bb;
        var bulan_server = (bulan < 10) ? "0" + bulan : bulan;

        tanggal = (tanggal < 10) ? "0" + tanggal : tanggal;
        tanggal_client = (dd < 10) ? "0" + dd : dd;

        if (getTimezoneName() === ' WIB' || getTimezoneName() == 'GMT+7') {
          var time = days[hari] + " " + tanggal + " " + months[bulan] + " " + h + ":" + m + ":" + s + " WIB";
          var val_time = tahun + '-' + months_number[bulan_server] + '-' + tanggal + ' ' + h + ':' + m + ':' + s;
        } else if (getTimezoneName() === ' WITA' || getTimezoneName() == 'GMT+8') {
          var time = days[hari] + " " + dd + " " + months[bulan] + " " + hh + ":" + mm + ":" + ss + " WITA";
          var val_time = yy + '-' + months_number[bulan_client] + '-' + dd + ' ' + hh + ':' + mm + ':' + ss;
        } else if (getTimezoneName() === ' WIT' || getTimezoneName() == 'GMT+9') {
          var time = days[hari] + " " + dd + " " + months[bulan] + " " + hh + ":" + mm + ":" + ss + " WIT";
          var val_time = yy + '-' + months_number[bulan_client] + '-' + dd + ' ' + hh + ':' + mm + ':' + ss;
        } else {
          var time = days[hari] + " " + tanggal + " " + months[bulan] + " " + h + ":" + m + ":" + s + " WIB";
          var val_time = tahun + '-' + months_number[bulan_server] + '-' + tanggal + ' ' + h + ':' + m + ':' + s;
        }

        // var time = days[hari] + " " + tanggal + " " + months[bulan] + " " + h + ":" + m + ":" + s + " WIB";
        // var val_time = yy + '-' + months_number[bulan_zero] + '-' + dd + ' ' + hh + ':' + mm + ':' + ss;

        var input_time = '<input name="input_time" value="' + val_time + '"> ';
        // console.log(getTimezoneName());

        $('.text-time2').html(time);
        $('.input-time2').html(input_time);
        indonesiaTime++;
      }
      $(function() {
        setInterval(updateTime, 1000);
      });
    </script>
  </body>

</html>