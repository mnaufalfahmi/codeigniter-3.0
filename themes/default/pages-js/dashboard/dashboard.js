$(document).ready(function() {

    var table = $('#assigment_incident').DataTable({
        buttons: [{
            extend: 'colvis',
            text: '<i class="fa fa-table"></i>',
        }, ],
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        bInfo: false
    })

    var table = $('#assigment_incident').DataTable();

    $.ajax({
        url: baseurl + 'dashboard/get_data',
        dataType: 'JSON',
        type: 'GET',
        success: function(data) {
            // untuk incident assigment
            var sum_open_incident = [];
            var sum_all_incident = [];

            $.each(data.incident_assignment, function(i, item) {

                table.row.add([
                    item.username,
                    item.open,
                    item.all
                ]).draw(false);

                sum_open_incident.push(item.open)
                sum_all_incident.push(item.all)
            })

            var tot_open = eval(sum_open_incident.join('+'));
            var tot_all = eval(sum_all_incident.join('+'));

            // untuk assigment total
            $('#assigment_total').html(
                `<tr>
                        <td class="text-left b-r b-dashed b-grey w-25">Unlock Incident</td>
                        <td class="text-center b-r b-dashed b-grey w-25">` + data.incident_unlock_open + `</td>
                        <td class="text-center w-25">` + data.incident_unlock_all + `</td>
                    </tr>
                    <tr>
                        <td class="text-left b-r b-dashed b-grey w-25">TOTAL</td>
                        <td class="text-center b-r b-dashed b-grey w-25">` + (tot_open + data.incident_unlock_open) + `</td>
                        <td class="text-center w-25">` + (tot_all + data.incident_unlock_all) + `</td>
                    </tr>
                    `
            );

            // untuk count incident
            $('#count_open').html(data.incident_open);
            $('#count_stop').html(data.incident_stop);
            $('#count_total').html(data.incident_open + data.incident_stop);

            // untuk open incident
            $('#open_incident').html(`<i class="fa fa-eye text-success"></i> ` + data.incident_open);
            $.each(data.incident_network, function(i, item) {
                $('#network_incident').append(`
                <div class="col-md-6">
                    <p>` + item.network + `</p>
                </div>
                <div class="col-md-6 text-right">
                    <p>` + item.total + `</p>
                </div>
                `);
            })

            // untuk stop clock incident
            $('#stop_clock').html(`<i class="fa fa-clock-o text-warning"></i> ` + data.incident_stop);

            // untuk incident per regional
            $.each(data.incident_regional, function(i, item) {
                $('#incident_regional').append(`
                <tr>
                    <td class="text-left b-r b-dashed b-grey w-25">` + item.region_name + `</td>
                    <td class="text-center">` + item.total + `</td>
                </tr>
                `);
            })

            // untuk incident per network
            $.each(data.incident_link, function(i, item) {
                $('#network').append(`
                <tr>
                    <td class="text-left b-r b-dashed b-grey w-25">` + item.link + `</td>
                    <td class="text-center">` + item.total + `</td>
                </tr>
                `);
            })

            // untuk open incident stop clock
            $.each(data.incident_stop_network, function(i, item) {
                $('#network_incident_stop').append(`
                <div class="col-md-6">
                    <p>` + item.network + `</p>
                </div>
                <div class="col-md-6 text-right">
                    <p>` + item.total + `</p>
                </div>
                `);
            })

            // untuk bobot tickets
            $.each(data.ticket_bobot, function(i, item) {
                $('#bobot').append(`
                <tr>
                    <td class="text-left b-r b-dashed b-grey w-25">` + item.description + `</td>
                    <td class="text-center">` + item.bobot + `</td>
                </tr>
                `);

                $('#bobot').on('dblclick', 'tr', function() {
                    var redirect = window.open(baseurl + 'incident/progress/' + item.id, "_blank");
                    redirect.location;
                });
            })

            // untuk open ticket
            $('#open_ticket').html(`<i class="fa fa-ticket"></i> ` + data.ticket_open)
        }
    })

})