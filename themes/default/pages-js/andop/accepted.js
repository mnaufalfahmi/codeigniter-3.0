$(document).ready(function(){

    $.fn.dataTable.ext.errMode = 'none';
    activeClass('andop','andop-accepted')
    addFieldSupport()
    addFileAttachment()
    datePicker()
    time()
    config_select()
    acceptedStatus()
    removed()

    var table = $('#andop_accepted').DataTable({
        dom: 'Bfrtpi Rl',
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
            {
                text: '<i class="fa fa-plus"></i> Add Andop',
                className: 'btn btn-primary btn-xs hidden',
                attr:  {
                    id: 'add-andop',
                    'data-target':'#modaladdandop',
                    'data-toggle':"modal"
                },
                init: function(api, node, config) {
                    $(node).removeClass('dt-button')
                 }
            }

        ],
        responsive:true,
        lengthChange: false,
        pageLength:15,
        ajax: baseurl +'andop/list_accepted_andop',
        columns: [
            {
            "data": "id",
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
                console.log(data)
                }
            },
            { "data": "created_on" },
            { "data": "fullname" },
            { "data": "job_number" },
            { "data": "title" },
            { "data": "execution_time" },
            { "data": "accepted"},
            { "data": "rejected"}
        ],
        initComplete: function () {
        if (create == 't') {
            $("#add-andop").removeClass("hidden");
            }
        },
    })

$('#andop_accepted tbody').on('dblclick', 'tr', function () {
        var data = table.row( this ).data();
        new_andop_detail(data.job_number)
        andop_detail(data.unique)
});

$('#add-andop').click(function(){
    $('#modaladdandop').modal('show')
});


// table affected services
$('#button_attach').on('click', function() {
    
    var affected_services = $('#attach_affected_service').DataTable({
            dom: 'Bfrtpi',
            select: {
                style: 'multi'
            },
            buttons: [

            ],
            responsive:true,
            lengthChange: false,
            pageLength:10,
            ajax: baseurl +'customer/list',
            columns: [
            {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    console.log(data)
                }
            },
            { "data": "service_id"},
            { "data": "service_name"},
            { "data": "origin" },
            { "data": "termination"},
            { "data": "customer_name"}
            ]
    })

    // sub function 
   $('#attach_affected_service tbody ').on('click', 'tr', function () {
        var data = affected_services.row( this ).data();


        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            // alert('sadis')
        }
        else
        {
           $('#affected_services tr:last').after('\
            <tr class="data">\
                <td>'+data.service_id+'</td>\
                <td>'+data.service_name+'</td>\
                <td>'+data.customer_name+'</td>\
                <td><button title="remove" type="button" class="btn btn-xs remove"><i class="fa fa-minus-circle" style="color:red"></i></button></td>\
                <td style="display:none">'+data.id+'<input name="arr_data_affected_services[]" class="attach" value="'+data.id+'"></td>\
            </tr>');   
        }
   })

    // action remove
    $(document).on('click', '.remove', function(){
        $(this).parents('tr').remove();
    })

    $(document).on('click', '.edit-remove', function(){
        $(this).parents('tr').remove();
    })
});

// table affected services on edit
$('#edit_button_attach').on('click', function() {
    
    var affected_services = $('#edit_attach_affected_service').DataTable({
            dom: 'Bfrtpi',
            select: {
                style: 'multi'
            },
            buttons: [

            ],
            responsive:true,
            lengthChange: false,
            pageLength:10,
            ajax: baseurl +'customer/list',
            columns: [
            {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    console.log(data)
                }
            },
            { "data": "service_id"},
            { "data": "service_name"},
            { "data": "origin" },
            { "data": "termination"},
            { "data": "customer_name"}
            ]
    })

    // sub function 
   $('#edit_attach_affected_service tbody ').on('click', 'tr', function () {
        var data = affected_services.row( this ).data();


        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            // alert('sadis')
        }
        else
        {
           $('#edit_affected_services tr:last').after('\
            <tr class="data edit-remove-div-as">\
                <td>'+data.service_id+'</td>\
                <td>'+data.service_name+'</td>\
                <td>'+data.customer_name+'</td>\
                <td><button title="remove" type="button" class="btn btn-xs remove"><i class="fa fa-minus-circle" style="color:red"></i></button></td>\
                <td style="display:none">'+data.id+'<input name="edit_arr_data_affected_services[]" class="attach" value="'+data.id+'">\
                <input type="hidden" class="id_effected_service" name="id_effected_service[]">\
                </td>\
            </tr>');
           // var data = {edit_arr_data_affected_services: $("input[name = 'edit_arr_data_affected_services[]']").val(),
           //            id_unique: data.unique};
            axios.get(baseurl +'andop/andop/add_affected_service').then(function (response) {
            response = response.data
            
            $('.id_effected_service').val(response.data);

            })
            .catch(function (error) {
            console.log(error)
            });

        }
   })

    // action remove
    $(document).on('click', '.remove', function(){
        $(this).parents('tr').remove();
    })

    $(document).on('click', '.edit-remove', function(){
        $(this).parents('tr').remove();
    })
});


function new_andop_detail(job_number){
    $("#modalAndopDetail").modal("show")

    $('.button-approved').remove();
    $('.button-rejected').remove();
    $('.button-report').remove();
    $('.button-edit').remove();
    $('.remove-data-fs').remove();
    $('.remove-data-fa').remove();
    $('.remove-data-as').remove();
    $('.edit-remove-div-fs').remove();
    $('.edit-remove-div-fa').remove();
    $('.edit-remove-div-as').remove();
    $('#add-remove-div-fa').remove();

    axios.get(baseurl +'andop/get_new_detail?job_number='+job_number+'&status='+where_status).then(function (response) {
            response = response.data
            $("#number_job").val(response.job_number);
            $("#title").val(response.title);
            $("#description").val(response.description);
            $("#status").val(response.status);
            $("#duration").val(response.duration_hours + ' Hours ' +response.duration_minutes +' Minute');
            $("#downtime").val(response.downtime_hours + ' Hours ' +response.downtime_minutes +' Minute');
            $("#execution_time").val(response.execution_time);
            $("#location").val(response.sitename);
            $("#severity").val(response.severity);
            $("#creator").val(response.fullname);
            $("#email").val(response.email);
            $("#andop_unique").val(response.unique);
            $('#validator').val(response.accepted);
            $('#validator_time').val(response.validator_time);
            $('#validator_reason').val(response.validator_raeson);


            // bagian edit            
            var date = response.execution_time;
            var arr  = date.split(" ");

            if (response.severity == $('#edit_Emergency').val()) 
            {
                $("#edit_Emergency").prop("checked", true);    
            }
            else if(response.severity == $('#edit_Major').val())
            {
                $("#edit_Major").prop("checked", true);
            }
            else
            {
                $("#edit_Minor").prop("checked", true);
            }
            
            $("#edit_andop_type").select2('val',[response.type]);
            $("#edit_andop_title").val(response.title);
            $("#edit_andop_description").val(response.description);
            $("#edit_andop_status").val(response.status);
            $("#edit_andop_duration_hours").val(response.duration_hours);
            $('#edit_andop_duration_minutes').val(response.duration_minutes);
            $("#edit_andop_downtime_hours").val(response.downtime_hours);
            $("#edit_andop_downtime_minutes").val(response.downtime_minutes);
            $("#editdatepicker").val(arr[0]);
            $('#edit_andop_execution_time_hours').val(arr[1]);
            $("#edit_andop_location").select2('val',[response.location]);
            $("#id_unique").val(response.unique);
            $('#edit_andop_fullname').val(response.fullname);
            $('#edit_andop_user_id').val(response.user_id);
            $('#edit_andop_email_user_id').val(response.email);


            // bagian add report
            $("#report_andop_title").val(response.title);
            $('#report_andop_number_job').val(response.job_number);
            $("#id_unique_report").val(response.unique);

            // bagian hapus button accepted dan rejected
            if (update == 't') 
            {   
                $('#id-footer').append(`
                    <button class="btn btn-sm btn-success button-edit" id="button_edit" type="button" data-target="#modaledit" data-toggle="modal"><i class="fa fa-pencil"></i>&nbsp; Edit</button>
                    <button class="btn btn-sm btn-success button-report" id="button_report" type="button" data-target="#modalreport" data-toggle="modal"><i class="fa fa-pencil-square-o"></i>&nbsp; Report</button>
                    `)   
            }
            // <button class="btn btn-sm btn-danger button-closed" id="button_closed" type="button" data-target="#modalclosed" data-toggle="modal"><i class="fa fa-times-circle"></i>&nbsp; Closed</button>
            
      })
      .catch(function (error) {
        console.log(error)
      });
}

function andop_detail(unique) 
{
    axios.get(baseurl +'andop/get_detail_andop?andop_unique='+unique).then(function (response) {
            response = response.data

            // field support
            for (var i = 0; i < response.data_field_support.length; i++) {
                $('#data-field-support').append(`<p class="remove-data-fs">`+response.data_field_support[i].name+` (phone: `+response.data_field_support[i].mobile+`)</p>`)

                var field = `<div class="row edit-remove-div-fs" id="edit-remove-div-fs">
                    <div style="width: 130px;padding-top: 10px;" class="col-xs-4">
                        <input id="edit_andop_name_field_support" type="text" name="edit_andop_name_field_support[]" class="form-control" placeholder="name" value="`+response.data_field_support[i].name+`" style="font-size:12px">
                        <input type="hidden" name="id_field_support[]" value="`+response.data_field_support[i].id+`">
                    </div>
                    <div style="width: 130px;padding-top: 10px;" class="col-xs-4">
                        <input id="edit_andop_mobile_field_support" type="text" name="edit_andop_mobile_field_support[]" class="form-control" placeholder="mobile" value="`+response.data_field_support[i].mobile+`" style="font-size:12px">
                    </div>
                    <div style="padding-top: 15px;" class="col-xs-4">
                        <button title="remove field support" type="button" class="btn btn-xs edit-remove-fs"><i class="fa fa-minus-circle" style="color:red"></i></button>
                    </div>
                  </div>`
                $('#edit-field-support').append(field);

            }

            // file attacment
            for (var i = 0; i < response.data_file_attachment.length; i++) {
                $('#data-file-attachment').append(`<p><a class="remove-data-fa" href="#">`+response.data_file_attachment[i].name+`</a></p>`)

                $('#edit-file-attachment').append(`<div class="row edit-remove-div-fa" id="edit-remove-div-fa">
                    <div style="width: 259px;padding-top: 10px;" class="col-xs-4">
                      
                      <a target="_blank" href=" `+baseurl+`andop/download?id=`+response.data_file_attachment[i].id+`">`+response.data_file_attachment[i].name+`</a>
                    </div>
                    <div style="padding-top: 15px;" class="col-xs-4">
                        <button title="remove file attachment" type="button" class="btn btn-xs edit-remove-fa"><i class="fa fa-minus-circle" style="color:red"></i></button>
                    <input type="hidden" name="id_file_attachment[]" value="`+response.data_file_attachment[i].id+`">
                    </div>
                  </div>`)
            }

            // affected service
            for (var i = 0; i < response.data_affected_service.length; i++) {
                $('#data-affected-service').append(`<p class="remove-data-as"><b>[`+response.data_affected_service[i].service_id+` `+response.data_affected_service[i].service_name+` `+response.data_affected_service[i].customer_name+`]</b> `+response.data_affected_service[i].termination+`</p>`)

                $('#edit_affected_services tr:last').after('\
                <tr class="data edit-remove-div-as">\
                    <td>'+response.data_affected_service[i].service_id+'</td>\
                    <td>'+response.data_affected_service[i].service_name+'</td>\
                    <td>'+response.data_affected_service[i].customer_name+'</td>\
                    <td><button title="remove" type="button" class="btn btn-xs edit-remove"><i class="fa fa-minus-circle" style="color:red"></i></button></td>\
                    <td style="display:none">'+response.data_affected_service[i].customer_id+'<input name="edit_arr_data_affected_services[]" class="attach" value="'+response.data_affected_service[i].customer_id+'"><input type="hidden" name="id_effected_service[]" value="'+response.data_affected_service[i].id+'"></td>\
                </tr>');

            }


      })
      .catch(function (error) {
        console.log(error)
      });
}

// action form_save add andop
$('.form_save').submit(function(e){
        e.preventDefault();

        $.ajax({
            url: baseurl +'andop/addAndop',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function (data) {
                alert('success!')
                removed_form();
                $('#modaladdandop').modal('hide');
                $('#andop_accepted').DataTable().ajax.reload();
                
            }
        });
});

// action form_edit update andop
$('.form_edit').submit(function(e){
        e.preventDefault();

        $.ajax({
            url: baseurl +'andop/andopEdit',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function (data) {
                alert('edit success!')
                $('#modaledit').modal('hide');
                $('#modalAndopDetail').modal('hide');
                $('#andop_accepted').DataTable().ajax.reload();
            }
        });
});

// action form_report update andop
$('.form_report').submit(function(e){
        e.preventDefault();

        $.ajax({
            url: baseurl +'andop/andopReport',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function (data) {
                alert('add report success!')
                window.location.href = baseurl+'andop/report';
                $('#andop_accepted').DataTable().ajax.reload();
            }
        });
});


function acceptedStatus()
{
    $(document).on('click', '#approved', function(){
        $('#andopStatus').empty()
          $('#andopStatus').html(`<input name="accepted_status" value="1" id="accepted_status">`)

          var data = {accepted_status: $('#accepted_status').val(),
                      andop_unique: $('#andop_unique').val()};
          $.ajax({
            url: baseurl +'andop/andopAccepted',
            type: "POST",
            data: data,
            success: function (data) 
            {
                alert('success!')
                 $('#approved').remove();
                 $('#rejected').remove();
                 $('#andop_accepted').DataTable().ajax.reload();
            }
          })
    })

    $(document).on('click', '#rejected', function(){
        $('#andopStatus').empty()
          $('#andopStatus').html(`<input name="accepted_status" value="2" id="accepted_status">`) 
          var data = {accepted_status: $('#accepted_status').val(),
                      andop_unique: $('#andop_unique').val()};
          $.ajax({
            url: baseurl +'andop/andopAccepted',
            type: "POST",
            data: data,
            success: function (data) 
            {
                alert('success!')
                $('#approved').remove();
                $('#rejected').remove();
                $('#andop_accepted').DataTable().ajax.reload();
            }
          }) 
    })
}

function addFieldSupport() 
{
    // add field support
    $('#add-button-fs').on('click', function() {
        var field = `<div class="row" id="remove-div-fs">
                    <div style="width: 130px;padding-top: 10px;" class="col-xs-4">
                        <input id="andop_name_field_support" type="text" name="andop_name_field_support[]" class="form-control" placeholder="name" style="font-size:12px">
                    </div>
                    <div style="width: 130px;padding-top: 10px;" class="col-xs-4">
                        <input id="andop_mobile_field_support" type="text" name="andop_mobile_field_support[]" class="form-control" placeholder="mobile" style="font-size:12px">
                    </div>
                    <div style="padding-top: 15px;" class="col-xs-4">
                        <button title="remove field support" type="button" class="btn btn-xs remove-fs"><i class="fa fa-minus-circle" style="color:red"></i></button>
                    </div>
                  </div>`
        $('#field-support').append(field);
    })

    // edit field support
    $('#edit-button-fs').on('click', function() {
        var id_unique = $("input[name = 'id_unique']").val();
        axios.get(baseurl +'andop/add_field_support?id_unique='+id_unique).then(function (response) {
            response = response.data
            var field = `<div class="row add-remove-div-fs" id="remove-div-fs">
                    <div style="width: 130px;padding-top: 10px;" class="col-xs-4">
                        <input id="andop_name_field_support" type="text" name="edit_andop_name_field_support[]" class="form-control" placeholder="name" style="font-size:12px">
                    </div>
                    <input type="hidden" name="id_field_support[]" value="`+response.data+`">
                    <div style="width: 130px;padding-top: 10px;" class="col-xs-4">
                        <input id="andop_mobile_field_support" type="text" name="edit_andop_mobile_field_support[]" class="form-control" placeholder="mobile" style="font-size:12px">
                    </div>
                    <div style="padding-top: 15px;" class="col-xs-4">
                        <button title="remove field support" type="button" class="btn btn-xs add-remove-fs"><i class="fa fa-minus-circle" style="color:red"></i></button>
                    </div>
                  </div>`
            $('#edit-field-support').append(field);
        })
        .catch(function (error) {
        console.log(error)
      });
        
    }) 
}

function addFileAttachment() 
{
    // add file attachment
    $('#add-button-fa').on('click', function() {
        var field = `<div class="row" id="remove-div-fa">
                    <div style="width: 259px;padding-top: 10px;" class="col-xs-4">
                        <input type="file" id="andop_file_attachment" name="andop_file_attachment[]" class="form-control" placeholder="name">
                    </div>
                    <div style="padding-top: 15px;" class="col-xs-4">
                        <button title="remove file attachment" type="button" class="btn btn-xs remove-fa"><i class="fa fa-minus-circle" style="color:red"></i></button>
                    </div>
                  </div>`
        $('#file-attachment').append(field);
    })

    // edit file attachment
    $('#edit-button-fa').on('click', function() {
        var id_unique = $("input[name = 'id_unique']").val();
        axios.get(baseurl +'andop/add_file_attachment?id_unique='+id_unique).then(function (response) {
            response = response.data
            var field = `<div class="row" id="add-remove-div-fa">
                    <div style="width: 259px;padding-top: 10px;" class="col-xs-4">
                        <input type="file" id="andop_file_attachment" name="edit_andop_file_attachment[]" class="form-control" placeholder="name">
                        <input type="hidden" name="edit_id_file_attachment[]" value="`+response.data+`">
                    </div>
                    <div style="padding-top: 15px;" class="col-xs-4">
                        <button title="remove file attachment" type="button" class="btn btn-xs add-remove-fa"><i class="fa fa-minus-circle" style="color:red"></i></button>
                    </div>
                  </div>`
        $('#edit-file-attachment').append(field);
        })
        .catch(function (error) {
        console.log(error)
      });
        
    })
}

function datePicker()
{
    // config datepicker
    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#editdatepicker').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#reportdatepicker-start').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#reportdatepicker-end').datepicker({
        format: 'yyyy-mm-dd',
    });
}

function time() 
{
    var now     = new Date(); 
    var year    = now.getFullYear();
    var month   = now.getMonth()+1; 
    var day     = now.getDate();
    var hour    = now.getHours();
    var minute  = now.getMinutes();
    var second  = now.getSeconds(); 
    if(month.toString().length == 1) {
     month = '0'+month;
    }
     if(day.toString().length == 1) {
         day = '0'+day;
     }   
     if(hour.toString().length == 1) {
         hour = '0'+hour;
     }
     if(minute.toString().length == 1) {
         minute = '0'+minute;
     }
     if(second.toString().length == 1) {
         second = '0'+second;
     }   
     var dateTime = hour+':'+minute+':'+second;
     var dateYear = year+'-'+month+'-'+day;
     document.getElementById("datepicker").value = dateYear;
     document.getElementById("andop_execution_time_hours").value = dateTime;  
     document.getElementById("report_andop_actual_minutes_start").value = dateTime;
     document.getElementById("report_andop_actual_minutes_end").value = dateTime;
     // return dateTime;
}

function config_select()
{
    $('#andop_type').select2({
        dropdownParent: $('#modaladdandop')
    });

    $('#andop_location').select2({
        dropdownParent: $('#modaladdandop')
    });
}

function removed()
{
    // remove field support for form input
    $(document).on('click', '.remove-fs', function(){
        $('#remove-div-fs').remove();
    })

    // remove field support on edit
    $(document).on('click', '.add-remove-fs', function(){
        var data = {id_field_support: $('.add-remove-div-fs').find("input[name = 'id_field_support[]']").val()};
        $.ajax({
            url: baseurl +'andop/delete_field_support',
            type: "POST",
            data: data,
            success: function (data) {
                alert('success!')
                
            }
        })
        $('#remove-div-fs').remove();
    })

    // remove on edit field support
    $(document).on('click', '.edit-remove-fs', function(){
        var data = {id_field_support: $('.edit-remove-div-fs').find("input[name = 'id_field_support[]']").val()};
        $.ajax({
            url: baseurl +'andop/delete_field_support',
            type: "POST",
            data: data,
            success: function (data) {
                alert('success!')
                
            }
        })
        $('#edit-remove-div-fs').remove();
    })

    // remove file attachment
    $(document).on('click', '.remove-fa', function(){
        $('#remove-div-fa').remove();
    })

    // remove on add in edit file attacment
    $(document).on('click', '.add-remove-fa', function(){
        var data = {edit_id_file_attachment: $("input[name = 'edit_id_file_attachment[]']").val()};
        $.ajax({
            url: baseurl +'andop/delete_file_attachment_',
            type: "POST",
            data: data,
            success: function (data) {
                alert('success!')
                
            }
        })
        $('#add-remove-div-fa').remove();
    })

    // remove on edit file attachment
    $(document).on('click', '.edit-remove-fa', function(){
        var data = {id_file_attacment: $("input[name = 'id_file_attachment[]']").val()};
        $.ajax({
            url: baseurl +'andop/delete_file_attachment',
            type: "POST",
            data: data,
            success: function (data) {
                alert('success!')
                
            }
        })
        $('#edit-remove-div-fa').remove();
    })

    // remove edit affected services
    $(document).on('click', '.edit-remove', function(){
        var data = {id_effected_service: $("input[name = 'id_effected_service[]']").val()};
        $.ajax({
            url: baseurl +'andop/delete_affected_service',
            type: "POST",
            data: data,
            success: function (data) {
                alert('success!')
                
            }
        })
        $(this).parents('tr').remove();
    })

    // remove on double click andop
    $(document).on('click', '#removed', function(){
        $('.remove-data-fs').remove();
        $('.remove-data-fa').remove();
        $('.remove-data-as').remove();
        $('.button-approved').remove();
        $('.button-rejected').remove();
        $('.button-report').remove();
        $('.button-edit').remove();
        // $('.button-closed').remove();

        $('.edit-remove-div-fs').remove();
        $('.edit-remove-div-fa').remove();
        $('.edit-remove-div-as').remove();
    })
}

})


