var table
$(document).ready(function() {

    activeClass('administrator', 'sub-group')
    $.fn.dataTable.ext.errMode = 'none';
    table = $('#access-group').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
            {
                text: 'Add Group',
                className: 'btn btn-primary btn-xs hidden',
                attr: {
                    id: 'add-user',
                    'data-target': '#modalAddRole',
                    'data-toggle': "modal"
                },
                init: function(api, node, config) {
                    $(node).removeClass('dt-button')
                }
            },
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        ajax: baseurl + 'administrator/group/list_group',
        columnDefs: [{
            "width": "10%",
            "targets": 5,
            "className": "text-center",
        }],
        columns: [{
                "data": "id",
                "width": "4%",
                "className": "text-center",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "group_code" },
            { "data": "name" },
            { "data": "type" },
            { "data": "initial" },
            {
                render: function(data, type, row) {
                    button = "";
                    if (del == 't') {
                        button += "<button class='btn btn-xs btn-danger ml-1' onClick='roleDelete(" + row.id + ")'>Deleted</button>"
                    }
                    if (update == 't') {
                        button += "<button class='btn btn-xs btn-primary ml-1' onClick='roleDetail(" + row.id + ")'>Edit</button> "
                    }
                    return button;
                }
            }
        ],
        initComplete: function() {
            if (create == 't') {
                $("#add-user").removeClass("hidden");
            }
        },
        deferLoading: 200
    })

})


// action form_save add user group
$('.form_save').submit(function(e) {
    e.preventDefault();

    $.ajax({
        url: baseurl + 'administrator/group/insert_group',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function(data) {
            // alert('success!')
            $('#access-group').DataTable().ajax.reload();
            $('#modalAddRole').modal('hide');
        }
    });
});

// action form_save add user group
$('.form_edit').submit(function(e) {
    e.preventDefault();

    $.ajax({
        url: baseurl + 'administrator/group/edit_group',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        success: function(data) {
            // alert('success!')
            $('#access-group').DataTable().ajax.reload();
            $('#modalEditRole').modal('hide');
        }
    });
});

function deleteRoleModal(id) {
    $("#modalDeleteRole").modal("show")
    $('#delete').click(function() {
        axios.delete(baseurl + '/lib/role/?id=' + id).then(
            function(response) {
                $("#modalDeleteRole").modal("hide")
                table.ajax.reload();
            }
        )
    })
}

$('#cancel').click(function() {
    $("#modalDeleteRole").modal("hide")
})

$('#add-user').click(function() {
    $("#modalAddRole").modal("show")
})

$('.close').click(() => {
    $(":checkbox").prop("checked", false);
    $("input").val(null)
    $('#username-danger').addClass('hidden')
    $('#email-danger').addClass('hidden')
    $(':submit').attr("disabled", false)
})


function roleDetail(id) {
    $("#modalEditRole").modal("show")
    axios.get(baseurl + '/administrator/group/detail?id=' + id).then((result) => {
        result = result.data
            // console.log(result)
            // alert(result.initial)
        $("#edit_group_name").val(result.name);
        $("#edit_group_code").val(result.group_code);
        $('#group_id').val(result.id);
        $('#edit_type').select2('val', [result.type]);
        $('#edit_member').select2('val', [result.member]);
        $('#edit_parent_id').select2('val', [result.parent_id]);
        $('#edit_status_mention').select2('val', [result.status_mention]);
        $("#edit_initial").val(result.initial);
    }).catch((err) => {

    });
}

function roleDelete(id) {
    axios.get(baseurl + 'administrator/group/delete_group?id=' + id).then((result) => {
        result = result.data
            // console.log(result)
        alert('success!')
        $('#access-group').DataTable().ajax.reload();
    }).catch((err) => {

    });
}