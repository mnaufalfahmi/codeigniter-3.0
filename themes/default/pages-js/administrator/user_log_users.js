var table
var Username = ""
var Email = ""
var option;
var select_incident = [];
var start_date;
var end_date;
var modules;

$(document).ready(function () {

    date();
    activeClass('administrator', 'sub-user-log-users')
    $.fn.dataTable.ext.errMode = 'none';
    table = $('#user-log-users').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            }
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        processing: true,
        serverSide: true,
        ajax: {
            "url": baseurl + 'administrator/user_log/log_users',
            "type": "POST",
            "data": function(d) {
                d.start_date = start_date;
                d.end_date = end_date;
                d.modules = modules;
            }
        },
        columns: [
            {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    console.log(data)
                }
            },
            { "data": "created_on" },
            { "data": "username" },
            { "data": "module" },
            { "data": "remark" },
            { "data": "ip" },
            { "data": "browser" },
            { "data": "browser_version" },
            {
                "data": "url",
                "render": function(data) {
                    if (data) {
                        return `<p title="` + data + `">` + data + `</p>`
                    }
                }
            },
            {
                render: function(data, type, row) {
                    return "<button type='button' class='btn btn-xs btn-success ml-1' onClick='showJsonUser(" + row.id + ")'>JSON</button>"
                }
            }
        ]
    })

    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" onClick=showFilter()><i class="fa fa-filter"></i></button>');

    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    // set auto reload 1 minutes 
    setInterval(function() {
        table.ajax.reload(null, false);
    }, 30000);

    $('#reset').on('click', function () {
        start_date = null;
        end_date = null;
        modules = null;
        $('#user-log-users').DataTable().ajax.reload();
        $('#modalFilter').modal('hide');
    });
   
    $('.form_filter').submit(function(e){
        e.preventDefault();
        start_date = $('#datepicker-start').val();
        end_date = $('#datepicker-end').val();
        modules = $('#modules').val();
        $('#user-log-users').DataTable().ajax.reload();
        $('#modalFilter').modal('hide');
        
    });

     $('#modules').select2({
        allowClear: true
    });

});

function showFilter() {
    $('#modalFilter').modal('show');
    $("#modules").empty();
    $.ajax({
        url:baseurl+"administrator/user_log/get_log_modules",
        success: function (result) 
        {
            $.each(JSON.parse(result), function(i, item) {
                $("#modules").append("<option value='"+item.module+"'>"+item.module+"</option>");
            });
        }
    })
}

function showJsonUser(id) {
    $('#modalLogUser').modal('show');
    $.ajax({
        url:baseurl+"administrator/user_log/get_log_json?id="+id,
        dataType: 'JSON',
        contentType: "application/json",
        success: function (result) 
        {
            var textedJsonParse = JSON.parse(result.json, undefined, 4);
            var textedJsonString = JSON.stringify(textedJsonParse, undefined, 4);
            $('#json').text(textedJsonString);
        }
    })
}

function date()
{
    // config datepicker
    $('#datepicker-start').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#datepicker-end').datepicker({
        format: 'yyyy-mm-dd'
    });
}