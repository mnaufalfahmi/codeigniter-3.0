var table
$(document).ready(function () {

    activeClass('administrator', 'sub-roles-group')
    $.fn.dataTable.ext.errMode = 'none';
    table = $('#access-group').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
            {
                text: 'Add Roles Group',
                className: 'btn btn-primary btn-xs hidden',
                attr: {
                    id: 'add-user',
                    'data-target': '#modalAddRole',
                    'data-toggle': "modal"
                },
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                }
            },
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 20,
        ajax: baseurl + 'administrator/roles_group/list_group',
        columns: [
            { "data": "nama_sub_group" },
            { "data": "group_code" },
            { "data": "type" },
            {
                render: function (data, type, row) {
                    button = "";
                    if (del == 't') {
                        button += "<button class='btn btn-xs btn-danger ml-1' onClick='roleDelete(" + row.id + ")'>Delete</button>"
                    }
                    if (update == 't') {
                        button += "<button class='btn btn-xs btn-primary ml-1' onClick='roleDetail(" + row.id + ")'>Edit</button>"
                    }
                    return button;
                }
            }
        ],
        initComplete: function () {
            if (create == 't') {
                $("#add-user").removeClass("hidden");
            }
        },
        deferLoading : 200
    })

})


// action form_save add user group
$('.form_save').submit(function(e){
        e.preventDefault();

        $.ajax({
            url: baseurl +'administrator/roles_group/insert_group',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function (data) {
                // alert('success!')
                $('#access-group').DataTable().ajax.reload();
                $('#modalAddRole').modal('hide');
            }
        });
});

// action form_save add user group
$('.form_edit').submit(function(e){
        e.preventDefault();

        $.ajax({
            url: baseurl +'administrator/roles_group/edit_group',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function (data) {
                // alert('success!')
                $('#access-group').DataTable().ajax.reload();
                $('#modalEditRole').modal('hide');
            }
        });
});

function deleteRoleModal(id){
    $("#modalDeleteRole").modal("show")
    $('#delete').click(function(){
        axios.delete(baseurl+'/lib/role/?id='+id).then(
            function (response) {
                $("#modalDeleteRole").modal("hide")
                table.ajax.reload();
            }
        )
    })
}

$('#cancel').click(function(){
    $("#modalDeleteRole").modal("hide")
})

$('#add-user').click(function() {
    $("#modalAddRole").modal("show")
})

$('.close').click(() => {
    $(":checkbox").prop("checked", false);
    $("input").val(null)
    $('#username-danger').addClass('hidden')
    $('#email-danger').addClass('hidden')
    $(':submit').attr("disabled", false)
})


function roleDetail(id){
    $("#modalEditRole").modal("show")
    axios.get(baseurl+'/administrator/roles_group/detail?id='+id).then((result) => {
        result = result.data
        $("#edit_name").val(result.nama_sub_group);
        $("#edit_group_code").val(result.group_code);
        $('#edit_type').select2('val',[result.type]);
        $('#id').val(result.id);
        $('#edit_reference_group_id').select2('val',[result.reference_group_id]);
    }).catch((err) => {
        
    });
}

function roleDelete(id) 
{
    axios.get(baseurl+'administrator/roles_group/delete_group?id='+id).then((result) => {
        result = result.data
        // console.log(result)
        alert('success!')
        $('#access-group').DataTable().ajax.reload();
    }).catch((err) => {
        
    });
}
