var table
var Username = ""
var Email = ""
var option;
var select_incident = [];

$(document).ready(function () {

    activeClass('administrator', 'sub-user-log-connection')
    $.fn.dataTable.ext.errMode = 'none';
    table = $('#user-log-connection').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            }
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        ajax: baseurl + 'administrator/user_log/log',
        columns: [
            {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    console.log(data)
                }
            },
            { "data": "date" },
            { "data": "ip" },
            { "data": "remark" },
            { "data": "connection" },
        ]
    })

    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

});