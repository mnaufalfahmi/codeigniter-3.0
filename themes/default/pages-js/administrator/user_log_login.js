var table
var Username = ""
var Email = ""
var option;
var select_incident = [];

$(document).ready(function () {

    activeClass('administrator', 'sub-user-log-login')
    $.fn.dataTable.ext.errMode = 'none';
    table = $('#user-log-login').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            }
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        processing: true,
        serverSide: true,
        ajax: {
            "url": baseurl + 'administrator/user_log/log_login',
            "type": "POST",
            "data": function(d) {
                d.type = option;
                d.select = select_incident.join(',');
            }
        },
        columns: [
            {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    console.log(data)
                }
            },
            { "data": "username" },
            { "data": "ip" },
            { "data": "remark" },
            { "data": "created_on" },
            { "data": "browser" },
            { "data": "browser_version" },
            { "data": "platform" },
            {
                "data": "full_user_agent",
                "render": function(data) {
                    if (data) {
                        return `<p title="` + data + `">` + data + `</p>`
                    }
                }
            },
        ]
    })

    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

});