var table
var Username = ""
var Email = ""
$(document).ready(function () {

    activeClass('administrator', 'sub-user-log')
    $.fn.dataTable.ext.errMode = 'none';
    table = $('#user-management').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            }
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        ajax: baseurl + '/administrator/user_log/user',
        columns: [
            {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    console.log(data)
                }
            },
            { "data": "change_date" },
            { "data": "change_act" },
            { "data": "change_by" },
            { "data": "old_email" },
            { "data": "new_email" },
            { "data": "old_fullname" },
            { "data": "new_fullname" }
            
        ],
        initComplete: function () {
            if (create == 't') {
                $("#add-user").removeClass("hidden");
            }
        },
    })

    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

})

$('#add-user').click(function () {
    $('#modalAddUser').modal('show')
})

$('.close').click(() => {
    $(":checkbox").prop("checked", false);
    $("input").val(null)
    $('#username-danger').addClass('hidden')
    $('#email-danger').addClass('hidden')
    $(':submit').attr("disabled", false)
})

$('#select-region, #edit_select-region, #select-surveilance, #edit_select-surveilance, #select-analyst, #edit_select-analyst, #select-mod, #edit_select-mod').select2({
    selectOnClose: true,
    minimumResultsForSearch: -1
});

function checkAll() {
    var checkAll = document.getElementById("check_all");
    if (checkAll.checked == true) {
        $(":checkbox").prop("checked", true);
    } else {
        $(":checkbox").prop("checked", false);
    }
}

$("#username").keyup(function () {
    axios.get(baseurl+'/administrator/user_management/check/username?key='+this.value+'&status=add').then(
        function (response) {
            response = response.data.data
            if (response.length > 0) {
                $('#username-danger').removeClass('hidden')
                $(':submit').attr("disabled", true)
            } else {
                $('#username-danger').addClass('hidden')
                $(':submit').attr("disabled", false)
            }
        }
    )
});

$("#edit_username").keyup(function () {
    axios.get(baseurl+'/administrator/user_management/check/username?key='+this.value+'&value='+Username).then(
        function (response) {
            response = response.data.data
            if (response.length > 0) {
                $('#edit_username-danger').removeClass('hidden')
                $(':submit').attr("disabled", true)
            } else {
                $('#edit_username-danger').addClass('hidden')
                $(':submit').attr("disabled", false)
            }
        }
    )
});

$("#email").keyup(function () {
    axios.get(baseurl+'/administrator/user_management/check/email?key='+this.value+'&status=add').then(
        function (response) {
            response = response.data.data
            if (response.length > 0) {
                $('#email-danger').removeClass('hidden')
                $(':submit').attr("disabled", true)
            } else {
                $('#email-danger').addClass('hidden')
                $(':submit').attr("disabled", false)
            }
        }
    )
});

$("#edit_email").keyup(function () {
    
    axios.get(baseurl+'/administrator/user_management/check/email?key='+this.value+'&value='+Email).then(
        function (response) {
            response = response.data.data
            console.log(Email)
            if (response.length > 0) {
                $('#edit_email-danger').removeClass('hidden')
                $(':submit').attr("disabled", true)
            } else {
                $('#edit_email-danger').addClass('hidden')
                $(':submit').attr("disabled", false)
            }
        }
    )
});

function userDetail(id){
    $('#modalEditUser').modal('show')
    axios.get(baseurl+'user/detail?id='+id).then(
        function (response) {
            response = response.data.data
            console.log(response)
            $('#user_id').val(response.detail.id);
            $("#edit_username").val(response.detail.username);
            Username = response.detail.username
            $("#edit_email").val(response.detail.email);
            Email = response.detail.email
            $("#edit_fullname").val(response.detail.fullname);
            $("#edit_bobot").val(response.detail.weight ? response.detail.weight : null);
            $('#edit_select-region').val(response.detail.region_code).trigger('change.select2');
            $('#edit_select-surveilance').val(response.detail.surveilance).trigger('change.select2');
            $('#edit_select-analyst').val(response.detail.analyst).trigger('change.select2');
            $('#edit_select-mod').val(response.detail.mod).trigger('change.select2');
            response.roles.map((value, index, arr) => {
                $("#edit_"+value.name+"").prop("checked", true);
            })
            
            
            if(response.detail.id == response.detail.parent_id)
            {
                
                var arr = [];
                $.each(response.sub_detail,function(i,item){
                    
                    if(response.detail.id != item.id)
                    {
                        arr.push(item.id);
                        $('#edit-select-child').select2('val',[arr]);
                    }
                    
                })

                $('#edit-child').show();
                $('#edit-select-parent').select2('val',[1]);
            }
            if(response.detail.id != response.detail.parent_id)
            {
                $('#edit-child').show();
                $('#edit-select-parent').select2('val',[0]);
                // $('#edit-select-child').select2('val',[response.detail.parent_id]);
            }
            
            
            // else if(response.id != response.parent_id)
            // {
            //     $('#edit-child').show();
            //     $('#edit-select-parent').select2('val',[1]);
            //     $('#edit-select-child').select2('val',[response.parent_id]);
            // }
            // else
            // {
            //     $('#edit-select-parent').select2('val',[0]);
            //     $('#edit-child').hide();
            // }
    })
}

function deleteUserModal(id){
    $("#modalDeleteUser").modal("show")
    $('#delete').click(function(){
        axios.delete(baseurl+'/administrator/user_management/user?id='+id).then(
            function (response) {
                $("#modalDeleteUser").modal("hide")
                table.ajax.reload();
            }
        )
    })
}

$('#cancel').click(function(){
    $("#modalDeleteUser").modal("hide")
})

$('#select-parent').on('change', function() {
    if (this.value == '1') {
        $('#child').show();
    }
    else{
        $('#child').hide();
        $('#select-child').select2('val',[0]);
    }
})

$('#edit-select-parent').on('change', function() {
    if (this.value == '1') {
        $('#edit-child').show();
    }
    else{
        $('#edit-child').hide();
        $('#edit-select-child').select2('val',[0]);
    }
})