var table
var Username = ""
var Email = ""
$(document).ready(function() {

    activeClass('administrator', 'sub-user-management')
    $.fn.dataTable.ext.errMode = 'none';

    table = $('#user-management').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
            {
                text: 'Add User',
                className: 'btn btn-primary btn-xs hidden',
                attr: {
                    id: 'add-user',
                    // 'data-target': '#modalAddUser',
                    // 'data-toggle': "modal"
                },
                init: function(api, node, config) {
                    $(node).removeClass('dt-button')
                }
            },
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        ajax: baseurl + '/administrator/user_management/user',
        columnDefs: [{
            "width": "10%",
            "targets": 5,
            "className": "text-center",
        }],
        columns: [{
                "data": "id",
                "width": "4%",
                "className": "text-center",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "username" },
            { "data": "fullname" },
            { "data": "email" },
            { "data": "group_name" },
            {
                render: function(data, type, row) {
                    button = "";
                    if (del == 't') {
                        button += "<button type='button' class='btn btn-xs btn-danger ml-1' onClick='deleteUserModal(" + row.id + ")'>Delete</button>"
                    }
                    if (update == 't') {
                        button += "<button type='button' class='btn btn-xs btn-primary ml-1' onClick='userDetail(" + row.id + ")'>Edit</button>"
                    }
                    return button;
                }
            }
        ],
        initComplete: function() {
            if (create == 't') {
                $("#add-user").removeClass("hidden");
            }
        },
    })

    $('#add-user').on('click', function(e) {
        $('#modalAddUser').modal('show')
        $('.select_region').select2('val', ['']);
    })
})

// $('#add-user').on('click',function(e) {
//     $('#modalAddUser').modal('show')
//     $('.select_region').select2('val', ['']);
// })

$('.close').click(() => {
    $(":checkbox").prop("checked", false);
    $("input").val(null)
    $('#username-danger').addClass('hidden')
    $('#email-danger').addClass('hidden')
    $('#submit-edit').attr("disabled", false)
})

$('#select-region, #edit_select-region, #select-surveilance, #edit_select-surveilance, #select-analyst, #edit_select-analyst, #select-mod, #edit_select-mod').select2({
    selectOnClose: true,
    minimumResultsForSearch: -1
});

function checkAll() {
    var checkAll = document.getElementById("check_all");
    if (checkAll.checked == true) {
        $(":checkbox").prop("checked", true);
    } else {
        $(":checkbox").prop("checked", false);
    }
}

$("#username").keyup(function() {
    axios.get(baseurl + '/administrator/user_management/check/username?key=' + this.value + '&status=add').then(
        function(response) {
            response = response.data.data
            if (response.length > 0) {
                $('#username-danger').removeClass('hidden')
                $('#submit-add').attr("disabled", true)
            } else {
                $('#username-danger').addClass('hidden')
                $('#submit-add').attr("disabled", false)
            }
        }
    )
});

$("#edit_username").keyup(function() {
    axios.get(baseurl + '/administrator/user_management/check/username?key=' + this.value + '&value=' + Username).then(
        function(response) {
            response = response.data.data
            if (response.length > 0) {
                $('#edit_username-danger').removeClass('hidden')
                $('#submit-edit').attr("disabled", true)
            } else {
                $('#edit_username-danger').addClass('hidden')
                $('#submit-edit').attr("disabled", false)
            }
        }
    )
});

$("#email").keyup(function() {
    axios.get(baseurl + '/administrator/user_management/check/email?key=' + this.value + '&status=add').then(
        function(response) {
            response = response.data.data
            if (response.length > 0) {
                $('#email-danger').removeClass('hidden')
                $('#submit-add').attr("disabled", true)
            } else {
                $('#email-danger').addClass('hidden')
                $('#submit-add').attr("disabled", false)
            }
        }
    )
});

$("#edit_email").keyup(function() {

    axios.get(baseurl + '/administrator/user_management/check/email?key=' + this.value + '&value=' + Email).then(
        function(response) {
            response = response.data.data
            console.log(Email)
            if (response.length > 0) {
                $('#edit_email-danger').removeClass('hidden')
                $('#submit-edit').attr("disabled", true)
            } else {
                $('#edit_email-danger').addClass('hidden')
                $('#submit-edit').attr("disabled", false)
            }
        }
    )
});

function userDetail(id) {
    $('#modalEditUser').modal('show')
    axios.get(baseurl + 'user/detail?id=' + id).then(
        function(response) {
            response = response.data.data
            console.log(response)
            $('#user_id').val(response.detail.id);
            $("#edit_username").val(response.detail.username);
            Username = response.detail.username
            $("#edit_email").val(response.detail.email);
            Email = response.detail.email
            $("#edit_fullname").val(response.detail.fullname);
            $("#old_edit_fullname").val(response.detail.fullname);
            $("#old_edit_email").val(response.detail.email);
            $("#edit_bobot").val(response.detail.weight ? response.detail.weight : null);
            // $('#edit_select-region').val(response.detail.region_code).trigger('change.select2');
            $('#edit_select-surveilance').val(response.detail.surveilance).trigger('change.select2');
            $('#edit_select-analyst').val(response.detail.analyst).trigger('change.select2');
            $('#edit_select-mod').val(response.detail.mod).trigger('change.select2');
            $('#old_password').val(response.detail.password);
            $('#edit_password').val("");

            // alert(response.detail.password);

            response.roles.map((value, index, arr) => {
                $("#edit_" + value.name + "").prop("checked", true);
            })
            var str = response.user_group;
            if (str == null) {
                $('#group_id').select2('val', [0]);
            } else {
                $('#group_id').select2('val', [response.user_group.group_id]);
                $('#user_group_id').val(response.user_group.user_group_id);
            }

            if (response.detail.id == response.detail.parent_id) {

                var arr = [];
                $.each(response.sub_detail, function(i, item) {

                    if (response.detail.id != item.id) {
                        arr.push(item.id);
                        $('#edit-select-child').select2('val', [arr]);
                        $('#edit-select-child2').select2('val', [arr]);
                    } else {
                        $('#edit-select-parent').val(0);
                    }

                })

                $('#edit-child').show();
                $('#edit-select-parent').select2('val', [1]);
            }
            if (response.detail.id != response.detail.parent_id) {
                $('#edit-child').show();
                $('#edit-select-parent').select2('val', [0]);
                // $('#edit-select-child').select2('val',[response.detail.parent_id]);
            }

            $('.select_region').select2('val', [response.detail.region_code])

            // alert(response.detail.region_code)

            // else if(response.id != response.parent_id)
            // {
            //     $('#edit-child').show();
            //     $('#edit-select-parent').select2('val',[1]);
            //     $('#edit-select-child').select2('val',[response.parent_id]);
            // }
            // else
            // {
            //     $('#edit-select-parent').select2('val',[0]);
            //     $('#edit-child').hide();
            // }
        })
}

function deleteUserModal(id) {
    // $("#modalDeleteUser2").modal("show")
    axios.get(baseurl + 'user/detail?id=' + id).then(
        function(response) {
            response = response.data.data
            $('#id').val(response.detail.id);
            $("#delete_fullname").val(response.detail.fullname);
            $("#delete_email").val(response.detail.email);
            $("#delete_username").val(response.detail.username);
            $(".confirm_user").html(' Apakah anda yakin menghapus <strong> ' + response.detail.fullname + '</strong> !!');
            // $("#modalDeleteUser2").modal("hide")
            // table.ajax.reload();
            $("#modalDeleteUser2").modal("show")
        }
    )
}

$('#cancel').click(function() {
    $("#modalDeleteUser2").modal("hide")
})

$('#select-parent').on('change', function() {
    if (this.value == '1') {
        $('#child').show();
        $('#select-child').attr("required", "required");
    } else {
        $('#child').hide();
        $('#select-child').select2('val', [0]);
        $('#select-child').removeAttr("required", "required");

    }
})

$('#edit-select-parent').on('change', function() {
    if (this.value == '1') {
        $('#edit-child').show();
    } else {
        $('#edit-child').hide();
        $('#edit-select-child').select2('val', [0]);
    }
})


$('#Field_Support').on('change', function() {
    var check = $(this).prop('checked');
    if (check === true) {
        $("#field_password").show();
        $("#password").attr("required", "required");
    } else {
        $("#field_password").hide();
        $("#password").removeAttr("required", "required");
    }
})

$.ajax({
    url: baseurl + 'region/get_region',
    type: 'GET',
    dataType: 'JSON',
    async: false,
    success: function(data) {
        $('.select_region').empty();
        $.each(data, function(i, item) {
            var option = "<option value=" + item.region_code + ">" + item.region_name + "</option>";
            $('.select_region').append(option);
        });
        $('.select_region').prepend("<option value='' selected>-</option>");
    }
})


// $('.select-group').on('change', function() {
//     alert(this.value)
// });