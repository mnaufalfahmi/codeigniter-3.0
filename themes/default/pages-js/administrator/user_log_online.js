var table
var Username = ""
var Email = ""
var option;
var select_incident = [];

$(document).ready(function () {

    activeClass('administrator', 'sub-user-log-online')
    $.fn.dataTable.ext.errMode = 'none';
    table = $('#user-log-online').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            }
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        processing: true,
        serverSide: true,
        ajax: {
            "url": baseurl + 'administrator/user_log/log_online',
            "type": "POST",
            "data": function(d) {
                d.type = option;
                d.select = select_incident.join(',');
            }
        },
        columns: [
            {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    console.log(data)
                }
            },
            { "data": "username" },
            {
                "data": "status",
                "render": function(data) {
                    if (data == 'online') {
                        return `<span class="badge badge-success p-1">Online</span>`
                    } else {
                        return `<span class="badge badge-danger p-1">Offline</span>`
                    }
                }
            },
            { "data": "last_active" },
        ]
    })

    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    // set auto reload 1 minutes 
    setInterval(function() {
        table.ajax.reload(null, false);
    }, 7000);

});