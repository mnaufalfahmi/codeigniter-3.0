var table
$(document).ready(function() {

    activeClass('administrator', 'sub-user-access-management')
    $.fn.dataTable.ext.errMode = 'none';

    table = $('#access-management').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
            {
                text: 'Add Role',
                className: 'btn btn-primary btn-xs hidden',
                attr: {
                    id: 'add-user',
                    'data-target': '#modalAddRole',
                    'data-toggle': "modal"
                },
                init: function(api, node, config) {
                    $(node).removeClass('dt-button')
                }
            },
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 20,
        ajax: baseurl + '/lib/role',
        columnDefs: [{
            "width": "10%",
            "targets": 3,
            "className": "text-center",
        }],
        columns: [{
                "data": "id",
                "width": "4%",
                "className": "text-center",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "name" },
            { "data": "description" },
            {
                render: function(data, type, row) {
                    button = "";
                    if (del == 't') {
                        button += "<button class='btn btn-xs btn-danger ml-1' onClick='deleteRoleModal(" + row.id + ")'>Delete</button>"
                    }
                    if (update == 't') {
                        button += "<button class='btn btn-xs btn-primary ml-1' onClick='roleDetail(" + row.id + ")'>Edit</button>"
                    }
                    return button;
                }
            }
        ],
        initComplete: function() {
            if (create == 't') {
                $("#add-user").removeClass("hidden");
            }
        },
        deferLoading: 200
    })
})

function deleteRoleModal(id) {
    $("#modalDeleteRole").modal("show")
    $('#delete').click(function() {
        axios.delete(baseurl + '/lib/role/?id=' + id).then(
            function(response) {
                $("#modalDeleteRole").modal("hide")
                table.ajax.reload();
            }
        )
    })
}

$('#cancel').click(function() {
    $("#modalDeleteRole").modal("hide")
})

$('#add-user').click(function() {
    $("#modalAddRole").modal("show")
})

$('.close').click(() => {
    $(":checkbox").prop("checked", false);
    $("input").val(null)
    $('#username-danger').addClass('hidden')
    $('#email-danger').addClass('hidden')
    $(':submit').attr("disabled", false)
})


function roleDetail(id) {
    $("#modalEditRole").modal("show")
    axios.get(baseurl + '/lib/role/detail?id=' + id).then((result) => {
        result = result.data.data
        console.log(result)
        $('#edit_role_id').val(result.role.id)
        $('#edit_role_name').val(result.role.name)
        $('#edit_description').val(result.role.description)
        result.permission.map((value, index, arr) => {
            console.log(value.read_permission)
            $("#edit_module_id_" + value.name + "").prop("checked", true);
            if (value.read_permission == 't') {
                $("#edit_read_" + value.name + "").prop("checked", true);
            }
            if (value.create_permission == 't') {
                $("#edit_create_" + value.name + "").prop("checked", true);
            }
            if (value.update_permission == 't') {
                $("#edit_update_" + value.name + "").prop("checked", true);
            }
            if (value.delete_permission == 't') {
                $("#edit_delete_" + value.name + "").prop("checked", true);
            }

        })
    }).catch((err) => {

    });
}