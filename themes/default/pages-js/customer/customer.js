var table
$(document).ready(function () {
    activeClass('customer', 'customer-list')
    // console.log(baseurl)
    table = $('#customer-table').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
            // {
            //     text: 'Add Customer',
            //     className: 'btn btn-primary btn-xs hidden',
            //     attr: {
            //         id: 'add-user',
            //         'data-target': '#modalAddCustomer',
            //         'data-toggle': "modal"
            //     },
            //     init: function (api, node, config) {
            //         $(node).removeClass('dt-button')
            //     }
            // },
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 20,
        ajax: baseurl + '/customer/list',
        columns: [
            { "data": "customer_name" },
            { "data": "service_id" },
            { "data": "service_name" },
            { "data": "origin" },
            { "data": "termination" },
            {
                render: function (data, type, row) {
                    button = "";
                    if (del == 't') {
                        button += "<button class='btn btn-xs btn-danger ml-1' onClick='deleteCustomerModal(" + row.id + ")'>Delete</button>"
                    }
                    // if (update == 't') {
                    //     button += "<button class='btn btn-xs btn-primary ml-1' onClick='roleDetail(" + row.id + ")'>Edit</button>"
                    // }
                    return button;
                }
            }
        ],
        initComplete: function () {
            if (create == 't') {
                $("#add-user").removeClass("hidden");
            }
        },
        deferLoading : 200
    })
})

function deleteCustomerModal(id){
    $("#modalDeleteCustomer").modal("show")
    $('#delete').click(function(){
        axios.delete(baseurl+'/customer?id='+id).then(
            function (response) {
                $("#modalDeleteCustomer").modal("hide")
                table.ajax.reload();
            }
        )
    })
}

$('#cancel').click(function(){
    $("#modalDeleteCustomer").modal("hide")
})
