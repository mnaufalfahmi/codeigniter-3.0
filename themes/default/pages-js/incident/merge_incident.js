$(document).ready(function() {

    // var api_url = 'http://localhost/fms-api/uploads/incidents/';
    var api_url = baseurl + 'fms-api/uploads/incidents/';

    activeClass('incident', 'merge-incident');

    $.fn.dataTable.ext.errMode = 'none';

    $('[data-toggle="tooltip"]').tooltip();

    // config datepicker
    $('#datepicker-component').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('.select2').select2({
        allowClear: true,
    });

    function fn_check_group(param) {
        var group_access = ['NOC'];
        if (jQuery.inArray(param, group_access) !== -1) {
            return '';
        } else {
            return 'none';
        }
    }

    var table = $('#merge_incident').DataTable({
        dom: 'Bfrtpi Rl',
        scrollX: true,
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
            {
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                    $(node).css('display', 'none');
                }
            }

        ],
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        ajax: baseurl + 'incident/merge/list_merge_incident',
        columnDefs: [{
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                    $(td).attr('data-toggle', 'tooltip');
                    $(td).attr('title', cellData);
                }
            },
            {
                "targets": [0, 8], // your case first column
                "className": "text-center",
            }
        ],
        columns: [{
                "data": "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "incident_id" },
            { "data": "ticket_id" },
            { "data": "open_time" },
            { "data": "region_code" },
            { "data": "status" },
            { "data": "description" },
            {
                render: function(data, type, row, meta) {
                    return "<a href='" + baseurl + "incident/progress/" + row.id_incident_old + "' target='_blank' class='text-white bg-danger p-1'>" + row.incident_ref + "</a>"
                }
            },
            {
                render: function(data, type, row, meta) {
                    return "<button class='btn btn-xs btn-success text-white btn_unmerge' style='display:" + fn_check_group(member) + "' data-id=" + row.id + " data-incident-old='" + row.incident_ref + "'  data-incident='" + row.incident_id + "' data-ticket='" + row.ticket_id + "'>Un Merge</button>"
                }
            }

        ],
    });

    table.on('order.dt search.dt', function() {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    table.on('click', 'tr .btn_unmerge', function(e) {
        e.preventDefault();
        var params = {
            ticket_id: $(this).data('ticket'),
            incident_id: $(this).data('incident'),
            id: $(this).data('id'),
            incident_id_old: $(this).data('incident-old')
        };

        $.ajax({
            url: baseurl + 'incident/merge/unmerge',
            type: 'POST',
            data: params,
            dataType: 'JSON',
            async: true,
            success: function(data) {
                table.ajax.reload();
            }

        })
    });


    setInterval(function() {
        table.ajax.reload();
    }, 30000);

});