// global variabel 
var type_filter = "token";
var pesan;
var status_incident;
var akses;
var status_close;
var token = [];
var start = 0;


$(document).ready(function() {

        // var api_url = 'http://localhost/fms-api/uploads/incidents/';
        var api_url = 'http://10.14.22.86/fms-api/uploads/incidents/';
        // var api_url = baseurl + '/fms-api/uploads/incidents/';
    
        activeClass('incident', 'open-incident');
    
        $.fn.dataTable.ext.errMode = 'none';
    
        $('[data-toggle="tooltip"]').tooltip();
    
        // config datepicker
        $('#datepicker-component').datepicker({
            format: 'yyyy-mm-dd',
        });
    
        $('.select2').select2({
            allowClear: true,
        });

		//tangkap apakah ada action dr client manapun
		Server.bind('message', function( result ) {
			dhtmlx.message({
						'text': "new message incidents",
						'expire': -1
                    });

                    play();

                    // participant
                    var total = 0;
                    $('#participant-' + id).empty();
                    $('#total_participant-' + id).empty();
                        $.each(result.participan, function(i, item) {
        
                            total = total + 1;
        
                            var part_list = '<a href="#" class="list-group-item list-group-item-action" style="padding-top:5px; padding-bottom:5px">' +
                                '<div class="row">' +
                                '<div class="col-sm-2">' +
                                '<p style="font-size:25px; margin-top:4px"><i class="fa fa-user-circle"></i></p>' +
                                '</div>' +
                                '<div class="col-sm-10">' +
                                '<p class="text-secondary" style="font-size:12px; text-transform:capitalize">' + item.username + '</p>' +
                                '<p class="text-secondary" style="font-size:12px">' + item.name + '</p>' +
                                '</div>' +
                                '</div>' +
                                '</a>';
        
                            $('#participant-' + id).append(part_list);
        
                        });
        
                        $('#total_participant-' + id).html(
                            'PARTICIPANTS (' + total + ')'
                        )
                        // end participant

                // message only
                // process 
                if (result.data.process === null) {
                    process = "NO PROCESS";
                } else {
                    process = result.data.process;
                }
                    
                var path = api_url + result.data.file_name;
                    // console.log(item.username.toLowerCase())
                    if (username == result.data.username.toLowerCase()) {
                        var c_progress = `
                        <hgroup class="speech-bubble-front shadow messages" data-messages="` + result.data.message + ` | ` + result.data.username + ` | ` + result.data.name + `"  style="margin-top:7px">
                            <div class="d-flex w-100 justify-content-between" style="padding: 10px;">
                            <small class="mb-1" style="text-transform:uppercase"><strong>` + result.data.username + ` | ` + result.data.name + ` | ` + process + `</strong></small><small>` + result.data.time_message + `</small></div>
                                <p style="padding-bottom: 12px;padding-left: 11px;white-space: pre-wrap;">` + result.data.message + ' ' + fn_check_lat_lng(result.data.lat, result.data.lng) + '<a href="' + baseurl + 'incident/download_file?id=' + result.data.id_file + '" class="text-dark" target="_blank"> ' + fn_check_attachment(result.data.file_name) + '</a></p>' +
                            fn_check_gambar(result.data.file_name, result.data.type, path) + `</p></hgroup>`;
                    } else {
                        var c_progress = `
                        <hgroup class="speech-bubble3 shadow messages" data-messages="` + result.data.message + ` | ` + result.data.username + ` | ` + result.data.name + `" style="margin-top:7px">
                            <div class="d-flex w-100 justify-content-between" style="
                                padding: 10px;
                            "><small class="mb-1" style="text-transform:uppercase"><strong>` + result.data.username + ` | ` + result.data.name + ` | ` + process + `</strong></small><small>` + result.data.time_message + `</small></div>
                                <p style="
                                padding-bottom: 12px;
                                padding-left: 11px;
                                white-space: pre-wrap;
                            ">` + result.data.message + ' ' + fn_check_lat_lng(result.data.lat, result.data.lng) + '. <a href="' + baseurl + 'incident/download_file?id=' + result.data.id_file + '" class="text-dark" target="_blank"> ' + fn_check_attachment(result.data.file_name) + '</a></p>' +
                            fn_check_gambar(result.data.file_name, result.data.type, path) + `</p></hgroup>`;
                    }

                $('#progress-' +id).append(c_progress);

                // total message incident progress
                $('#total_pregress_' + id).html(
                    "INCIDENT PROGRESS (" + result.kpi.total_progress + ")"
                );

                // Last Progress
                $('#last_progress-' + id).html(
                    "LAST PROGRESS  : " + result.kpi.last_progress
                );

                // auto scroll bottom
                $('#progress-' + id).scrollTop($('#progress-' + id)[0].scrollHeight);
                // end message only

                // token
                $('#token-' + id).empty();
                $('#token_id-' + id).empty();
                token = [];

                $.each(result.token, function(i, item) {
                    // view token 
                    var data_token = '<span><strong>' + fn_check_undifined(item.mention_to) + '&nbsp; </strong>' +
                        '<span style="display:block"><small>' + fn_check_undifined(item.username) + '</small></span></span>';
                    var input_token = '<input type="hidden" name="token[]" value="' + fn_check_undifined(item.mention_to) + '"/>';

                    $('#token-' + id).append(data_token);
                    $('#token_id-' + id).append(input_token);

                    token.push(item.mention_to);
                // end token
            });
    
		});
		 
    function blink_text() {
        $('.text-request > strong').fadeOut(500);
        $('.text-request > strong').fadeIn(500);
    }
    setInterval(blink_text, 1000);

    // edit incident 
    $("#select_root_cause").remoteChained({
        parents: "#select_classification",
        url: baseurl + "ticket/load_root_cause"
    });

    $("#select_root_cause_detail").remoteChained({
        parents: "#select_root_cause",
        url: baseurl + "ticket/load_root_cause_detail"
    });

    $("#site_name1").remoteChained({
        parents: "#region1",
        url: baseurl + "ticket/load_incident_regions1"
    });

    $("#link_start_site").remoteChained({
        parents: "#link_start_region",
        url: baseurl + "ticket/load_incident_regions_start"
    });

    $("#link_end_site").remoteChained({
        parents: "#link_end_region",
        url: baseurl + "ticket/load_incident_regions_end"
    });

    $("#brand1").remoteChained({
        parents: "#site_name1",
        url: baseurl + "ticket/load_brands"
    });

    $("#me1").remoteChained({
        parents: "#brand1",
        url: baseurl + "ticket/load_me"
    });

    // func show and hide in classification of define incidents
    $('#select_classification').on('change', function() {
        if (this.value) {

            $('#region1').val(null).trigger('change');
            $('#link_start_region').val(null).trigger('change');
            $('#link_end_region').val(null).trigger('change');
            $('#site_name1').empty();
            $('#brand1').empty();
            $('#me1').empty();
            $('#link_start_site').empty();
            $('#link_end_site').empty();


        }

        if (this.value == '151' || this.value == '121' || this.value == '171' || this.value == '161' || this.value == '661' || this.value == '131' || this.value == '411' || this.value == '666') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOT")
            $('#source_category_input').val("FOT")
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }
        if (this.value == '251' || this.value == '241' || this.value == '141') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("PS")
            $('#source_category_input').val("PS")
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }
        if (this.value == '111') {
            $('#1').hide()
            $('#3').show()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOC")
            $('#source_category_input').val("FOC")
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }
        if (this.value == '381' || this.value == '361' || this.value == '888') {
            $('#1').hide()
            $('#3').hide()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').hide()
            $('#div_link_upstream_isp').show()
            $('#div_region').show()
            $('#div_site_name').show()
        }


        if (this.value == '999') {
            $('#1').hide()
            $('#3').hide()
            $('#11').hide()
            $('#foc-show').hide()
                // $('#foc-hide').show()
            $('#source_category').hide()
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }
        if (this.value == '221') {
            $('#1').hide()
            $('#3').show()
            $('#11').show()
            $('#foc-show').show()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOC")
            $('#source_category_input').val("FOC")
            $('#div_link_upstream_isp').hide()
            $('#div_region').hide()
            $('#div_site_name').hide()
        }
    });


    $('#link_upstream_isp').on('change', function() {

        if (this.value) {
            $('#region1').val(null).trigger('change');
            $('#region').val(null).trigger('change');
            $('#link_start_region').val(null).trigger('change');
            $('#link_end_region').val(null).trigger('change');
            $('#site_name1').empty();
            $('#site_name').empty();
            $('#brand').empty();
            $('#me').empty();
            $('#link_start_site').empty();
            $('#link_end_site').empty();
        }

        // kondisi jika pilih link upstream ISP bagian source category 
        if (this.value == '1') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("FOT")
            $('#foc-hide').show()
            $('#foc-show').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }

        if (this.value == '2') {
            $('#1').hide()
            $('#3').show()
            $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("FOC")
            $('#foc-hide').hide()
            $('#foc-show').show()
            $('#div_region').hide()
            $('#div_site_name').hide()
        }

        if (this.value == '3') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("PS")
            $('#foc-hide').show()
            $('#foc-show').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }


    });

    // on load browser 
    status_incident = $('.incident_progress').data('status');
    var t_ticket_progress = $('#ticket_progress').DataTable({
        dom: 'flrtpi',
        responsive: true,
        lengthChange: false,
        pageLength: 10,
        destroy: true,
        columnDefs: [{
                "targets": [0], // your case first column
                "className": "text-center",
            },
            {
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                }
            }
        ]
    });

    // jquery typehead 
    var typeaheadSource = ['John', 'Alex', 'Terry'];
    $('input.typeahead').typeahead({
        source: typeaheadSource
    });
    // end of typehead 

    // check inside or outside plan 
    $('.form_close').find("input[name='physical_inv_change1']").click(function() {
        if ($(this).prop("checked") == true) {
            var inv = $(this).val();

            if ($('.form_close').find("input[name='physical_inv_change2']").prop("checked") == true) {
                $('.form_close').find("input[name='physical_inv_change']").val(3);
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
            }

            $('.inv_change_desc').show();
        } else if ($(this).prop("checked") == false) {

            if ($('.form_close').find("input[name='physical_inv_change2']").prop("checked") == false) {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
                $('.inv_change_desc').hide();
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(2);
            }
        }
    });

    $('.form_close').find("input[name='physical_inv_change2']").click(function() {
        if ($(this).prop("checked") == true) {

            var inv = $(this).val();
            if ($('.form_close').find("input[name='physical_inv_change1']").prop("checked") == true) {
                $('.form_close').find("input[name='physical_inv_change']").val(3);
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
            }
            $('.inv_change_desc').show();
        } else if ($(this).prop("checked") == false) {
            if ($('.form_close').find("input[name='physical_inv_change1']").prop("checked") == false) {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
                $('.inv_change_desc').hide();
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(1);
            }
        }
    });
    // end of check inside or outside plan 

    // select2 select_configuration_change on click
    $('.select_configuration_change').change(function() {
        var selectedObj = $(this).children("option:selected").val();

        if (selectedObj == 'no' || selectedObj == '') {
            $('.configurasi_change_activity').hide();
        } else {
            $('.configurasi_change_activity').show();
        }
    });

    if (member != 'NOC') {
        $('.btn-request-stopclock').css('display', 'none');
    }

    // fillin value id incident 
    $('.form_create_message').attr('id', 'form-' + id);
    $('.form_create_message').find("input[name='id']").val(id);
    $('.form_close').find("input[name='id']").val(id);
    // $('.form_create_message').find("textarea[name='message']").attr('disabled', true);
    // set id incident button summary
    $('.btn-incident-summary').attr("data-incident", id);
    // set id progress incident participant 
    $('.scroll-incident-progress').attr('id', 'progress-' + id);
    // set id incident participant 
    $('.list-incident-participant').attr('id', 'participant-' + id);
    // set total participant 
    $('.participants').attr('id', 'total_participant-' + id);
    // set id incident mttr 
    $('.mttr_incident').attr('id', 'mttr-' + id);
    // set id token incident 
    $('.token_incident').attr('id', 'token-' + id);
    $('.token_class').attr('id', 'token_id-' + id);
    // set kpi incident 
    $('.response_time_new').attr('id', 'response_time_new-' + id);
    $('.analisis_time_new').attr('id', 'analisis_time_new-' + id);
    $('.recovery_time_new').attr('id', 'recovery_time_new-' + id);
    $('.mttr_time_new').attr('id', 'mttr_time_new-' + id);
    $('.confirmation_time_new').attr('id', 'confirmation_time_new-' + id);
    // upload file incident 
    $('.btn-attach-incident').attr('data-id', id);
    // set id form upload attach incident 
    $('.form_attach_file_incident').find("input[name='incident_id']").val(id);
    // set id incident progress count 
    $('.total_incident_progress').attr('id', 'total_pregress_' + id);
    // set id incident log 
    $('.btn-log').attr('data-id', id);
    // set id incident log 
    $('.status_incident').attr('id', 'status-' + id);
    // add last progress
    $('.last_progress').attr('id', 'last_progress-' + id);


    // get list incident 
    // fn_incident_list(id);
    fn_list_incident()
        // get ticket incident
    fn_ticket_incident(id);
    // event stream incident participant 
    fn_sse_incident_participant(id);
    // event stream incident progress
    fn_sse_incident_progress(id);
    // event stream token incident 
    fn_sse_incident_token(id)
        // count ticket 
    fn_count_tiket(id);
    // check duration 
    fn_check_duration(id);
    // get summary incident porgress
    // fn_progress_summary(id);
    // get maps las serpo location 
    fn_progress_maps(id);
    // get status incident 
    fn_sse_status_incident(id);
    // kpi incident new
    fn_sse_kpi_incident(id);

    // cek status close 
    if (status_incident === 'close') {

        $('.form_create_message').css('display', 'none');
        $('.btn-close').css('display', 'none');
        $('.btn-duration').css('display', 'none');
        $('.btn-startclock').css('display', 'none');
        $('.btn-stopclock').css('display', 'none');
        $('.btn-attach-incident').css('display', 'none');

    } else if (status_incident === 'merge' || status_incident === 'stop') {

        $('.btn-close').css('display', '');
        $('.form_create_message').css('display', 'none');
        $('.btn-attach-incident').css("display", 'none');

        akses = ["NOC", "SBU"];
        if (jQuery.inArray(member, akses) !== -1) {
            $('.btn-duration').css("display", '');
        } else {
            $('.btn-duration').css("display", 'none');
        }

    } else if (status_incident === 'open' || status_incident === 'request' || status_incident === 'clear') {

        akses = ["NOC", "SBU", "CONTACTCENTER"];
        if (jQuery.inArray(member, akses) !== -1) {
            $('.btn-close').css('display', '');
            $('.btn-startclock').css("display", '');
        } else {
            $('.btn-close').css('display', 'none');
            $('.btn-startclock').css("display", 'none');
        }

        $('.form_create_message').css('display', '');
        $('.btn-attach-incident').css('display', '');
        // $('.form_create_message').find("textarea[name='message']").attr('disabled', false);
        // $('.form_create_message').find("button[type='submit']").attr('disabled', false);
        // $('.form_create_message').find("button[type='button']").attr('disabled', false);
        $('.btn-submit-span').attr("disabled", false);
        $('.btn-attach-incident').attr("disabled", false);

    }

    // click list incident 
    $('.scroll-incident-list').on('click', 'a.btn_list', function(e) {
        e.preventDefault();

        id = $(this).data('id');
        status_incident = $(this).data('status');

        // add attribute id in form create message 
        $('.form_create_message').attr('id', 'form-' + id);
        // remove class active incident 
        $('.scroll-incident-list').find('.btn_list').removeClass('active_incident');
        // set id incident in form create message
        $('.form_create_message').find("input[name='id']").val(id);
        // set id incident form close incident 
        $('.form_close').find("input[name='id']").val(id);
        // set id incident value in button stop clock
        $('.btn-duration').attr('data-id', id);
        // set id incident in button summary 
        $('.btn-incident-summary').attr("data-incident", id);
        // set incident progress id
        $('.scroll-incident-progress').attr('id', 'progress-' + id);
        // incident participant id
        $('.list-incident-participant').attr('id', 'participant-' + id);
        // set id incident in html total incident partipant
        $('.participants').attr('id', 'total_participant-' + id);
        // set id incident in incident mttr 
        $('.mttr_incident').attr('id', 'mttr-' + id);
        // set id incident in token incident 
        $('.token_incident').attr('id', 'token-' + id);
        $('.token_class').attr('id', 'token_id-' + id);
        $('.form_history').find("input[name='id']").val(id);
        // config kpi incident 
        $('.response_time_new').attr('id', 'response_time_new-' + id);
        $('.analisis_time_new').attr('id', 'analisis_time_new-' + id);
        $('.recovery_time_new').attr('id', 'recovery_time_new-' + id);
        $('.mttr_time_new').attr('id', 'mttr_time_new-' + id);
        $('.confirmation_time_new').attr('id', 'confirmation_time_new-' + id);
        // upload file incident 
        $('.btn-attach-incident').attr('data-id', id);
        $('.form_attach_file_incident').find("input[name='incident_id']").val(id);
        // set id incident progress count 
        $('.total_incident_progress').attr('id', 'total_pregress_' + id);
        // set id incident log 
        $('.btn-log').attr('data-id', id);
        // set incident progress merge 
        $('.status_incident').attr('id', 'status-' + id);
        // add last progress
        $('.last_progress').attr('id', 'last_progress-' + id);

        $(this).addClass("active_incident");

        // get list incident 
        // fn_incident_list(id);
        // get ticket incident
        fn_ticket_incident(id);
        // event stream incident participant 
        fn_sse_incident_participant(id);
        // event stream incident progress
        fn_sse_incident_progress(id);
        // event stream token incident 
        fn_sse_incident_token(id)
            // count ticket 
        fn_count_tiket(id);
        // check duration 
        fn_check_duration(id);
        // get summary incident porgress
        // fn_progress_summary(id);
        // get maps las serpo location 
        fn_progress_maps(id);
        // get status incident 
        fn_sse_status_incident(id);
        // get kpi incident 
        fn_sse_kpi_incident(id);

        // cek status close 
        if (status_incident === 'close') {

            $('.form_create_message').css('display', 'none');
            $('.btn-close').css('display', 'none');
            $('.btn-duration').css('display', 'none');
            $('.btn-startclock').css('display', 'none');
            $('.btn-stopclock').css('display', 'none');
            $('.btn-attach-incident').css('display', 'none');

        } else if (status_incident === 'merge' || status_incident === 'stop') {

            $('.form_create_message').css('display', 'none');
            $('.form_create_message').css('display', 'none');
            $('.btn-attach-incident').css("display", 'none');

            akses = ["NOC", "SBU"];
            if (jQuery.inArray(member, akses) !== -1) {
                $('.btn-duration').css("display", '');
                $('.btn-close').css('display', '');
            } else {
                $('.btn-duration').css("display", 'none');
                $('.btn-close').css('display', 'none');
            }

        } else if (status_incident === 'open' || status_incident === 'request' || status_incident === 'clear') {

            akses = ["NOC", "SBU"];
            if (jQuery.inArray(member, akses) !== -1) {
                $('.btn-close').css('display', '');
                $('.btn-startclock').css("display", '');
            } else {
                $('.btn-close').css('display', 'none');
                $('.btn-startclock').css("display", 'none');
            }

            $('.form_create_message').css('display', '');
            $('.btn-attach-incident').css('display', '');
            $('.btn-attach-incident').attr("disabled", false);
            $('.btn-submit').attr("disabled", false);

        }

    });
    // end of click list incident 


    // get activeon load browser
    function fn_get_active(p1, p2, status, ttr) {

        // cek status incident 
        // alert(p1+' - '+p2+' - '+status+' - '+ ttr)
        if (p1 == p2) {
            return 'active_incident';
        } else {
            if (status === 'close') {
                return 'close_incident'; // grey
            } else if (status === 'clear') {
                return 'ready_incident'; // green
            } else {
                if (ttr > 14400) {
                    return 'ttr_incident'; // red
                } else {
                    if (status === 'stop') {
                        return 'stop_incident';
                    } else {
                        return '';
                    }
                }
            }
        }
    }

    // list incident 
    // function fn_incident_list(id) {

    //     $.ajax({
    //         url: baseurl + 'incident/list_open_incident_progress?type=' + type_filter,
    //         type: 'GET',
    //         dataType: 'JSON',
    //         async: true,
    //         success: function(data) {

    //             $('.scroll-incident-list').empty();

    //             var raw_data = data;
    //             var raw_total = 0;

    //             $.each(raw_data, function(i, item) {

    //                 var l_incident = '<a href="#" class="list-group-item list-group-item-action flex-column align-items-start btn_list mt-1 ' + fn_get_active(item.id, id, item.status, item.ttr) + '" data-status="' + item.status + '" data-id="' + item.id + '" data-toggle="tooltip" title="' + item.description + '" data-ttr="' + item.ttr + '">' +
    //                     '<div class="d-flex w-100 justify-content-between">' +
    //                     '<small class="mb-1 text-secondary">' + item.incident_id + '</small>' +
    //                     '</div>' +
    //                     '<p class="mb-1 incident-desc">' + item.description.substr(0, 50) + '...</p>' +
    //                     '<small class="text-secondary">' + item.open_time + '</small>' +
    //                     '</a>'

    //                 $('.scroll-incident-list').append(l_incident);

    //                 raw_total = raw_total + 1;

    //             });

    //             $('.count_incident').html(
    //                 'INCIDENT LIST (' + raw_total + ')'
    //             );

    //         }
    //     });
    // }
    // end of list incident 

    // display ticket attach 
    function fn_ticket_incident(id) {

        $('.list-ticket-attch').empty();

        $.ajax({
            url: baseurl + 'incident/socket/get_ticket_incident_progress?id=' + id,
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                var t_data = data;
                var count = 0;

                $.each(t_data, function(i, item) {

                    count = count + 1;

                    var ticket_list = '<span data-toggle="tooltip" title="' + t_data[i].description + '"><a href="#" class="list-group-item list-group-item-action btn-ticket" data-service="' + t_data[i].service_id + '" data-ticket=' + t_data[i].ticket_id + ' data-toggle="modal" data-target="#modalTicketProgress">' +
                        '<div class="row">' +
                        '<div class="col-sm-2">' +
                        '<p style="font-size:25px"><i class="fa fa-credit-card"></i></p>' +
                        '</div>' +
                        '<div class="col-sm-10">' +
                        '<p style="font-size:11px">' + t_data[i].ticket_id + '</p>' +
                        '<p style="font-size:11px">' + t_data[i].description.substring(0, 50) + '...</p>' +
                        '</div>' +
                        '</div>' +
                        '</a></span>';

                    $('.list-ticket-attch').append(ticket_list);

                });

                $('.ticket_attch').html('TICKET ATTACH (' + count + ')')

            }
        });
    }
    // end of ticket attach

    // var table cm 
    var t_cm = $('#tb_cm').DataTable({
        lengthChange: false,
        dom: 'frtpi',
        pageLength: 10,
        columnDefs: [{
                "targets": [0], // your case first column
                "className": "text-center",
                "width": "20px"
            },
            {
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                    $(td).attr('title', cellData);
                }
            }
        ]
    });

    // var table alarm 
    var t_alarm = $('#tb_alarm').DataTable({
        dom: 'frtpi Rl',
        lengthChange: false,
        // responsive: true,
        pageLength: 10,
        // scrollX: true,
        columnDefs: [{
                "targets": [0], // your case first column
                "className": "text-center",
                "width": "20px"
            },
            {
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                    $(td).attr('title', cellData);
                }
            }
        ]
    });

    var t_assets = $('#tb_assets').DataTable({
        dom: 'frtpi Rl',
        lengthChange: false,
        // responsive: true,
        // scrollX: true,
        pageLength: 10,
        columnDefs: [{
                "targets": [0], // your case first column
                "className": "text-center",
                "width": "20px"
            },
            {
                "targets": [4], // your case first column
                "width": "50px"
            },
            {
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                    $(td).attr('title', cellData);
                }
            }
        ]
    });

    // tombol cm klik
    $('.btn-cm').click(function(e) {
        e.preventDefault();

        $('#modalcm').modal('show');

        var sid = $('.sid').attr('data-sid');
        // var sid = '01000071778';

        $.ajax({
            url: baseurl + 'incident/integration/',
            type: 'POST',
            data: { sid: sid, modul: 'cm' },
            dataType: 'JSON',
            async: true,
            success: function(data) {

                // jika data tidak ada 
                if (data.msg !== 'Config File not found') {

                    var no = 1;
                    t_cm.clear().draw();

                    $.each(data, function(i, item) {

                        t_cm.row.add([
                            (i + no),
                            item.no_so,
                            item.service_id,
                            item.id_so,
                            item.name,
                            '<a href="' + item.link + '" target="_blank">' + item.link + '</a>'
                        ]).draw(false);

                    });
                }
            }
        });
    });


    // tombol alarm klik
    $('.btn-alarm').click(function(e) {
        e.preventDefault();

        $('#modalalarm').modal('show');

        var sid = $('.sid').attr('data-sid');
        // var sid = '01000071778';

        $.ajax({
            url: baseurl + 'incident/integration/',
            type: 'POST',
            data: { sid: sid, modul: 'alarm' },
            async: true,
            dataType: 'JSON',
            success: function(data) {
                // jika data tidak ada 
                if (data.msg !== 'Config File not found') {
                    var no = 1;
                    t_cm.clear().draw();

                    $.each(data, function(i, item) {

                        t_alarm.row.add([
                            (i + no),
                            item.id,
                            item.trig_id,
                            item.eventid,
                            item.zabb,
                            item.priority,
                            item.hostid,
                            item.ipaddr,
                            item.host_,
                            item.description,
                            item.sid,
                            item.clock,
                            item.ack,
                            item.msg,
                            item.indate,
                        ]).draw(false);

                    });
                }
            }
        });
    });

    // integration 
    $('.btn-assets').click(function(e) {
        e.preventDefault();

        $('#modalassets').modal('show');

        var sid = $('.sid').attr('data-sid');
        // var sid = '140001576';

        $.ajax({
            url: baseurl + 'incident/integration/',
            type: 'POST',
            data: { sid: sid, modul: 'assets' },
            async: true,
            dataType: 'JSON',
            success: function(data) {

                if (data.msg !== 'Customer not found') {
                    var no = 1;
                    t_assets.clear().draw();
                    $.each(data, function(i, item) {
                        t_assets.row.add([
                            (i + no),
                            // item.rownum,
                            // item.id,
                            // item.code,
                            item.name,
                            item.phone,
                            item.pic,
                            item.pic_phone,
                            item.pic_email,
                            item.desc,
                            item.username,
                            item.updated_at,
                            item.sts
                        ]).draw(false);
                    });
                }
            }
        });
    });


    // display ticket progress 
    $(document).on('click', '.btn-ticket', function() {
        var ticket_id = $(this).attr('data-ticket');
        var sid = $(this).attr('data-service');

        $('.sid').attr('data-sid', sid);
        $('.btn-progress').attr('data-ticket_id', ticket_id);

        $.ajax({
            url: baseurl + "incident/socket/ticket_progress?ticket_id=" + ticket_id,
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                var no = 1;
                t_ticket_progress.clear().draw();

                $.each(data, function(i, item) {

                    // alert(item.service_id);
                    t_ticket_progress.row.add([
                        (i + no),
                        item.username,
                        item.timestamp,
                        item.description_progress
                    ]).draw(false);

                });

            }
        });
    });


    // count ticket 
    function fn_count_tiket(id) {

        // reload count ticket 
        $.ajax({
            url: baseurl + "incident/socket/count_ticket?id=" + id,
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                var total = data.total_ticket;
                $('.ticket_attch').html(
                    'TICKET ATTACH (' + total + ')'
                );

            }
        });

    }
    // end of count ticket 

    // incident status
    // function fn_get_status(id) {

    //     $('.status_incident').empty();

    //     $.ajax({
    //         url: baseurl + 'incident/get_status?id=' + id,
    //         type: 'GET',
    //         dataType: 'JSON',
    //         async: true,
    //         success: function(data) {
    //             if (data.status == 'close') {
    //                 $('#incident_close').hide();
    //             } else {
    //                 $('#incident_close').show();
    //             }
    //             $('.status_incident').html('<strong>' + data.status.toUpperCase() + '&nbsp;&nbsp;<i class="fa fa-check-circle-o"></i></strong>');
    //         }
    //     });
    // }
    // // end of incident status 

    // mention data
    $('#created_message').suggest('@', {
        data: group,
        map: function(user) {
            return {
                value: user.group_code,
                text: '<strong>' + user.group_code + '</strong>'
            }
        }

    });


    // $('#created_message').keyup(function() {
    // var val_message = $('.form_create_message').find("textarea[name='message']").val();
    // var mention = val_message.includes("@");
    // // // var mention_cc = val_message.includes("@CONTACTCENTER ");
    // // // var mention_cc_space = val_message.includes("@CONTACTCENTER");
    // // if (mention === true) {
    // //     // $('.form_validation').show();
    // //     // $('#justification').show();
    // // } else {
    // //     // $('.form_validation').hide();
    // //     // $('#justification').hide();
    // // }
    // });


    // hide alert 
    $('#ok').click(function(e) {
        e.preventDefault();
        $('#modalerror').modal('hide');
    });

    //form create message 
    $('.form_create_message').submit(function(e) {
        e.preventDefault();
        var val_message = $('.form_create_message').find("textarea[name='message']").val();
        var form_validasi = $('.select-process').val();

        // check session checin and mod
        if (user_checkin === 0 || user_checkin === "0" || typeof user_checkin === "undefined") {
            pesan = "Please Check in ";
            fn_show_alert(pesan);
            return false;
        }

        // check message is empty
        if (val_message === '') {
            // $('.content_alert').html('Please Enter Message');
            pesan = 'Pelase Enter Message';
            fn_show_alert(pesan);
            return false;
        }

        //display preeleoad
        $('.preeload').css('display', '');

        $.ajax({
            type: 'POST',
            data: new FormData(this),
            url: baseurl + 'incident/socket/created_progress',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            async: true,
            success: function(result) {
            
                dhtmlx.message({
                    'text': "successfully!",
                    'expire': -1
                });
                Server.send( 'message', result );
                Server.send( 'notification2', result );

                $('.preeload').css('display', 'none');
                $('#created_message').val(null);
                
                // start participant
                var total_participant = 0;
                $('#participant-' + id).empty();
                $('#total_participant-' + id).empty();
                $.each(result.participan, function(i, item) {
                    
                    total_participant = total_participant + 1;

                    var part_list = '<a href="#" class="list-group-item list-group-item-action" style="padding-top:5px; padding-bottom:5px">' +
                        '<div class="row">' +
                        '<div class="col-sm-2">' +
                        '<p style="font-size:25px; margin-top:4px"><i class="fa fa-user-circle"></i></p>' +
                        '</div>' +
                        '<div class="col-sm-10">' +
                        '<p class="text-secondary" style="font-size:12px; text-transform:capitalize">' + item.username + '</p>' +
                        '<p class="text-secondary" style="font-size:12px">' + item.name + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</a>';

                    $('#participant-' + id).append(part_list);

                });

                $('#total_participant-' + id).html(
                    'PARTICIPANTS (' + total_participant + ')'
                );
                // end participant

                // start message only
                // process 
                if (result.data.process === null) {
                    process = "NO PROCESS";
                } else {
                    process = result.data.process;
                }

                    var path = api_url + result.data.file_name;
                    // console.log(item.username.toLowerCase())
                    if (username == result.data.username.toLowerCase()) {
                        var c_progress = `
                        <hgroup class="speech-bubble-front shadow messages" data-messages="` + result.data.message + ` | ` + result.data.username + ` | ` + result.data.name + `"  style="margin-top:7px">
                            <div class="d-flex w-100 justify-content-between" style="padding: 10px;">
                            <small class="mb-1" style="text-transform:uppercase"><strong>` + result.data.username + ` | ` + result.data.name + ` | ` + process + `</strong></small><small>` + result.data.time_message + `</small></div>
                                <p style="padding-bottom: 12px;padding-left: 11px;white-space: pre-wrap;">` + result.data.message + ' ' + fn_check_lat_lng(result.data.lat, result.data.lng) + '<a href="' + baseurl + 'incident/download_file?id=' + result.data.id_file + '" class="text-dark" target="_blank"> ' + fn_check_attachment(result.data.file_name) + '</a></p>' +
                            fn_check_gambar(result.data.file_name, result.data.type, path) + `</p></hgroup>`;
                    } else {
                        var c_progress = `
                        <hgroup class="speech-bubble3 shadow messages" data-messages="` + result.data.message + ` | ` + result.data.username + ` | ` + result.data.name + `" style="margin-top:7px">
                            <div class="d-flex w-100 justify-content-between" style="
                                padding: 10px;
                            "><small class="mb-1" style="text-transform:uppercase"><strong>` + result.data.username + ` | ` + result.data.name + ` | ` + process + `</strong></small><small>` + result.data.time_message + `</small></div>
                                <p style="
                                padding-bottom: 12px;
                                padding-left: 11px;
                                white-space: pre-wrap;
                            ">` + result.data.message + ' ' + fn_check_lat_lng(result.data.lat, result.data.lng) + '. <a href="' + baseurl + 'incident/download_file?id=' + result.data.id_file + '" class="text-dark" target="_blank"> ' + fn_check_attachment(result.data.file_name) + '</a></p>' +
                            fn_check_gambar(result.data.file_name, result.data.type, path) + `</p></hgroup>`;
                    }

                $('#progress-' + id).append(c_progress);

                // total message incident progress
                $('#total_pregress_' + id).html(
                    "INCIDENT PROGRESS (" + result.kpi.total_progress + ")"
                );

                // Last Progress
                $('#last_progress-' + id).html(
                    "LAST PROGRESS  : " + result.kpi.last_progress
                );

                // auto scroll bottom
                $('#progress-' + id).scrollTop($('#progress-' + id)[0].scrollHeight);
                // end message only

                // start token
                token = [];

                // empty token
                $('#token-' + id).empty();
                $('#token_id-' + id).empty();

                $.each(result.token, function(i, item) {
                    // view token 
                    var data_token = '<span><strong>' + fn_check_undifined(item.mention_to) + '&nbsp; </strong>' +
                        '<span style="display:block"><small>' + fn_check_undifined(item.username) + '</small></span></span>';
                    var input_token = '<input type="hidden" name="token[]" value="' + fn_check_undifined(item.mention_to) + '"/>';

                    $('#token-' + id).append(data_token);
                    $('#token_id-' + id).append(input_token);

                    token.push(item.mention_to);

                });
                // end token

            
            }

        });

        $('.delete_file').click();

    });
    // end of function

    // close ticket incident 
    $('.btn-close').click(function() {
        var id = $('.form_close').find("input[name='id']").val();
        var message;

        $.ajax({
            url: baseurl + 'incident/socket/close_permission?id=' + id,
            type: 'POST',
            dataType: 'JSON',
            async: false,
            success: function(data) {
                status_close = data.status;
                message = data.message;
            }
        });

        // check session checkin and mod
        if (status_close == false) {
            // pesan = "Incident not ready to close";
            pesan = message;
            fn_show_alert(pesan);
            return false;
        }

        // check session checin and mod
        if (user_checkin === 0 || user_checkin === "0" || typeof user_checkin === "undefined") {
            pesan = "Please Check in ";
            fn_show_alert(pesan);
            return false;
        }

        // check account permission 
        // if (member == 'CONTACTCENTER' || member == 'SERPO') {
        if (member == 'SERPO' || member == "CONTACTCENTER") {
            pesan = "Your Account Cannot Close Incident ";
            fn_show_alert(pesan);
            return false;
        }

        $('#modalCloseIncident').modal('show');

        $.ajax({
            url: baseurl + 'incident/socket/get_summary_incident?id=' + id,
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                var root_cause = "<option value=" + data.root_cause_id + ">" + data.root_cause_id + "</option>";
                var root_cause_detail = "<option value=" + data.root_causes_detail_id + ">" + data.root_cause_detail_name + "</option>";
                var link = "<option value=" + data.link + ">" + data.link + "</option>";
                var site_name = "<option value=" + data.site_name_id + ">" + data.site_name + "</option>";

                $('.form_close').find("input[name='creator']").val(data.creator);
                $("#select_classification").select2('val', [data.classification_id]);
                $("#select_root_cause").html(root_cause);
                $("#select_root_cause_detail").html(root_cause_detail);
                $("#link").html(link);
                $("#region1").select2('val', [data.region_code]);
                $(".form_close").find("textarea[name='description']").val(data.description);
                $('#' + data.network).attr('checked', 'checked');
                $('#' + data.priority).attr('checked', 'checked');
                $('#site_name1').html(site_name);

            }
        });

    });


    // close incident 
    $('.form_close').submit(function(e) {
        e.preventDefault();

        var incident_id = $('.form_close').find("input[name='id']").val();

        $('.form_create_message').find("textarea[name='message']").val('Incident Close');

        $.ajax({
            url: baseurl + 'incident/socket/close_incident_prosses',
            data: new FormData(this),
            type: 'POST',
            dataType: 'JSON',
            async: true,
            contentType: false,
            processData: false,
            success: function(data) {

                if (data.status === 'error') {
                    pesan = "Your AccountC Cannot Close Incident ";
                    fn_show_alert
                    $('#modalCloseIncident').modal('hide');
                    return false;
                }

                $('#modalCloseIncident').modal('hide');
                $('.form_create_message').find("textarea[name='message']").val("");
                $('.btn-duration').attr('data-id', incident_id).css("display", 'none');
                $('.btn-attach-incident').css("display", 'none');
                $('.form_create_message').css('display', 'none');
                $('.btn-close').css('display', 'none');

                // reset form close 
                $('.form_close').trigger('reset');
                $('.configurasi_change_activity').select2("val", ['']);
                $('.select_configuration_change').select2("val", ['no']);

            }
        });

    });
    // end of close incident 

    // select reason type
    function select_reason_type() {
        $.ajax({
            url: baseurl + 'incident/socket/incident_reason_stopclock',
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                $('.select_reason_type').empty();

                $.each(data, function(i, item) {
                    var html = "<option value=" + item.id + ">" + item.reason + "</option>";
                    $('.select_reason_type').append(html);
                })

            }
        });
    }

    // start clock
    $('.btn-duration').click(function(e) {
        e.preventDefault();

        var id = $(this).attr('data-id'),
            type = $(this).attr('data-type'),
            status = $(this).attr('data-status');

        // Jika group SERPO
        if (member === 'SERPO') {
            pesan = "You're account cannot stop incident";
            fn_show_alert(pesan);
            return false;
        }

        // Jika group SBU
        if (member === 'SBU' || member === 'CONTACTCENTER') {

            if (jQuery.inArray(groupcode, token) !== -1) {

                $.ajax({
                    url: baseurl + 'incident/socket/get_summary_incident?id=' + id,
                    type: 'GET',
                    dataType: 'JSON',
                    async: true,
                    success: function(data) {

                        $('.form_stop_clock').find("input[name='id']").val(id);
                        $('.form_stop_clock').find("input[name='type']").val(type);
                        $('.form_stop_clock').find("input[name='status']").val(status);
                        $('.form_stop_clock').find("input[name='timestamp']").val((data.current_date).substring(0, 19));
                        $('.form_stop_clock').find("textarea[name='description']").val(data.description);
                        $('.form_stop_clock').find("input[name='duration']").val($('#mttr-' + id + ' > strong').html());
                        $('.form_stop_clock').find("textarea[name='reason']").val('');

                        select_reason_type();

                        // jika klik tombol start
                        if (type === '1') {
                            // tampilkan modal
                            $('.btn-form-stop').click();
                        } else {
                            // klik tombol submit form
                            $('#modalStopClock').modal('show');
                        }

                    }
                });

            } else {
                pesan = "You're account cannot stop incident";
                fn_show_alert(pesan);
            }

            return false;
        }


        // Jika group NOC
        if (member === 'NOC') {

            $.ajax({
                url: baseurl + 'incident/socket/get_summary_incident?id=' + id,
                type: 'GET',
                dataType: 'JSON',
                async: true,
                success: function(data) {

                    $('.form_stop_clock').find("input[name='id']").val(id);
                    $('.form_stop_clock').find("input[name='type']").val(type);
                    $('.form_stop_clock').find("input[name='status']").val(status);
                    $('.form_stop_clock').find("input[name='timestamp']").val((data.current_date).substring(0, 19));
                    $('.form_stop_clock').find("textarea[name='description']").val(data.description);
                    $('.form_stop_clock').find("input[name='duration']").val($('#mttr-' + id + ' > strong').html());
                    $('.form_stop_clock').find("textarea[name='reason']").val('');

                    select_reason_type();

                    // jika klik tombol start
                    if (type === '1') {
                        // tampilkan modal
                        $('.btn-form-stop').click();
                    } else {
                        // klik tombol submit form
                        $('#modalStopClock').modal('show');
                    }

                }
            });

            return false;

        }

        // get reason request stop clock
        // $.ajax({
        //     url: baseurl + 'incident/incident_reason_stopclock',
        //     type: 'GET',
        //     dataType: 'JSON',
        //     async: true,
        //     success: function(data) {

        //         $('.select_reason_type').empty();

        //         $.each(data, function(i, item) {
        //             var html = "<option value=" + item.id + ">" + item.reason + "</option>";
        //             $('.select_reason_type').append(html);
        //         })

        //     }
        // });


        // if (member === 'NOC') {
        // fill form stopclock
        // $.ajax({
        //     url: baseurl + 'incident/get_summary_incident?id=' + id,
        //     type: 'GET',
        //     dataType: 'JSON',
        //     async: true,
        //     success: function(data) {

        //         $('.form_stop_clock').find("input[name='id']").val(id);
        //         $('.form_stop_clock').find("input[name='type']").val(type);
        //         $('.form_stop_clock').find("input[name='status']").val(status);
        //         $('.form_stop_clock').find("input[name='timestamp']").val((data.current_date).substring(0, 19));
        //         $('.form_stop_clock').find("textarea[name='description']").val(data.description);
        //         $('.form_stop_clock').find("input[name='duration']").val($('#mttr-' + id + ' > strong').html());
        //         $('.form_stop_clock').find("textarea[name='reason']").val('');

        // jika klik tombol start
        // if (type === '1') {
        // tampilkan modal
        //     $('.btn-form-stop').click();
        // } else {
        // klik tombol submit form
        //             $('#modalStopClock').modal('show');
        //         }

        //     }
        // });

        // } else {

        //     if (jQuery.inArray(groupcode, token) !== -1) {
        //         // define variable 
        //         var id = $(this).attr('data-id'),
        //             type = $(this).attr('data-type'),
        //             status = $(this).attr('data-status');

        //         // fill form stopclock
        //         $.ajax({
        //             url: baseurl + 'incident/get_summary_incident?id=' + id,
        //             type: 'GET',
        //             dataType: 'JSON',
        //             async: true,
        //             success: function(data) {

        //                 $('.form_stop_clock').find("input[name='id']").val(id);
        //                 $('.form_stop_clock').find("input[name='type']").val(type);
        //                 $('.form_stop_clock').find("input[name='status']").val(status);
        //                 $('.form_stop_clock').find("input[name='timestamp']").val((data.current_date).substring(0, 19));
        //                 $('.form_stop_clock').find("textarea[name='description']").val(data.description);
        //                 $('.form_stop_clock').find("input[name='duration']").val($('#mttr-' + id + ' > strong').html());
        //                 $('.form_stop_clock').find("textarea[name='reason']").val('');

        //                 // jika klik tombol start
        //                 if (type === '1') {
        //                     // tampilkan modal
        //                     $('.btn-form-stop').click();
        //                 } else {
        //                     // klik tombol submit form
        //                     $('#modalStopClock').modal('show');
        //                 }

        //             }
        //         });

        //     } else {
        //         pesan = "You're account cannot stop incident";
        //         fn_show_alert(pesan);
        //         return false;
        //     }

        // }

    });

    // submit stop clock
    $('.form_stop_clock').submit(function(e) {
        e.preventDefault();
        var access_stopclock = ['NOC', 'SBU'];
        $('#modalStopClock').modal('hide');

        var id = $(this).find("input[name='id']").val(),
            type = $(this).find("input[name='type']").val();

        if (type == "1") {

            if (jQuery.inArray(member, access_stopclock) == -1) {
                // if (member !== 'NOC') {
                pesan = "You're account cannot stop incident";
                fn_show_alert(pesan);
                return false;

            } else {

                // if member is noc direct to stoped incident 
                $.ajax({
                    url: baseurl + 'incident/socket/duration',
                    type: 'POST',
                    dataType: 'JSON',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: true,
                    success: function(data) {
                        t_request_stopclock.ajax.reload();
                    }

                });

                pesan = "Incident Start";
                fn_show_alert(pesan);
                $('.btn-startclock').hide();
                $('.btn-stopclock').show();

                return false;

            }

        }

        // request stop clock
        // if (jQuery.inArray(member, access_stopclock) == -1) {
        if (member !== 'NOC') {
            $.ajax({
                url: baseurl + 'incident/socket/request_stopclock',
                type: 'POST',
                dataType: 'JSON',
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                success: function(data) {
                    // alert(data)
                    fn_show_alert(data);
                    t_request_stopclock.ajax.reload();
                }

            });
            return false;
        }

        if (type == 1) {
            // START INCIDENT 
            pesan = "Incident Start";
            fn_show_alert(pesan);
            $('.btn-startclock').hide();
            $('.btn-stopclock').show();
        } else {
            // STOP INCIDENT
            pesan = "Incident Stoped";
            fn_show_alert(pesan);
            $('.btn-startclock').show();
            $('.btn-stopclock').hide();
        }


        // // if member is noc direct to stoped incident 
        // $.ajax({
        //     url: baseurl + 'incident/duration',
        //     type: 'POST',
        //     dataType: 'JSON',
        //     data: new FormData(this),
        //     processData: false,
        //     contentType: false,
        //     cache: false,
        //     async: true,
        //     success: function(data) {
        //         t_request_stopclock.ajax.reload();
        //     }

        // });


        // if (type == 1) {
        //     // START INCIDENT 
        //     // alert('Incident start');
        //     pesan = "Incident Start";
        //     fn_show_alert(pesan);
        //     $('.btn-startclock').hide();
        //     $('.btn-stopclock').show();
        // } else {
        //     // STOP INCIDENT
        //     // alert('Incident stoped');
        //     pesan = "Incident Stoped";
        //     fn_show_alert(pesan);
        //     $('.btn-startclock').show();
        //     $('.btn-stopclock').hide();
        // }

        // fn_get_status(id);

    });

    // cek start and stop clock
    function fn_check_duration(id) {

        $.ajax({
            url: baseurl + "incident/socket/duration/reload_token?id=" + id,
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                if (status_incident !== 'close') {

                    if (data.type == 1) {

                        $('.btn-startclock').hide();
                        $('.btn-stopclock').show();
                    } else {

                        $('.btn-startclock').show();
                        $('.btn-stopclock').hide();
                    }

                }
            }
        });
    }

    // check file attacment incident progress (message on incident)
    function fn_check_attachment(file_name) {
        if (file_name === null) {
            return '';
        } else {
            return '<strong><i class="fa fa-folder"></i> Download File </strong>';
        }
    }

    // check image in incident progress (message on incident)
    function fn_check_gambar(file_name, type, path) {

        if (file_name === null) {
            return '';
        } else {
            var p_type = type.split('/');

            if (p_type[0] === 'image') {
                return '<img src="' + path + '" style="height:100px">';
            } else {
                return ''
            }
        }

    }


    // Server send event
    function fn_sse_incident_progress(id) {

        var process;
            var url = baseurl + "incident/socket/reload_progress?id=" + id;
            var total = 0;

            $.ajax({
                url: url,
                dataType: 'JSON',
                async: true,
                success: function(data) {

                    // reset total
                    total = 0;

                    $('#progress-' + id).empty();
                    $('#total_pregress_' + id).empty();
                    $('#last_progress-' + id).empty();

                        $.each(data.data, function(i, item) {

                            // counter total message
                            total = total + 1;

                            // process 
                            if (item.process === null) {
                                process = "NO PROCESS";
                            } else {
                                process = item.process;
                            }

                            var path = api_url + item.file_name;
                            // console.log(item.username.toLowerCase())
                            if (username == item.username.toLowerCase()) {
                                var c_progress = `
                                <hgroup class="speech-bubble-front shadow messages" data-messages="` + item.message + ` | ` + item.username + ` | ` + item.name + `"  style="margin-top:7px">
                                    <div class="d-flex w-100 justify-content-between" style="padding: 10px;">
                                    <small class="mb-1" style="text-transform:uppercase"><strong>` + item.username + ` | ` + item.name + ` | ` + process + `</strong></small><small>` + item.time_message + `</small></div>
                                        <p style="padding-bottom: 12px;padding-left: 11px;white-space: pre-wrap;">` + item.message + ' ' + fn_check_lat_lng(item.lat, item.lng) + '<a href="' + baseurl + 'incident/download_file?id=' + item.id_file + '" class="text-dark" target="_blank"> ' + fn_check_attachment(item.file_name) + '</a></p>' +
                                    fn_check_gambar(item.file_name, item.type, path) + `</p></hgroup>`;
                            } else {
                                var c_progress = `
                                <hgroup class="speech-bubble3 shadow messages" data-messages="` + item.message + ` | ` + item.username + ` | ` + item.name + `" style="margin-top:7px">
                                    <div class="d-flex w-100 justify-content-between" style="
                                        padding: 10px;
                                    "><small class="mb-1" style="text-transform:uppercase"><strong>` + item.username + ` | ` + item.name + ` | ` + process + `</strong></small><small>` + item.time_message + `</small></div>
                                        <p style="
                                        padding-bottom: 12px;
                                        padding-left: 11px;
                                        white-space: pre-wrap;
                                    ">` + item.message + ' ' + fn_check_lat_lng(item.lat, item.lng) + '. <a href="' + baseurl + 'incident/download_file?id=' + item.id_file + '" class="text-dark" target="_blank"> ' + fn_check_attachment(item.file_name) + '</a></p>' +
                                    fn_check_gambar(item.file_name, item.type, path) + `</p></hgroup>`;
                            }

                            $('#progress-' + id).prepend(c_progress);

                        });

                        // total message incident progress
                        $('#total_pregress_' + id).html(
                            "INCIDENT PROGRESS (" + total + ")"
                        );

                        // Last Progress
                        $('#last_progress-' + id).html(
                            "LAST PROGRESS  : " + data.last_progress
                        );

                        // auto scroll bottom
                        $('#progress-' + id).scrollTop($('#progress-' + id)[0].scrollHeight);

                        return false;
                }
            })
    }


    // get message location 
    $(document).on('click', '.get_location', function(e) {
        e.preventDefault();
        var lat = $(this).attr('data-lat');
        var lng = $(this).attr('data-lng');

        fn_message_maps(lat, lng);

        $('#modalSerpoLocation').modal('show');

    });


    // check lat long null
    function fn_check_lat_lng(lat, lng) {
        var lat_lng;
        if (lat == 0 || lat == null) {
            // lat_lng = "Tap to open lat long " + lat + "" + lng;
            lat_lng = "";
        } else {
            lat_lng = "<a href='#' class='text-dark get_location' data-lat=" + lat + " data-lng=" + lng + ">Tap to open location : " + lat + " " + lng + "</a>";
        }

        return lat_lng;
    }

    // fn time 
    function get_puluhan(p) {
        if (p < 10) {
            return '0' + p;
        } else {
            return p;
        }
    }


    // Server send event participan
    function fn_sse_incident_participant(id) {

        var url = baseurl + "incident/socket/reload_participan?id=" + id
        $.ajax({
            url: url,
            dataType: 'JSON',
            async: true,
            success: function(data) {
                $('#participant-' + id).empty();
                $('#total_participant-' + id).empty();

                var total = 0;

                $.each(data, function(i, item) {

                    total = total + 1;

                    var part_list = '<a href="#" class="list-group-item list-group-item-action" style="padding-top:5px; padding-bottom:5px">' +
                        '<div class="row">' +
                        '<div class="col-sm-2">' +
                        '<p style="font-size:25px; margin-top:4px"><i class="fa fa-user-circle"></i></p>' +
                        '</div>' +
                        '<div class="col-sm-10">' +
                        '<p class="text-secondary" style="font-size:12px; text-transform:capitalize">' + item.username + '</p>' +
                        '<p class="text-secondary" style="font-size:12px">' + item.name + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</a>';

                    $('#participant-' + id).append(part_list);

                });

                $('#total_participant-' + id).html(
                    'PARTICIPANTS (' + total + ')'
                )
            }
        })
        
    }

    function fn_check_undifined(p) {
        if (p == 'undifined') {
            return '';
        } else {
            return p;
        }
    }

    // Server send event participan
    function fn_sse_incident_token(id) {

        var url = baseurl + "incident/socket/reload_token?id=" + id;
        $.ajax({
            url: url,
            dataType: 'JSON',
            async: true,
            success: function(data) {
                
                $('#token-' + id).empty();
                $('#token_id-' + id).empty();
                token = [];

                $.each(data, function(i, item) {
                    // view token 
                    var data_token = '<span><strong>' + fn_check_undifined(item.mention_to) + '&nbsp; </strong>' +
                        '<span style="display:block"><small>' + fn_check_undifined(item.username) + '</small></span></span>';
                    var input_token = '<input type="hidden" name="token[]" value="' + fn_check_undifined(item.mention_to) + '"/>';

                    $('#token-' + id).append(data_token);
                    $('#token_id-' + id).append(input_token);

                    token.push(item.mention_to);

                });
            }
        });
    }

    // Server send event load data incident by id 
    function fn_sse_status_incident(id) {

        if (typeof(EventSource) !== "undefined") {

            var url = baseurl + "incident/socket/load_incident?id=" + id;
            var source = new EventSource(url);

            source.onmessage = function(event) {
                var params = event.data;
                var json_data = JSON.parse(params);

                // alert(json_data.status_incident);
                $('#status-' + id).html('<strong>' + json_data.status.toUpperCase() + '&nbsp;&nbsp;<i class="fa fa-check-circle-o"></i></strong>');

                if (json_data.status == 'stop' || json_data.status == 'close') {
                    $('#form-' + id).css('display', 'none');
                } else {
                    $('#form-' + id).css('display', '');
                }

            };

        } else {
            document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
        }
        // end of server sent event 
    }

    // incident progress attach file upload 
    $('#attach').change(function(e) {
        var fileName = e.target.files[0].name;
        if (fileName != '') {
            $('.input-file').show();
            $('.input-file').html('<strong>' + fileName + '</strong> has been selected <button type="button" class="delete_file"><i class="fa fa-times"></i></button>');
        }
        // alert('The file "' + fileName +  '" has been selected.');
    });
    // end of incident file upload 

    // delete upload file incident 
    $('p').on('click', '.delete_file', function(e) {
        e.preventDefault();
        $('#attach').val("");
        $('.input-file').hide();
    });
    // end of delete


    // incident attach file upload
    $('#attach_incident_file').change(function(e) {
        var fileName = e.target.files[0].name;
        if (fileName != '') {
            $('.form_attach_file_incident').find("input[name='incident_file_text']").val(fileName);
        }
        // alert('The file "' + fileName +  '" has been selected.');
    });

    // button attach incident file click
    $('.btn-attach-incident').click(function(e) {
        e.preventDefault();

        var id_incident = $(this).attr('data-id');

        $.ajax({
            type: 'GET',
            url: baseurl + 'incident/socket/get_summary_incident?id=' + id_incident,
            dataType: 'JSON',
            async: true,
            success: function(data) {
                $('.incident-info').html(
                    '<p>' + data.incident_id + ' | ' + data.description + '</p>'
                );

                $('#modalAttachFileIncident').modal('show');

                fn_dt_incident_file_attachment(id_incident);

            }
        });

    });
    // end of 

    $('.btn-progress-summary').click(function(e) {
        e.preventDefault();
        var sum_process;
        var sum_process_detail;
        var html;
        var html_detail;
        // $('#modalProgressSummary').modal('show');
        $('#modalSummaryProgress').modal('show');

        // get summary progress active timeline 
        $.ajax({
            type: 'GET',
            url: baseurl + 'incident/socket/summary_progress_active/' + id,
            dataType: 'JSON',
            async: true,
            success: function(data) {
                // console.log(data.process_detail)

                // header timeline 
                $('#progressbar').empty();
                sum_process = data.process;
                $.each(sum_process, function(i, item) {
                    html = '<span class="btn_kpi_duration" data-value="' + item.process + '"><li class="text-center ' + item.status + '" id="' + item.process + '"><strong>' + item.description + '</strong><span class="kpi_duration">' + item.duration + '</span></li></span>';
                    $('#progressbar').append(html);
                });

                $('#progressbar').append(
                    '<span class="btn_kpi_duration" data-value="all"><li class="text-center btn_kpi_duration" id="all"><strong>All</strong><span class="kpi_duration"></span></li></span>'
                );


                //summary prosses detail
                $('.summary_progress_detail').empty();
                sum_process_detail = data.process_detail;
                $.each(sum_process_detail, function(i, item) {
                    html_detail = '<div class="card mt-1 shadow">' +
                        '<div class="card-body">' +
                        '<div class="row">' +
                        '<div class="col-lg-6"><strong>' + item.username.toUpperCase() + ' | ' + item.group_name.toUpperCase() + ' | ' + item.process_name.toUpperCase() + '</strong></div>' +
                        '<div class="col-lg-6"><small class="float-right">' + item.time_message + '</small></div>' +
                        '</div><p style="white-space:pre-wrap; font-size:11px">' +
                        item.message + ' ' + fn_check_lat_lng(item.lat, item.lng)
                    '</p></div>' +
                    '</div>"'

                    $('.summary_progress_detail').append(html_detail);
                });

            }
        });

    });


    $(document).on('click', '.btn_kpi_duration', function() {
        var process = $(this).attr('data-value');

        $.ajax({
            type: 'POST',
            url: baseurl + 'incident/socket/summary_progress_active_process/' + id + '/' + process,
            async: true,
            dataType: 'JSON',
            cache: false,
            success: function(data) {
                $('.summary_progress_detail').empty();
                $.each(data, function(i, item) {
                    html_detail = '<div class="card mt-2 shadow">' +
                        '<div class="card-body">' +
                        '<div class="row">' +
                        '<div class="col-lg-6"><strong>' + item.username.toUpperCase() + ' | ' + item.group_name.toUpperCase() + ' | ' + item.process_name.toUpperCase() + '</strong></div>' +
                        '<div class="col-lg-6"><small class="float-right">' + item.time_message + '</small></div>' +
                        '</div><p style="white-space:pre-wrap; font-size:11px">' +
                        item.message + '</p></div>' +
                        '</div>"'

                    $('.summary_progress_detail').append(html_detail);
                });

            }
        });

    });

    // incident file attachment 
    $('.form_attach_file_incident').submit(function(e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: baseurl + 'incident/socket/upload_file_incident',
            async: true,
            data: new FormData(this),
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                fn_dt_incident_file_attachment(data)
            }
        });
    });
    // end of 

    // incident file attachment 
    $(document).on('click', '.btn_remove_file_incident', function(e) {
        e.preventDefault();

        var id = $(this).attr("data-file"),
            incident_id = $(this).attr("data-incident_id");

        $.ajax({
            url: baseurl + 'incident/socket/remove_file_incident?id=' + id + '&incident_id=' + incident_id,
            type: 'GET',
            dataType: 'JSON',
            success: function(data) {
                fn_dt_incident_file_attachment(data)
            }
        });
    });
    // end of 

    // incident file attachment 
    function fn_dt_incident_file_attachment(id) {
        // DataTable list incident open
        $('#tb_incident_file_attach').DataTable({
            dom: 'flrtpi',
            lengthChange: false,
            searching: false,
            pageLength: 10,
            destroy: true,
            ajax: baseurl + 'incident/socket/get_file_attach_incident?id=' + id,
            columnDefs: [{
                    "targets": [0, 3], // your case first column
                    "className": "text-center",
                },
                {
                    "targets": "_all",
                    "createdCell": function(td, cellData, rowData, row, col) {
                        $(td).css('padding', '3px');
                    }
                },
                {
                    "targets": 3,
                    "width": "5%"
                }
            ],
            columns: [{
                    "data": "id",
                    "width": "4%",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "uploader", "wdith": "7%" },
                {
                    render: function(data, type, row, meta) {
                        return '<a class="text-dark" target="_blank" href="' + api_url + row.filename + '">' + row.filename + '</a>';
                    }
                },
                {
                    render: function(data, type, row, meta) {
                        return '<a class="btn btn-xs btn-info text-white shadow mr-2" target="_blank" title="Download File" href="' + api_url + row.filename + '"><i class="fa fa-download"></i></a>' +
                            '<a class="btn btn-xs btn-danger text-white shadow btn_remove_file_incident" data-toggle="tooltip" title="Remove File" href="#" data-file="' + row.id + '" data-incident_id ="' + id + '"><i class="fa fa-trash"></i></a>';
                    }
                },
                // { "data": "description" },
            ],
        });
    }

    // incident summary 
    $('.btn-incident-summary').click(function(e) {
        e.preventDefault();
        var incident_id = $(this).attr('data-incident');

        // axios get data incident by id 
        axios.get(baseurl + 'incident/socket/get_summary_incident?id=' + incident_id).then(function(response) {
                response = response.data;

                $("#mod_row_incidentId").val(response.incident_id);
                $("#mod_row_creator").val(response.creator);
                $("#mod_row_notificationTrigger").val(response.notification_trigger);
                $("#mod_row_state").val(response.status);
                $("#mod_row_clasification").val(response.classification_name);
                $("#mod_row_rootCause").val(response.root_cause_name);
                $("#mod_row_rootCauseDetail").val(response.root_cause_detail_name);
                $("#mod_row_description").val(response.description);
                $("#mod_row_hirarchy").val(response.hierarchy);
                $("#mod_row_link").val(response.link);
                $("#mod_row_link_start_region").val(response.link_start_region);
                $("#mod_row_link_end_region").val(response.link_end_region);
                $("#mod_row_link_start_site").val(response.link_start_site);
                $("#mod_row_link_end_site").val(response.link_end_site);
                $("#mod_row_region").val(response.region_name);
                $("#mod_row_site").val(response.site);
                $("#mod_source_category").val(response.source_category);
                $("#mod_row_creationTime").val(response.creation_time);
                $("#mod_row_duration").val(fn_time(response.duration));
                $("#mod_row_stopLock").val(fn_time(response.total_stop_clock));
                $("#mod_row_ttr").val(fn_time(response.ttr));

                $("#modalSummary").modal("show");

            })
            .catch(function(error) {
                console.log(error)
            });
    });


    // function convert numeric to time 
    function fn_time(number) {

        var sisa_waktu = number % 3600;
        var h = Math.floor(number / 3600);
        var m = Math.floor(sisa_waktu / 60);
        var s = Math.floor(sisa_waktu % 60);
        var hasil = get_puluhan(h) + ':' + get_puluhan(m) + ':' + get_puluhan(s);
        if (number == 0 || number == null) {
            var data = '00:00:00';
        } else {
            var data = hasil;
        }
        return data;

    }

    $('.btn-download').click(function(e) {
        e.preventDefault();
        window.open(baseurl + 'incident/socket/get_pdf?id=' + id);
    })

    // get message maps
    function fn_message_maps(lat, lng) {

        $('.header-maps').html('Coodinate Location');

        $('#lat').val(lat);
        $('#long').val(lng);

        var myLatLng = {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
        };
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: myLatLng
        });
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            animation: google.maps.Animation.DROP,
            title: 'coordinate location',
        });

    }

    function fn_progress_maps(id) {
        $('.btn-serpo-location').click(function(e) {
            e.preventDefault();

            $.ajax({
                url: baseurl + 'incident/socket/get_maps?id=' + id,
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    $.each(data, function(i, item) {
                        $('#lat').val(item.latitude);
                        $('#long').val(item.longitude);

                        var test = {
                            lat: -6.21462,
                            lng: 106.84513
                        };
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 10,
                            center: test
                        });
                        var myLatLng = {
                            lat: parseFloat(item.latitude),
                            lng: parseFloat(item.longitude)
                        };
                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            animation: google.maps.Animation.DROP,
                            title: 'serpo location',
                        });
                    })

                }
            })
        })
    }


    // display request stop clock modal 
    $('.btn-request-stopclock').click(function(e) {
        e.preventDefault();

        $('#modalRequestStopClock').modal('show');

        t_request_stopclock.ajax.reload();
    });


    function fn_show_alert(pesan) {
        $('#modalerror').modal('show');
        $('.content_alert').html(pesan);
    }

    $("#myProgress").keyup(function() {
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(),
            count = 0;

        // Loop through the comment list
        $(".scroll-incident-progress").find('.messages').each(function() {
            // If the list item does not contain the text phrase fade it out
            // if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            if ($(this).data('messages').search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });

    });

    // integration autocheck 
    $('.btn-autocheck').click(function(e) {
        e.preventDefault();

        $('#modalautocheck').modal('show');

        var sid = $('.sid').attr('data-sid');

        var frameElement = frameElement = document.getElementById("frameAutocheck");
        frameElement.src = 'http://10.14.22.210:96/api/cm/autocheck/' + sid;

    });

    // auto height textarea 
    var textarea = document.querySelector('textarea');

    textarea.addEventListener('keydown', autosize);

    function autosize() {
        var el = this;
        setTimeout(function() {
            el.style.cssText = 'height:auto; padding:0';
            el.style.cssText = 'height:' + el.scrollHeight + 'px';
        }, 0);
    }

    $('.btn-log').click(function(e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        $('#modallog').modal('show');

        var t_log = $('#tb_log').DataTable({
            dom: 'flrtpi Rl',
            responsive: true,
            lengthChange: false,
            pageLength: 10,
            destroy: true,
            ajax: baseurl + 'incident/log/list_log_incident?id=' + id,
            columnDefs: [{
                    "targets": [0], // your case first column
                    "className": "text-center",
                    "width": "25px",
                },
                {
                    "targets": [1], // your case first column
                    "width": "50px",
                },
                {
                    "targets": [4, 2], // your case first column
                    "width": "100px",
                },
                {
                    "targets": '_all',
                    "createdCell": function(td, cellData, rowData, row, col) {
                        $(td).css('padding', '3px');
                        $(td).attr('data-toggle', 'tooltip');
                        $(td).attr('title', cellData);
                    }
                }
            ],
            columns: [{
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "username" },
                { "data": "group_code" },
                { "data": "action" },
                { "data": "created_on" },
            ]
        });

    });


    // progress 
    $('.btn-progress').click(function(e) {
        e.preventDefault();
        var incident_merge = $(this).attr('data-incident');
        var ticket_id = $(this).attr('data-ticket_id');

        $.ajax({
            type: 'POST',
            url: baseurl + 'incident/merge/get_merge_incident_by_ticket',
            data: { ticket_id: ticket_id },
            dataType: 'JSON',
            async: true,
            success: function(data) {
                console.log(data)

                if (data == null) {
                    pesan = "There's no history incident";
                    fn_show_alert(pesan);
                    return false;
                } else {
                    var url = baseurl + 'incident/socket/progress/' + data.id;
                    window.open(url);
                }

            }

        });

    });


    // select filter
    $('.select-filter').on('change', function(e) {
        e.preventDefault();

        type_filter = this.value;

        fn_list_incident()

    });

    // select role process 
    $.ajax({
        url: baseurl + 'group/role_process?group_code=' + groupcode,
        type: 'POST',
        dataType: 'JSON',
        async: true,
        success: function(data) {
            var html;
            console.log(data);
            $('.select-process').empty()
            $('.select-process').append("<option value='none' selected>None</option>")
            $.each(data, function(i, item) {
                html = "<option value=" + item.process + ">" + item.description + "</option>";
                $('.select-process').append(html)
            })
        }
    });


    // display modal serpo location
    $('.btn-serpo-location').click(function(e) {
        e.preventDefault();

        $('#modalSerpoLocation').modal('show');

    });

    // setInterval(function(){ console.log("tes") }, 1000);
    // setInterval(function(){ fn_sse_kpi_incident }, 2000);
    // setInterval(function(){ fn_sse_kpi_incident();  }, 10000);
    
    // display progress bar kpi incident 
    function fn_sse_kpi_incident(id) {

        var url = baseurl + "incident/socket/get_kpi_incident?id=" + id;
        $.ajax({
            url: url,
            dataType: 'JSON',
            async: true,
            success: function(data) {
                var json_data = data;
                var analisis = json_data.analisis;
                var recovery = json_data.recovery;
                var confirmation = json_data.confirmation_time;
                var arr_color = ['bg-success', 'bg-warning', 'bg-info', 'bg-completed', 'bg-danger'];
                var analisis_progress;
                var recovery_progress;
                var confirmation_progress;
                var no_analisis = 0;
                var no_recovery = 0;
                var no_confirmation = 0;

                $('#response_time_new-' + id).empty();
                $('#analisis_time_new-' + id).empty();
                $('#recovery_time_new-' + id).empty();
                $('#mttr_time_new-' + id).empty();
                $('#mttr-' + id).empty();
                $('#confirmation_time_new-' + id).empty();


                // response time 
                $('#response_time_new-' + id).html(
                    '<div data-toggle="tooltip" data-placement="bottom" title="' + json_data.response.response + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (json_data.response.response_int / 10) * 100 + '%" aria-valuenow="' + json_data.response.response_int + '" aria-valuemin="0" aria-valuemax="10">' +
                    '<strong><span class="text-white mr-2 ml-2">' + json_data.response.response + '</span></strong>' +
                    '</div>'
                );

                // analisis time 
                $.each(analisis, function(i, item) {

                    if (no_analisis !== json_data.total_analisis) {
                        analisis_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[no_analisis] + '" role="progressbar" style="width: ' + (item.to_duration_int / 10) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                            '<strong><span class="text-white mr-2 ml-2">' + item.initial + '</span></strong>' +
                            '</div>'
                        $('#analisis_time_new-' + id).append(analisis_progress);
                    }

                    no_analisis = no_analisis + 1;
                });


                // recovery time 
                $.each(recovery, function(i, item) {

                    if (no_recovery !== json_data.total_recovery) {
                        recovery_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[no_recovery] + '" role="progressbar" style="width: ' + (item.to_duration_int / 120) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                            '<strong><span class="text-white mr-2 ml-2">' + item.to_duration + '</span></strong>' +
                            '</div>'
                        $('#recovery_time_new-' + id).append(recovery_progress);
                    }

                    no_recovery = no_recovery + 1;
                });

                // MTTR time                 
                $('#mttr_time_new-' + id).html(
                    '<div data-toggle="tooltip" data-placement="bottom" title="' + json_data.mttr.to_duration + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (json_data.mttr.to_duration_int / 240) * 100 + '%" aria-valuenow="' + json_data.mttr.to_duration_int + '" aria-valuemin="0" aria-valuemax="240">' +
                    '<strong><span class="text-white mr-2 ml-2">' + json_data.mttr.to_duration + '</span></strong>' +
                    '</div>'
                );

                // MTTR time 
                $('#mttr-' + id).html("<strong><span style='font-size:17px'>" + json_data.mttr.to_duration + "</span></strong>");

                // Confirmation time
                $.each(confirmation, function(i, item) {
                    confirmation_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (item.to_duration_int / 10) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                        '<strong><span class="text-white mr-2 ml-2">' + item.to_duration + '</span></strong>' +
                        '</div>'
                    $('#confirmation_time_new-' + id).append(confirmation_progress);
                });
            }
        })

        return false;
        if (typeof(EventSource) !== "undefined") {

            var url = baseurl + "incident/get_kpi_incident?id=" + id;
            var source = new EventSource(url);

            source.onmessage = function(event) {
                var params = event.data;
                var json_data = JSON.parse(params);
                var analisis = json_data.analisis;
                var recovery = json_data.recovery;
                var confirmation = json_data.confirmation_time;
                var arr_color = ['bg-success', 'bg-warning', 'bg-info', 'bg-completed', 'bg-danger'];
                var analisis_progress;
                var recovery_progress;
                var confirmation_progress;
                var no_analisis = 0;
                var no_recovery = 0;
                var no_confirmation = 0;

                $('#response_time_new-' + id).empty();
                $('#analisis_time_new-' + id).empty();
                $('#recovery_time_new-' + id).empty();
                $('#mttr_time_new-' + id).empty();
                $('#mttr-' + id).empty();
                $('#confirmation_time_new-' + id).empty();


                // response time 
                $('#response_time_new-' + id).html(
                    '<div data-toggle="tooltip" data-placement="bottom" title="' + json_data.response.response + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (json_data.response.response_int / 10) * 100 + '%" aria-valuenow="' + json_data.response.response_int + '" aria-valuemin="0" aria-valuemax="10">' +
                    '<strong><span class="text-white mr-2 ml-2">' + json_data.response.response + '</span></strong>' +
                    '</div>'
                );

                // analisis time 
                $.each(analisis, function(i, item) {

                    if (no_analisis !== json_data.total_analisis) {
                        analisis_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[no_analisis] + '" role="progressbar" style="width: ' + (item.to_duration_int / 10) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                            '<strong><span class="text-white mr-2 ml-2">' + item.initial + '</span></strong>' +
                            '</div>'
                        $('#analisis_time_new-' + id).append(analisis_progress);
                    }

                    no_analisis = no_analisis + 1;
                });


                // recovery time 
                $.each(recovery, function(i, item) {

                    if (no_recovery !== json_data.total_recovery) {
                        recovery_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[no_recovery] + '" role="progressbar" style="width: ' + (item.to_duration_int / 120) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                            '<strong><span class="text-white mr-2 ml-2">' + item.to_duration + '</span></strong>' +
                            '</div>'
                        $('#recovery_time_new-' + id).append(recovery_progress);
                    }

                    no_recovery = no_recovery + 1;
                });

                // MTTR time 
                $('#mttr_time_new-' + id).html(
                    '<div data-toggle="tooltip" data-placement="bottom" title="' + json_data.mttr.to_duration + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (json_data.mttr.to_duration_int / 240) * 100 + '%" aria-valuenow="' + json_data.mttr.to_duration_int + '" aria-valuemin="0" aria-valuemax="240">' +
                    '<strong><span class="text-white mr-2 ml-2">' + json_data.mttr.to_duration + '</span></strong>' +
                    '</div>'
                );

                // MTTR time 
                $('#mttr-' + id).html("<strong><span style='font-size:17px'>" + json_data.mttr.to_duration + "</span></strong>");

                // Confirmation time
                $.each(confirmation, function(i, item) {
                    confirmation_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (item.to_duration_int / 10) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                        '<strong><span class="text-white mr-2 ml-2">' + item.to_duration + '</span></strong>' +
                        '</div>'
                    $('#confirmation_time_new-' + id).append(confirmation_progress);
                });


            };

        } else {
            document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
        }
    }


    // get list incident 
    function fn_list_incident() {

        start = 0;

        $.ajax({
            url: baseurl + 'incident/socket/get_list_incident?start=' + start + '&limit=' + 10 + '&filter=' + type_filter,
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                $('.scroll-incident-list').empty();

                var raw_data = data.data;

                $.each(raw_data, function(i, item) {

                    var l_incident = '<a href="#" class="list-group-item list-group-item-action flex-column align-items-start btn_list mt-1 ' + fn_get_active(item.id, id, item.status, item.ttr) + '" data-status="' + item.status + '" data-id="' + item.id + '" data-toggle="tooltip" title="' + item.description + '" data-ttr="' + item.ttr + '">' +
                        '<div class="d-flex w-100 justify-content-between">' +
                        '<small class="mb-1 text-secondary">' + item.incident_id + '</small>' +
                        '</div>' +
                        '<p class="mb-1 incident-desc">' + item.description.substr(0, 50) + '...</p>' +
                        '<small class="text-secondary">' + item.open_time + '</small>' +
                        '</a>'

                    $('.scroll-incident-list').append(l_incident);

                });

                $('.count_incident').html(
                    'INCIDENT LIST (' + data.total + ')'
                );

            }
        })
    }


    // get list incident after scroll 
    $('.scroll-incident-list').on('scroll', function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {

            start = start + 10;

            $.ajax({
                url: baseurl + 'incident/socket/get_list_incident?start=' + start + '&limit=' + 10 + '&filter=' + type_filter,
                type: 'GET',
                dataType: 'JSON',
                async: true,
                success: function(data) {
                    var raw_data = data.data;

                    $.each(raw_data, function(i, item) {

                        var l_incident = '<a href="#" class="list-group-item list-group-item-action flex-column align-items-start btn_list mt-1 ' + fn_get_active(item.id, id, item.status, item.ttr) + '" data-status="' + item.status + '" data-id="' + item.id + '" data-toggle="tooltip" title="' + item.description + '" data-ttr="' + item.ttr + '">' +
                            '<div class="d-flex w-100 justify-content-between">' +
                            '<small class="mb-1 text-secondary">' + item.incident_id + '</small>' +
                            '</div>' +
                            '<p class="mb-1 incident-desc">' + item.description.substr(0, 50) + '...</p>' +
                            '<small class="text-secondary">' + item.open_time + '</small>' +
                            '</a>'

                        $('.scroll-incident-list').append(l_incident);


                    });

                    $('.count_incident').html(
                        'INCIDENT LIST (' + data.total + ')'
                    );
                }
            })

        }
    });

}); // end of document ready function 


// show filter incident 
function filterIncident() {
    var input, filter, table, tr, td, i, txtValue;

    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByClassName("btn_list");

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByClassName("mb-1")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;

            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }

        }
    }
}
// end of filter incident

// add info message
$('#process').on('change', function() {

    var val_exst = $('#created_message').val();

    if (this.value == 'analyst') {
        $('#created_message').val(`
- Hasil pengecekan ke arah lastmile*:
- Hasil pengecekan ke Backbone*:
- Indikasi Gangguan*:
- Solusi*:
- Plan progres*: 
- kendala:
Capture hasil pengecekan*:`);
    } else if (this.value == 'order_team') {
        $('#created_message').val(`
- Hasil pengecekan: Cek Mac Addres tidak terdeteksi, traffic tidak terdeteksi, (bisa look up dari Analisa)
- Indikasi Gangguan: Kabel FO/ Patchchord putus (bisa look up dari Analisa)
- Request team: Mohon di kirim tim Kabel untuk melakukan pengecekan di lastmile
- Tujuan: 1. Lastmile
          2. POP Cawang (jika lastmile sudah tutup)`);
    } else if (this.value == 'team_berangkat') {
        $('#created_message').val(`
- Progess: Tim sedang menuju lastmile pelanggan BSM Cawang
- Dari Serpo: Duren tiga
- Perkiraan Estimasi sampai lokasi : kurang lebih 2 jam
- Kendala : tidak ada
- Solusi : - `);
    } else if (this.value == 'recovery') {
        $('#created_message').val(`
Progres:
hasil pengukuran:
jenis kabel fo: (combo: Fig8, FA(fiber armor), ADSS)
putus di jarak:
dari jarak total:
mengukur dari: (lastmile/POP(sebutkan nama POP nya) 
ke arah: (lastmile/POP(sebutkan nama POP nya)
kendala:
Solusi: `);
    } else if (this.value == 'confirm') {
        $('#created_message').val(val_exst + `
- Penyebab Gangguan : (look up dari hasil pengecekan noc)
- Solusi/perbaikan : (look up dari hasil pengecekan noc)
- Kategori gangguan : (combo: gangguan/keluhan)
- link normal pukul : 
Capture hasil pengecekan terakhir:
      
Hasil konfirmasi ke pelanggan :
Step to : - Ticket Closed (jika sudah normal)=>closed incident(jika semua ticket terkait sudah closed)
          - Back to analisa (jika hasil konfirmasi ke pelanggan, link masih down/gangguan). `);
    } else {
        $('#created_message').val(null);
    }
});