// jquery start
var option;
var status_incident;
var incident_id;
var select_incident = [];
$(document).ready(function() {

    // menu active
    activeClass('incident', 'open-incident');

    var progress_url = baseurl + "incident/progress/";
    var username = $('.info_login').data('username');
    var usergroup = $('.info_login').data('usergroup');

    $.fn.dataTable.ext.errMode = 'none';

    $('#select_user').select2({
        allowClear: true,
    });

    $('.select2').select2({
        allowClear: true,
    });

    $('[data-toggle="tooltip"]').tooltip();

    // setInterval(function () {
    //     tb_incident.ajax.reload(null, false);
    //     tb_incident_filter.ajax.reload(null, false);
    // }, 30000);

    $('[id=datepicker-component]').datepicker({
        format: 'yyyy-mm-dd',
    });

    // select2 classification 
    $("#select_root_cause").remoteChained({
        parents: "#select_classification",
        url: baseurl + "ticket/load_root_cause"
    });

    $("#select_root_cause_detail").remoteChained({
        parents: "#select_root_cause",
        url: baseurl + "ticket/load_root_cause_detail"
    });

    $("#site_name1").remoteChained({
        parents: "#region1",
        url: baseurl + "ticket/load_incident_regions1"
    });

    $("#link_start_site").remoteChained({
        parents: "#link_start_region",
        url: baseurl + "ticket/load_incident_regions_start"
    });

    $("#link_end_site").remoteChained({
        parents: "#link_end_region",
        url: baseurl + "ticket/load_incident_regions_end"
    });

    $("#brand1").remoteChained({
        parents: "#site_name1",
        url: baseurl + "ticket/load_brands"
    });

    $("#me1").remoteChained({
        parents: "#brand1",
        url: baseurl + "ticket/load_me"
    });


    // func show and hide in classification of define incidents
    $('#select_classification').on('change', function() {
        if (this.value) {

            $('#region1').val(null).trigger('change');
            $('#link_start_region').val(null).trigger('change');
            $('#link_end_region').val(null).trigger('change');
            $('#site_name1').empty();
            $('#brand1').empty();
            $('#me1').empty();
            $('#link_start_site').empty();
            $('#link_end_site').empty();

        }

        if (this.value == '151' || this.value == '121' || this.value == '171' || this.value == '161' || this.value == '661' || this.value == '131' || this.value == '411' || this.value == '666') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOT")
            $('#source_category_input').val("FOT")
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }
        if (this.value == '251' || this.value == '241' || this.value == '141') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("PS")
            $('#source_category_input').val("PS")
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }
        if (this.value == '111') {
            $('#1').hide()
            $('#3').show()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOC")
            $('#source_category_input').val("FOC")
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }
        if (this.value == '381' || this.value == '361' || this.value == '888') {
            $('#1').hide()
            $('#3').hide()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').hide()
            $('#div_link_upstream_isp').show()
            $('#div_region').show()
            $('#div_site_name').show()
        }


        if (this.value == '999') {
            $('#1').hide()
            $('#3').hide()
            $('#11').hide()
            $('#foc-show').hide()
                // $('#foc-hide').show()
            $('#source_category').hide()
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }
        if (this.value == '221') {
            $('#1').hide()
            $('#3').show()
            $('#11').show()
            $('#foc-show').show()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOC")
            $('#source_category_input').val("FOC")
            $('#div_link_upstream_isp').hide()
            $('#div_region').hide()
            $('#div_site_name').hide()
        }
    });


    $('#link_upstream_isp').on('change', function() {

        if (this.value) {
            $('#region1').val(null).trigger('change');
            $('#region').val(null).trigger('change');
            $('#link_start_region').val(null).trigger('change');
            $('#link_end_region').val(null).trigger('change');
            $('#site_name1').empty();
            $('#site_name').empty();
            $('#brand').empty();
            $('#me').empty();
            $('#link_start_site').empty();
            $('#link_end_site').empty();
        }

        // kondisi jika pilih link upstream ISP bagian source category 
        if (this.value == '1') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("FOT")
            $('#foc-hide').show()
            $('#foc-show').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }

        if (this.value == '2') {
            $('#1').hide()
            $('#3').show()
            $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("FOC")
            $('#foc-hide').hide()
            $('#foc-show').show()
            $('#div_region').hide()
            $('#div_site_name').hide()
        }

        if (this.value == '3') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("PS")
            $('#foc-hide').show()
            $('#foc-show').hide()
            $('#div_region').show()
            $('#div_site_name').show()
        }


    });


    // untuk menyembunyikan status lock by pada table incident 
    function show_unlock(param) {

        if (param === "" || param === null) {
            // jika lock_by kosong 
            return "none";
        } else {
            // jika lock_by isi
            return "";
        }

    }


    // check priority
    function show_priority(param3) {

        if (param3 == 'low') {

            return '<span data-toggle="tooltip" title="low priority"><strong><i class="fa fa-warning" style="color:green"></strong></span>';
        } else if (param3 == 'medium') {

            return '<span data-toggle="tooltip" title="Medium Priority"><strong><i class="fa fa-warning" style="color:yellow"></strong></span>';
        } else if (param3 == 'high') {

            return '<span data-toggle="tooltip" title="High Priority"><strong><i class="fa fa-warning" style="color:red"></strong></span>';
        } else {

            return '';
        }

    }


    function show_assign(lock_by) {

        if (lock_by === "" || lock_by === null) {

            return '';
        } else {

            return 'none';
        }

    }


    // untuk show hide attch ticket 
    function show_total_ticket(param2) {

        if (param2 == 0) {

            return '';
        } else {

            return param2;
        }
    }


    function get_last_coment(param1, param2) {
        // var str = param.split('|');
        // var last_comment = str[0];
        // var last_comment_status = str[1];

        // alert(last_comment_status)

        if (param2 == 1) {
            // alert('one')
            return '<span style="color:red">' + param1 + '</span>';
        } else {
            // alert('not one')
            return '<span style="color:green" data-toggle="tooltip" title=' + param1 + '>' + param1 + '</span>';
        }


    }

    var arr_type = ["NOC", "CONTACTCENTER", "SBU", "SERPO"];
    if (jQuery.inArray(member, arr_type) !== -1) {
        option = 'all';
    } else {
        option = 'token';
    }

    // DataTable list incident open
    var tb_incident = $('#incident_open').DataTable({
        dom: 'B<"toolbar-left">frtpi Rl',
        stateSave: true,
        scrollX: true,
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
            {
                init: function(api, node, config) {
                    $(node).removeClass('dt-button')
                    $(node).removeClass('dt-button > button').css('display', 'none')
                }
            }
        ],
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        processing: true, //Feature control the processing indicator.
        serverSide: false, //Feature control DataTables' server-side processing mode.
        ajax: {
            "url": baseurl + 'incident/list_open_incident',
            "type": "POST",
            "data": function(d) {
                d.type = option;
                d.select = select_incident.join(',');
            }
        },
        columnDefs: [{
                "width": "15%",
                "targets": 15,
                "className": "text-center",
                "bSortable": true,
            },{
                "searchable": false,
                "orderable": false,
                "targets": 0
            },
            {
                "width": "17%",
                "targets": 4,
                "bSortable": true,
            },
            {
                "width": "7%",
                "targets": [2, 3, 6, 7],
                "bSortable": true,
            },
            {
                "width": "3%",
                "targets": [0, 1, 8, 9, 10, 11, 12, 13, 14]
            },
            {
                "targets": [0, 7, 8, 9, 11], // your case first column
                "className": "text-center",
            },
            {
                "targets": "_all",
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '2px');
                    $(td).attr('data-toggle', 'tooltip');
                    $(td).attr('title', cellData);
                    // $('td', row).eq(14).addClass('highlight');
                }
            },
            {
                "targets": [1, 2, 5, 9],
                "visible": false,
                "searchable": true,
                "bSortable": true,
            },
            // {
            //     "targets": [5],
            //     "visible": false,
            //     "searchable": true,
            // }
        ],
        columns: [{
                "data": "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "id" },
            // { "data": "incident_id" },
            {
                render: function(data, type, row, meta) {
                    return row.incident_id.toUpperCase()
                }
            },
            { "data": "start_time" },
            { "data": "description" },
            { "data": "assign_by" },
            { "data": "mention_to" },
            { "data": "lock_by" },
            {
                render: function(data, type, row, meta) {
                    return show_priority(row.priority)
                }
            },
            { "data": "region_code" },
            {
                render: function(data, type, row, meta) {
                    return row.status.toUpperCase()
                }
            },
            {
                render: function(data, type, row, meta) {
                    return show_total_ticket(row.total_ticket)
                }
            },
            { "data": "analyse_time_" },
            { "data": "recovery_time_" },
            // { "data": "last_comment" },
            {
                render: function(data, type, row, meta) {
                    return get_last_coment(row.last_comment, row.last_comment_status);
                }
            },
            {
                render: function(data, type, row, meta) {
                    return "<button class='btn btn-xs progress-color text-white ml-1 btn-incident-progress' data-id=" + row.id + " data-lockby=" + row.lock_by + " data-toggle='tooltip' title='Incident Progress'>Progress</button>" +
                        "<button class='btn btn-xs btn-warning ml-1 text-dark btn-assign' data-toggle='tooltip' title='Assign Incident' data-id='" + row.id + "' data-creator=" + row.creator + " data-description='" + row.description + "' data-incident_id=" + row.incident_id + " style='display:" + show_assign(row.lock_by) + "'>Assign</button>" +
                        // "<button class='btn btn-xs btn-danger ml-1 btn-unlock' data-toggle='tooltip' title='Lock Incident' data-id=" + row.id + "  data-lockby=" + row.lock_by + " data-username=" + row.username + " style='display:" + show_unlock(row.lock_by) + "'>Unlock</button>" +
                        "<button class='btn btn-xs btn-danger ml-1 btn-unlock' data-toggle='tooltip' title='Lock Incident' data-id=" + row.id + "  data-lockby=" + row.lock_by + " style='display:" + show_unlock(row.lock_by) + "'>Unlock</button>" +
                        // "<button class='btn btn-xs close-color ml-1 text-white btn-close' data-toggle='tooltip' title='Close Incident' data-stop_clock='" + row.last_stop_clock + "' data-id=" + row.id + ">Close</button>" +
                        "<button class='btn btn-xs close-color ml-1 text-white btn-close' data-toggle='tooltip' title='Close Incident'  data-id=" + row.id + ">Close</button>" +
                        "<button class='btn btn-xs manage-color ml-1 text-white btn-manage' data-toggle='tooltip' title='Manage Incident' data-id=" + row.id + " data-lockby=" + row.lock_by + ">Manage</button>"
                }
            },
        ],
        rowCallback: function(row, data, index) {
            // first td
            $('td:first', row).css('border-left', '10px solid ' + set_color(data.status, data.ttr));

            if (data.mention_to === getCookie('group_code') && data.status === 'open') {
                $('td', row).css('background-color', '#fedfb0');
            }

        }
    });
    // end of list incident

    tb_incident.on('order.dt search.dt', function() {
        tb_incident.column(0, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();


    $("#incident_open").on('click', '.btn-incident-progress', function(e) {
        e.preventDefault();

        var id = $(this).attr('data-id'),
            lock_by = $(this).attr('data-lockby');

        var p_param = { id: id, lock_user: lock_by },
            p_url = baseurl + 'incident/unlock_incident';

        if (lock_by == null || lock_by === 'null') {
            // update lock by 
            $.ajax({
                type: 'POST',
                data: p_param,
                url: p_url,
                async: true,
                success: function(data) {
                    // reload datatables 
                    tb_incident.ajax.reload(null, false);
                    // window.location.replace(baseurl + 'incident/progress/' + id);
                    var redirect = window.open(baseurl + 'incident/progress/' + id, "_blank");
                    redirect.location;

                }

            });

        } else {

            // load incident progress page 
            // window.location.replace(baseurl + 'incident/progress/' + id);
            var redirect = window.open(baseurl + 'incident/progress/' + id, "_blank");
            redirect.location;
        }

    });

    // toolbar 
    // var arr_type = ["NOC", "CONTACTCENTER"];
    // if (jQuery.inArray(member, arr_type) !== -1) {
    $("div.toolbar-left").html('&nbsp;' +
        '<select class="select-type" style="height: 33px;width: 150px;">' +
        '<option value="all" selected>All</option>' +
        '<option value="assign">My Incident</option>' +
        '<option value="token">My Token</option>' +
        '</select>&nbsp;' +
        '<button class="btn btn-sm btn-filter">Filter</btn>');

    $('.select-type').change(function(e) {
        e.preventDefault();
        option = this.value;

        select_incident = [];
        tb_incident.state.clear();

        tb_incident.ajax.reload(null, true);

    });

    // declare variable option select
    var option = $('.select-type').children("option:selected").val();

    function set_color(status, ttr) {
        // cek status incident 
        if (status === 'clear') { // ready close
            return '#4CAF50'; // green color
        } else {
            if (ttr > 14400) { // over ttr
                return '#FF6362'; // red color 
            } else {
                if (status === 'stop') { // stop
                    return '#FFEB3B'; // yellow
                } else {
                    return '#B0BEC5'; // grey of normal color
                }
            }
        }
    }

    // tabel merge incident 
    var table_merge = $('#merge_incident').DataTable({
        // dom: 'flrtpi',
        dom: 'flrtpi Rl',
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },

        ],
        responsive: true,
        lengthChange: false,
        pageLength: 10,
        destroy: true,
        processing: true, //Feature control the processing indicator.
        serverSide: true, //Feature control DataTables' server-side processing mode.
        ajax: {
            "url": baseurl + 'incident/list_merge_incident',
            "type": "POST",
            "data": function(d) {
                d.type = option;
            }
        },
        columnDefs: [{
                "targets": [0], // your case first column
                "className": "text-center",
            },
            {
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '5px');
                    $(td).attr('data-toggle', 'tooltip');
                    $(td).attr('title', cellData);
                }
            },
        ],
        columns: [{
                "width": "5%",
                "data": "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "start_time" },
            { "data": "creator" },
            { "data": "description" },
            // {
            //     render: function(data, type, row, meta) {
            //         return show_total_ticket(row.total_ticket);
            //     }
            // },
            { "data": "incident_id", "width": "50%" },
        ],
    });

    // create selected tr merge incident     
    $('#merge_incident tbody').on('click', 'tr', function() {

        if ($(this).hasClass('selected')) {
            // jika diselect
            $(this).removeClass('selected');

        } else {
            // jika remove selected
            table_merge.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

        }

        $('#button').click(function() {
            table_merge.row('.selected').remove().draw(false);
        });

    });
    // end of merge incident 

    // save merge incident click
    $('.btn_merge').click(function(e) {
        e.preventDefault();

        stream.close();

        $('#merge_incident > tbody').each(function(index) {
            var incident_select = $(this).find('.selected > td:last').text();
            var incident_current = $('.incident_id').val();
            var merge_reason = $('.select-merge-reason').val();

            if (merge_reason == '' || merge_reason == null) {
                var pesan = 'Merge Reason cannot Empty !!';
                // fn_show_alert(pesan)
                alert(pesan);
                return false;
            }

            if (incident_select == '' || incident_select == null) {
                var pesan = 'Merge Reason cannot Empty !!';
                // fn_show_alert(pesan)
                alert(pesan);
                return false;
            }

            if (incident_select === incident_current) {
                alert('Cannot Merge This Incident');
                return false;
            }

            $.ajax({
                type: 'POST',
                data: {
                    id: incident_current,
                    id_select: incident_select,
                    merge_reason: merge_reason
                },
                dataType: 'JSON',
                url: baseurl + 'incident/merge_incident',
                success: function(data) {
                    fn_userdata()
                        // option = $('.select-type').children("option:selected").val();

                    fn_show_alert(data);
                    $('#modalMergeIncident').modal('hide');
                    $('#modalLogDetail').modal('hide');
                    tb_incident.ajax.reload(null, false);
                    // get_list_merge_incident(option)

                    // alert()

                }
            });

        });

    });

    // double click tbody datatables 
    $('#incident_open tbody').on('click', 'tr .btn-manage', function(e) {
        e.preventDefault();
        // **** declare variable ****
        var incident = $(this).attr('data-id'),
            lockby = $(this).attr('data-lockby');

        // memanggil fungsi check incident)
        check_incident(incident, lockby);

    });

    // double click tbody datatables 
    $('#incident_open tbody').on('dblclick', 'tr', function(e) {
        e.preventDefault();
        // **** declare variable ****

        var data = tb_incident.row(this).data();
        var incident_id = data.id;
        var lock_by = data.lock_by;
        var p_param = { id: incident_id, lock_user: lock_by },
            p_url = baseurl + 'incident/unlock_incident';

        // check null lock by
        if (lock_by == null || lock_by === '') {

            // update lock by 
            $.ajax({
                type: 'POST',
                data: p_param,
                url: p_url,
                async: true,
                success: function(data) {
                    // reload datatables 
                    tb_incident.ajax.reload(null, false);
                    window.location.replace(baseurl + 'incident/progress/' + incident_id);
                }

            });

        } else {
            // load incident progress page
            window.location.replace(baseurl + 'incident/progress/' + incident_id);
        }

    });

    // click btn-unlock datatables 
    $('#incident_open tbody').on('click', 'tr .btn-unlock', function(e) {
        e.preventDefault();
        // **** declare variable ****
        var id = $(this).data('id'),
            lock_by = $(this).data('lockby');

        // if (user_checkin === "0" || typeof user_checkin === "undefined" || user_checkin == 0) {
        //     $('.text-confirm').html("Please Check in ");
        //     $('#modalUnlockFail').modal('show');
        //     return false;
        // }
        if (status_checkin === 0 || status_checkin === "0" || typeof status_checkin === "undefined") {
            pesan = "Please Check in ";
            fn_show_alert(pesan);
            return false;
        }

        if (user_mod === "0" || typeof user_checkin === "undefined" || user_checkin == 0) {

            if (lock_by !== username) {

                $('#modalUnlockFail').modal('show');
            } else {

                $('#modalUnlock').modal('show');
                $('.form_confirm').find("input[name='id']").val(id);
                $('.form_confirm').find("input[name='lock_by']").val(lock_by);
            }

        } else {

            $('#modalUnlock').modal('show');
            $('.form_confirm').find("input[name='id']").val(id);
            $('.form_confirm').find("input[name='lock_by']").val(lock_by);
            // $('.form_confirm').find("input[name='option']").val(option);
        }

    });

    // check inside or outside plan 
    $('.form_close').find("input[name='physical_inv_change1']").click(function() {
        if ($(this).prop("checked") == true) {
            var inv = $(this).val();

            if ($('.form_close').find("input[name='physical_inv_change2']").prop("checked") == true) {
                $('.form_close').find("input[name='physical_inv_change']").val(3);
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
            }

            $('.inv_change_desc').show();
        } else if ($(this).prop("checked") == false) {

            if ($('.form_close').find("input[name='physical_inv_change2']").prop("checked") == false) {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
                $('.inv_change_desc').hide();
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(2);
            }
        }
    });

    // change outside plam 
    $('.form_close').find("input[name='physical_inv_change2']").click(function() {
        if ($(this).prop("checked") == true) {

            var inv = $(this).val();
            if ($('.form_close').find("input[name='physical_inv_change1']").prop("checked") == true) {
                $('.form_close').find("input[name='physical_inv_change']").val(3);
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
            }
            $('.inv_change_desc').show();
        } else if ($(this).prop("checked") == false) {
            if ($('.form_close').find("input[name='physical_inv_change1']").prop("checked") == false) {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
                $('.inv_change_desc').hide();
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(1);
            }
        }
    });
    // end of check inside or outside plan 

    // select2 select_configuration_change on click
    $('.select_configuration_change').change(function() {
        var selectedObj = $(this).children("option:selected").val();

        if (selectedObj == 'no' || selectedObj == '') {
            $('.configurasi_change_activity').hide();
        } else {
            $('.configurasi_change_activity').show();
        }
    });


    // double click tbody datatables 
    $('#incident_open tbody').on('click', 'tr .btn-close', function() {
        // **** declare variable ****
        var message;
        incident_id = $(this).attr('data-id');
        // check close permission
        $.ajax({
            url: baseurl + 'incident/close_permission?id=' + incident_id,
            type: 'POST',
            dataType: 'JSON',
            async: false,
            success: function(data) {
                status_incident = data.status;
                message = data.message;
            }
        });

        if (status_incident == false) {
            // pesan = "Incident not ready to close";
            pesan = message;
            fn_show_alert(pesan);
            return false;
        }

        // check permission 
        if (member == "SERPO" || member == "CONTACTCENTER") {
            pesan = "Your Account Cannot Close Incident ";
            fn_show_alert(pesan);
            return false;
        }

        // check session checin and mod
        // if (user_checkin === "0" || typeof user_checkin === "undefined" || user_checkin == 0) {
        //     $('.text-confirm').html("Please Check in ");
        //     $('#modalUnlockFail').modal('show');
        //     return false;
        // }
        if (status_checkin === 0 || status_checkin === "0" || typeof status_checkin === "undefined") {
            pesan = "Please Check in ";
            fn_show_alert(pesan);
            return false;
        }

        // if (user_mod === "0" || typeof user_checkin === "undefined") {
        //     $('.text-confirm').html("You Must Mod");
        //     $('#modalUnlockFail').modal('show');
        //     return false;
        // }



        // if (type === 'ANALYSE') {

        var id = $(this).attr('data-id');
        // last_stop_clock = $(this).attr('data-stop_clock');

        // declare variable
        $('#modalCloseIncident').modal('show');
        $('.form_close').find("input[name='id']").val(id);
        // $('.form_close').find("input[name='last_stop_clock']").val(last_stop_clock);


        // set value close incident 
        $.ajax({
            url: baseurl + 'incident/get_summary_incident?id=' + id,
            type: 'POST',
            dataType: 'JSON',
            async: true,
            success: function(data) {
                // console.log(data)

                $('.form_close').find("input[name='creator']").val(data.creator);
                $("#select_classification_close").select2('val', [data.classification_id]);
                $("#select_root_cause_close").html('<option value=' + data.root_cause_id + '>' + data.root_cause_name + '</option>');
                if (data.link == 'backbone') {
                    var link = '<option value=' + data.link + '>' + data.link + '</option><option value="access">Access</option><option value="distribution">Distribution</option>';
                } else if (data.link == 'Access') {
                    var link = '<option value=' + data.link + '>' + data.link + '</option><option value="backbone">Backbone</option><option value="distribution">Distribution</option>';
                } else {
                    var link = '<option value=' + data.link + '>' + data.link + '</option><option value="access">Access</option><option value="backbone">Backbone</option>';
                }
                $("#link_close").html(link);
                // $("#link_close").html('<option value=' + data.link + '>' + data.link + '</option>');
                $("#region1_close").select2('val', [data.region_code]);
                $(".form_close").find("textarea[name='description']").val(data.description);
                $("#" + data.network + "_close").attr('checked', 'checked');
                $("#" + data.priority + "_close").attr('checked', 'checked');
                $("#site_name1_close").html("<option value=" + data.site_name_id + ">" + data.site_name + "</option>");
                $('#link_upstream_isp_close').select2('val', [data.source_category]);
                $("#select_root_cause_detail_close").select2('val', [data.root_causes_detail_id]);
            }
        });

        // } else {

        //     $('.text-confirm').html("Your Account cannot close incident");
        //     $('#modalUnlockFail').modal('show');
        // }


    });

    function showTime() {
        var date = new Date();
        var h = date.getHours(); // 0 - 23
        var m = date.getMinutes(); // 0 - 59
        var s = date.getSeconds(); // 0 - 59
        var session = "AM";

        if (h == 0) {
            h = 12;
        }

        h = (h < 10) ? "0" + h : h;
        m = (m < 10) ? "0" + m : m;
        s = (s < 10) ? "0" + s : s;

        var time = h + ":" + m + ":" + s;
        $('.form_close').find("input[name='time']").val(time);

        setTimeout(showTime, 1000);

    }

    // display time 
    showTime();

    // close incident 
    $('.form_close').submit(function(e) {
        e.preventDefault();

        var incident_id = $('.form_close').find("input[name='id']").val();

        $('.form_create_message').find("textarea[name='message']").val('Incident Close');

        $.ajax({
            url: baseurl + 'incident/close_incident_prosses',
            data: new FormData(this),
            type: 'POST',
            dataType: 'JSON',
            async: true,
            contentType: false,
            processData: false,
            success: function(data) {

                $('#modalCloseIncident').modal('hide');
                tb_incident.ajax.reload(null, false);

            }
        });

    });
    // end of close incident 

    // double click tbody datatables 
    $('#incident_open tbody').on('click', 'tr .btn-assign', function(e) {
        e.preventDefault();

        // if (user_checkin === "0" || typeof user_checkin === "undefined" || user_checkin == 0) {
        //     $('.text-confirm').html("Please Check in ");
        //     $('#modalUnlockFail').modal('show');
        //     return false;
        // }
        if (status_checkin === 0 || status_checkin === "0" || typeof status_checkin === "undefined") {
            pesan = "Please Check in ";
            fn_show_alert(pesan);
            return false;
        }

        if (user_mod === "0" || typeof user_checkin === "undefined" || user_checkin == 0) {
            $('.text-confirm').html("You Must Mod");
            $('#modalUnlockFail').modal('show');
            return false;
        }


        var data = tb_incident.row(this).data();
        var creator = $(this).data('creator');
        var id = $(this).data('id');
        var incident_id = $(this).data('incident_id');
        var description = $(this).data('description');

        $('#modalAssign').modal('show');

        $('.form_assign').find("input[name='id']").val(id);
        $('.form_assign').find("input[name='incident_id']").val(incident_id);
        $('.form_assign').find("textarea[name='description']").val(description);
        $('.form_assign').find("input[name='creator']").val(creator);

        $.ajax({
            url: baseurl + 'incident/get_user',
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                $('#select_user').empty();

                $.each(data, function(i, item) {
                    var user = '<option value=' + item.username + '>' + item.username + '</option>';
                    $('#select_user').append(user);
                });

            }
        });

    });


    // submit form assign
    $('.form_assign').submit(function(e) {
        e.preventDefault();

        var creator = $('.form_assign').find("input[name='creator']").val();

        $.ajax({
            url: baseurl + 'incident/assign',
            data: new FormData(this),
            type: 'POST',
            dataType: 'JSON',
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#modalAssign').modal('hide');
                if (data == 'sukses') {
                    // location.reload(true);
                    tb_incident.ajax.reload(null, false);
                }

            }
        });

    });

    // menentukan modal yang akan tampil
    function check_incident(id, lock_by) {

        if (lock_by === null || lock_by === '' || lock_by == 'null') {

            $.ajax({
                url: baseurl + 'incident/merge/get_merge_reason',
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    $('.select-merge-reason').empty();
                    $.each(data, function(i, item) {
                        var option = '<option value=' + item.id + '>' + item.reason + '</option>';
                        $('.select-merge-reason').append(option);
                    });
                }
            });

            var arr_merge = ["NOC", "SBU", "CONTACTCENTER"];

            if (jQuery.inArray(member, arr_merge) !== -1) {

                $('.btn-merge').css('display', '');
                $('.btn-edit').css('display', '');
            } else {

                $('.btn-merge').css('display', 'none');
                $('.btn-edit').css('display', 'none');
            }

            var p_param = { id: id, lock_user: lock_by },
                p_url = baseurl + 'incident/unlock_incident';

            // update lock by 
            $.ajax({
                type: 'POST',
                data: p_param,
                url: p_url,
                success: function(data) {

                    // reload datatables 
                    tb_incident.ajax.reload(null, false);

                }

            });

            var status = 'detail';

        } else {

            // maka akan memanggil function summary incident 
            // get_summary(id);
            $('.btn-edit').css('display', 'none');
            $('.btn-merge').css('display', 'none');
            var status = 'summary';

        }

        // display incident detail
        incident_detail(id, status);

    }

    // get puluhan 
    function get_puluhan(p) {
        if (p < 10) {
            return '0' + p;
        } else {
            return p;
        }
    }


    // get time
    function fn_time(number) {

        var sisa_waktu = number % 3600;
        var h = Math.floor(number / 3600);
        var m = Math.floor(sisa_waktu / 60);
        var s = Math.floor(sisa_waktu % 60);
        var hasil = get_puluhan(h) + ':' + get_puluhan(m) + ':' + get_puluhan(s);

        return hasil;

    }

    // display detail incident when click body
    function incident_detail(id, status) {

        // display modal 
        $("#modalLogDetail").modal("show");

        // axios get data incident by id 
        axios.get(baseurl + 'incident/get_summary_incident?id=' + id).then(function(response) {
                response = response.data;

                $("#mod_row_incidentId").val(response.incident_id);
                $("#mod_row_creator").val(response.creator);
                $("#mod_row_notificationTrigger").val(response.creator);
                $("#mod_row_state").val(response.status);
                $("#mod_row_clasification").val(response.classification_name);
                $("#mod_row_rootCause").val(response.root_cause_name);
                $("#mod_row_rootCauseDetail").val(response.root_cause_detail);
                $("#mod_row_description").val(response.description);
                $("#mod_row_hirarchy").val(response.hierarchy);
                $("#mod_row_link").val(response.link);
                $("#mod_row_link_start_region").val(response.link_start_region);
                $("#mod_row_link_end_region").val(response.link_end_region);
                $("#mod_row_link_start_site").val(response.link_start_site);
                $("#mod_row_link_end_site").val(response.link_end_site);
                $("#mod_row_region").val(response.region_name);
                $("#mod_row_site").val(response.site_name);
                $("#mod_source_category").val(response.source_category);
                $("#mod_row_creationTime").val(response.start_time);
                $("#mod_row_duration").val(response.duration);
                $("#mod_row_stopLock").val(response.stopclock);
                $("#mod_row_ttr").val(response.ttr);

                // display incident ticket
                dt_incident_ticket(id, status);
                // get incident detail
                get_edit_incident(id);
                // put id in merge incident
                $('.incident_id').val(response.incident_id);
                $('.incident_detail').val(response.description);

            })
            .catch(function(error) {
                console.log(error)
            });

    }
    // end of detail incident 

    function undifiend(params) {
        if (params == null || params == '') {
            return "<option value='' selected></option>";
        } else {
            return '<option value=' + params + '>' + params + '</option>';
        }
    }

    // displasy edit incident by id
    function get_edit_incident(id) {

        // axios get data incident by id 
        axios.get(baseurl + 'incident/get_summary_incident?id=' + id).then(function(response) {
                response = response.data;

                // console.log(response);

                // insert value to form 
                $(".form_edit").find("input[name='id']").val(response.id);
                $(".form_edit").find("input[name='creator']").val(response.creator);
                $(".form_edit").find("input[name='timestamp']").val(response.creation_time);
                $("#select_classification").select2('val', [response.classification_id]);
                $("#select_root_cause").html('<option value=' + response.root_cause_id + '>' + response.root_cause_name + '</option>');
                // if(e.root_causes_detail_id == null || e.root_causes_detail_id == '') {
                //     var root_causes_detail = '<option value=""></option>';
                // } else {
                //     var root_causes_detail = '<option value=' + response.root_causes_detail_id + '>' + response.root_cause_detail_name + '</option>';
                // }
                // $("#select_root_cause_detail").html(root_causes_detail);
                // var root_causes_detail = '<option value=' + response.root_causes_detail_id + '>' + response.root_cause_detail_name + '</option>';
                $("#select_root_cause_detail").html(undifiend(response.root_causes_detail_id));
                if (response.link == 'backbone') {
                    var link = '<option value=' + response.link + '>' + response.link + '</option><option value="access">Access</option><option value="distribution">Distribution</option>';
                } else if (response.link == 'Access') {
                    var link = '<option value=' + response.link + '>' + response.link + '</option><option value="backbone">Backbone</option><option value="distribution">Distribution</option>';
                } else {
                    var link = '<option value=' + response.link + '>' + response.link + '</option><option value="access">Access</option><option value="backbone">Backbone</option>';
                }
                $("#link").html(link);
                $("#region1").select2('val', [response.region_code]);
                $(".form_edit").find("textarea[name='description']").val(response.description);
                $('#' + response.network).attr('checked', 'checked');
                $('#' + response.priority).attr('checked', 'checked');
                $("#site_name1").html("<option value=" + response.site_name_id + ">" + response.site_name + "</option>");

            })
            .catch(function(error) {
                console.log(error)
            });

    }
    // end of display editincident 

    // ********* display datatable incident ticket *********
    function dt_incident_ticket(id, status) {
        $('#detail_incident_ticket').DataTable({
            dom: 'flrtpi',
            responsive: true,
            lengthChange: false,
            pageLength: 15,
            destroy: true,
            columnDefs: [{
                    "targets": [0, 5], // your case first column
                    "className": "text-center",
                },
                {
                    "targets": '_all',
                    "createdCell": function(td, cellData, rowData, row, col) {
                        $(td).css('padding', '3px');
                    }
                }
            ],
            ajax: baseurl + 'incident/get_ticket_incident?id=' + id,
            columns: [{
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "service_id" },
                { "data": "ticket_id" },
                { "data": "open_time" },
                { "data": "bobot" },
                { "data": "description" },
                {
                    render: function(data, type, row, meta) {
                        if (status === 'summary') {

                            return "<button type='button' data-toggle='tooltip' title='Show Ticket Reference IPDB, Sisfo' class='btn btn-xs btn-warning shadow ml-1 btn-ismiling' data-service_id=" + row.service_id + "><i class='fa fa-file'></i></button>" +
                                "<button type='button' data-toggle='tooltip' title='Show Ticket Progress' class='btn btn-xs btn-complete shadow ml-1 btn-progress' data-ticket_id='" + row.ticket_id + "'><i class='fa fa-share'></i></button>"

                        } else {

                            return "<button type='button' data-toggle='tooltip' title='Show Ticket Reference IPDB, Sisfo' class='btn btn-xs btn-warning shadow ml-1 btn-ismiling' data-service_id=" + row.service_id + "><i class='fa fa-file'></i></button>" +
                                "<button type='button' data-toggle='tooltip' title='Show Ticket Progress' class='btn btn-xs btn-complete shadow ml-1 btn-progress' data-ticket_id='" + row.ticket_id + "'><i class='fa fa-share'></i></button>" +
                                "<button type='button' data-toggle='tooltip' title='UnAttach Ticket' class='btn btn-xs btn-danger shadow ml-1 btn-untiket' data-incident=" + id + " data-ticket=" + row.ticket_id + " data-description='" + row.description + "' data-class=" + row.class + "><i class='fa fa-minus'></i></button>"
                        }
                    }
                }
            ]
        });
    }
    // ***** end of function ****** 

    // get crm by service id
    $(document).on('click', '.btn-ismiling', function() {

        $('#modalCrm').modal('show');

        var service_id = $(this).attr('data-service_id');
        var frameElement = document.getElementById("frameIsmilling");

        frameElement.src = `http://10.14.22.211:85/fmsapi/getservice?sid=` + service_id;

    });


    // get ticket progress
    $(document).on('click', '.btn-progress', function() {

        var ticket_id = $(this).attr('data-ticket_id');
        dt_ticket_progress(ticket_id);
        $('#modalTicketProgress').modal('show');

    });

    // untiket
    $(document).on('click', '.btn-untiket', function() {
        var ticket = $(this).attr('data-ticket'),
            incident = $(this).attr('data-incident'),
            t_class = $(this).attr('data-class'),
            description = $(this).attr('data-description');


        $.ajax({
            url: baseurl + 'incident/unattach_ticket',
            async: true,
            data: {
                ticket_id: ticket,
                incident_id: incident,
                description: description,
                class: t_class
            },
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {

                dt_incident_ticket(data);
                tb_incident.ajax.reload(null, false);

            }
        });
    });


    // ticket progress
    function dt_ticket_progress(ticket_id) {
        $('#ticket_progress').DataTable({
            dom: 'flrtpi',
            responsive: true,
            lengthChange: false,
            pageLength: 10,
            destroy: true,
            columnDefs: [{
                    "targets": [0], // your case first column
                    "className": "text-center",
                },
                {
                    "targets": '_all',
                    "createdCell": function(td, cellData, rowData, row, col) {
                        $(td).css('padding', '3px');
                    }
                }
            ],
            ajax: baseurl + "ticket/get_ticket_progress?ticket_id=" + ticket_id,
            columns: [{
                    "data": "ticket_id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "username" },
                { "data": "timestamp" },
                { "data": "description_progress" }
            ]
        });
    }

    // function show hide button unlock
    $('.form_confirm').submit(function(event) {
        event.preventDefault();

        // var option = $(this).find("input[name='option']").val();
        var p_id = $(this).find("input[name='id']").val(),
            p_lock_by = $(this).find("input[name='lock_by']").val(),
            p_param = { id: p_id, lock_user: p_lock_by };

        $.ajax({
            type: 'POST',
            data: p_param,
            url: baseurl + 'incident/unlock_incident',
            success: function(data) {
                tb_incident.ajax.reload(null, false);
            }

        });

        // hidden modal 
        $('#modalUnlock').modal('hide');

    });


    // incident file attchment 
    $('.form_upload').submit(function(e) {
        e.preventDefault();

        $.ajax({
            url: baseurl + 'incident/upload_file',
            dataType: 'JSON',
            type: 'POST',
            data: new FormData(this),
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {

                dt_incident_file_attachment(data);

            }
        });
    });

    function fn_show_alert(pesan) {
        $('#modalerror').modal('show');
        $('.content_alert').html(pesan);
    }

    // close incident 
    // select2 classification 
    $("#select_root_cause_close").remoteChained({
        parents: "#select_classification_close",
        url: baseurl + "ticket/load_root_cause"
    });

    $("#select_root_cause_detail_close").remoteChained({
        parents: "#select_root_cause_close",
        url: baseurl + "ticket/load_root_cause_detail"
    });

    $("#site_name1_close").remoteChained({
        parents: "#region1_close",
        url: baseurl + "ticket/load_incident_regions1"
    });

    $("#link_start_site_close").remoteChained({
        parents: "#link_start_region",
        url: baseurl + "ticket/load_incident_regions_start"
    });

    $("#link_end_site_close").remoteChained({
        parents: "#link_end_region_close",
        url: baseurl + "ticket/load_incident_regions_end"
    });

    $("#brand1_close").remoteChained({
        parents: "#site_name1_close",
        url: baseurl + "ticket/load_brands"
    });

    $("#me1_close").remoteChained({
        parents: "#brand1_close",
        url: baseurl + "ticket/load_me"
    });


    // func show and hide in classification of define incidents
    $('#select_classification_close').on('change', function() {
        if (this.value) {

            $('#region1_close').val(null).trigger('change');
            $('#link_start_region_close').val(null).trigger('change');
            $('#link_end_region_close').val(null).trigger('change');
            $('#site_name1_close').empty();
            $('#brand1_close').empty();
            $('#me1_close').empty();
            $('#link_start_site_close').empty();
            $('#link_end_site_close').empty();
            $('#11_close').show()

        }

        if (this.value == '151' || this.value == '121' || this.value == '171' || this.value == '161' || this.value == '661' || this.value == '131' || this.value == '411' || this.value == '666') {
            $('#1_close').show()
            $('#3_close').hide()
                // $('#11_close').show()
            $('#foc-show_close').hide()
                // $('#foc-hide').hide()
            $('#source_category_close').show()
            $('#source_category_close').text("FOT")
            $('#source_category_input_close').val("FOT")
                // $('#div_link_upstream_isp_close').hide()
            $('#div_region_close').show()
            $('#div_site_name_close').show()
        }
        if (this.value == '251' || this.value == '241' || this.value == '141') {
            $('#1_close').show()
            $('#3_close').hide()
                // $('#11_close').show()
            $('#foc-show_close').hide()
                // $('#foc-hide').hide()
            $('#source_category_close').show()
            $('#source_category_close').text("PS")
            $('#source_category_input_close').val("PS")
                // $('#div_link_upstream_isp_close').hide()
            $('#div_region_close').show()
            $('#div_site_name_close').show()
        }
        if (this.value == '111') {
            $('#1_close').hide()
            $('#3_close').show()
                // $('#11_closee').show()
            $('#foc-show_close').hide()
                // $('#foc-hide').hide()
            $('#source_category_close').show()
            $('#source_category_close').text("FOC")
            $('#source_category_input_close').val("FOC")
                // $('#div_link_upstream_isp_close').hide()
            $('#div_region_close').show()
            $('#div_site_name_close').show()
        }
        if (this.value == '381' || this.value == '361' || this.value == '888') {
            $('#1_close').hide()
            $('#3_close').hide()
                // $('#11_close').show()
            $('#foc-show_close').hide()
                // $('#foc-hide').hide()
            $('#source_category_close').hide()
                // $('#div_link_upstream_isp_close').show()
            $('#div_region_close').show()
            $('#div_site_name_close').show()
        }


        if (this.value == '999') {
            $('#1_close').hide()
            $('#3_close').hide()
                // $('#11_close').hide()
            $('#foc-show_close').hide()
                // $('#foc-hide').show()
            $('#source_category_close').hide()
                // $('#div_link_upstream_isp_close').hide()
            $('#div_region_close').show()
            $('#div_site_name_close').show()
        }
        if (this.value == '221') {
            $('#1_close').hide()
            $('#3_close').show()
                // $('#11_close').show()
            $('#foc-show_close').show()
                // $('#foc-hide').hide()
            $('#source_category_close').show()
            $('#source_category_close').text("FOC")
            $('#source_category_input_close').val("FOC")
                // $('#div_link_upstream_isp_close').hide()
            $('#div_region_close').hide()
            $('#div_site_name_close').hide()
        }
    });


    $('#link_upstream_isp_close').on('change', function() {

        if (this.value) {
            $('#region1_close').val(null).trigger('change');
            $('#region_close').val(null).trigger('change');
            $('#link_start_region_close').val(null).trigger('change');
            $('#link_end_region_close').val(null).trigger('change');
            $('#site_name1_close').empty();
            $('#site_name_close').empty();
            $('#brand_close').empty();
            $('#me_close').empty();
            $('#link_start_site_close').empty();
            $('#link_end_site_close').empty();
        }

        // kondisi jika pilih link upstream ISP bagian source category 
        if (this.value == '1') {
            $('#1_close').show()
            $('#3_close').hide()
                // $('#11_close').show()
            $('#source_category_close').hide()
            $('#source_category_input_close').val("FOT")
            $('#foc-hide_close').show()
            $('#foc-show_close').hide()
            $('#div_region_close').show()
            $('#div_site_name_close').show()
        }

        if (this.value == '2') {
            $('#1_close').hide()
            $('#3_close').show()
                // $('#11_close').show()
            $('#source_category_close').hide()
            $('#source_category_input_close').val("FOC")
            $('#foc-hide_close').hide()
            $('#foc-show_close').show()
            $('#div_region_close').hide()
            $('#div_site_name_close').hide()
        }

        if (this.value == '3') {
            $('#1_close').show()
            $('#3_close').hide()
                // $('#11_close').show()
            $('#source_category_close').hide()
            $('#source_category_input_close').val("PS")
            $('#foc-hide_close').show()
            $('#foc-show_close').hide()
            $('#div_region_close').show()
            $('#div_site_name_close').show()
        }


    });


    // filter list incident 
    var tb_incident_filter;
    var total_select;

    $('.btn-filter').on('click', function(e) {
        e.preventDefault();

        $('#modalFilterIncident').modal('show');

        total_select = 0;

        tb_incident_filter = $('#incident_filter').DataTable({
            dom: 'flrtpi Rl',
            // stateSave: true,
            // scrollX: true,
            responsive: true,
            lengthChange: false,
            pageLength: 10,
            processing: true, //Feature control the processing indicator.
            serverSide: true, //Feature control DataTables' server-side processing mode.
            columnDefs: [{
                    'width': '7%',
                    // 'className': 'float-right',
                    'targets': 1,
                    'searchable': false,
                    'orderable': false,
                    'className': 'dt-body-center',
                    'render': function(data, type, full, meta) {
                        return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                    }
                },
                {
                    "width": "7%",
                    "targets": 0,
                    "className": "text-center"
                },
                {
                    "targets": "_all",
                    "createdCell": function(td, cellData, rowData, row, col) {
                        $(td).css('padding', '5px');
                        $(td).attr('data-toggle', 'tooltip');
                        $(td).attr('title', cellData);
                    }
                },
                {
                    "width": "10%",
                    "targets": [2, 3, 5],
                },
                {
                    "width": "70%",
                    "targets": 4,
                }
            ],
            order: [1, 'asc'],
            ajax: {
                "url": baseurl + 'incident/list_open_incident',
                "type": "POST",
                "data": function(d) {
                    d.type = 'all';
                }
            },
            columns: [{
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "id" },
                { "data": "incident_id" },
                { "data": "start_time" },
                { "data": "description" },
                { "data": "mention_to" },
            ]
        });

        // update table 
        $('#incident_filter tbody').on('change', 'input[type="checkbox"]', function() {

            //     // If checkbox is not checked
            if (this.checked === true) {
                select_incident.push(this.value);
                // alert(this.value)
            } else {
                // alert(this.value)
                var arrindex = select_incident.indexOf(this.value);
                // alert(arrindex)
                select_incident.splice(arrindex, 1);
            }

            // console.log(select_incident);

            // tb_incident.ajax.reload(null, false)

        });
    });

    // set auto reload 3 minutes 
    setInterval(function() {
        tb_incident.ajax.reload(null, false);
    }, 30000);

    // Handle click on "Select all" control
    $('#example-select-all').on('click', function() {
        // Check/uncheck all checkboxes in the table
        var rows = tb_incident_filter.rows({ 'search': 'applied' }).nodes();
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // $('#incident_filter tbody').on('change', 'input[type="checkbox"]', function() {
    // If checkbox is not checked
    // alert(this.checked);
    // select_incident = [];

    // if (this.checked === true) {
    //     select_incident.push(this.value);

    //     tb_incident.ajax.reload(null, false)
    // }


    // });

    $('.form_submit').on('submit', function(e) {
        e.preventDefault();
        // Iterate over all checkboxes in the table
        // select_incident = [];
        tb_incident.state.clear();
        var count = 0;
        tb_incident_filter.$('input[type="checkbox"]').each(function() {
            // If checkbox doesn't exist in DOM
            if (this.checked === true) {
                count = count + 1;
                // select_incident.push(this.value);

            }
        });

        if (count === 0) {
            select_incident = [];
        }

        // console.log(select_incident)
        tb_incident.ajax.reload(null, true)

        $('#modalFilterIncident').modal('hide');

    });

});