$(document).ready(function() {
    $.fn.dataTable.ext.errMode = 'none';
    var t_request_stopclock;

    $('.btn-listrequest').click(function(e) {
        e.preventDefault();

        $('#modalRequestStopClock').modal('show');

        // table request incident 
        t_request_stopclock = $('#tb_request_stopclock').DataTable({
            dom: 'flrtpi Rl',
            responsive: true,
            // scrollX: true,
            lengthChange: false,
            pageLength: 10,
            destroy: true,
            ajax: baseurl + 'incident/list_request_stopclock',
            columnDefs: [{
                    "targets": [0], // your case first column
                    "className": "text-center",
                },
                {
                    "targets": '_all',
                    "createdCell": function(td, cellData, rowData, row, col) {
                        $(td).css('padding', '3px');
                        $(td).attr('title', cellData);
                    }
                }
            ],
            columns: [{
                    "data": "id",
                    "width": "3%",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "incident_id", "width": "3%", },
                { "data": "description", "width": "5%", },
                { "data": "clock", "width": "5%", },
                { "data": "reason", "width": "5%", },
                { "data": "reason_type", "width": "5%", },
                { "data": "username", "width": "5%", },
                { "data": "group_code", "width": "5%", },
                {
                    "width": "3%",
                    render: function(data, type, row, meta) {
                        if (row.file_name === null) {
                            return ''
                        } else {
                            return '<a href="' + row.file_path + '" target="_blank">' + row.file_name + '</a>'
                        }
                    }
                },
                {
                    "width": "5%",
                    "className": "text-center",
                    render: function(data, type, row, meta) {
                        return '<button class="btn btn-xs btn-success shadow btn-approve-stopclock mr-1" data-type="approve" data-incident=' + row.incident_id_ + ' data-id=' + row.id + ' title="Approve"><i class="fa fa-check"></i></button>' +
                            '<button class="btn btn-xs btn-danger shadow btn-approve-stopclock" data-type="deleted" data-id=' + row.id + ' title="Cancel" ><i class="fa fa-times"></i></button>'

                    }
                }
            ]
        });

        // approve request stop clock
        $('#tb_request_stopclock tbody').on('click', 'tr .btn-approve-stopclock', function(e) {
            e.preventDefault();

            var id = $(this).data('id'),
                type = $(this).data('type'),
                incident_id = $(this).data('incident'),
                param = {
                    id: id,
                    type: type,
                    incident_id: incident_id
                };

            $.ajax({
                url: baseurl + 'incident/prosses_request_stopclock',
                dataType: 'JSON',
                type: 'POST',
                data: param,
                async: true,
                success: function(data) {

                    // alert(data);
                    fn_show_alert(data);
                    t_request_stopclock.ajax.reload();
                }
            });

        });

    });

    function fn_show_alert(pesan) {
        $('#modalerror').modal('show');
        $('.content_alert').html(pesan);
    }

});