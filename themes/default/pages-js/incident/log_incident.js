$(document).ready(function() {

    // var api_url = 'http://localhost/fms-api/uploads/incidents/';
    var api_url = baseurl + 'fms-api/uploads/incidents/';
    var table_log;

    activeClass('incident', 'log-incident');

    $.fn.dataTable.ext.errMode = 'none';

    $('[data-toggle="tooltip"]').tooltip();

    // config datepicker
    $('#datepicker-component').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('.select2').select2({
        allowClear: true,
    });

    // repeat interval
    setInterval(function() {
        table_log.ajax.reload(null, false);
    }, 30000);

    table_log = $('#log_incident').DataTable({
        dom: 'Bfrtpi Rl',
        scrollX: true,
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
            {
                init: function(api, node, config) {
                    $(node).removeClass('dt-button');
                    $(node).css('display', 'none');
                }
            }

        ],
        stateSave: true,
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        processing: true,
        serverSide: true,
        // ajax: baseurl + 'incident/log/list_log_incident',
        ajax: {
            "url": baseurl + 'incident/log/list_log_incident_all',
            "type": "POST"
        },
        columnDefs: [{
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '5px');
                    $(td).attr('data-toggle', 'tooltip');
                    $(td).attr('title', cellData);
                }
            },
            {
                "targets": [0], // your case first column
                "className": "text-center",
            }
        ],
        columns: [{
                "data": "id",
                "width": "2%",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "incident_id", "width": "3%" },
            { "data": "username", "width": "3%" },
            { "data": "action", "width": "15%" },
            { "data": "remarks", "width": "5%" },
            { "data": "created_on", "width": "5%" },

        ],
    });

});