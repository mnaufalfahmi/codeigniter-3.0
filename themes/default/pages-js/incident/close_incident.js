var tb_close_incident;
var start_date;
var end_date;

// start document ready function 
$(document).ready(function() {

    activeClass('incident', 'close-incident');

    $.fn.dataTable.ext.errMode = 'none';

    $('.select2').select2({
        allowClear: true,
    });

    $('[id=datepicker-component]').datepicker({
        format: 'yyyy-mm-dd',
    });

    // get region 
    $.ajax({
        url: baseurl + 'incident/region',
        dataType: 'JSON',
        type: 'GET',
        async: true,
        success: function(data) {

            $('.select_region').empty();

            $.each(data, function(i, item) {
                var region = item.region_code;
                var region_name = item.region_name;
                var option = '<option value="' + region + '">' + region_name + '</option>';

                $('.select_region').append(option);

            });

            $('.select_region').append("<option value='' selected></option>");
        }
    });

    tb_close_incident = $('#incident_close').DataTable({
        dom: '<"toolbar">Bfrtpi Rl',
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },

        ],
        stateSave: true,
        scrollX: true,
        responsive: true,
        lengthChange: false,
        pageLength: 15,
        processing: true,
        serverSide: false,
        ajax: {
            "url": baseurl + 'incident/list_close_incident_new',
            "type": "POST",
            "data": function(d) {
                d.start_date = start_date;
                d.end_date = end_date;
                d.id = id;
            }
        },
        columnDefs: [{
                "targets": '_all',
                "bSortable": true,
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                    $(td).attr('data-toggle', 'tooltip');
                    $(td).attr('title', cellData);
                }
            },
            {
                "targets": [0, 1, 2, 3, 4, 5, 6],
                "bSortable": true,
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                    $(td).attr('data-toggle', 'tooltip');
                    $(td).attr('title', cellData);
                }
            }
        ],
        columns: [{
                "data": "id",
                "width": "3%",
                "className": "text-center",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "incident_id", "width": "5%" },
            { "data": "service_id", "width": "4%" },
            { "data": "ismilling_id", "width": "4%" },
            { "data": "start_time", "width": "4%" },
            { "data": "region_code", "width": "4%" },
            { "data": "close_time", "width": "4%" },
            { "data": "mttr_time_str", "width": "4%" },
            { "data": "description", "width": "15%" },
            {
                "className": "text-center",
                "width": "10%",
                render: function(data, type, row, meta) {
                    return '<button class="btn progress-color btn-xs shadow btn-incident-progress text-white" data-id="' + row.id + '" >Progress</button>&nbsp;' +
                        '<button class="btn btn-complete btn-xs shadow btn-summary" data-id="' + row.id + '" >Summary</button>&nbsp;' +
                        '<button class="btn btn-primary btn-xs shadow btn-pdf" data-id="' + row.id + '" >Download PDF</button>'
                }
            }
        ],
    });

    // set auto reload 3 minutes 
    setInterval(function() {
        tb_close_incident.ajax.reload(null, false);
    }, 30000);


    $('.form_filter').submit(function(e) {
        e.preventDefault();

        $('#modalFilter').modal('hide');

        start_date = $(".form_filter").find("input[name = 'start_date']").val();
        end_date = $(".form_filter").find("input[name = 'end_date']").val();
        id = "";

        tb_close_incident.ajax.reload(null, true)


    });

    // DataTable list incident close


    $("#incident_close").on('click', '.btn-incident-progress', function(e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        var redirect = window.open(baseurl + 'incident/progress/' + id, "_blank");
        redirect.location;

    });


    // click summary open 
    $('#incident_close tbody').on('click', 'tr .btn-summary', function() {

        $('#modalLogDetail').modal('show');
        var id = $(this).attr('data-id');

        $.ajax({
            url: baseurl + 'incident/get_summary_incident?id=' + id,
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(response) {

                // get data ticket 
                $('#detail_incident_ticket').DataTable({
                    dom: 'flrtpi Rl',
                    lengthChange: false,
                    pageLength: 15,
                    destroy: true,
                    ajax: baseurl + 'incident/get_ticket_incident?id=' + response.id,
                    columnDefs: [{
                            "targets": [0, 5], // your case first column
                            "className": "text-center",
                        },
                        {
                            "targets": '_all',
                            "createdCell": function(td, cellData, rowData, row, col) {
                                $(td).css('padding', '3px');
                            }
                        }
                    ],
                    columns: [{
                            "data": "id",
                            "width": "2%",
                            render: function(data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        { "data": "service_id", "width": "5%" },
                        { "data": "ticket_id", "width": "5%" },
                        { "data": "open_time", "width": "5%" },
                        { "data": "description", "width": "15%" },
                        {
                            "width": "3%",
                            render: function(data, type, row, meta) {
                                return "<button type='button' class='btn btn-xs btn-warning ml-1 shadow btn-ismiling' data-service_id=" + row.service_id + "><i class='fa fa-file'></i></button>" +
                                    "<button type='button' class='btn btn-xs btn-complete ml-1 shadow btn-progress' data-ticket_id='" + row.ticket_id + "'><i class='fa fa-share'></i></button>"

                            }
                        }
                    ]
                });

                // fill in summar incident 
                $("#mod_row_incidentId").val(response.incident_id);
                $("#mod_row_creator").val(response.creator);
                $("#mod_row_notificationTrigger").val(response.creator);
                $("#mod_row_state").val(response.status);
                $("#mod_row_clasification").val(response.classification_name);
                $("#mod_row_rootCause").val(response.root_cause_name);
                $("#mod_row_rootCauseDetail").val(response.root_cause_detail);
                $("#mod_row_description").val(response.description);
                $("#mod_row_hirarchy").val(response.hierarchy);
                $("#mod_row_link").val(response.link);
                $("#mod_row_link_start_region").val(response.link_start_region);
                $("#mod_row_link_end_region").val(response.link_end_region);
                $("#mod_row_link_start_site").val(response.link_start_site);
                $("#mod_row_link_end_site").val(response.link_end_site);
                $("#mod_row_region").val(response.region_name);
                $("#mod_row_site").val(response.site_name);
                $("#mod_source_category").val(response.source_category);
                $("#mod_row_creationTime").val(response.start_time);
                $("#mod_row_duration").val(response.duration);
                $("#mod_row_stopLock").val(response.stopclock);
                $("#mod_row_ttr").val(response.ttr);

            }
        });

    }); // end of summary open 

    // start download pdf
    $('#incident_close tbody').on('click', 'tr .btn-pdf', function() {

        var id = $(this).attr('data-id');
        window.open(baseurl + 'incident/get_pdf?id=' + id)

    });
    // end download pdf

    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalFilter" data-toggle="modal"><i class="fa fa-filter"></i></button>');

    // convert time 
    function fn_time(number) {

        var sisa_waktu = number % 3600;
        var h = Math.floor(number / 3600);
        var m = Math.floor(sisa_waktu / 60);
        var s = Math.floor(sisa_waktu % 60);
        var hasil = get_puluhan(h) + ':' + get_puluhan(m) + ':' + get_puluhan(s);

        return hasil;

    }

    // get puluhan 
    function get_puluhan(p) {
        if (p < 10) {
            return '0' + p;
        } else {
            return p;
        }
    }


    // get crm by service id
    $(document).on('click', '.btn-ismiling', function() {

        $('#modalCrm').modal('show');

        var service_id = $(this).attr('data-service_id');
        var frameElement = document.getElementById("frameIsmilling");

        frameElement.src = `http://10.14.22.211:85/fmsapi/getservice?sid=` + service_id;

    });


    // get ticket progress
    $(document).on('click', '.btn-progress', function() {

        // var ticket
        var ticket_id = $(this).attr('data-ticket_id');

        // display modal
        $('#modalTicketProgress').modal('show');

        // display data 
        $('#ticket_progress').DataTable({
            dom: 'flrtpi',
            lengthChange: false,
            pageLength: 10,
            destroy: true,
            fixedColumns: {
                leftColumns: 1,
                rightColumns: 1
            },
            columnDefs: [{
                    "targets": [0], // your case first column
                    "className": "text-center",
                },
                {
                    "targets": '_all',
                    "createdCell": function(td, cellData, rowData, row, col) {
                        $(td).css('padding', '3px');
                    }
                }
            ],
            ajax: baseurl + "ticket/get_ticket_progress?ticket_id=" + ticket_id,
            columns: [{
                    "data": "ticket_id",
                    "width": "2%",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "username", "width": "3%" },
                { "data": "timestamp", "width": "3%" },
                { "data": "description_progress", "width": "20%" }
            ]
        });

    });


}); // end of document ready function