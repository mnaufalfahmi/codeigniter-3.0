// jquery start
var option;
var status_incident;
var incident_id;
var select_incident = [];
$(document).ready(function() {

    $.fn.dataTable.ext.errMode = 'none';

    // untuk show hide attch ticket 
    function show_total_ticket(param2) {

        if (param2 == 0) {

            return '';
        } else {

            return param2;
        }
    }

    $('.select-type').change(function(e) {
        e.preventDefault();
        option = this.value;

        select_incident = [];

    });

    // declare variable option select
    var option = $('.select-type').children("option:selected").val();

    // tabel merge incident 
    var table_merge = $('#merge_incident').DataTable({
        // dom: 'flrtpi',
        dom: 'flrtpi Rl',
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },

        ],
        responsive: true,
        lengthChange: false,
        pageLength: 10,
        destroy: true,
        processing: true, //Feature control the processing indicator.
        serverSide: true, //Feature control DataTables' server-side processing mode.
        ajax: {
            "url": baseurl + 'incident/list_merge_incident',
            "type": "POST",
            "data": function(d) {
                d.type = option;
            }
        },
        columnDefs: [{
                "targets": [0], // your case first column
                "className": "text-center",
            },
            {
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '5px');
                    $(td).attr('data-toggle', 'tooltip');
                    $(td).attr('title', cellData);
                }
            },
        ],
        columns: [{
                "width": "5%",
                "data": "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "start_time" },
            { "data": "creator" },
            { "data": "description" },
            // {
            //     render: function(data, type, row, meta) {
            //         return show_total_ticket(row.total_ticket);
            //     }
            // },
            { "data": "incident_id", "width": "50%" },
        ],
    });

    // create selected tr merge incident     
    $('#merge_incident tbody').on('click', 'tr', function() {

        if ($(this).hasClass('selected')) {
            // jika diselect
            $(this).removeClass('selected');

        } else {
            // jika remove selected
            table_merge.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');

        }

        $('#button').click(function() {
            table_merge.row('.selected').remove().draw(false);
        });

    });
    // end of merge incident 

});