// global variabel 
var type_filter = "token";
var pesan;
var status_incident;
var akses;
var status_close;
var token = [];
var start = 0;
var mttr_incident;
var arr_color = ['bg-success', 'bg-warning', 'bg-info', 'bg-completed', 'bg-danger'];
// var glob_mttr;
// var glob_response;
var glob_analisis = 0;
// var glob_recovery = [];
// var glob_confirmation = [];
// var glob_total_analisis;
// var glob_total_recovery;
// var glob_total_confirmation;
// var glob_token = [];
// var arrMention = [];
$(document).ready(function() {

    // var api_url = 'http://localhost/fms-api/uploads/incidents/';
    var api_url = 'http://10.14.22.86/fms-api/uploads/incidents/';
    // var api_url = baseurl + '/fms-api/uploads/incidents/';
    // var api_url = baseurlapi + '/uploads/incidents/';

    $(window).on('beforeunload', function() {
        // return 'Are you sure you want to leave?';
        // alert('Are you sure you want to leave');
        source.close();
        source_kpi.close();
    });

    activeClass('incident', 'open-incident');

    $.fn.dataTable.ext.errMode = 'none';

    $('[data-toggle="tooltip"]').tooltip();

    // config datepicker
    $('#datepicker-component').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('.select2').select2({
        allowClear: true,
    });

    // edit incident 
    $("#select_root_cause").remoteChained({
        parents: "#select_classification",
        url: baseurl + "ticket/load_root_cause"
    });

    $("#select_root_cause_detail").remoteChained({
        parents: "#select_root_cause",
        url: baseurl + "ticket/load_root_cause_detail"
    });

    $("#site_name1").remoteChained({
        parents: "#region1",
        url: baseurl + "ticket/load_incident_regions1"
    });

    $("#link_start_site").remoteChained({
        parents: "#link_start_region",
        url: baseurl + "ticket/load_incident_regions_start"
    });

    $("#link_end_site").remoteChained({
        parents: "#link_end_region",
        url: baseurl + "ticket/load_incident_regions_end"
    });

    $("#brand1").remoteChained({
        parents: "#site_name1",
        url: baseurl + "ticket/load_brands"
    });

    $("#me1").remoteChained({
        parents: "#brand1",
        url: baseurl + "ticket/load_me"
    });

    // func show and hide in classification of define incidents
    $('#select_classification').on('change', function() {
        if (this.value) {

            $('#region1').val(null).trigger('change');
            $('#link_start_region').val(null).trigger('change');
            $('#link_end_region').val(null).trigger('change');
            $('#site_name1').empty();
            $('#brand1').empty();
            $('#me1').empty();
            $('#link_start_site').empty();
            $('#link_end_site').empty();
            $('#select_root_cause_detail').select2('val', [0]);
            $('#11').show()

        }

        if (this.value == '151' || this.value == '121' || this.value == '171' || this.value == '161' || this.value == '661' || this.value == '131' || this.value == '411' || this.value == '666') {
            $('#1').show()
            $('#3').hide()
                // $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOT")
            $('#source_category_input').val("FOT")
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }
        if (this.value == '251' || this.value == '241' || this.value == '141') {
            $('#1').show()
            $('#3').hide()
                // $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("PS")
            $('#source_category_input').val("PS")
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }
        if (this.value == '111') {
            $('#1').hide()
            $('#3').show()
                // $('#11').show()
            $('#foc-show').show()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOC")
            $('#source_category_input').val("FOC")
            $('#div_link_upstream_isp').hide()
            $('#div_region').hide()
            $('#div_site_name').hide()
            $('#broken_fo').val('1')
        }
        if (this.value == '381' || this.value == '361' || this.value == '888') {
            $('#1').hide()
            $('#3').hide()
                // $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').hide()
            $('#div_link_upstream_isp').show()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }


        if (this.value == '999') {
            $('#1').hide()
            $('#3').hide()
                // $('#11').hide()
            $('#foc-show').hide()
                // $('#foc-hide').show()
            $('#source_category').hide()
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }
        if (this.value == '221') {
            $('#1').hide()
            $('#3').show()
                // $('#11').show()
            $('#foc-show').show()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOC")
            $('#source_category_input').val("FOC")
            $('#div_link_upstream_isp').hide()
            $('#div_region').hide()
            $('#div_site_name').hide()
            $('#broken_fo').val('1')
        }
    })

    $('#link_upstream_isp').on('change', function() {

        if (this.value) {
            $('#region1').val(null).trigger('change');
            $('#region').val(null).trigger('change');
            $('#link_start_region').val(null).trigger('change');
            $('#link_end_region').val(null).trigger('change');
            $('#site_name1').empty();
            $('#site_name').empty();
            $('#brand').empty();
            $('#me').empty();
            $('#link_start_site').empty();
            $('#link_end_site').empty();
        }

        // kondisi jika pilih link upstream ISP bagian source category 
        if (this.value == '1') {
            $('#1').show()
            $('#3').hide()
                // $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("FOT")
            $('#foc-hide').show()
            $('#foc-show').hide()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }

        if (this.value == '2') {
            $('#1').hide()
            $('#3').show()
                // $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("FOC")
            $('#foc-hide').hide()
            $('#foc-show').show()
            $('#div_region').hide()
            $('#div_site_name').hide()
            $('#broken_fo').val('1')
        }

        if (this.value == '3') {
            $('#1').show()
            $('#3').hide()
                // $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("PS")
            $('#foc-hide').show()
            $('#foc-show').hide()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }


    })

    // on load browser 
    status_incident = $('.incident_progress').data('status');
    // var t_ticket_progress = $('#ticket_progress').DataTable({
    //     dom: 'flrtpi',
    //     responsive: true,
    //     lengthChange: false,
    //     pageLength: 10,
    //     destroy: true,
    //     columnDefs: [{
    //             "targets": [0], // your case first column
    //             "className": "text-center",
    //         },
    //         {
    //             "targets": '_all',
    //             "createdCell": function(td, cellData, rowData, row, col) {
    //                 $(td).css('padding', '3px');
    //             }
    //         }
    //     ]
    // });

    // check inside or outside plan 
    $('.form_close').find("input[name='physical_inv_change1']").click(function() {
        if ($(this).prop("checked") == true) {
            var inv = $(this).val();

            if ($('.form_close').find("input[name='physical_inv_change2']").prop("checked") == true) {
                $('.form_close').find("input[name='physical_inv_change']").val(3);
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
            }

            $('.inv_change_desc').show();
        } else if ($(this).prop("checked") == false) {

            if ($('.form_close').find("input[name='physical_inv_change2']").prop("checked") == false) {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
                $('.inv_change_desc').hide();
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(2);
            }
        }
    });

    $('.form_close').find("input[name='physical_inv_change2']").click(function() {
        if ($(this).prop("checked") == true) {

            var inv = $(this).val();
            if ($('.form_close').find("input[name='physical_inv_change1']").prop("checked") == true) {
                $('.form_close').find("input[name='physical_inv_change']").val(3);
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
            }
            $('.inv_change_desc').show();
        } else if ($(this).prop("checked") == false) {
            if ($('.form_close').find("input[name='physical_inv_change1']").prop("checked") == false) {
                $('.form_close').find("input[name='physical_inv_change']").val(inv);
                $('.inv_change_desc').hide();
            } else {
                $('.form_close').find("input[name='physical_inv_change']").val(1);
            }
        }
    });
    // end of check inside or outside plan 

    // select2 select_configuration_change on click
    $('.select_configuration_change').change(function() {
        var selectedObj = $(this).children("option:selected").val();

        if (selectedObj == 'no' || selectedObj == '') {
            $('.configurasi_change_activity').hide();
        } else {
            $('.configurasi_change_activity').show();
        }
    });

    if (member != 'NOC') {
        $('.btn-request-stopclock').css('display', 'none');
    }

    // fillin value id incident 
    $('.form_create_message').attr('id', 'form-' + id);
    $('.form_create_message').find("input[name='id']").val(id);
    $('.form_close').find("input[name='id']").val(id);
    // $('.form_create_message').find("textarea[name='message']").attr('disabled', true);
    // set id incident button summary
    $('.btn-incident-summary').attr("data-incident", id);
    // set id progress incident participant 
    $('.scroll-incident-progress').attr('id', 'progress-' + id);
    // set id incident participant 
    $('.list-incident-participant').attr('id', 'participant-' + id);
    // set total participant 
    $('.participants').attr('id', 'total_participant-' + id);
    // set id incident mttr 
    $('.mttr_incident').attr('id', 'mttr-' + id);
    // set id token incident 
    $('.token_incident').attr('id', 'token-' + id);
    $('.token_class').attr('id', 'token_id-' + id);
    // set kpi incident 
    $('.response_time_new').attr('id', 'response_time_new-' + id);
    $('.analisis_time_new').attr('id', 'analisis_time_new-' + id);
    $('.recovery_time_new').attr('id', 'recovery_time_new-' + id);
    $('.mttr_time_new').attr('id', 'mttr_time_new-' + id);
    $('.confirmation_time_new').attr('id', 'confirmation_time_new-' + id);
    // upload file incident 
    $('.btn-attach-incident').attr('data-id', id);
    // set id form upload attach incident 
    $('.form_attach_file_incident').find("input[name='incident_id']").val(id);
    // set id incident progress count 
    $('.total_incident_progress').attr('id', 'total_pregress_' + id);
    // set id incident log 
    $('.btn-log').attr('data-id', id);
    // set id incident log 
    $('.status_incident').attr('id', 'status-' + id);
    // add last progress
    $('.last_progress').attr('id', 'last_progress-' + id);
    // get list incident
    fn_list_incident()
        // add id merge
    $('.btn_merge').attr('id', 'btn_merge-' + id);
    fn_progress_maps(id);
    fn_sse_data(id);
    fn_sse_data_kpi(id);
    // fn_get_kpi(id);

    // click list incident 
    $('.scroll-incident-list').on('click', 'a.btn_list', function(e) {
        e.preventDefault();

        source.close();
        source_kpi.close();
        // stream.close();

        id = $(this).data('id');
        status_incident = $(this).data('status');

        // add attribute id in form create message 
        $('.form_create_message').attr('id', 'form-' + id);
        // remove class active incident 
        $('.scroll-incident-list').find('.btn_list').removeClass('active_incident');
        // set id incident in form create message
        $('.form_create_message').find("input[name='id']").val(id);
        // set id incident form close incident 
        $('.form_close').find("input[name='id']").val(id);
        // set id incident value in button stop clock
        $('.btn-duration').attr('data-id', id);
        // set id incident in button summary 
        $('.btn-incident-summary').attr("data-incident", id);
        // set incident progress id
        $('.scroll-incident-progress').attr('id', 'progress-' + id);
        // incident participant id
        $('.list-incident-participant').attr('id', 'participant-' + id);
        // set id incident in html total incident partipant
        $('.participants').attr('id', 'total_participant-' + id);
        // set id incident in incident mttr 
        $('.mttr_incident').attr('id', 'mttr-' + id);
        // set id incident in token incident 
        $('.token_incident').attr('id', 'token-' + id);
        $('.token_class').attr('id', 'token_id-' + id);
        $('.form_history').find("input[name='id']").val(id);
        // config kpi incident 
        $('.response_time_new').attr('id', 'response_time_new-' + id);
        $('.analisis_time_new').attr('id', 'analisis_time_new-' + id);
        $('.recovery_time_new').attr('id', 'recovery_time_new-' + id);
        $('.mttr_time_new').attr('id', 'mttr_time_new-' + id);
        $('.confirmation_time_new').attr('id', 'confirmation_time_new-' + id);
        // upload file incident 
        $('.btn-attach-incident').attr('data-id', id);
        $('.form_attach_file_incident').find("input[name='incident_id']").val(id);
        // set id incident progress count 
        $('.total_incident_progress').attr('id', 'total_pregress_' + id);
        // set id incident log 
        $('.btn-log').attr('data-id', id);
        // set incident progress merge 
        $('.status_incident').attr('id', 'status-' + id);
        // add last progress
        $('.last_progress').attr('id', 'last_progress-' + id);

        $(this).addClass("active_incident");

        // add id merge
        $('.btn_merge').attr('id', 'btn_merge-' + id);


        fn_progress_maps(id);
        fn_sse_data(id);
        fn_sse_data_kpi(id);

        // source.open();
        // source_kpi.open();
        // stream.open();


    });
    // end of click list incident 


    // get activeon load browser
    function fn_get_active(p1, p2, status, ttr) {

        // cek status incident 
        // alert(p1+' - '+p2+' - '+status+' - '+ ttr)
        if (p1 == p2) {
            return 'active_incident';
        } else {
            if (status === 'close') {
                return 'close_incident'; // grey
            } else if (status === 'clear') {
                return 'ready_incident'; // green
            } else {
                if (ttr > 14400) {
                    return 'ttr_incident'; // red
                } else {
                    if (status === 'stop') {
                        return 'stop_incident';
                    } else {
                        return '';
                    }
                }
            }
        }
    }


    // var table cm 
    var t_cm = $('#tb_cm').DataTable({
        lengthChange: false,
        dom: 'frtpi',
        pageLength: 10,
        columnDefs: [{
                "targets": [0], // your case first column
                "className": "text-center",
                "width": "20px"
            },
            {
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                    $(td).attr('title', cellData);
                }
            }
        ]
    });

    // var table alarm 
    var t_alarm = $('#tb_alarm').DataTable({
        dom: 'frtpi Rl',
        lengthChange: false,
        // responsive: true,
        pageLength: 10,
        // scrollX: true,
        columnDefs: [{
                "targets": [0], // your case first column
                "className": "text-center",
                "width": "20px"
            },
            {
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                    $(td).attr('title', cellData);
                }
            }
        ]
    });

    var t_assets = $('#tb_assets').DataTable({
        dom: 'frtpi Rl',
        lengthChange: false,
        // responsive: true,
        // scrollX: true,
        pageLength: 10,
        columnDefs: [{
                "targets": [0], // your case first column
                "className": "text-center",
                "width": "20px"
            },
            {
                "targets": [4], // your case first column
                "width": "50px"
            },
            {
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                    $(td).attr('title', cellData);
                }
            }
        ]
    });

    // tombol cm klik
    $('.btn-cm').click(function(e) {
        e.preventDefault();

        $('#modalcm').modal('show');

        var sid = $('.sid').attr('data-sid');
        // var sid = '01000071778';

        $.ajax({
            url: baseurl + 'incident/integration/',
            type: 'POST',
            data: { sid: sid, modul: 'cm' },
            dataType: 'JSON',
            async: true,
            success: function(data) {

                // jika data tidak ada 
                if (data.msg !== 'Config File not found') {

                    var no = 1;
                    t_cm.clear().draw();

                    $.each(data, function(i, item) {

                        t_cm.row.add([
                            (i + no),
                            item.no_so,
                            item.service_id,
                            item.id_so,
                            item.name,
                            '<a href="' + item.link + '" target="_blank">' + item.link + '</a>'
                        ]).draw(false);

                    });
                }
            }
        });
    });


    // tombol alarm klik
    $('.btn-alarm').click(function(e) {
        e.preventDefault();

        $('#modalalarm').modal('show');

        var sid = $('.sid').attr('data-sid');
        // var sid = '01000071778';

        $.ajax({
            url: baseurl + 'incident/integration/',
            type: 'POST',
            data: { sid: sid, modul: 'alarm' },
            async: true,
            dataType: 'JSON',
            success: function(data) {
                // jika data tidak ada 
                if (data.msg !== 'Config File not found') {
                    var no = 1;
                    t_cm.clear().draw();

                    $.each(data, function(i, item) {

                        t_alarm.row.add([
                            (i + no),
                            item.id,
                            item.trig_id,
                            item.eventid,
                            item.zabb,
                            item.priority,
                            item.hostid,
                            item.ipaddr,
                            item.host_,
                            item.description,
                            item.sid,
                            item.clock,
                            item.ack,
                            item.msg,
                            item.indate,
                        ]).draw(false);

                    });
                }
            }
        });
    });

    // integration 
    $('.btn-assets').click(function(e) {
        e.preventDefault();

        $('#modalassets').modal('show');

        var sid = $('.sid').attr('data-sid');
        // var sid = '140001576';

        $.ajax({
            url: baseurl + 'incident/integration/',
            type: 'POST',
            data: { sid: sid, modul: 'assets' },
            async: true,
            dataType: 'JSON',
            success: function(data) {

                if (data.msg !== 'Customer not found') {
                    var no = 1;
                    t_assets.clear().draw();
                    $.each(data, function(i, item) {
                        t_assets.row.add([
                            (i + no),
                            // item.rownum,
                            // item.id,
                            // item.code,
                            item.name,
                            item.phone,
                            item.pic,
                            item.pic_phone,
                            item.pic_email,
                            item.desc,
                            item.username,
                            item.updated_at,
                            item.sts
                        ]).draw(false);
                    });
                }
            }
        });
    });


    // btn ticket detail click function 
    $(document).on('click', '.btn-ticket-detail', function() {
        var ticket_id = $(this).attr('data-ticket_id');

        $('#modalTicketDetail').modal('show');
        $.ajax({
            url: baseurl + '/ticket/get_ticket_detail?ticket_id=' + ticket_id,
            type: 'GET',
            dataType: "JSON",
            async: true,
            success: function(data) {
                // console.log(data)
                $('.form_ticket_detail').find('input[name="ticket_id"]').val(data.ticket_id);
                $('.form_ticket_detail').find('input[name="ismilling_id"]').val(data.ismilling_id);
                $('.form_ticket_detail').find('input[name="service_id"]').val(data.service_id);
                $('.form_ticket_detail').find('input[name="open_time"]').val(data.create_ticket);
                $('.form_ticket_detail').find('input[name="attach_time"]').val(data.attach_time);
                $('.form_ticket_detail').find('textarea[name="description"]').val(data.desc_ticket);
            }
        });
    });

    // display ticket progress 
    $(document).on('click', '.btn-ticket', function() {
        var ticket_id = $(this).attr('data-ticket');
        var sid = $(this).attr('data-service');

        $('.sid').attr('data-sid', sid);
        $('.btn-progress').attr('data-ticket_id', ticket_id);
        $('.btn-ticket-detail').attr('data-ticket_id', ticket_id);

        var t_ticket_progress = $('#ticket_progress').DataTable({
            dom: 'flrtpi Rl',
            responsive: true,
            lengthChange: false,
            pageLength: 10,
            destroy: true,
            ajax: baseurl + "incident/ticket_progress?ticket_id=" + ticket_id,
            columnDefs: [{
                    "targets": [0], // your case first column
                    "className": "text-center",
                },
                {
                    "targets": '_all',
                    "createdCell": function(td, cellData, rowData, row, col) {
                        $(td).css('padding', '3px');
                        $(td).attr('data-toggle', 'tooltip');
                        $(td).attr('title', cellData);
                    }
                }
            ],
            columns: [{
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "username" },
                { "data": "timestamp" },
                // { "data": "ticket_id" },
                // { "data": "ismilling_id" },
                // { "data": "service_id" },
                { "data": "description_progress" },
            ]
        });
    });

    // console.log(group)

    // mention data
    $('#created_message').suggest('@', {
        data: group,
        map: function(user) {
            return {
                value: user.group_code,
                text: '<strong>' + user.group_code + '</strong>'
            }
        }

    });


    // hide alert 
    $('#ok').click(function(e) {
        e.preventDefault();
        $('#modalerror').modal('hide');
    });

    //form create message 
    $('.form_create_message').submit(function(e) {
        e.preventDefault();

        // $('.btn-submit-span').css('display', 'none');

        var val_message = $('.form_create_message').find("textarea[name='message']").val();
        var form_validasi = $('.select-process').val();

        // check session checin and mod
        if (member !== 'SERPO') {
            if (user_checkin === 0 || user_checkin === "0" || typeof user_checkin === "undefined") {
                pesan = "Please Check in ";
                fn_show_alert(pesan);
                return false;
            }
        }

        // check message is empty
        if (val_message === '') {
            // $('.content_alert').html('Please Enter Message');
            pesan = 'Pelase Enter Message';
            fn_show_alert(pesan);
            return false;
        }

        source.close();
        // stream.close();
        source_kpi.close();

        //display preeleoad
        $('.preeload').css('display', '');

        $.ajax({
            type: 'POST',
            data: new FormData(this),
            url: baseurl + 'incident/created_progress',
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            async: true,
            success: function(data) {
                // $('.btn-submit-span').css('display', '');
                // arrMention = [];
                // Server.send( 'kpi', data);
                // glob_mttr = data.kpi.mttr.to_duration_int;
                // glob_analisis = data.kpi.analisis;
                // glob_recovery = data.kpi.recovery;
                // glob_confirmation = data.kpi.confirmation_time;
                // glob_token = data.token;

                // $.each(glob_token, function(i, contents) {
                //     arrMention.push(contents.mention_to);
                // });

                if (data.status !== 'error') {

                    $('#penyebab_gangguan').val('');
                    $('#solusi_perbaikan').val('');
                    $('#lokasi').val('');
                    $('#perangkat').val('');
                    $('#jam_operasional').val('');

                    if (username == data.data.username.toLowerCase()) {
                        var c_progress = `
                        <hgroup class="speech-bubble-front shadow messages" data-messages="` + data.data.username + ` | ` + data.data.group_name + `"  style="margin-top:7px">
                        <div class="d-flex w-100 justify-content-between" style="padding: 10px;">
                        <small class="mb-1" style="text-transform:uppercase"><strong>` + data.data.username + ` | ` + data.data.group_name + ` | ` + data.data.process_desc + `</strong></small><small>` + data.data.time_message + `</small></div>
                        <p style="padding-bottom: 12px;padding-left: 11px;white-space: pre-wrap;">` + data.data.message + ' ' + fn_check_lat_lng(data.data.lat, data.data.lng) + '<a href="' + baseurl + 'incident/download_file?id=' + data.data.id_file + '" class="text-dark" target="_blank"> ' + fn_check_attachment(data.data.file_name) + '</a></p>' +
                            fn_check_gambar(data.data.file_name, data.data.type, path) + `</p></hgroup>`;
                    } else {
                        var c_progress = `
                        <hgroup class="speech-bubble3 shadow messages" data-messages="` + data.data.username + ` | ` + data.data.group_name + `" style="margin-top:7px">
                        <div class="d-flex w-100 justify-content-between" style="
                        padding: 10px;
                        "><small class="mb-1" style="text-transform:uppercase"><strong>` + data.data.username + ` | ` + data.data.group_name + ` | ` + data.data.process_desc + `</strong></small><small>` + data.data.time_message + `</small></div>
                        <p style="
                        padding-bottom: 12px;
                        padding-left: 11px;
                        white-space: pre-wrap;
                        ">` + data.data.message + ' ' + fn_check_lat_lng(data.data.lat, data.data.lng) + '. <a href="' + baseurl + 'incident/download_file?id=' + data.data.id_file + '" class="text-dark" target="_blank"> ' + fn_check_attachment(data.data.file_name) + '</a></p>' +
                            fn_check_gambar(data.data.file_name, data.data.type, path) + `</p></hgroup>`;
                    }

                    $('#progress-' + id).append(c_progress);

                    $('.form_create_message').find("textarea[name='message']").val("");
                    $('.form_create_message').find("input[name='userfile']").val("");
                    $('.select-process').select2("val", ['none']);

                } else {

                    // if error 
                    pesan = data.message;
                    fn_show_alert(pesan);

                }

                $('.preeload').css('display', 'none');
                fn_sse_data(id);
                fn_sse_data_kpi(id);

                // source.open();
                // source_kpi.open();
                // stream.open();

                Server.send('notification2', data);

            }

        });

        $('.delete_file').click();

        // source.open();
        // stream.open();
        // source_kpi.open();

    });
    // end of function


    // close ticket incident 
    $('.btn-close').click(function() {
        var id = $('.form_close').find("input[name='id']").val();
        var message;

        $.ajax({
            url: baseurl + 'incident/close_permission?id=' + id,
            type: 'POST',
            dataType: 'JSON',
            async: false,
            success: function(data) {
                status_close = data.status;
                message = data.message;
            }
        });

        // check session checkin and mod
        if (status_close == false) {
            // pesan = "Incident not ready to close";
            pesan = message;
            fn_show_alert(pesan);
            return false;
        }

        // check session checin and mod
        if (user_checkin === 0 || user_checkin === "0" || typeof user_checkin === "undefined") {
            pesan = "Please Check in ";
            fn_show_alert(pesan);
            return false;
        }

        // check account permission 
        // if (member == 'CONTACTCENTER' || member == 'SERPO') {
        if (member == 'SERPO' || member == "CONTACTCENTER") {
            pesan = "Your Account Cannot Close Incident ";
            fn_show_alert(pesan);
            return false;
        }

        $('#modalCloseIncident').modal('show');

        $.ajax({
            url: baseurl + 'incident/get_summary_incident?id=' + id,
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                var root_cause = "<option value=" + data.root_cause_id + ">" + data.root_cause_name + "</option>";
                var root_cause_detail = "<option value=" + data.root_causes_detail_id + ">" + data.root_cause_detail_name + "</option>";
                var link = "<option value=" + data.link + ">" + data.link + "</option>";
                var site_name = "<option value=" + data.site_name_id + ">" + data.site_name + "</option>";
                if (data.source_category == null || data.source_category == '') {
                    var sc = `<option value="FOT">FOT</option>
                    <option value="FOC">FOC</option>
                    <option value="PS">PS</option>`;
                } else {
                    var sc = "<option value=" + data.source_category + ">" + data.source_category + "</option>";
                }

                $('.form_close').find("input[name='creator']").val(data.creator);
                $("#select_classification").select2('val', [data.classification_id]);
                $("#select_root_cause").html(root_cause);
                $("#select_root_cause_detail").html('val', [data.root_causes_detail_id]);
                $("#link").html(link);
                $("#region1").select2('val', [data.region_code]);
                $(".form_close").find("textarea[name='description']").val(data.description);
                $('#' + data.network).attr('checked', 'checked');
                $('#' + data.priority).attr('checked', 'checked');
                $('#site_name1').html(site_name);
                $('#source_category_input').select2('val', [data.source_category]);

            }
        });

    });


    // close incident 
    $('.form_close').submit(function(e) {
        e.preventDefault();

        var incident_id = $('.form_close').find("input[name='id']").val();

        $('.form_create_message').find("textarea[name='message']").val('Incident Close');

        $.ajax({
            url: baseurl + 'incident/close_incident_prosses',
            data: new FormData(this),
            type: 'POST',
            dataType: 'JSON',
            async: true,
            contentType: false,
            processData: false,
            success: function(data) {

                if (data.status === 'error') {
                    pesan = data.prosses;
                    fn_show_alert(pesan)
                    $('#modalCloseIncident').modal('hide');
                    return false;
                } else {
                    pesan = data.prosses;
                    fn_show_alert(pesan)
                    $('#modalCloseIncident').modal('hide');

                    $('.form_create_message').find("textarea[name='message']").val("");
                    $('.btn-duration').attr('data-id', incident_id).css("display", 'none');
                    $('.btn-attach-incident').css("display", 'none');
                    $('.form_create_message').css('display', 'none');
                    $('.btn-close').css('display', 'none');

                    // reset form close 
                    $('.form_close').trigger('reset');
                    $('.configurasi_change_activity').select2("val", ['']);
                    $('.select_configuration_change').select2("val", ['no']);
                    return false;
                }

            }
        });
    });
    // end of close incident 

    // select reason type
    function select_reason_type() {
        $.ajax({
            url: baseurl + 'incident/incident_reason_stopclock',
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                $('.select_reason_type').empty();

                $.each(data, function(i, item) {
                    var html = "<option value=" + item.id + ">" + item.reason + "</option>";
                    $('.select_reason_type').append(html);
                })

            }
        });
    }

    // start clock
    // $('.btn-duration').click(function(e) {
    $('.btn-stopclock-incident').click(function(e) {
        e.preventDefault();

        var id = $(this).attr('data-id'),
            type = $(this).attr('data-type'),
            status = $(this).attr('data-status');

        // Jika group SERPO
        if (member === 'SERPO') {
            pesan = "You're account cannot stop incident";
            fn_show_alert(pesan);
            return false;
        }

        // Jika group SBU
        if (member === 'SBU' || member === 'CONTACTCENTER') {

            if (jQuery.inArray(groupcode, token) !== -1) {

                $.ajax({
                    url: baseurl + 'incident/get_summary_incident?id=' + id,
                    type: 'GET',
                    dataType: 'JSON',
                    async: true,
                    success: function(data) {

                        $('.form_stop_clock').find("input[name='id']").val(id);
                        $('.form_stop_clock').find("input[name='type']").val(type);
                        $('.form_stop_clock').find("input[name='status']").val(status);
                        $('.form_stop_clock').find("textarea[name='description']").val(data.description);
                        // $('.form_stop_clock').find("input[name='duration']").val($('#mttr-' + id + ' > strong').html());
                        $('.form_stop_clock').find("textarea[name='reason']").val('');

                        select_reason_type();

                        // jika klik tombol start
                        if (type === '1') {
                            // tampilkan modal
                            // $('.btn-form-stop').click();
                            $.ajax({
                                url: baseurl + 'incident/duration',
                                type: 'POST',
                                data: {
                                    id: id,
                                    type: type,
                                    status: status,
                                    reason: '',
                                    reason_type: '',
                                    request_duration_hours: '00',
                                    request_duration_minutes: '00',
                                },
                                success: function(data) {
                                    pesan = "Incident Start";
                                    fn_show_alert(pesan);
                                    glob_mttr = data.mttr.to_duration_int;
                                    glob_recovery = data.recovery;
                                    glob_analisis = data.analisis;
                                    glob_confirmation = data.confirmation_time;
                                }
                            });
                        } else {
                            // klik tombol submit form
                            $('#modalStopClock').modal('show');
                        }

                    }
                });

            } else {
                pesan = "You're account cannot stop incident";
                fn_show_alert(pesan);
            }

            return false;
        }

        // Jika group NOC
        if (member === 'NOC') {

            $.ajax({
                url: baseurl + 'incident/get_summary_incident?id=' + id,
                type: 'GET',
                dataType: 'JSON',
                async: true,
                success: function(data) {

                    $('.form_stop_clock').find("input[name='id']").val(id);
                    $('.form_stop_clock').find("input[name='type']").val(type);
                    $('.form_stop_clock').find("input[name='status']").val(status);
                    $('.form_stop_clock').find("textarea[name='description']").val(data.description);
                    // $('.form_stop_clock').find("input[name='duration']").val($('#mttr-' + id + ' > strong').html());
                    $('.form_stop_clock').find("textarea[name='reason']").val('');

                    select_reason_type();

                    // jika klik tombol start
                    if (type === '1') {
                        // tampilkan modal
                        // $('.btn-form-stop').click();
                        $.ajax({
                            url: baseurl + 'incident/duration',
                            type: 'POST',
                            data: {
                                id: id,
                                type: type,
                                status: status,
                                reason: '',
                                reason_type: '',
                                request_duration_hours: '00',
                                request_duration_minutes: '00',
                            },
                            success: function(data) {
                                pesan = "Incident Start";
                                fn_show_alert(pesan);
                                glob_mttr = data.mttr.to_duration_int;
                                glob_recovery = data.recovery;
                                glob_analisis = data.analisis;
                                glob_confirmation = data.confirmation_time;
                            }
                        });

                    } else {

                        // klik tombol submit form
                        $('#modalStopClock').modal('show');
                    }

                }
            });

            return false;

        }

    });

    // submit stop clock
    $('.form_stop_clock').submit(function(e) {
        e.preventDefault();

        var access_stopclock = ['NOC', 'SBU'];
        $('#modalStopClock').modal('hide');

        var id = $(this).find("input[name='id']").val(),
            type = $(this).find("input[name='type']").val();

        // } else if (type == "0") {
        if (type == "0" || type === "0") {

            if (member === 'NOC') {
                $.ajax({
                    url: baseurl + 'incident/duration',
                    type: 'POST',
                    dataType: 'JSON',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: true,
                    success: function(data) {
                        t_request_stopclock.ajax.reload();
                        status_incident = 'stop';
                        glob_mttr = data.mttr.to_duration_int;
                        glob_recovery = data.recovery;
                        glob_analisis = data.analisis;
                        glob_confirmation = data.confirmation_time;
                    }

                });

                pesan = "Incident Stop";
                fn_show_alert(pesan);
            }


        }

        // request stop clock
        // if (jQuery.inArray(member, access_stopclock) == -1) {
        if (member !== 'NOC') {
            $.ajax({
                url: baseurl + 'incident/request_stopclock',
                type: 'POST',
                dataType: 'JSON',
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: true,
                success: function(data) {
                    // alert(data)
                    fn_show_alert(data);
                    t_request_stopclock.ajax.reload();
                }

            });
            return false;
        }


    });

    // check file attacment incident progress (message on incident)
    function fn_check_attachment(file_name) {
        if (file_name === null) {
            return '';
        } else {
            return '<strong><i class="fa fa-folder"></i> Download File </strong>';
        }
    }

    // check image in incident progress (message on incident)
    function fn_check_gambar(file_name, type, path) {

        if (file_name === null) {
            return '';
        } else {
            var p_type = type.split('/');

            if (p_type[0] === 'image') {
                return '<img src="' + path + '" style="height:100px">';
            } else {
                return ''
            }
        }

    }


    // get message location 
    $(document).on('click', '.get_location', function(e) {
        e.preventDefault();
        var lat = $(this).attr('data-lat');
        var lng = $(this).attr('data-lng');

        fn_message_maps(lat, lng);

        $('#modalSerpoLocation').modal('show');

    });


    // check lat long null
    function fn_check_lat_lng(lat, lng) {
        var lat_lng;
        if (lat == 0 || lat == null) {
            // lat_lng = "Tap to open lat long " + lat + "" + lng;
            lat_lng = "";
        } else {
            lat_lng = "<a href='#' class='text-dark get_location' data-lat=" + lat + " data-lng=" + lng + ">Tap to open location : " + lat + " " + lng + "</a>";
        }

        return lat_lng;
    }

    // fn time 
    function get_puluhan(p) {
        if (p < 10) {
            return '0' + p;
        } else {
            return p;
        }
    }


    function fn_check_undifined(p) {
        if (p == 'undifined') {
            return '';
        } else {
            return p;
        }
    }

    // incident progress attach file upload 
    $('#attach').change(function(e) {
        var fileName = e.target.files[0].name;
        if (fileName != '') {
            $('.input-file').show();
            $('.input-file').html('<strong>' + fileName + '</strong> has been selected <button type="button" class="delete_file"><i class="fa fa-times"></i></button>');
        }
        // alert('The file "' + fileName +  '" has been selected.');
    });
    // end of incident file upload 

    // delete upload file incident 
    $('p').on('click', '.delete_file', function(e) {
        e.preventDefault();
        $('#attach').val("");
        $('.input-file').hide();
    });
    // end of delete


    // incident attach file upload
    $('#attach_incident_file').change(function(e) {
        var fileName = e.target.files[0].name;
        if (fileName != '') {
            $('.form_attach_file_incident').find("input[name='incident_file_text']").val(fileName);
        }
        // alert('The file "' + fileName +  '" has been selected.');
    });

    // button attach incident file click
    $('.btn-attach-incident').click(function(e) {
        e.preventDefault();

        var id_incident = $(this).attr('data-id');

        $.ajax({
            type: 'GET',
            url: baseurl + 'incident/get_summary_incident?id=' + id_incident,
            dataType: 'JSON',
            async: true,
            success: function(data) {
                $('.incident-info').html(
                    '<p>' + data.incident_id + ' | ' + data.description + '</p>'
                );

                $('#modalAttachFileIncident').modal('show');

                fn_dt_incident_file_attachment(id_incident);

            }
        });

    });
    // end of 

    $('.btn-progress-summary').click(function(e) {
        e.preventDefault();
        var sum_process;
        var sum_process_detail;
        var html;
        var html_detail;
        // $('#modalProgressSummary').modal('show');
        $('#modalSummaryProgress').modal('show');

        // get summary progress active timeline 
        $.ajax({
            type: 'GET',
            url: baseurl + 'incident/summary_progress_active/' + id,
            dataType: 'JSON',
            async: true,
            success: function(data) {
                // console.log(data.process_detail)

                // header timeline 
                $('#progressbar').empty();
                sum_process = data.process;
                $.each(sum_process, function(i, item) {
                    html = '<span class="btn_kpi_duration" data-value="' + item.process + '"><li class="text-center ' + item.status + '" id="' + item.process + '"><strong>' + item.description + '</strong><span class="kpi_duration">' + item.duration + '</span></li></span>';
                    $('#progressbar').append(html);
                });

                $('#progressbar').append(
                    '<span class="btn_kpi_duration" data-value="all"><li class="text-center btn_kpi_duration" id="all"><strong>All</strong><span class="kpi_duration"></span></li></span>'
                );


                //summary prosses detail
                $('.summary_progress_detail').empty();
                sum_process_detail = data.process_detail;
                $.each(sum_process_detail, function(i, item) {
                    html_detail = '<div class="card mt-1 shadow">' +
                        '<div class="card-body">' +
                        '<div class="row">' +
                        '<div class="col-lg-6"><strong>' + item.username.toUpperCase() + ' | ' + item.group_name.toUpperCase() + ' | ' + item.process_name.toUpperCase() + '</strong></div>' +
                        '<div class="col-lg-6"><small class="float-right">' + item.time_message + '</small></div>' +
                        '</div><p style="white-space:pre-wrap; font-size:11px">' +
                        item.message + ' ' + fn_check_lat_lng(item.lat, item.lng)
                    '</p></div>' +
                    '</div>"'

                    $('.summary_progress_detail').append(html_detail);
                });

            }
        });

    });


    $(document).on('click', '.btn_kpi_duration', function() {
        var process = $(this).attr('data-value');

        $.ajax({
            type: 'POST',
            url: baseurl + 'incident/summary_progress_active_process/' + id + '/' + process,
            async: true,
            dataType: 'JSON',
            cache: false,
            success: function(data) {
                $('.summary_progress_detail').empty();
                $.each(data, function(i, item) {
                    html_detail = '<div class="card mt-2 shadow">' +
                        '<div class="card-body">' +
                        '<div class="row">' +
                        '<div class="col-lg-6"><strong>' + item.username.toUpperCase() + ' | ' + item.group_name.toUpperCase() + ' | ' + item.process_name.toUpperCase() + '</strong></div>' +
                        '<div class="col-lg-6"><small class="float-right">' + item.time_message + '</small></div>' +
                        '</div><p style="white-space:pre-wrap; font-size:11px">' +
                        item.message + '</p></div>' +
                        '</div>'

                    $('.summary_progress_detail').append(html_detail);
                });

            }
        });

    });

    // incident file attachment 
    $('.form_attach_file_incident').submit(function(e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: baseurl + 'incident/upload_file_incident',
            async: true,
            data: new FormData(this),
            dataType: 'JSON',
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                fn_dt_incident_file_attachment(data)
            }
        });
    });
    // end of 

    // incident file attachment 
    $(document).on('click', '.btn_remove_file_incident', function(e) {
        e.preventDefault();

        var id = $(this).attr("data-file"),
            incident_id = $(this).attr("data-incident_id");

        $.ajax({
            url: baseurl + 'incident/remove_file_incident?id=' + id + '&incident_id=' + incident_id,
            type: 'GET',
            dataType: 'JSON',
            success: function(data) {
                fn_dt_incident_file_attachment(data)
            }
        });
    });
    // end of 


    // incident file attachment 
    function fn_dt_incident_file_attachment(id) {
        // DataTable list incident open
        $('#tb_incident_file_attach').DataTable({
            dom: 'flrtpi',
            lengthChange: false,
            searching: false,
            pageLength: 10,
            destroy: true,
            ajax: baseurl + 'incident/get_file_attach_incident?id=' + id,
            columnDefs: [{
                    "targets": [0, 3], // your case first column
                    "className": "text-center",
                },
                {
                    "targets": "_all",
                    "createdCell": function(td, cellData, rowData, row, col) {
                        $(td).css('padding', '3px');
                    }
                },
            ],
            columns: [{
                    "data": "id",
                    "width": "4%",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "uploader", "wdith": "5%" },
                {
                    render: function(data, type, row, meta) {
                        return '<a class="text-dark" target="_blank" href="' + api_url + row.filename + '">' + row.filename + '</a>';
                    }
                },
                {
                    "width": "10%",
                    render: function(data, type, row, meta) {
                        return '<a class="btn btn-xs btn-info text-white shadow mr-2" target="_blank" title="Download File" href="' + api_url + row.filename + '"><i class="fa fa-download"></i></a>' +
                            '<a class="btn btn-xs btn-danger text-white shadow btn_remove_file_incident" data-toggle="tooltip" title="Remove File" href="#" data-file="' + row.id + '" data-incident_id ="' + id + '"><i class="fa fa-trash"></i></a>';
                    }
                },
                // { "data": "description" },
            ],
        });
    }

    // incident summary 
    $('.btn-incident-summary').click(function(e) {
        e.preventDefault();
        var incident_id = $(this).attr('data-incident');

        // axios get data incident by id 
        axios.get(baseurl + 'incident/get_summary_incident?id=' + incident_id).then(function(response) {
                response = response.data;

                $("#mod_row_incidentId").val(response.incident_id);
                $("#mod_row_creator").val(response.creator);
                $("#mod_row_notificationTrigger").val(response.creator);
                $("#mod_row_state").val(response.status);
                $("#mod_row_clasification").val(response.classification_name);
                $("#mod_row_rootCause").val(response.root_cause_name);
                $("#mod_row_rootCauseDetail").val(response.root_cause_detail);
                $("#mod_row_description").val(response.description);
                $("#mod_row_hirarchy").val(response.hierarchy);
                $("#mod_row_link").val(response.link);
                $("#mod_row_link_start_region").val(response.link_start_region);
                $("#mod_row_link_end_region").val(response.link_end_region);
                $("#mod_row_link_start_site").val(response.link_start_site);
                $("#mod_row_link_end_site").val(response.link_end_site);
                $("#mod_row_region").val(response.region_name);
                $("#mod_row_site").val(response.site_name);
                $("#mod_source_category").val(response.source_category);
                $("#mod_row_creationTime").val(response.start_time);
                $("#mod_row_duration").val(response.duration);
                $("#mod_row_stopLock").val(response.stopclock);
                $("#mod_row_ttr").val(response.ttr);

                $("#modalSummary").modal("show");

            })
            .catch(function(error) {
                console.log(error)
            });
    });


    // function convert numeric to time 
    function fn_time(number) {

        var sisa_waktu = number % 3600;
        var h = Math.floor(number / 3600);
        var m = Math.floor(sisa_waktu / 60);
        var s = Math.floor(sisa_waktu % 60);
        var hasil = get_puluhan(h) + ':' + get_puluhan(m) + ':' + get_puluhan(s);
        if (number == 0 || number == null) {
            var data = '00:00:00';
        } else {
            var data = hasil;
        }
        return data;

    }

    $('.btn-download').click(function(e) {
        e.preventDefault();
        window.open(baseurl + 'incident/get_pdf?id=' + id);
    })

    // get message maps
    function fn_message_maps(lat, lng) {

        $('.header-maps').html('Coodinate Location');

        $('#lat').val(lat);
        $('#long').val(lng);

        var myLatLng = {
            lat: parseFloat(lat),
            lng: parseFloat(lng)
        };
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: myLatLng
        });
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            animation: google.maps.Animation.DROP,
            title: 'coordinate location',
        });

    }

    function fn_progress_maps(id) {
        $('.btn-serpo-location').click(function(e) {
            e.preventDefault();

            $.ajax({
                url: baseurl + 'incident/get_maps?id=' + id,
                type: 'GET',
                dataType: 'JSON',
                success: function(data) {
                    $.each(data, function(i, item) {
                        $('#lat').val(item.latitude);
                        $('#long').val(item.longitude);

                        var test = {
                            lat: -6.21462,
                            lng: 106.84513
                        };
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 10,
                            center: test
                        });
                        var myLatLng = {
                            lat: parseFloat(item.latitude),
                            lng: parseFloat(item.longitude)
                        };
                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            animation: google.maps.Animation.DROP,
                            title: 'serpo location',
                        });
                    })

                }
            })
        })
    }


    // display request stop clock modal 
    $('.btn-request-stopclock').click(function(e) {
        e.preventDefault();

        $('#modalRequestStopClock').modal('show');

        t_request_stopclock.ajax.reload();
    });


    function fn_show_alert(pesan) {
        $('#modalerror').modal('show');
        $('.content_alert').html(pesan);
    }

    $("#myProgress").keyup(function() {
        // Retrieve the input field text and reset the count to zero
        var filter = $(this).val(),
            count = 0;

        // Loop through the comment list
        $(".scroll-incident-progress").find('.messages').each(function() {
            // If the list item does not contain the text phrase fade it out
            // if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            if ($(this).data('messages').search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut();
                // Show the list item if the phrase matches and increase the count by 1
            } else {
                $(this).show();
                count++;
            }
        });

    });

    // integration autocheck 
    $('.btn-autocheck').click(function(e) {
        e.preventDefault();

        $('#modalautocheck').modal('show');

        var sid = $('.sid').attr('data-sid');

        var frameElement = frameElement = document.getElementById("frameAutocheck");
        frameElement.src = 'http://10.14.22.210:96/api/cm/autocheck/' + sid;

    });

    // auto height textarea 
    var textarea = document.querySelector('textarea');

    textarea.addEventListener('keydown', autosize);

    function autosize() {
        var el = this;
        setTimeout(function() {
            el.style.cssText = 'height:auto; padding:0';
            el.style.cssText = 'height:' + el.scrollHeight + 'px';
        }, 0);
    }


    // button mlog incident 
    $('.btn-log').click(function(e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        $('#modallog').modal('show');

        var t_log = $('#tb_log').DataTable({
            dom: 'flrtpi Rl',
            responsive: true,
            lengthChange: false,
            pageLength: 10,
            destroy: true,
            ajax: baseurl + 'incident/log/list_log_incident?id=' + id,
            columnDefs: [{
                    "targets": [0], // your case first column
                    "className": "text-center",
                    "width": "25px",
                },
                {
                    "targets": [1], // your case first column
                    "width": "50px",
                },
                {
                    "targets": [3, 5, 2], // your case first column
                    "width": "100px",
                },
                {
                    "targets": '_all',
                    "createdCell": function(td, cellData, rowData, row, col) {
                        $(td).css('padding', '3px');
                        $(td).attr('data-toggle', 'tooltip');
                        $(td).attr('title', cellData);
                    }
                }
            ],
            columns: [{
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "username" },
                { "data": "group_code" },
                { "data": "action" },
                { "data": "remarks" },
                { "data": "created_on" },
            ]
        });

    });


    // progress 
    $('.btn-progress').click(function(e) {
        e.preventDefault();
        var incident_merge = $(this).attr('data-incident');
        var ticket_id = $(this).attr('data-ticket_id');

        $.ajax({
            type: 'POST',
            url: baseurl + 'incident/merge/get_merge_incident_by_ticket',
            data: { ticket_id: ticket_id },
            dataType: 'JSON',
            async: true,
            success: function(data) {
                // console.log(data)

                if (data == null) {
                    pesan = "There's no history incident";
                    fn_show_alert(pesan);
                    return false;
                } else {
                    var url = baseurl + 'incident/progress/' + data.id;
                    window.open(url);
                }

            }

        });

    });


    // select filter
    $('.select-filter').on('change', function(e) {
        e.preventDefault();

        type_filter = this.value;

        fn_list_incident()

    });

    // select role process 
    $.ajax({
        url: baseurl + 'group/role_process?group_code=' + groupcode,
        type: 'POST',
        dataType: 'JSON',
        async: true,
        success: function(data) {
            var html;
            // console.log(data);
            $('.select-process').empty()
            $('.select-process').append("<option value='none' selected>None</option>")
            $.each(data, function(i, item) {
                html = "<option value=" + item.process + ">" + item.description + "</option>";
                $('.select-process').append(html)
            })
        }
    });


    // display modal serpo location
    $('.btn-serpo-location').click(function(e) {
        e.preventDefault();

        $('#modalSerpoLocation').modal('show');

    });


    // get list incident 
    function fn_list_incident() {

        start = 0;

        $.ajax({
            url: baseurl + 'incident/get_list_incident?start=' + start + '&limit=' + 10 + '&filter=' + type_filter,
            type: 'GET',
            dataType: 'JSON',
            async: true,
            success: function(data) {

                $('.scroll-incident-list').empty();

                var raw_data = data.data;

                $.each(raw_data, function(i, item) {

                    var l_incident = '<a href="#" class="list-group-item list-group-item-action flex-column align-items-start btn_list mt-1 ' + fn_get_active(item.id, id, item.status, item.ttr) + '" data-status="' + item.status + '" data-id="' + item.id + '" data-toggle="tooltip" title="' + item.description + '" data-ttr="' + item.ttr + '">' +
                        '<div class="d-flex w-100 justify-content-between">' +
                        '<small class="mb-1 text-secondary">' + item.incident_id + '</small>' +
                        '</div>' +
                        '<p class="mb-1 incident-desc">' + item.description.substr(0, 50) + '...</p>' +
                        '<small class="text-secondary">' + item.open_time + '</small>' +
                        '</a>'

                    $('.scroll-incident-list').append(l_incident);

                });

                $('.count_incident').html(
                    'INCIDENT LIST (' + data.total + ')'
                );

            }
        })
    }


    // get list incident after scroll 
    $('.scroll-incident-list').on('scroll', function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {

            start = start + 10;

            $.ajax({
                url: baseurl + 'incident/get_list_incident?start=' + start + '&limit=' + 10 + '&filter=' + type_filter,
                type: 'GET',
                dataType: 'JSON',
                async: true,
                success: function(data) {
                    var raw_data = data.data;

                    $.each(raw_data, function(i, item) {

                        var l_incident = '<a href="#" class="list-group-item list-group-item-action flex-column align-items-start btn_list mt-1 ' + fn_get_active(item.id, id, item.status, item.ttr) + '" data-status="' + item.status + '" data-id="' + item.id + '" data-toggle="tooltip" title="' + item.description + '" data-ttr="' + item.ttr + '">' +
                            '<div class="d-flex w-100 justify-content-between">' +
                            '<small class="mb-1 text-secondary">' + item.incident_id + '</small>' +
                            '</div>' +
                            '<p class="mb-1 incident-desc">' + item.description.substr(0, 50) + '...</p>' +
                            '<small class="text-secondary">' + item.open_time + '</small>' +
                            '</a>'

                        $('.scroll-incident-list').append(l_incident);


                    });

                    $('.count_incident').html(
                        'INCIDENT LIST (' + data.total + ')'
                    );
                }
            })

        }
    });

    $('.btn-merge').on('click', function() {
        $.ajax({
            url: baseurl + 'incident/merge/get_merge_reason',
            type: 'GET',
            dataType: 'JSON',
            success: function(data) {
                $('.select-merge-reason').empty();
                $.each(data, function(i, item) {
                    var option = '<option value=' + item.id + '>' + item.reason + '</option>';
                    $('.select-merge-reason').append(option);
                });
            }
        });
    });

    // save merge incident click
    $('#btn_merge-' + id).click(function(e) {
        e.preventDefault();

        $('.btn_merge').css('display', 'none');

        $('#merge_incident > tbody').each(function(index) {
            var incident_select = $(this).find('.selected > td:last').text();
            var incident_current = id_incident;
            var merge_reason = $('.select-merge-reason').val();

            if (merge_reason == '' || merge_reason == null) {
                var pesan = 'Merge Reason cannot Empty !!';
                // fn_show_alert(pesan)
                alert(pesan);
                return false;
            }

            if (incident_select == '' || incident_select == null) {
                var pesan = 'Merge Reason cannot Empty !!';
                // fn_show_alert(pesan)
                alert(pesan);
                return false;
            }

            if (incident_select === incident_current) {
                alert('Cannot Merge This Incident');
                return false;
            }

            $.ajax({
                type: 'POST',
                data: {
                    id: incident_current,
                    id_select: incident_select,
                    merge_reason: merge_reason
                },
                dataType: 'JSON',
                url: baseurl + 'incident/merge_incident',
                success: function(data) {
                    option = $('.select-type').children("option:selected").val();

                    $('#modalMergeIncident').modal('hide');
                    $('.btn_merge').css('display', '');
                    // $('#modalLogDetail').modal('hide');
                    // tb_incident.ajax.reload(null, false);
                    // get_list_merge_incident(option)

                }
            });

        });

    });

    // function all data sse
    function fn_sse_data_kpi(id) {
        // set global total
        // var total = 0;
        // function all data sse
        if (typeof(EventSource) !== "undefined") {
            source_kpi = new EventSource(baseurlapi + 'incident/sse/api_incident_kpi?incident_id=' + id + '&token=' + getCookie('token'))
                // source_kpi = new EventSource(baseurl + 'incident/sse/kpi?incident_id=' + id)
                // source_kpi = new EventSource('http://10.14.22.85:3000/incident/kpi?incident_id=' + id)
                // source_kpi = new EventSource('http://127.0.0.1:3000/incident/kpi?incident_id=' + id)
            source_kpi.onmessage = function(event) {
                var result_data = JSON.parse(event.data);
                var r_kpi = result_data.data;
                // var r_token = r_kpi.token;
                var analisis = r_kpi.analisis;
                var recovery = r_kpi.recovery;
                // var confirmation = r_kpi.confirmation_time;
                var arr_color = ['bg-success', 'bg-warning', 'bg-info', 'bg-completed', 'bg-danger'];
                var no_analisis = 0;
                var no_recovery = 0;
                var analisis_progress;
                var recovery_progress;
                // var confirmation_progress;

                // Display token incident
                // $('#token-' + id).empty();
                // $('#token_id-' + id).empty();
                // token = [];

                // $.each(r_token, function(i, item_token) {
                //     // view token 
                //     var data_token = '<span><strong>' + fn_check_undifined(item_token.mention_to) + '&nbsp; </strong>' +
                //         '<span style="display:block"><small>' + fn_check_undifined(item_token.username) + '</small></span></span>';
                //     var input_token = '<input type="hidden" name="token[]" value="' + fn_check_undifined(item_token.mention_to) + '"/>';

                //     $('#token-' + id).append(data_token);
                //     $('#token_id-' + id).append(input_token);

                //     token.push(item_token.mention_to);

                // });
                // end of token incident 

                // Display KPI Incident 
                // $('#response_time_new-' + id).empty();
                $('#analisis_time_new-' + id).empty();
                $('#recovery_time_new-' + id).empty();
                $('#mttr_time_new-' + id).empty();
                $('#mttr-' + id).empty();
                // $('#confirmation_time_new-' + id).empty();


                // response time 
                // $('#response_time_new-' + id).html(
                //     '<div data-toggle="tooltip" data-placement="bottom" title="' + r_kpi.response.response + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (r_kpi.response.response_int / 10) * 100 + '%" aria-valuenow="' + r_kpi.response.response_int + '" aria-valuemin="0" aria-valuemax="10">' +
                //     '<strong><span class="text-white mr-2 ml-2">' + r_kpi.response.response + '</span></strong>' +
                //     '</div>'
                // );

                // analisis time 
                $.each(analisis, function(i, item) {

                    if (no_analisis !== r_kpi.total_analisis) {
                        analisis_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[no_analisis] + '" role="progressbar" style="width: ' + (item.to_duration_int / 10) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                            '<strong><span class="text-white mr-2 ml-2">' + item.initial + '</span></strong>' +
                            '</div>'
                        $('#analisis_time_new-' + id).append(analisis_progress);
                    }

                    no_analisis = no_analisis + 1;
                });


                // recovery time 
                $.each(recovery, function(i, item) {

                    if (no_recovery !== r_kpi.total_recovery) {
                        recovery_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[no_recovery] + '" role="progressbar" style="width: ' + (item.to_duration_int / 120) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                            '<strong><span class="text-white mr-2 ml-2">' + item.to_duration + '</span></strong>' +
                            '</div>'
                        $('#recovery_time_new-' + id).append(recovery_progress);
                    }

                    no_recovery = no_recovery + 1;
                });

                // MTTR time 
                $('#mttr_time_new-' + id).html(
                    '<div data-toggle="tooltip" data-placement="bottom" title="' + r_kpi.mttr.to_duration + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (r_kpi.mttr.to_duration_int / 240) * 100 + '%" aria-valuenow="' + r_kpi.mttr.to_duration_int + '" aria-valuemin="0" aria-valuemax="240">' +
                    '<strong><span class="text-white mr-2 ml-2">' + r_kpi.mttr.to_duration + '</span></strong>' +
                    '</div>'
                );

                // MTTR time 
                $('#mttr-' + id).html("<strong><span style='font-size:17px'>" + r_kpi.mttr.to_duration + "</span></strong>");

                // Confirmation time
                // $.each(confirmation, function(i, item) {
                //     confirmation_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (item.to_duration_int / 10) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                //         '<strong><span class="text-white mr-2 ml-2">' + item.to_duration + '</span></strong>' +
                //         '</div>'
                //     $('#confirmation_time_new-' + id).append(confirmation_progress);
                // });
                // end of KPI incident 


                // set mttr incident 
                mttr_incident = r_kpi.mttr.to_duration_int;

            }
        }
    }

    // function all data sse incident progress
    function fn_sse_data(id) {
        // set global total
        var total = 0;
        // function all data sse
        if (typeof(EventSource) !== "undefined") {
            // source = new EventSource(baseurl + 'incident/sse?incident_id=' + id, {
            source = new EventSource(baseurlapi + 'incident/sse/api_incident?incident_id=' + id + '&token=' + getCookie('token'))
                // source = new EventSource('http://10.14.22.85:3000/incident/progress?incident_id=' + id)
                // source = new EventSource('http://127.0.0.1:3000/incident/progress?incident_id=' + id)
            source.onmessage = function(event) {
                var result_data = JSON.parse(event.data);
                var r_progress = result_data.progress;
                var r_token = result_data.token;
                var r_participan = result_data.participant;
                // var r_kpi = result_data.kpi;
                var r_ticket = result_data.ticket;
                var process;

                // display incident progress
                if (result_data.total !== total) {

                    // reset total
                    total = 0;

                    $('#progress-' + id).empty();
                    $('#total_pregress_' + id).empty();
                    $('#last_progress-' + id).empty();

                    // parsing count data analisis
                    if (Array.isArray(result_data.process_analyst) && result_data.process_analyst.length) {
                        // alert('ada')
                    } else {
                        // if ($(".select-process").val('analyst')) {

                        //     // $(".select-process").append(`<option value="analyst">Analisa</option>`);
                        // } else {}
                    }

                    $.each(result_data.process_analyst, function(i, item) {
                        if (group_id == item.group_id) {
                            if (parseInt(item.count) >= 1) {
                                $(".select-process option[value='analyst']").remove();
                            }
                        }
                    });

                    // parsing data message / incident progress
                    $.each(r_progress, function(i, item) {

                        // counter total message
                        total = total + 1;

                        // process 
                        if (item.process === null) {
                            process = "NO PROCESS";
                        } else {
                            process = item.process;
                        }

                        var path = api_url + item.file_name;
                        // console.log(item.username.toLowerCase())
                        if (username == item.username.toLowerCase()) {
                            var c_progress = `
                    <hgroup class="speech-bubble-front shadow messages" data-messages="` + item.username + ` | ` + item.name + `"  style="margin-top:7px">
                        <div class="d-flex w-100 justify-content-between" style="padding: 10px;">
                        <small class="mb-1" style="text-transform:uppercase"><strong>` + item.username + ` | ` + item.name + ` | ` + process + `</strong></small><small>` + item.time_message + `</small></div>
                            <p style="padding-bottom: 12px;padding-left: 11px;white-space: pre-wrap;">` + item.message + ' ' + fn_check_lat_lng(item.lat, item.lng) + '<a href="' + baseurl + 'incident/download_file?id=' + item.id_file + '" class="text-dark" target="_blank"> ' + fn_check_attachment(item.file_name) + '</a></p>' +
                                fn_check_gambar(item.file_name, item.type, path) + `</p></hgroup>`;
                        } else {
                            var c_progress = `
                    <hgroup class="speech-bubble3 shadow messages" data-messages="` + item.username + ` | ` + item.name + `" style="margin-top:7px">
                        <div class="d-flex w-100 justify-content-between" style="
                            padding: 10px;
                        "><small class="mb-1" style="text-transform:uppercase"><strong>` + item.username + ` | ` + item.name + ` | ` + process + `</strong></small><small>` + item.time_message + `</small></div>
                            <p style="
                            padding-bottom: 12px;
                            padding-left: 11px;
                            white-space: pre-wrap;
                        ">` + item.message + ' ' + fn_check_lat_lng(item.lat, item.lng) + '. <a href="' + baseurl + 'incident/download_file?id=' + item.id_file + '" class="text-dark" target="_blank"> ' + fn_check_attachment(item.file_name) + '</a></p>' +
                                fn_check_gambar(item.file_name, item.type, path) + `</p></hgroup>`;
                        }

                        $('#progress-' + id).prepend(c_progress);

                    });

                    // total message incident progress
                    $('#total_pregress_' + id).html(
                        "INCIDENT PROGRESS (" + total + ")"
                    );

                    // Last Progress
                    $('#last_progress-' + id).html(
                        "LAST PROGRESS  : " + result_data.last_progress
                    );

                    // auto scroll bottom
                    $('#progress-' + id).scrollTop($('#progress-' + id)[0].scrollHeight);

                }
                //  end of display incident progress


                // Display token incident
                $('#token-' + id).empty();
                $('#token_id-' + id).empty();
                token = [];

                $.each(r_token, function(i, item_token) {
                    // view token 
                    var data_token = '<span><strong>' + fn_check_undifined(item_token.mention_to) + '&nbsp; </strong>' +
                        '<span style="display:block"><small>' + fn_check_undifined(item_token.username) + '</small></span></span>';
                    var input_token = '<input type="hidden" name="token[]" value="' + fn_check_undifined(item_token.mention_to) + '"/>';

                    $('#token-' + id).append(data_token);
                    $('#token_id-' + id).append(input_token);

                    token.push(item_token.mention_to);

                });
                // end of token incident 


                // Display status incident 
                $('#status-' + id).html('<strong>' + result_data.status_incident + '&nbsp;&nbsp;<i class="fa fa-check-circle-o"></i></strong>');
                status_incident = result_data.status_incident;

                //Display lock incident 
                $('.lock_incident').empty();
                if (result_data.lock_by === null || result_data.lock_by === '' || result_data.lock_by == 'null') {
                    $('.lock_incident').html(``);
                    $('.btn-merge').removeAttr('disabled', 'disabled');
                    $('.btn-merge').css('display', '');
                } else {
                    if (username === result_data.lock_by) {
                        $('.lock_incident').html(`lock by : ` + result_data.lock_by);
                        $('.btn-merge').removeAttr('disabled', 'disabled');
                        $('.btn-merge').css('display', '');
                    } else {
                        $('.lock_incident').html(`lock by : ` + result_data.lock_by);
                        $('.btn-merge').attr('disabled', 'disabled');
                        $('.btn-merge').css('display', '');

                    }
                }

                $('.incident_detail').empty();
                $('.incident_detail').val(result_data.description);

                // check status
                if (result_data.status_incident == 'stop') {
                    $('#form-' + id).css('display', 'none');
                    akses = ["NOC", "SBU", "CONTACTCENTER"];
                    if (jQuery.inArray(member, akses) !== -1) {
                        $('.btn-stopclock-incident').css('display', '');
                        $('.btn-stopclock-incident').html("<strong><i class='fa fa-lock'></i>&nbsp; Start Clock</strong>");
                    } else {
                        $('.btn-stopclock-incident').css('display', 'none');
                        $('.btn-stopclock-incident').html("<strong><i class='fa fa-lock'></i>&nbsp; Start Clock</strong>");
                    }
                    $('.btn-stopclock-incident').attr('data-id', id);
                    $('.btn-stopclock-incident').attr('data-type', 1);
                    $('.btn-stopclock-incident').attr('data-status', 'open');
                    // $('.btn-request-stopclock').css('display', 'none');
                    $('.btn-close').css('display', 'none');
                    $('.btn-merge').css('display', 'none');
                } else if (result_data.status_incident == 'close') {
                    $('#form-' + id).css('display', 'none');
                    $('.btn-stopclock-incident').css('display', 'none');
                    // $('.btn-request-stopclock').css('display', 'none');
                    $('.btn-close').css('display', 'none');
                    $('.btn-merge').css('display', 'none');
                } else if (result_data.status_incident == 'request') {
                    $('#form-' + id).css('display', '');
                    $('.btn-stopclock-incident').css('display', 'none');
                    // $('.btn-stopclock-incident').html("<strong><i class='fa fa-lock'></i>&nbsp; Stop Clock</strong>");
                    // $('.btn-stopclock-incident').attr('data-id', id);
                    // $('.btn-stopclock-incident').attr('data-type', 0);
                    // $('.btn-stopclock-incident').attr('data-status', 'stop'); // $('.btn-request-stopclock').css('display', 'none');
                    akses = ["NOC", "SBU"];
                    if (jQuery.inArray(member, akses) !== -1) {
                        $('.btn-close').css('display', '');
                    } else {
                        $('.btn-close').css('display', 'none');
                    }
                } else if (result_data.status_incident == 'merge') {
                    $('#form-' + id).css('display', 'none');
                    $('.btn-merge').css('display', 'none');
                    akses = ["NOC", "SBU"];
                    if (jQuery.inArray(member, akses) !== -1) {
                        $('.btn-stopclock-incident').css('display', '');
                        $('.btn-stopclock-incident').html("<strong><i class='fa fa-lock'></i>&nbsp; Stop Clock</strong>");
                        $('.btn-close').css('display', '');
                    } else {
                        $('.btn-stopclock-incident').css('display', 'none');
                        $('.btn-stopclock-incident').html("<strong><i class='fa fa-lock'></i>&nbsp; Stop Clock</strong>");
                        $('.btn-close').css('display', 'none');
                    }
                    $('.btn-stopclock-incident').attr('data-id', id);
                    $('.btn-stopclock-incident').attr('data-type', 1);
                    $('.btn-stopclock-incident').attr('data-status', 'open');
                    // $('.btn-request-stopclock').css('display', '');
                } else if (result_data.status_incident == 'clear') {
                    $('#form-' + id).css('display', 'none');
                    $('.btn-stopclock-incident').css('display', 'none');
                    // $('.btn-stopclock-incident').css('display', '');
                    // $('.btn-stopclock-incident').html("<strong><i class='fa fa-lock'></i>&nbsp; Stop Clock</strong>");
                    // $('.btn-stopclock-incident').attr('data-id', id);
                    // $('.btn-stopclock-incident').attr('data-type', 0);
                    // $('.btn-stopclock-incident').attr('data-status', 'stop');
                    akses = ["NOC", "SBU"];
                    if (jQuery.inArray(member, akses) !== -1) {
                        $('.btn-close').css('display', '');
                    } else {
                        $('.btn-close').css('display', 'none');
                    }
                    $('.btn-merge').css('display', 'none');
                } else {
                    $('#form-' + id).css('display', '');
                    $('.btn-stopclock-incident').html("<strong><i class='fa fa-lock'></i>&nbsp; Stop Clock</strong>");
                    $('.btn-stopclock-incident').css('display', '');
                    $('.btn-stopclock-incident').attr('data-id', id);
                    $('.btn-stopclock-incident').attr('data-type', 0);
                    $('.btn-stopclock-incident').attr('data-status', 'stop');
                    // $('.btn-request-stopclock').css('display', '');
                    akses = ["NOC", "SBU"];
                    if (jQuery.inArray(member, akses) !== -1) {
                        $('.btn-close').css('display', '');
                    } else {
                        $('.btn-close').css('display', 'none');
                    }
                }


                // Display perticipant 
                $('#participant-' + id).empty();
                $('#total_participant-' + id).empty();

                var tot_paticipant = 0;

                $.each(r_participan, function(i, item) {

                    tot_paticipant = tot_paticipant + 1;

                    var list_participant = '<a href="#" class="list-group-item list-group-item-action" style="padding-top:5px; padding-bottom:5px">' +
                        '<div class="row">' +
                        '<div class="col-sm-2">' +
                        '<p style="font-size:25px; margin-top:4px"><i class="fa fa-user-circle"></i></p>' +
                        '</div>' +
                        '<div class="col-sm-10">' +
                        '<p class="text-secondary" style="font-size:12px; text-transform:capitalize">' + item.username + '</p>' +
                        '<p class="text-secondary" style="font-size:12px">' + item.name + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</a>';

                    $('#participant-' + id).append(list_participant);

                });

                $('#total_participant-' + id).html(
                    'PARTICIPANTS (' + tot_paticipant + ')'
                );
                // end of participant


                // Display KPI Incident 
                // var analisis = r_kpi.analisis;
                // var recovery = r_kpi.recovery;
                // var confirmation = r_kpi.confirmation_time;
                // var arr_color = ['bg-success', 'bg-warning', 'bg-info', 'bg-completed', 'bg-danger'];
                // var analisis_progress;
                // var recovery_progress;
                // var confirmation_progress;
                // var no_analisis = 0;
                // var no_recovery = 0;
                // var no_confirmation = 0;

                // $('#response_time_new-' + id).empty();
                // $('#analisis_time_new-' + id).empty();
                // $('#recovery_time_new-' + id).empty();
                // $('#mttr_time_new-' + id).empty();
                // $('#mttr-' + id).empty();
                // $('#confirmation_time_new-' + id).empty();


                // // response time 
                // $('#response_time_new-' + id).html(
                //     '<div data-toggle="tooltip" data-placement="bottom" title="' + r_kpi.response.response + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (r_kpi.response.response_int / 10) * 100 + '%" aria-valuenow="' + r_kpi.response.response_int + '" aria-valuemin="0" aria-valuemax="10">' +
                //     '<strong><span class="text-white mr-2 ml-2">' + r_kpi.response.response + '</span></strong>' +
                //     '</div>'
                // );

                // // analisis time 
                // $.each(analisis, function(i, item) {

                //     if (no_analisis !== r_kpi.total_analisis) {
                //         analisis_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[no_analisis] + '" role="progressbar" style="width: ' + (item.to_duration_int / 10) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                //             '<strong><span class="text-white mr-2 ml-2">' + item.initial + '</span></strong>' +
                //             '</div>'
                //         $('#analisis_time_new-' + id).append(analisis_progress);
                //     }

                //     no_analisis = no_analisis + 1;
                // });


                // // recovery time 
                // $.each(recovery, function(i, item) {

                //     if (no_recovery !== r_kpi.total_recovery) {
                //         recovery_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[no_recovery] + '" role="progressbar" style="width: ' + (item.to_duration_int / 120) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                //             '<strong><span class="text-white mr-2 ml-2">' + item.to_duration + '</span></strong>' +
                //             '</div>'
                //         $('#recovery_time_new-' + id).append(recovery_progress);
                //     }

                //     no_recovery = no_recovery + 1;
                // });

                // // MTTR time 
                // $('#mttr_time_new-' + id).html(
                //     '<div data-toggle="tooltip" data-placement="bottom" title="' + r_kpi.mttr.to_duration + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (r_kpi.mttr.to_duration_int / 240) * 100 + '%" aria-valuenow="' + r_kpi.mttr.to_duration_int + '" aria-valuemin="0" aria-valuemax="240">' +
                //     '<strong><span class="text-white mr-2 ml-2">' + r_kpi.mttr.to_duration + '</span></strong>' +
                //     '</div>'
                // );

                // // MTTR time 
                // $('#mttr-' + id).html("<strong><span style='font-size:17px'>" + r_kpi.mttr.to_duration + "</span></strong>");

                // // Confirmation time
                // $.each(confirmation, function(i, item) {
                //     confirmation_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + item.to_duration + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (item.to_duration_int / 10) * 100 + '%" aria-valuenow="' + item.to_duration_int + '" aria-valuemin="0" aria-valuemax="10">' +
                //         '<strong><span class="text-white mr-2 ml-2">' + item.to_duration + '</span></strong>' +
                //         '</div>'
                //     $('#confirmation_time_new-' + id).append(confirmation_progress);
                // });
                // end of KPI incident 


                // Dsiplay ticket incident 
                var t_data = r_ticket;
                var count = 0;

                $('.list-ticket-attch').empty();

                $.each(t_data, function(i, item) {

                    count = count + 1;
                    var ticket_list2 = `
                    <a title="` + item.description + `" style="` + statusTicket(item.status) + `" href="#" class="list-group-item list-group-item-action flex-column align-items-start mt-1 btn-ticket"  data-service="` + item.service_id + `" data-ticket=` + item.ticket_id + ` data-toggle="modal" data-target="#modalTicketProgress" data-toggle="tooltip">
                        <div class="d-flex w-100 justify-content-between">
                            <p style="font-size:11px">` + item.ticket_id + ` | ` + item.ismilling_id + `</p>
                        </div>
                        <p style="font-size:13px" class="mb-1 incident-desc">` + item.description.substring(0, 100) + `...</p>
                    </a>`;
                    var ticket_list = '<span data-toggle="tooltip" title="' + item.description + '"><a href="#" class="list-group-item list-group-item-action btn-ticket" data-service="' + item.service_id + '" data-ticket=' + item.ticket_id + ' data-toggle="modal" data-target="#modalTicketProgress">' +
                        '<div class="row">' +
                        '<div class="col-sm-2">' +
                        '<p style="font-size:25px"><i class="fa fa-credit-card"></i></p>' +
                        '</div>' +
                        '<div class="col-sm-10">' +
                        '<p style="font-size:11px">' + item.ticket_id + ' | ' + item.ismilling_id + '</p>' +
                        '<p style="font-size:13px">' + item.description.substring(0, 100) + '...</p>' +
                        '</div>' +
                        '</div>' +
                        '</a></span>';

                    $('.list-ticket-attch').append(ticket_list2);

                });

                $('.ticket_attch').html('TICKET ATTACH (' + count + ')');
                // end of ticket incident 

                id_incident = result_data.incident_id;

                // set mttr incident 
                // mttr_incident = r_kpi.mttr.to_duration_int;

            }
        }
    }

    // function get_time(param) {
    //     var hari, jam, menit, detik, sisa;
    //     hari = Math.floor(param / 86400);
    //     sisa = Math.floor(param % 86400);
    //     jam = Math.floor(sisa / 3600);
    //     sisa = Math.floor(sisa % 3600);
    //     menit = Math.floor(sisa / 60);
    //     detik = Math.floor(sisa % 60);

    //     var h = (jam < 10) ? "0" + jam : jam;
    //     var m = (menit < 10) ? "0" + menit : menit;
    //     var s = (detik < 10) ? "0" + detik : detik;

    //     return hari + ' days ' + h + ':' + m + ':' + s;
    // }


    //tangkap apakah ada action dr client manapun
    // Server.bind('kpi', function( data ) {
    //     arrMention = [];
    //     glob_mttr      = data.kpi.mttr.to_duration_int;
    //     glob_analisis  = data.kpi.analisis;
    //     glob_recovery  = data.kpi.recovery;
    //     glob_confirmation = data.kpi.confirmation_time;
    //     glob_token     = data.token;

    //     $.each(glob_token, function(i, contents) {
    //         arrMention.push(contents.mention_to);
    //     });
    // });

    // var b = 1;
    // $.each(glob_token, function(i, contents) {
    //     arrMention.push(contents.mention_to);
    // });

    // setInterval(function() {
    //     $('#confirmation_time_new-' + id).empty();
    //     $('#analisis_time_new-' + id).empty();
    //     $('#recovery_time_new-' + id).empty();
    //     $('#mttr-' + id).empty();
    //     b = b + 1;

    //     if(status_incident == 'close' || status_incident == 'stop') {
    //         var a = parseInt(glob_mttr) * 60;
    //         var c = a;
    //         var to_duration = get_time(c);
    //         var to_duration_int = c;

    //         // MTTR time bagian bawah
    //         $('#mttr_time_new-' + id).html(
    //             '<div data-toggle="tooltip" data-placement="bottom" title="' + to_duration + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (to_duration_int / 14400) * 100 + '%" aria-valuenow="' + to_duration_int + '" aria-valuemin="0" aria-valuemax="240">' +
    //             '<strong><span class="text-white mr-2 ml-2">'+to_duration+'</span></strong>' +
    //             '</div>'
    //             );

    //         // MTTR time bagian atas
    //         $('#mttr-' + id).html("<strong><span style='font-size:17px'>"+to_duration+"</span></strong>");

    //     } else {
    //         var a = parseInt(glob_mttr) * 60;
    //         var c = a + b;
    //         var to_duration = get_time(c);
    //         var to_duration_int = c;

    //         // MTTR time bagian bawah
    //         $('#mttr_time_new-' + id).html(
    //             '<div data-toggle="tooltip" data-placement="bottom" title="' + to_duration + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (to_duration_int / 14400) * 100 + '%" aria-valuenow="' + to_duration_int + '" aria-valuemin="0" aria-valuemax="240">' +
    //             '<strong><span class="text-white mr-2 ml-2">'+to_duration+'</span></strong>' +
    //             '</div>'
    //             );

    //         // MTTR time bagian atas
    //         $('#mttr-' + id).html("<strong><span style='font-size:17px'>"+to_duration+"</span></strong>");

    //     }

    //     // analisis
    //     var no_analisis = 0;
    //     var analisis_progress;
    //     $.each(glob_analisis, function(i, item) {

    //         // kondisi jika status incident close atau stop
    //         if(status_incident == 'close' || status_incident == 'stop') {
    //             var a1 = parseInt(item.to_duration_int) * 60;
    //             var c1 = a1;
    //             var to_analisis = get_time(c1);
    //             var to_analisis_int = c1;

    //             if (no_analisis !== glob_total_analisis) {
    //                 analisis_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + to_analisis + '" class="progress-bar ' + arr_color[no_analisis] + '" role="progressbar" style="width: ' + (to_analisis_int / 600) * 100 + '%" aria-valuenow="' + to_analisis_int + '" aria-valuemin="0" aria-valuemax="10">' +
    //                     '<strong><span class="text-white mr-2 ml-2">' + item.initial + '</span></strong>' +
    //                     '</div>'
    //                 $('#analisis_time_new-' + id).append(analisis_progress);
    //             }

    //             no_analisis = no_analisis + 1;
    //         } else {
    //             var activeToken = arrMention.includes(item.mention_to);
    //             if (activeToken) {
    //                 var a1 = parseInt(item.to_duration_int) * 60;
    //                 var c1 = a1 + b;
    //                 var to_analisis = get_time(c1);
    //                 var to_analisis_int = c1;
    //             } else {
    //                 var a1 = parseInt(item.to_duration_int) * 60;
    //                 var c1 = a1;
    //                 var to_analisis = get_time(c1);
    //                 var to_analisis_int = c1;
    //             }
    //             console.log(to_analisis_int+' '+item.mention_to);
    //             // console.log();
    //             analisis_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + to_analisis + '" class="progress-bar ' + arr_color[no_analisis] + '" role="progressbar" style="width: ' + (to_analisis_int / 600) * 100 + '%" aria-valuenow="' + to_analisis_int + '" aria-valuemin="0" aria-valuemax="10">' +
    //                     '<strong><span class="text-white mr-2 ml-2">' + item.initial + '</span></strong>' +
    //                     '</div>'
    //             $('#analisis_time_new-' + id).append(analisis_progress);

    //             no_analisis = no_analisis + 1;

    //         }
    //     });

    //     // recovery
    //     var no_recovery = 0;
    //     var recovery_progress;
    //     $.each(glob_recovery, function(i, item) {

    //         if(status_incident == 'close' || status_incident == 'stop') {
    //             var a2 = parseInt(item.to_duration_int) * 60;
    //             var c2 = a2;
    //             var to_recovery = get_time(c2);
    //             var to_recovery_int = c2;

    //             if (no_recovery !== glob_total_recovery) {
    //                 recovery_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + to_recovery + '" class="progress-bar ' + arr_color[no_recovery] + '" role="progressbar" style="width: ' + (to_recovery_int / 7200) * 100 + '%" aria-valuenow="' + to_recovery_int + '" aria-valuemin="0" aria-valuemax="10">' +
    //                     '<strong><span class="text-white mr-2 ml-2">' + to_recovery + '</span></strong>' +
    //                     '</div>'
    //                 $('#recovery_time_new-' + id).append(recovery_progress);
    //             }

    //             no_recovery = no_recovery + 1;

    //         } else {
    //             var activeToken = arrMention.includes(item.mention_to);
    //             if (activeToken) {
    //                 var a2 = parseInt(item.to_duration_int) * 60;
    //                 var c2 = a2 + b;
    //                 var to_recovery = get_time(c2);
    //                 var to_recovery_int = c2;
    //             } else {
    //                 var a2 = parseInt(item.to_duration_int) * 60;
    //                 var c2 = a2;
    //                 var to_recovery = get_time(c2);
    //                 var to_recovery_int = c2;
    //             }

    //             recovery_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + to_recovery + '" class="progress-bar ' + arr_color[no_recovery] + '" role="progressbar" style="width: ' + (to_recovery_int / 7200) * 100 + '%" aria-valuenow="' + to_recovery_int + '" aria-valuemin="0" aria-valuemax="10">' +
    //                     '<strong><span class="text-white mr-2 ml-2">' + to_recovery + '</span></strong>' +
    //                     '</div>'
    //                 $('#recovery_time_new-' + id).append(recovery_progress);

    //             no_recovery = no_recovery + 1;
    //         }

    //     });

    //     // Confirmation time
    //     var confirmation_progress;
    //     $.each(glob_confirmation, function(i, item) {
    //         if(status_incident == 'close' || status_incident == 'stop') {
    //             var a3 = parseInt(item.to_duration_int) * 60;
    //             var c3 = a3;
    //             var to_confirmation = get_time(c3);
    //             var to_confirmation_int = c3;

    //             confirmation_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + to_confirmation + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (to_confirmation_int / 600) * 100 + '%" aria-valuenow="' + to_confirmation_int + '" aria-valuemin="0" aria-valuemax="10">' +
    //                 '<strong><span class="text-white mr-2 ml-2">' + to_confirmation + '</span></strong>' +
    //                 '</div>'
    //             $('#confirmation_time_new-' + id).append(confirmation_progress);

    //         } else {
    //             var activeToken = arrMention.includes(item.mention_to);
    //             if(activeToken) {
    //                 var a3 = parseInt(item.to_duration_int) * 60;
    //                 var c3 = a3 + b;
    //                 var to_confirmation = get_time(c3);
    //                 var to_confirmation_int = c3;
    //             } else {
    //                 var a3 = parseInt(item.to_duration_int) * 60;
    //                 var c3 = a3;
    //                 var to_confirmation = get_time(c3);
    //                 var to_confirmation_int = c3;
    //             }

    //             confirmation_progress = '<div data-toggle="tooltip" data-placement="bottom" title="' + item.mention_to + ' \n ' + to_confirmation + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (to_confirmation_int / 600) * 100 + '%" aria-valuenow="' + to_confirmation_int + '" aria-valuemin="0" aria-valuemax="10">' +
    //                     '<strong><span class="text-white mr-2 ml-2">' + to_confirmation + '</span></strong>' +
    //                     '</div>'
    //                 $('#confirmation_time_new-' + id).append(confirmation_progress);
    //         }

    //     });


    // }, 3000);

    // get kpi

    // function fn_get_kpi(id) {

    //     $.ajax({
    //         url: baseurl + 'incident/kpi?id=' + id,
    //         type: 'GET',
    //         dataType: 'JSON',
    //         // async: false,
    //         success: function(result) {

    //             var analisis = result.analisis;
    //             var recovery = result.recovery;
    //             var confirmation = result.confirmation_time;
    //             var response = result.response;
    //             var arr_color = ['bg-success', 'bg-warning', 'bg-info', 'bg-completed', 'bg-danger'];
    //             // set glob variable
    //             // arrMention          = [];
    //             // glob_mttr           = result.mttr.to_duration_int;
    //             // glob_total_analisis = result.total_analisis;
    //             // glob_analisis       = analisis;
    //             // glob_recovery       = recovery;
    //             // glob_total_recovery = result.total_recovery;
    //             // glob_confirmation   = confirmation;
    //             // glob_token          = result.token;

    //             // $.each(glob_token, function(i, contents) {
    //             //     arrMention.push(contents.mention_to);
    //             // });

    //             $('#response_time_new-' + id).empty();
    //             $('#analisis_time_new-' + id).empty();
    //             $('#recovery_time_new-' + id).empty();
    //             $('#confirmation_time_new-' + id).empty();
    //             $('#mttr_time_new-' + id).empty();
    //             $('#mttr-' + id).empty();

    //             // response time 
    //             $('#response_time_new-' + id).html(
    //                 '<div data-toggle="tooltip" data-placement="bottom" title="' + response.response + '" class="progress-bar ' + arr_color[0] + '" role="progressbar" style="width: ' + (response.response_int / 10) * 100 + '%" aria-valuenow="' + response.response_int + '" aria-valuemin="0" aria-valuemax="10">' +
    //                 '<strong><span class="text-white mr-2 ml-2">' + response.response + '</span></strong>' +
    //                 '</div>'
    //             );
    //         }
    //     });
    // }


}); // end of document ready function 

// background status tickets
function statusTicket(status) {
    if (status == 0) {
        return `border-left: 15px solid #dadada`;
    } else if (status == 1) {
        return `border-left: 15px solid rgb(76, 175, 80)`;
    }
}

// show filter incident 
function filterIncident() {
    var input, filter, table, tr, td, i, txtValue;


    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByClassName("btn_list");

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByClassName("mb-1")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;

            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }

        }
    }
}
// end of filter incident

// add info message
$('#process').on('change', function() {

    var val_exst = $('#created_message').val();

    if (this.value == 'analyst') {
        $('#mandatory').remove();
        $('#created_message').val(`
hasil pengecekan  : (normal/down) *example
Indikasi Gangguan : (port down/no traffic/alarm/ect) *example
Penyebab gangguan : Perangkat Error*example
Nama Perangkat    : Router Cisco 1811 *example
Segment terganggu : (backbone/Distribusi/lastmile) *example
nama POP/Lokasi   : (Gambir Baru) *example
next progress     : (disposisi to SBU Jakarta) *example
Kendala           : tidak bisa akses router*example`);
    } else if (this.value == 'prepare_team') {
        $('#mandatory').remove();
        $('#created_message').val(`persiapan di basecamp (cikokol)`);
    } else if (this.value == 'waiting_process') {
        $('#mandatory').remove();
        $('#created_message').val(`
Sedang tidak ada Team Idle
next progress          : `);
    } else if (this.value == 'recovery_logic') {
        $('#mandatory').remove();
        $('#created_message').val(`
Action Recovery   : (Reset port/reroute/reset xconnect/ect)
Penyebab gangguan : Perangkat Error*example
Nama Perangkat    : Router Cisco 1811 *example
nama POP          : (Gambir Baru) *example
Result            : Layanan Kembali Normal
next progress     : (Ganti router) *example
Kendala           : stock router habis di gudang*example`);
    } else if (this.value == 'perjalanan') {
        $('#mandatory').remove();
        $('#created_message').val(`
tujuan        : (POP/Lastmile)
estimasi      : (2 jam)
Kendala       : `);
    } else if (this.value == 'recovery_time') {
        $('#mandatory').remove();
        $('#created_message').val(`
        update informasi setiap x jam :
progress 1 Jam : Pengukuran Kabel FO/OTDR *example
Perangkat      : -
Lokasi         : (POP/lastmile)
Prediksi estimasi progres : 20 menit*example
Kendala              : tidak ada*example
progress 2 jam : Hasil OTDR putus di jarak 2,1 KM, tim lokalisir
Perangkat      : -
Lokasi         : (POP/lastmile)
Kendala              : Hujan
Prediksi estimasi progres : 60 menit*example
progress selanjutnya :
Perangkat      : 
Lokasi         : 
Prediksi estimasi progres : 
Kendala              :`);
    } else if (this.value == 'confirm_time') {
        if (groupcode != 'CONTACTCENTER') {
            var mandatory = `<div id="mandatory">
                <div class="form-group">
                    <label for="">Penyebab gangguan :</label>
                    <input type="text" class="form-control" placeholder="Penyebab gangguan" name="penyebab_gangguan" id="penyebab_gangguan" required>
                </div>
                <div class="form-group">
                    <label for="">Solusi perbaikan :</label>
                    <input type="text" class="form-control" placeholder="Solusi perbaikan" name="solusi_perbaikan" id="solusi_perbaikan" required>
                </div>
                <div class="form-group">
                    <label for="">Lokasi :</label>
                    <input type="text" class="form-control" placeholder="Lokasi" name="lokasi" id="lokasi" required>
                </div>
                <div class="form-group">
                    <label for="">Perangkat :</label>
                    <input type="text" class="form-control" placeholder="Perangkat" name="perangkat" id="perangkat" required>
                </div>
                <div class="form-group">
                    <label for="">Jam operasional :</label>
                    <input type="text" class="form-control" placeholder="Jam operasional" name="jam_operasional" id="jam_operasional" required>
                </div>
            </div>`;
            $('#process-mandatory').html(mandatory);
        }
        $('#created_message').val(`Capture hasil pengecekan terakhir:`);
    } else {
        $('#mandatory').remove();
        $('#created_message').val(null);
    }
});