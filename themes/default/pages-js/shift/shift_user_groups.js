// jquery start
$(document).ready(function() {

    // menu active
    activeClass('shift', 'shift-groups');

    $('.select2').select2({
        allowClear: true,
    });


    // DataTable list incident open
    var t_shift_groups = $('#shift_groups').DataTable({
        dom: 'Bfrtpi Rl',
        stateSave: true,
        scrollX: true,
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
                className: 'shadow'
            },
            {
                init: function(api, node, config) {
                    $(node).removeClass('dt-button')
                    $(node).removeClass('dt-button > button').css('display', 'none')
                }
            },

        ],
        lengthChange: false,
        pageLength: 15,
        ajax: baseurl + 'shift/groups/list_shift_user_groups',
        columnDefs: [{
            "targets": '_all',
            "createdCell": function(td, cellData, rowData, row, col) {
                $(td).css('padding', '5px');
            }
        }, ],
        columns: [{
                "data": "id",
                "width": "3%",
                "className": "text-center",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "mod", "width": "10%" },
            { "data": "mod_name", "width": "10%" },
            { "data": "user_mod", "width": "10%" },
            { "data": "group_code", "width": "3%" }
        ],
    });
    // end of list incident


    // edit group 
    $('#shift_groups tbody').on('click', '.btn-edit', function() {
        var id = $(this).data('id');

        $.ajax({
            url: baseurl + 'shift/groups/edit?id=' + id,
            type: 'GET',
            dataType: 'JSON',
            success: function(data) {

                $('#modalEditGroups').modal('show');

                $('.select-mod').select2('val', [data.user_mod]);
                $('.select-user').select2('val', [data.user_group]);
                $('.form_edit').find("input[name='id']").val(data.id);
                $('.form_edit').find("input[name='group_name']").val(data.group_name);
                $('.form_edit').find("input[name='old_group_name']").val(data.group_name);

            }
        });
    });
    // end of edit


    // delete group 
    $('#shift_groups tbody').on('click', '.btn-deleted', function() {
        var id = $(this).data('id');

        $('#modalDeleted').modal('show');
        $('.form_deleted').find("input[name='id']").val(id);

    });


    // get list user
    $.ajax({
        url: baseurl + 'shift/user/list_data',
        type: 'GET',
        dataType: 'JSON',
        success: function(data) {
            var s_data = data;

            $.each(s_data, function(i, item) {

                var opt = '<option value="' + s_data[i].id + '">' + s_data[i].username + '</option>';
                $('.select-mod').append(opt);
                $('.select-user').append(opt);

            });
        }
    });
    // end of get user 

    if (create === 'f') {
        $('.btn-add').hide();
    }

});