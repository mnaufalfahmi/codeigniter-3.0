var user_checkin;
var user_mod;
var user_mod_act;
var pesan;
// var status_checkin;
var status_prosses;

fn_userdata()

function fn_userdata() {
    // stream = new EventSource(baseurl + 'incident/sse/get_userdata')
    stream = new EventSource(baseurlapi + 'incident/sse/api_userdata?group_code=' + groupcode + '&user_id=' + userid + '&token=' + getCookie('token'))
        // stream = new EventSource('http://10.14.22.85:3000/userdata?groupcode='+ groupcode +'&userid='+ userid)
        // stream = new EventSource('http://127.0.0.1:3000/userdata?groupcode='+ groupcode +'&userid='+ userid)
    stream.onmessage = function(event) {
        var result_data = JSON.parse(event.data);
        var stopclock = result_data.stopclock;
        var mod = result_data.mod;
        user_mod = result_data.status_mod;
        user_checkin = result_data.status_checkin;
        document.cookie = "status_checkin=" + result_data.status_checkin; // set status check in cookies
        status_checkin = getCookie('status_checkin'); // get status checkin in cookies

        // display request stopclock incident 
        if (stopclock.total == 0) {
            // $('div.notif_stopclock').css("display", "none");
            $('div.total-request').html('0');
        } else {
            // $('div.notif_stopclock').css("display", "");
            $('div.total-request').html(stopclock.total);
        }
        // end of request stopclock


        // display mod active based on group 
        if (mod !== null) {
            user_mod_act = result_data.mod.username;

            $('.btn-claim-mod').html(
                '<strong><i class="fa fa-user-circle fa-lg"></i>&nbsp; Mod : ' + mod.username + '</strong>' +
                '<span class="user_mod_active" style="display:none">' + mod.username + '</span>' +
                '<span class="id_user_mod_active" style="display:none">' + mod.id + '</span>'
            );
        } else {
            user_mod_act = '';

            $('.btn-claim-mod').html(
                '<strong><i class="fa fa-user-circle fa-lg"></i>&nbsp; I`M MOD</strong>' +
                '<span class="user_mod_active" style="display:none"></span>'
            );
        }
        // end of active mod on group 

    }
}

$(document).ready(function() {
    var pesan = "";

    $(window).on('beforeunload', function() {
        // return 'Are you sure you want to leave?';
        // alert('are u sure want to leave')
        stream.close();
    });

    $('.btn-claim-mod').click(function(e) {
        e.preventDefault();
        var message;

        if (user_mod_act !== '') {
            if (username === user_mod_act) {
                message = "Are You Sure Want Exit MOD";
            } else {
                $('.btn-yes').css('display', 'none');
                $('.btn-no').html('Yes');
                message = "You're Cannot Claim MOD, another user is active MOD";
            }
        } else {
            message = "Are You Sure Want to be MOD";
        }

        fn_confirm_mod(message);

    });

    // display modal modal confirmation 
    function fn_confirm_mod(message) {
        $('#modalConfirm').modal('show');
        $('.content_message').html(message);
    }

    // prosses mod 
    $('.form_create_mod').submit(function(e) {
        e.preventDefault();

        $('#modalConfirm').modal('hide');

        $.ajax({
            url: baseurl + 'shift/claim_mod',
            type: 'POST',
            dataType: 'JSON',
            data: { id: userid },
            async: true,
            success: function(data) {
                pesan = data;
                fn_show_alert(pesan);
            }
        });
    });

    // display alert 
    function fn_show_alert(pesan) {
        $('#modalerror').modal('show');
        $('.content_alert').html(pesan);
    }

    // close button 
    $('#ok').click(function(e) {
        e.preventDefault();
        $('#modalerror').modal('hide');
    });

    // check in check out
    $('.mycheckin').on('change', function() {
        var check = $(this).prop('checked');
        var param;

        if (check == true) {
            status_checkin = 1;
        } else {
            status_checkin = 0;
        }

        param = { user_id: userid, status: status_checkin };
        $.ajax({
            url: baseurl + 'shift/log/log_prosses',
            dataType: 'JSON',
            type: 'POST',
            data: param,
            success: function(data) {
                user_checkin = status_checkin;
                document.cookie = "status_checkin=" + status_checkin;
            }

        });

    });

});
// end of jquery