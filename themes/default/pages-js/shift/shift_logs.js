$(document).ready(function() {

    // menu active
    activeClass('shift', 'shift-log');
    date();

    $('.select2').select2({
        allowClear: true
    });

    $.fn.dataTable.ext.errMode = 'none';

    setInterval(function() {
        t_shift_logs.ajax.reload();
    }, 30000);

    // DataTable list incident open
    var t_shift_logs = $('#shift_logs').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        stateSave: true,
        scrollX: true,
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
                className: 'shadow'
            },
            {
                init: function(api, node, config) {
                    $(node).removeClass('dt-button')
                    $(node).removeClass('dt-button > button').css('display', 'none')
                }
            }

        ],
        lengthChange: false,
        pageLength: 15,
        ajax: {
            "url": baseurl + 'shift/log/list_shift_log',
            "data": function(d) {
                d.start_date = start_date;
                d.end_date = end_date;
            }
        },
        columnDefs: [{
            "targets": '_all',
            "createdCell": function(td, cellData, rowData, row, col) {
                $(td).css('padding', '5px');
            }
        }],
        columns: [{
                "data": "id",
                "width": "3%",
                "className": "text-center",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "username", "width": "7%" },
            { "data": "group_code", "width": "7%" },
            { "data": "mod", "width": "7%", "className": "text-center" },
            { "data": "shift_date", "width": "7%", "className": "text-center" },
            { "data": "start_time", "width": "7%", "className": "text-center" },
            { "data": "end_time", "width": "7%", "className": "text-center" },
            { "data": "shift_day", "width": "7%", "className": "text-center" },
        ],
    });
    // end of list incident

    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalFilter" data-toggle="modal"><i class="fa fa-filter"></i></button>');

    $('.form_filter').submit(function(e) {
        e.preventDefault();
        $('#modalFilter').modal('hide');

        start_date = $('.form_filter').find("input[name='start_date']").val();
        end_date = $('.form_filter').find("input[name='end_date']").val();

        t_shift_logs.ajax.reload();
    });

});