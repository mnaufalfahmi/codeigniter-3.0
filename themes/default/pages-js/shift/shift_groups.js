// jquery start
$(document).ready(function(){

    // menu active
    activeClass('shift', 'shift-user-groups');

    $('.select2').select2({
        allowClear: true
    });

    // function get date 
    // function _date(param){

    //     var str_tgl = new Date(param);
    //     var tanggal= str_tgl.getDate();
    //     var bulan = str_tgl.getMonth()+1;
    //     var tahun = str_tgl.getFullYear();

    //     if(tanggal < 10){
    //         r_tanggal = "0"+tanggal;
    //     }else{
    //         r_tanggal = tanggal;
    //     }

    //     if(bulan < 10){
    //         r_bulan = "0"+bulan;
    //     }else{
    //         r_bulan = bulan;
    //     }

    //     if(tahun < 10){
    //         r_tahun = "0"+ tahun;
    //     }else{
    //         r_tahun = tahun;
    //     }
        
    //     // return String(str_tanggal.getHours())+':'+String(str_tanggal.getMinutes()+':'+String(str_tanggal.getSeconds()));
    //     return r_tahun+'-'+r_bulan+'-'+r_tanggal;

    // }

    // DataTable list incident open
    var t_shift_groups = $('#shift_groups').DataTable({
        dom: 'Bfrtip Rl',
        stateSave: true,
        scrollX: true,
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
                className:'shadow'
            },
            {
                text: '<strong><i class="fa fa-plus"></i> Add User</strong>',
                className: 'btn btn-success btn-xs shadow',
                attr:  {
                    'data-target':'#modalAddUser',
                    'data-toggle':"modal"
    
                },
                init: function(api, node, config) {
                    $(node).removeClass('dt-button')
                }
            },
            
        ],
        lengthChange: false,
        pageLength:15,
        ajax: baseurl + 'shift/user/list_user_shift_group',
        columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                }],
        columns: [
                {
                    "data": "id",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "username" },
                // { "data": "created_on" },
                // { render: function ( data, type, row, meta ) {
                //     return _date(row.created_on)
                // }},
                { "data": "shift_name" },
                { "data": "start_time" },
                { "data": "end_time" },
                { render: function ( data, type, row, meta ) {
                    return  '<center>'+
                            '<button class="shadow btn btn-xs btn-warning mr-1 btn-edit" data-id='+ row.id +'><i class="fa fa-pencil"></i></button>' +
                            '<button class="shadow btn btn-xs btn-danger btn-deleted" data-id='+ row.id +'><i class="fa fa-trash"</button>'+
                            '</center>'
                }
            }
        ],
    });
    // end of list incident


    // edit user shift group
    $('#shift_groups tbody').on('click', '.btn-edit', function(){
        var id = $(this).data('id');

        $('#modalEditUser').modal('show');
        $('.form_edit').find("input[name='id']").val(id)

        $.ajax({
            url: baseurl + 'shift/user/list_user_shift_by_id?id='+ id,
            type: 'GET',
            dataType: 'JSON',
            success:function(data){
                
                $('.select-shift').select2('val', [data.shift_group_id]);
                $('.select-user').select2('val', [data.user_id]);

            }
        });
    });


    // delete user shift group
    $('#shift_groups tbody').on('click', '.btn-deleted', function(){
        var id = $(this).data('id');

        $('#modalDeleted').modal('show');

        $('.form_deleled').find("input[name='id']").val(id);

        return false;
        $.ajax({
            url: baseurl + 'shift/user/delete_user_shift?id='+ id,
            type: 'GET',
            dataType: 'JSON',
            success:function(data){
                
                location.reload();

            }
        });
    });

    // get shift name
    $.ajax({
        url: baseurl + 'shift/user/list_data?type=shift',
        type: 'GET',
        dataType: 'JSON',
        success: function(data){
            var u_data = data;

            $.each(u_data, function(i, item){

                var opt = '<option value="'+ u_data[i].id +'">'+ u_data[i].day +' | '+ u_data[i].start_time +' | '+ u_data[i].end_time +'</option>';
                $('.select-shift').append(opt);

            });
        }
    });
    // end of shift

    // get user
    $.ajax({
        url: baseurl + 'shift/user/list_data?type=user',
        type: 'GET',
        dataType: 'JSON',
        success: function(data){
            var s_data = data;

            $.each(s_data, function(i, item){

                var opt = '<option value="'+ s_data[i].id +'">'+ s_data[i].username +'</option>';
                $('.select-user').append(opt);

            });
        }
    });
    // end of get user 

});