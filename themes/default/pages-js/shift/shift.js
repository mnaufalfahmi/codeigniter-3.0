// jquery start
$(document).ready(function(){

    // menu active
    activeClass('shift', 'shift');

    $('.select2').select2({
        allowClear: true
    });

    // DataTable list incident open
    var t_shift_groups = $('#shift_groups').DataTable({
        dom: 'Bfrtip Rl',
        stateSave: true,
        scrollX: true,
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
                className:'shadow'
            },
            
        ],
        lengthChange: false,
        pageLength:15,
        ajax: baseurl + 'shift/list_shift_group',
        columnDefs: [
                {
                    "targets": 0, // your case first column
                    "className": "text-center",
                }],
        columns: [
                {
                    "data": "id",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "day" },
                { "data": "start_time" },
                { "data": "end_time" },
                { render: function ( data, type, row, meta ) {
                    if(update == 't'){
                        return  '<center>'+
                            '<button class="shadow btn btn-xs btn-warning mr-1 btn-edit" data-id='+ row.id +'><i class="fa fa-pencil"></i></button>' +
                            '</center>'
                    }else{
                        return '';
                    }
                }
            }
        ],
    });
    // end of list incident


    // edit function 
    $('#shift_groups tbody').on('click','.btn-edit', function(){
        var id = $(this).data('id');

        $.ajax({
            url: baseurl + "shift/list_shift_group_by_id?id="+ id,
            type: 'GET',
            dataType: 'JSON',
            success:function(data){

                var start_hour      = data.start_time.substring(0,2);
                var start_minute    = data.start_time.substring(3,5);
                var end_hour        = data.end_time.substring(0,2);
                var end_minute      = data.end_time.substring(3,5);

                $('.form_edit').find("input[name='id']").val(data.id);
                $('.select_day').select2('val', [data.day]);
                $('.select_start_hour').select2('val', [start_hour]);
                $('.select_start_minute').select2('val', [start_minute]);
                $('.select_end_hour').select2('val', [end_hour]);
                $('.select_end_minute').select2('val', [end_minute]);

                $('#modalEditShift').modal('show');
                
            }
        });

    }); 


    // deleted function 
    $('#shift_groups tbody').on('click','.btn-deleted', function(){
        var id = $(this).data('id');

        $('#modalDeleted').modal('show');

        $('.form_deleted').find("input[name='id']").val(id);

        return false;

        $.ajax({
            url: baseurl + "shift/deleted_shift_group_by_id?id="+ id,
            type: 'GET',
            dataType: 'JSON',
            success:function(data){

                location.reload();

            }
        });

    }); 

});