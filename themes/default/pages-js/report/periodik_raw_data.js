$(document).ready(function(){

    activeClass('report','rawdata')
    open_incident()
    rawdata_incident()
    summary_gangguan()
    service_incident()
    serpo()
    inventory_change()
    resume_incident()
    response_time()
    action_request()
    PNOC()
    kpi() 
    date_picker()
    kpi_serpo()
    kpi_sbu()

function date_picker() 
{
    $('#datepicker-start').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#datepicker-end').datepicker({
        format: 'yyyy-mm-dd',
    });
}

function rawdata_incident() 
{
    // body...
    $('#rawdata_incident').click(function(){
        $('#form-input').show()
        $('#pnoc').hide()
        $('#to').show()
        $('#from').show()
        $('#data_periode').show()
    })
}

function open_incident() 
{
    // body...
    $('#open_incident').click(function(){
        $('#form-input').hide()
        $('#pnoc').hide()
        $('#data_periode').show()
    })
}

function summary_gangguan() 
{
    // body...
    $('#summary_gangguan').click(function(){
        $('#form-input').show()
        $('#pnoc').hide()
        $('#to').show()
        $('#from').show()
        $('#data_periode').show()
    })
}

function service_incident() 
{
    // body...
    $('#service_incident').click(function(){
        $('#form-input').show()
        $('#pnoc').hide()
        $('#to').show()
        $('#from').show()
        $('#data_periode').show()
    })
}

function serpo() 
{
    // body...
    $('#serpo').click(function(){
        $('#form-input').show()
        $('#pnoc').hide()
        $('#to').show()
        $('#from').show()
        $('#data_periode').show()
    })
}

function inventory_change() 
{
    // body...
    $('#inventory_change').click(function(){
        $('#form-input').show()
        $('#to').show()
        $('#from').show()
        $('#pnoc').hide()
        $('#data_periode').show()
    })
}

function resume_incident() 
{
    // body...
    $('#resume_incident').click(function(){
        $('#to').show()
        $('#from').hide()
        $('#pnoc').hide()
        $('#data_periode').show()
    })
}

function response_time() 
{
    // body...
    $('#response_time').click(function(){
        $('#form-input').show()
        $('#to').show()
        $('#from').show()
        $('#pnoc').hide()
        $('#data_periode').show()
    })
}

function kpi() 
{
    // body...
    $('#kpi').click(function(){
        $('#form-input').show()
        $('#to').show()
        $('#from').show()
        $('#pnoc').hide()
        $('#data_periode').show()
    })
}

function kpi_sbu() 
{
    // body...
    $('#sbu').click(function(){
        $('#form-input').show()
        $('#to').show()
        $('#from').show()
        $('#pnoc').hide()
        $('#data_periode').show()
    })
}

function kpi_serpo() 
{
    // body...
    $('#serpo').click(function(){
        $('#form-input').show()
        $('#to').show()
        $('#from').show()
        $('#pnoc').hide()
        $('#data_periode').show()
    })
}


function action_request() 
{
    // body...
    $('#action_request').click(function(){
        $('#form-input').show()
        $('#pnoc').hide()
        $('#to').show()
        $('#from').show()
        $('#data_periode').show()
    })
}

function PNOC() 
{
    // body...
    $('#PNOC').click(function(){
        $('#data_periode').hide()
        $('#pnoc').show()
        $('#to').hide()
        $('#from').hide()
    })
}

})

