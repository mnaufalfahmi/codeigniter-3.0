$(document).ready(function() {

    activeClass('report', 'trending', 'recovery-time')

    var table = $('#trending_recovery_time').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        buttons: [{
            extend: 'colvis',
            text: '<i class="fa fa-table"></i>',
        }, ],
        responsive: true,
        lengthChange: false,
        pageLength: 15
    })
    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalSelector" data-toggle="modal"><i class="fa fa-filter"></i></button>');

    // action form_save
    $('.form_filter').submit(function(e) {
        e.preventDefault();
        // $('#table').empty();
        var table = $('#trending_recovery_time').DataTable();

        $.ajax({
            url: baseurl + 'report/trending_filter_recovery_time',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: 'JSON',
            success: function(data) {
                $('#modalSelector').modal('hide');
                var no = 1;
                table.clear().draw();

                $.each(data, function(i, item) {
                    table.row.add([
                        (no),
                        item.week,
                        item.jkt,
                        item.jbr,
                        item.jtg,
                        item.jtm,
                        item.bli,
                        item.sbu,
                        item.sbt,
                        item.sbs,
                        item.ibt,
                        // conv_time(item.jkt),
                        // conv_time(item.jbr),
                        // conv_time(item.jtg),
                        // conv_time(item.jtm),
                        // conv_time(item.bli),
                        // conv_time(item.sbu),
                        // conv_time(item.sbt),
                        // conv_time(item.sbs),
                        // conv_time(item.ibt),
                        conv_time(0)
                    ]).draw(false);
                    no++;
                    // $('#table').append(`
                    //     <tr>
                    //         <td>`+item.week+`</td>
                    //         <td>`+conv_time(item.jkt)+`</td>
                    //         <td>`+conv_time(item.jbr)+`</td>
                    //         <td>`+conv_time(item.jtg)+`</td>
                    //         <td></td>
                    //         <td>`+conv_time(item.bli)+`</td>
                    //         <td></td>
                    //         <td></td>
                    //         <td></td>
                    //         <td>`+conv_time(item.ibt)+`</td>
                    //         <td></td>
                    //     </tr>
                    //     `);
                });

            }
        })
    })

    function conv_time(params) {
        if (params == 'undefined' || params == null) {
            return '00:00:00';
        } else {
            var sisa_waktu = params % 3600;
            var hours = Math.floor(params / 60);
            var minutes = Math.floor(sisa_waktu / 60);
            var seconds = Math.floor(sisa_waktu % 60);

            var h = (hours < 10) ? "0" + hours : hours;
            var m = (minutes < 10) ? "0" + minutes : minutes;
            var s = (seconds < 10) ? "0" + seconds : seconds;

            return h + ':' + m + ':' + s;
        }


    }
});