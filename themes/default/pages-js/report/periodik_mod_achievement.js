$(document).ready(function(){

    activeClass('report','periodic', 'mod-achievement')
    $.fn.dataTable.ext.errMode = 'none';
    datePicker()

    var table = $('#mod-ach').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            }
            // {   text: 'View Personnel',
            //     className: 'btn btn-primary btn-xs',
            //     attr:  {
            //     id: 'view-personnel',
            //     'data-target':'#modalViewPersonnel',
            //     'data-toggle':"modal",

            // },
            //     init: function(api, node, config) {
            //         $(node).removeClass('dt-button')
            //      }
            // }
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15
    })

    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalSelector" data-toggle="modal"><i class="fa fa-filter"></i></button>');

    // view personil
    $('#view-personnel').on('click', function(){
        var personil = $('#personil-list').DataTable({
            dom: 'Bfrtpi Rl',
            buttons: [
                {
                    extend: 'colvis',
                    text: '<i class="fa fa-table"></i>',
                }
            ],
            responsive:true,
            lengthChange: false,
            pageLength:15,
            ajax: baseurl +'report/periodik_list_personil',
            columns: [
    
                { "data": "no" },
                { "data": "shift_date" },
                { "data": "shift_name"},
                { "data": "full_name"},
                { "data": "mod"}
            ]
        });
    });
    

    // $('#corporate-mttr').on('dblclick', 'tr', function () {
    //     var data = table.row( this ).data();
    //     fs_sbu_detail(data[0])
    // } );
});

function add_zero_hours(hours)
{
    var h = (hours < 10) ? "0" + hours : hours;
    return h;
}
function add_zero_minutes(minutes)
{
    var m = (minutes < 10) ? "0" + minutes : minutes;
    return m;
}
function add_zero_second(second)
{
    var s = (second < 10) ? "0" + second : second;
    return s;
}
function define(params)
{
    if(params == 'undefined' || params == null || params == '')
    {
        return '0';
    }
    else
    {
        return params;
    }
}


$('.form_filter').submit(function(e){
    e.preventDefault();
    // $('#table').empty();
    var table = $('#mod-ach').DataTable();

    $.ajax({
        url: baseurl+'report/periodik_filter_mod_ach',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        dataType : 'JSON',
        success: function (data) {
            $('#modalSelector').modal('hide');
            var no = 1;
            // console.log(data);
            table.clear().draw();
            // return false;
            $.each(data, function (i, item) {
                
            // convert format time
            var sisa_waktu = item.response_time % 3600;
            var h1         = Math.floor(item.response_time / 60);
            var m1         = Math.floor(sisa_waktu / 60);
            var s1         = Math.floor(sisa_waktu % 60);

            var sisa_waktu_analisis = item.analisis_time % 3600;
            var h_analisis = Math.floor(item.analisis_time / 60);
            var m_analisis = Math.floor(sisa_waktu_analisis / 60);
            var s_analisis = Math.floor(sisa_waktu_analisis % 60);

            table.row.add( [
                (no),
                item.username,
                item.response_time,
                item.analisis_time,
                define(item.good_closure_number),
                define(item.good_closure_ratio),
                define(item.self_closure_number),
                define(item.self_closure_ratio),
                define(item.close_closure_number),
                define(item.close_closure_ratio),
                define(item.unattach_ticket),
                define(item.unattach_icr)
            ] ).draw( false );
            no++;
            });
        }
    });
});

function datePicker()
{
    // config datepicker
    $('#start_date').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#end_date').datepicker({
        format: 'yyyy-mm-dd',
    });
}

// function ActionSummary(id){
// $("#modalSummary").modal("show")
// axios.get('http://localhost/fms/field_support/fs_sbu_summary?id='+id).then(function (response) {
//             response = response.data
//             $("#email").val(response.fullname);
//             $("#fullname").val(response.fullname);
//             $("#ym").val(response.ym);
//             $("#region").val(response.region);
//             $("#bobot").val(response.bobot);
//             $("#surveilance").val(response.surveilance);
//             $("#analyst").val(response.analyst);
//             $("#editmod").val(response.mod);
//             $("#shift_group").val(response.shift_group);
//       })
//       .catch(function (error) {
//         console.log(error)
//       });
// }

// function fs_sbu_detail(id){
//     $("#modalFSDetail").modal("show")
//     axios.get('http://localhost/fms_dev/field_support/get_fs_sbu_detail?id='+id).then(function (response) {
//             response = response.data
//             $("#create_time").val(response.create_time);
//             $("#dispatched").val(response.dispatched);
//             $("#duration").val(response.duration);
//             $("#destination").val(response.destination);
//             $("#state").val(response.state);
//             $("#ar_request").val(response.ar_request);
//             $("#incident").val(response.incident);
//             $("#last_reply").val(response.last_reply);
//       })
//       .catch(function (error) {
//         console.log(error)
//       });
// }
