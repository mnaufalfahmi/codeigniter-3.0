$(document).ready(function(){

    activeClass('report','periodic', 'region-mttr')
    $.fn.dataTable.ext.errMode = 'none';
    datePicker()

    var table = $('#table-region-mttr').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15
    })
    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalSelector" data-toggle="modal"><i class="fa fa-filter"></i></button>');
});

    function statment(val)
    {
        if(val == null)
        {
            return '0%';
        }
        else
        {
            return val+'%';
        }
    }

    // action form_save
    $('.form_filter').submit(function(e){
        e.preventDefault();
        $('#table').empty();
        var t = $('#table-region-mttr').DataTable();
        var no = 1;

        $.ajax({
            url: baseurl+'report/periodik_filter_region_mttr',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType : 'JSON',
            success: function (data) {
                $('#modalSelector').modal('hide');
                t.clear().draw();
                $.each(data, function(i,item){
                    t.row.add( [
                        (i+no),
                        item.region_code,
                        statment(item.fot),
                        statment(item.foc),
                        statment(item.ps)
                    ] ).draw( false );

                    // $('#table').append(
                    //     `<tr>
                    //         <td></td>
                    //         <td>`+item.region_code+`</td>
                    //         <td>`+statment(item.fot)+`</td>
                    //         <td>`+statment(item.foc)+`</td>
                    //         <td>`+statment(item.ps)+`</td>
                    //     </tr>`
                    // )

                })
            }
        })
    })

 // hight chart 
 Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Incidents from 25/09/2019 to 01/10/2019'
    },
    xAxis: {
        categories: ['BLI', 'IBT'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: '%'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'FOT',
        data: [107, 31]
    }, {
        name: 'FOC',
        data: [133, 156]
    }, {
        name: 'PS',
        data: [1216, 1001]
    }]
});

function datePicker()
{
    // config datepicker
    $('#start_date').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#end_date').datepicker({
        format: 'yyyy-mm-dd',
    });
}