$(document).ready(function() {

    activeClass('report', 'periodic', 'service-impact')
    $.fn.dataTable.ext.errMode = 'none';
    datePicker()

    var table = $('#table-service-impact').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        buttons: [{
            extend: 'colvis',
            text: '<i class="fa fa-table"></i>',
        }, ],
        responsive: true,
        lengthChange: false,
        pageLength: 15
    })
    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalSelector" data-toggle="modal"><i class="fa fa-filter"></i></button>');
});

$('.form_filter').submit(function(e) {
    e.preventDefault();
    // $('#table').empty();
    var table = $('#table-service-impact').DataTable();

    $.ajax({
        url: baseurl + 'report/periodik_filter_service_impact',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        dataType: 'JSON',
        success: function(data) {
            $('#modalSelector').modal('hide');
            var no = 1;
            table.clear().draw();

            $.each(data, function(i, item) {

                table.row.add([
                    (no),
                    item.incident_id,
                    item.description,
                    item.classification_name,
                    item.link,
                    item.type,
                    item.ttr,
                    item.service
                ]).draw(false);
                no++;

                // $('#table').append(`
                // <tr>
                //     <td></td>
                //     <td>`+item.incident_id+`</td>
                //     <td>`+item.description+`</td>
                //     <td>`+item.classification_name+`</td>
                //     <td>`+item.link+`</td>
                //     <td>`+item.type+`</td>
                //     <td>`+item.ttr+`</td>
                //     <td>`+item.service+`</td>
                // </tr>
                // `);
            });
        }
    });
});

function datePicker() {
    // config datepicker
    $('#start_date').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#end_date').datepicker({
        format: 'yyyy-mm-dd',
    });
}

function conv_time(params) {
    if (params == 'undefined' || params == null) {
        return '00:00:00';
    } else {
        var sisa_waktu = params % 3600;
        var hours = Math.floor(params / 60);
        var minutes = Math.floor(sisa_waktu / 60);
        var seconds = Math.floor(sisa_waktu % 60);

        var h = (hours < 10) ? "0" + hours : hours;
        var m = (minutes < 10) ? "0" + minutes : minutes;
        var s = (seconds < 10) ? "0" + seconds : seconds;

        return h + ':' + m + ':' + s;
    }

}