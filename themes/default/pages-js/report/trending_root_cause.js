$(document).ready(function(){

    activeClass('report','trending', 'root-cause')
    $.fn.dataTable.ext.errMode = 'none';

    var table = $('#trending_root_cause').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15
    })
    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalSelector" data-toggle="modal"><i class="fa fa-filter"></i></button>');
    

        // action form_save
        $('.form_filter').submit(function(e){
            e.preventDefault();
            $('#table').empty();
    
            var table = $('#trending_root_cause').DataTable();

            $.ajax({
                url: baseurl+'report/trending_filter_root_cause',
                type: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                dataType : 'JSON',
                success: function (data) {
                    $('#modalSelector').modal('hide');

                    var no = 1;
                    table.clear().draw();
                    
                    $.each(data, function(i,item){

                        table.row.add( [
                            item.root_cause_detail_name,
                            item.januari,
                            item.februari,
                            item.maret,
                            item.april,
                            item.mei,
                            item.juni,
                            item.juli,
                            item.agustus,
                            item.september,
                            item.oktober,
                            item.november,
                            item.desember
                        ] ).draw( false );

                    })
                }
            })
        })
});