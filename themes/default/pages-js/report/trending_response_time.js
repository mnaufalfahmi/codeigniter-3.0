$(document).ready(function(){

    activeClass('report','trending', 'response-time')
    $.fn.dataTable.ext.errMode = 'none';

    var table = $('#trending_response_time').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15
    })
    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalSelector" data-toggle="modal"><i class="fa fa-filter"></i></button>');
});

function add_zero_hours(hours)
{
    var h = (hours < 10) ? "0" + hours : hours;
    return h;
}
function add_zero_minutes(minutes)
{
    var m = (minutes < 10) ? "0" + minutes : minutes;
    return m;
}
function add_zero_second(second)
{
    var s = (second < 10) ? "0" + second : second;
    return s;
}

function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);
    if (h<9){h= "0"+h;}
    if (m<9){m= "0"+m;}
    if (s<9){s= "0"+s;}
    return (h+":"+m+":"+ s);
    }

$('.form_filter').submit(function(e){
    e.preventDefault();
    $('#table').empty();

    var table = $('#trending_response_time').DataTable();

    $.ajax({
        url: baseurl+'report/trending_filter_response_time',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        dataType : 'JSON',
        success: function (data) {
            $('#modalSelector').modal('hide');
            var data_week = [];
            var arrdata = [];
            var arrdata2 = [];
            var no = 1;
            table.clear().draw();
            $.each(data, function (i, item) {
                // convert format time
                
                var sisa_waktu = item.respontime % 3600;
                var h1         = Math.floor(item.respontime / 60);

                table.row.add( [
                    item.week,
                    item.respontime_str
                ] ).draw( false );
                
                var t = item.week;
                data_week.push(t)
                arrdata.push(h1);
                arrdata2.push(item.respontime_week);
                // console.log(item.respontime_week)
                // console.log(arrdata)
            })
            
            // hightchart 
            Highcharts.SVGRenderer.prototype.symbols.cross = function (x, y, w, h) {
                return ['M', x, y, 'L', x + w, y + h, 'M', x + w, y, 'L', x, y + h, 'z'];
            };
            if (Highcharts.VMLRenderer) {
                Highcharts.VMLRenderer.prototype.symbols.cross = Highcharts.SVGRenderer.prototype.symbols.cross;
            }
            
            Highcharts.chart('container', {
            
                title: {
                    text: 'Response Time'
                },
            
                xAxis: {
                    tickmarkPlacement: 'between',
                    //tickmarkPlacement: 'on',
                    categories: $.each(arrdata2, function(i, item){
                        arrdata2
                    })
                  },
                yAxis: {
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    labels: {
                        enabled: false
                    },
                    minorTickLength: 0,
                    tickLength: 0,
                    title: {
                        text: 'Response Time'
                    }
                },
            
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        color: 'gray'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
                
                series: [{
                    name: "response time",
                    data: $.each(arrdata, function(i, item){
                        arrdata
                    }),
                    marker: {
                        symbol: 'cross',
                        lineColor: 'blue',
                        lineWidth: 2
            
                    }
                }],
                
                tooltip: {
                    useHTML: true,
                    formatter: function() {
                            return Highcharts.dateFormat('%e. %b', this.x) +': '+this.x;
                    }
                },
                credits: {
                    enabled: false
                },
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            },
                            xAxis: {
                                labels: {
                                  step: 2
                                }
                              },
                        }
                    }]
                }
            });
            

        }
    })
})

