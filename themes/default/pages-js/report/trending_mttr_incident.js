$(document).ready(function(){
    
    activeClass('report','trending', 'mttr-incident')
    $.fn.dataTable.ext.errMode = 'none';

    var table = $('#trending_mttr_incident').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15
    })
    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalSelector" data-toggle="modal"><i class="fa fa-filter"></i></button>');
    

    function times(time)
    {
        var sisa_waktu = time % 3600;
        var h1         = Math.floor(time / 60);
        var m1         = Math.floor(time / 60);
        var s1         = Math.floor(time % 60);      
        
        var h = (h1 < 10) ? "0" + h1 : h1;
        var m = (m1 < 10) ? "0" + m1 : m1;
        var s = (s1 < 10) ? "0" + s1 : s1;

        return h+`:`+m+`:`+s
    }

    // action form_save
    $('.form_filter').submit(function(e){
        e.preventDefault();
        // $('#table').empty();
        var table = $('#trending_mttr_incident').DataTable();

        $.ajax({
            url: baseurl+'report/trending_filter_mttr_incident',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType : 'JSON',
            success: function (data) {
                $('#modalSelector').modal('hide');
                
                var data_week = [];
                var arrjkt   = [];
                var arrjbr   = [];
                var arrjtg   = [];
                var arrjtm   = [];
                var arrbli   = [];
                var arribt   = [];

                var no = 1;
                table.clear().draw();

                $.each(data, function(i,item){

                    data_week.push(item.week)
                    arrjkt.push(Math.floor(item.jkt));
                    arrjbr.push(Math.floor(item.jbr));
                    arrjtg.push(Math.floor(item.jtg));
                    arrjtm.push(Math.floor(item.jtm));
                    arrbli.push(Math.floor(item.bli));
                    arribt.push(Math.floor(item.ibt));

                    table.row.add( [
                        no,
                        item.week,
                        item.jkt_str,
                        item.jbr_str,
                        item.jtg_str,
                        item.jtm_str,
                        item.bli_str,
                        item.sbu_str,
                        conv_time(0),
                        // item.sbt_str,
                        item.sbs_str,
                        item.ibt_str,
                        conv_time(0)
                        // corp
                    ] ).draw( false );
                    no++;
                    
                    // $('#table').append(
                    //     `<tr>
                    //         <td>`+item.week+`</td>
                    //         <td>`+times(item.jkt)+`</td>
                    //         <td>`+times(item.jbr)+`</td>
                    //         <td>`+times(item.jtg)+`</td>
                    //         <td>`+times(item.jtm)+`</td>
                    //         <td>`+times(item.bli)+`</td>
                    //         <td></td>
                    //         <td></td>
                    //         <td></td>
                    //         <td>`+times(item.ibt)+`</td>
                    //         <td></td>
                    //     </tr>`
                    // );
                });
            // higthchart
            Highcharts.SVGRenderer.prototype.symbols.cross = function (x, y, w, h) {
                return ['M', x, y, 'L', x + w, y + h, 'M', x + w, y, 'L', x, y + h, 'z'];
            };
            if (Highcharts.VMLRenderer) {
                Highcharts.VMLRenderer.prototype.symbols.cross = Highcharts.SVGRenderer.prototype.symbols.cross;
            }

            Highcharts.chart('container', {

                title: {
                    text: 'MTTR Incident'
                },

                xAxis: {
                    tickmarkPlacement: 'between',
                    //tickmarkPlacement: 'on',
                    categories: data_week
                },
                yAxis: {
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    labels: {
                        enabled: false
                    },
                    minorTickLength: 0,
                    tickLength: 0,
                    title: {
                        text: 'MTTR Incident'
                    }
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                series: [{
                    name: 'Jakarta dan Banten',
                    data: arrjkt
                }, {
                    name: 'Jawa Barat',
                    data: arrjbr
                }, {
                    name: 'Jawa Tengah',
                    data: arrjtg
                }, {
                    name: 'Jawa Timur',
                    data: arrjtm
                }, {
                    name: 'Bali dan Nusra',
                    data: arrbli
                },
                {
                    name: 'Sumatra Utara',
                    data: null
                },
                {
                    name: 'Sumatra Tengah',
                    data: null
                },
                {
                    name: 'Sumatra Selatan',
                    data: null
                },
                {
                    name: 'Indonesia Timur',
                    data: arribt
                },
                {
                    name: 'Corporate Average',
                    data: null
                }],

                
                credits: {
                    enabled: false
                },
                tooltip: {
                    formatter: function() {
                            return Highcharts.dateFormat('%e. %b', this.x) +': '+ this.y;
                    }
                },
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            },
                            xAxis: {
                                labels: {
                                step: 2
                                }
                            },
                        }
                    }]
                }
            });
            }
        })
    })

    function conv_time(params)
        {
            if(params == 'undefined' || params == null)
            {
                return '00:00:00';
            }
            else
            {
                var sisa_waktu = params % 3600;
                var hours      = Math.floor(params / 60);
                var minutes    = Math.floor(sisa_waktu / 60);
                var seconds    = Math.floor(sisa_waktu % 60);

                var h = (hours < 10) ? "0" + hours : hours;
                var m = (minutes < 10) ? "0" + minutes : minutes;
                var s = (seconds < 10) ? "0" + seconds : seconds;

                return h + ':' + m + ':' + s;
            }

            
        }
});


