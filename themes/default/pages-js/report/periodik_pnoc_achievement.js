$(document).ready(function(){

    activeClass('report','periodic', 'pnoc-achievement')
    $.fn.dataTable.ext.errMode = 'none';
    datePicker()

    var table = $('#pnoc-ach').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15
    })

    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalSelector" data-toggle="modal"><i class="fa fa-filter"></i></button>');
    // $('#corporate-mttr').on('dblclick', 'tr', function () {
    //     var data = table.row( this ).data();
    //     fs_sbu_detail(data[0])
    // } );
});

function define(params)
{
    if(params == 'undefined' || params == null)
    {
        return '0';
    }
    else
    {
        return params;
    }
}

$('.form_filter').submit(function(e){
    e.preventDefault();
    // $('#table').empty();
    var table = $('#pnoc-ach').DataTable();

    $.ajax({
        url: baseurl+'report/periodik_filter_pnoc',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        dataType : 'JSON',
        success: function (data) {
            $('#modalSelector').modal('hide');
            var no = 1;
            table.clear().draw();

            $.each(data, function (i, item) {
                table.row.add( [
                    (no),
                    item.username,
                    define(item.good_closure),
                    define(item.good_cross_closure),
                    define(item.bad_cross_closure),
                    define(item.unclose),
                    define(item.lock_all),
                    define(item.hourly_comment),
                    define(item.late_response)+'/'+define(item.total_response),
                    define(item.late_quarter),
                    define(item.late_half),
                    define(item.late_one)
                ] ).draw( false );
                no++;

                // $('#table').append(`
                // <tr>
                //     <td></td>
                //     <td>`+item.username+`</td>
                //     <td>`+item.good_closure+`</td>
                //     <td>`+item.good+`</td>
                //     <td>`+item.bad+`</td>
                //     <td>`+item.unclose+`</td>
                //     <td></td>
                //     <td></td>
                //     <td></td>
                //     <td></td>
                //     <td></td>
                //     <td></td>
                //     <td></td>
                // </tr>
                // `);
            });
        }
    });
});


function datePicker()
{
    // config datepicker
    $('#start_date').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#end_date').datepicker({
        format: 'yyyy-mm-dd',
    });
}

// function ActionSummary(id){
// $("#modalSummary").modal("show")
// axios.get('http://localhost/fms/field_support/fs_sbu_summary?id='+id).then(function (response) {
//             response = response.data
//             $("#email").val(response.fullname);
//             $("#fullname").val(response.fullname);
//             $("#ym").val(response.ym);
//             $("#region").val(response.region);
//             $("#bobot").val(response.bobot);
//             $("#surveilance").val(response.surveilance);
//             $("#analyst").val(response.analyst);
//             $("#editmod").val(response.mod);
//             $("#shift_group").val(response.shift_group);
//       })
//       .catch(function (error) {
//         console.log(error)
//       });
// }

// function fs_sbu_detail(id){
//     $("#modalFSDetail").modal("show")
//     axios.get('http://localhost/fms_dev/field_support/get_fs_sbu_detail?id='+id).then(function (response) {
//             response = response.data
//             $("#create_time").val(response.create_time);
//             $("#dispatched").val(response.dispatched);
//             $("#duration").val(response.duration);
//             $("#destination").val(response.destination);
//             $("#state").val(response.state);
//             $("#ar_request").val(response.ar_request);
//             $("#incident").val(response.incident);
//             $("#last_reply").val(response.last_reply);
//       })
//       .catch(function (error) {
//         console.log(error)
//       });
// }
