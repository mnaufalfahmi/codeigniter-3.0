
$(document).ready(function(){

    activeClass('report','periodic', 'periodic-root-cause')
    $.fn.dataTable.ext.errMode = 'none';
    datePicker()

    var table = $('#table-root-cause').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15
    })
    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalSelector" data-toggle="modal"><i class="fa fa-filter"></i></button>');
});

$('.form_filter').submit(function(e){
    e.preventDefault();
    // $('#table').empty();
    var t = $('#table-root-cause').DataTable();
    var no = 1;

    $.ajax({
        url: baseurl+'report/periodik_filter_root_cause',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        dataType : 'JSON',
        success: function (data) {
            $('#modalSelector').modal('hide');
            $.each(data, function (i, item) {

                t.clear().draw();
                $.each(data, function(i,item){
                    t.row.add( [
                        (i+no),
                        item.classification_name,
                        item.jkt,
                        item.jbr,
                        item.jtg,
                        item.jtm,
                        item.bli,
                        0,
                        0,
                        0,
                        item.ibt,
                        0,
                        0
                    ] ).draw( false );
                })

                // $('#table').append(`
                // <tr>
                //     <td></td>
                //     <td>`+item.classification_name+`</td>
                //     <td>`+item.jkt+`</td>
                //     <td>`+item.jbr+`</td>
                //     <td>`+item.jtg+`</td>
                //     <td>`+item.jtm+`</td>
                //     <td>`+item.bli+`</td>
                //     <td></td>
                //     <td></td>
                //     <td></td>
                //     <td>`+item.ibt+`</td>
                //     <td></td>
                //     <td></td>
                // </tr>
                // `);
            });
        }
    });
});

function datePicker()
{
    // config datepicker
    $('#start_date').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#end_date').datepicker({
        format: 'yyyy-mm-dd',
    });
}