activeClass('report','trending', 'serpo-utilization')
$.fn.dataTable.ext.errMode = 'none';

Highcharts.SVGRenderer.prototype.symbols.cross = function (x, y, w, h) {
    return ['M', x, y, 'L', x + w, y + h, 'M', x + w, y, 'L', x, y + h, 'z'];
};
if (Highcharts.VMLRenderer) {
    Highcharts.VMLRenderer.prototype.symbols.cross = Highcharts.SVGRenderer.prototype.symbols.cross;
}
var tahun = new Date();
var series1 = [1432883717000, 1432883717000, 1433916917000];
var series2 = [null, 1433221380470, null];
var min1 = Math.min.apply(null, series1);
Highcharts.chart('container', {

	title: {
        text: 'Utilization'
    },

    xAxis: {
        type: 'datetime'
    },
    yAxis: {
    	
        title: {
            text: 'Utilization'
        }
    },

    plotOptions: {
        series: {
        	label: {
                connectorAllowed: false
            },
            color: 'gray',
            pointStart: Date.UTC(tahun.getFullYear(), 0, 1),
            pointInterval: (24 * 3600 * 1000) // one day
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    series: [{
    	name: 'Utilization',
        data: [29.9, 71.5],
        marker: {
            symbol: 'cross',
            lineColor: 'blue',
            lineWidth: 2

        }
    }],
    credits: {
        enabled: false
    },
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
});

$(document).ready(function(){

var table = $('#utilization').DataTable({
        dom: 'Bfrtip Rl',
        stateSave: true,
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            }
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15,
        ajax: baseurl,
        columns: [

            { "data": "week" },
            { "data": "utilization" }
        ]
    })
})