$(document).ready(function(){

    activeClass('report','periodic', 'corporate-mttr')
    $.fn.dataTable.ext.errMode = 'none';
    datePicker()

    // var table = $('#corporate-mttr').DataTable({
    //     dom: 'Bfrtpi Rl',
    //     buttons: [
    //         {
    //             extend: 'colvis',
    //             text: '<i class="fa fa-table"></i>',
    //         },
    //     ],
    //     responsive:true,
    //     lengthChange: false,
    //     pageLength:15,
    //     ajax: baseurl+'report/periodik_filter_corporate_mttr',
    //     columns: [

    //         { "data": "network" },
    //         { "data": "hirarchy" },
    //         { "data": "total_incident"}
    //     ]
    // })

});



function datePicker()
{
    // config datepicker
    $('#start_date').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#end_date').datepicker({
        format: 'yyyy-mm-dd',
    });
}

    // action form_save
    $('.form_filter').submit(function(e){
        e.preventDefault();
        $('#table').empty();

        

        $.ajax({
            url: baseurl+'report/periodik_filter_corporate_mttr',
            type: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType : 'JSON',
            success: function (data) {
                $('#modalSelector').modal('hide');
                var start = $('#start_date').val();
                var end   = $('#end_date').val();

                var filter_date = `Incident from `+start+` to `+end+``
                
                var arr = [];
                var hie = [];
                $.each(data, function(i, item){
                    
                    
                    arr.push({
                        name: item.network,
                        y: parseInt(item.total)
                    });
                    hie.push(item.link)
                })
                
                console.log(arr)
                // return false;
                // pie chart
                Highcharts.chart('container', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: filter_date,
                        id : 'text-filter'
                    },
                    tooltip: {
                        pointFormat: '{series.arr}: <b>{point.y:.1f}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.arr}</b>: {point.y:.1f}'
                            }
                        }
                    },
                    series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: arr
                    }]
                });
                
                $.each(data, function (i, item) {
                    $('#table').append(`
                    <tr>
                        <td>`+item.network+`</td>
                        <td>`+item.link+`</td>
                        <td>`+item.total+`</td>
                    </tr>
                    `);
                })
                // $('#corporate-mttr').DataTable().ajax.reload();
                
            }
        });
    });