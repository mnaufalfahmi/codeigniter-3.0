$(document).ready(function(){

    activeClass('report','trending', 'kpi-achievement')
    $.fn.dataTable.ext.errMode = 'none';

    var table = $('#mttr_achievement').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15,
        ajax: baseurl +'report/trending_list_kpi_mttr_achievement',
        columns: [

            { "data": "week" },
            { "data": "mttr",
            "render" : function(data)
            {
                if (data)
                {
                    return data+`%</p>`
                }
            }
        }
        ]
    })
});

$('.form_filter').submit(function(e){
    e.preventDefault();
    $('#table-mttr').empty();
    $('#table-service').empty();

    $.ajax({
        url: baseurl+'report/trending_filter_kpi_ach',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        dataType : 'JSON',
        success: function (data) {
            $('#modalSelector').modal('hide');
        }
    });
});


$(document).ready(function(){

    var table = $('#service_problem').DataTable({
        dom: 'Bfrtpi Rl',
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            },
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15,
        ajax: baseurl +'report/trending_list_kpi_service_problem',
        columns: [

            { "data": "week",
                "render" : function(data)
                {
                    if (data)
                    {
                        return data.toUpperCase()
                    }
                }
            },
            { "data": "srv",
                "render" : function(data)
                {
                    if (data)
                    {
                        return data+`%`
                    }
                }
            }
        ]
    })
});


Highcharts.SVGRenderer.prototype.symbols.cross = function (x, y, w, h) {
    return ['M', x, y, 'L', x + w, y + h, 'M', x + w, y, 'L', x, y + h, 'z'];
};
if (Highcharts.VMLRenderer) {
    Highcharts.VMLRenderer.prototype.symbols.cross = Highcharts.SVGRenderer.prototype.symbols.cross;
}

// mttr
$.ajax({
        url: baseurl + 'report/json_kpi_mttr_achievement',
        type: "POST",
        data: {year: $("input[name = 'year']").val(), analisis:$("input[name = 'analisis']:checked").val()},
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        dataType : 'JSON',
        success: function (data) {
            arr_cat = [];
            arr_val = [];
            $.each(data,function(i,item){
                // declare week
                arr_cat.push(item.week);

                // declare val
                arr_val.push(Number(item.mttr));
            })
        }
});

// service
$.ajax({
    url: baseurl + 'report/json_kpi_service_problem',
    type: "POST",
    data: {year: $("input[name = 'year']").val(), analisis:$("input[name = 'analisis']:checked").val()},
    processData: false,
    contentType: false,
    cache: false,
    async: false,
    dataType : 'JSON',
    success: function (data) {
        arr_cat_service = [];
        arr_val_service = [];
        $.each(data,function(i,item){
            // declare week
            arr_cat_service.push(item.week.toUpperCase());

            // declare val
            arr_val_service.push(Number(item.srv));
        })
    }
});

Highcharts.chart('container', {

    title: {
        text: 'MTTR Achievement'
    },

    xAxis: {
        categories: arr_cat
    },
    yAxis: {
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: 'transparent',
        labels: {
            enabled: false
        },
        minorTickLength: 0,
        tickLength: 0,
        title: {
            text: 'MTTR Achievement'
        }
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            color: 'gray'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    series: [{
        name: 'MTTR Achievement',
        data: arr_val,
        marker: {
            symbol: 'cross',
            lineColor: 'blue',
            lineWidth: 2

        }
    }],
    credits: {
        enabled: false
    },
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
});


// 
Highcharts.chart('container2', {

    title: {
        text: 'Service Problems'
    },

    xAxis: {
        categories: arr_cat_service
    },
    yAxis: {
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: 'transparent',
        labels: {
            enabled: false
        },
        minorTickLength: 0,
        tickLength: 0,
        title: {
            text: 'Service Problems'
        }
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            color: 'gray',
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    series: [{
        name: 'Service Problems',
        data: arr_val_service,
        marker: {
            symbol: 'cross',
            lineColor: 'blue',
            lineWidth: 2

        }
    }],
    credits: {
        enabled: false
    },
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
});