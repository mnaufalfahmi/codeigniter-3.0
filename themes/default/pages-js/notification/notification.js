$(document).ready(function() {


    load_last_notification();

    function load_last_notification() {

        $.ajax({
            url: baseurl + "/notification",
            method: "POST",
            data: { groupcode: type },
            contentType: "application/json",
            success: function(data) {

                var total = 0;

                if (data.total_notif > 99) {
                    total = "99+";
                } else {
                    total = data.total_notif;
                }

                $('.count-token').html(data.total_not_read)

                var count = 0;
                for (var i = 0; i < data.notification.length; i++) {
                    // console.log(data.count[i].not_read);
                    if (data.notification[i].is_read == '1') {
                        var html = `<div class="notification-item clearfix" id="unread">
                        <div class="heading">
                          <a href="#" id="cs" class="text-danger pull-left toggle-more-details">
                            <div class="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
                            <img width="30" height="30" data-src-retina="` + path + `assets/img/profiles/contact-center.png" data-src="` + path + `assets/img/profiles/contact-center.png" alt="" src="` + path + `assets/img/profiles/contact-center.png">
                          </div>
                            <span class="bold">` + data.notification[i].group_code + `</span>
                            <span class="fs-12 m-l-10">` + data.notification[i].progress_fullname + `</span>
                          </a>
                          <div class="pull-right">
                            <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
                              <div><i class="fa fa-angle-left"></i>
                              </div>
                            </div>
                            <span class=" time">` + data.notification[i].progress_created_on + `</span>
                          </div>
                          <div class="more-details">
                            <div class="more-details-inner">
                              <h5 class="semi-bold fs-16">“` + data.notification[i].progress_message + `.”</h5>
                              <p class="small hint-text">
                                Commented on incidents <b><a onClick="is_read(` + data.notification[i].id + `)" href="` + baseurl + `incident/progress/` + data.notification[i].incident_id + `">` + data.notification[i].description + `.
                              </a></b></p>
                            </div>
                          </div>
                        </div>
                        ` + statment_read(data.notification[i].is_read) + `
                        
                          <a href="#" class="mark" onClick="is_read(` + data.notification[i].id + `)"></a>
                        </div>
                      </div>`
                    } else {
                        var html = `<div class="notification-item unread clearfix" id="unread">
                        <div class="heading" onClick="is_read(` + data.notification[i].id + `)">
                          <a href="#" id="cs" class="text-danger pull-left toggle-more-details">
                            <div class="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
                            <img width="30" height="30" data-src-retina="` + path + `assets/img/profiles/contact-center.png" data-src="` + path + `assets/img/profiles/contact-center.png" alt="" src="` + path + `assets/img/profiles/contact-center.png">
                          </div>
                            <span class="bold">` + data.notification[i].group_code + `</span>
                            <span class="fs-12 m-l-10">` + data.notification[i].progress_fullname + `</span>
                          </a>
                          <div class="pull-right">
                            <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
                              <div><i class="fa fa-angle-left"></i>
                              </div>
                            </div>
                            <span class=" time">` + data.notification[i].progress_created_on + `</span>
                          </div>
                          <div class="more-details">
                            <div class="more-details-inner">
                              <h5 class="semi-bold fs-16">“` + data.notification[i].progress_message + `.”</h5>
                              <p class="small hint-text">
                                Commented on incidents <b><a onClick="is_read(` + data.notification[i].id + `)" href="` + baseurl + `incident/progress/` + data.notification[i].incident_id + `">` + data.notification[i].description + `.
                              </a></b></p>
                            </div>
                          </div>
                        </div>
                        ` + statment_read(data.notification[i].is_read) + `
                        
                          <a href="#" class="mark" onClick="is_read(` + data.notification[i].id + `)"></a>
                        </div>
                      </div>`
                    }


                    // if (data.notification[i].mention_to == groupcode) {
                        if (data.notification[i].is_read == '1') {
                            $('#notification').append(html);
                            // $('#notification-center').html(`<span> </span>`);
                            $('#footer-notification').html(`<a href="#" class="">notifications</a>
                              <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                                <i class="pg-refresh_new"></i>
                              </a>`);
                        } else {
                            $('#notification').append(html);
                            // $('#notification-center').html(`<span class="bubble" </span>`);
                            $('#footer-notification').html(`<a href="#" class="">notifications</a>
                              <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                                <i class="pg-refresh_new"></i>
                              </a>`);
                        }

                    // } else {
                    //     // $('#notification-center').html(`<span></span>`);
                    //     $('#footer-notification').html(`<a href="#" class="">none notifications</a>
                    //       <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                    //         <i class="pg-refresh_new"></i>
                    //       </a>`);
                    // }
                }
            }
        })

    }

});

function is_read(params) {
    $.ajax({
        url: baseurl + 'notification/is_read_progress',
        method: "POST",
        data: { id: params },
        dataType: 'JSON',
        success: function(data) {

          // buat di broadcast ke client
          Server.send( 'notification', data );

          // buat fungsi angka
          $('.count-token').empty();
          $('.count-token').html(data.total_not_read);

          // buat validasi siapa saja yang dapat menerima notifikasinya
          $('#notification').empty();
          $('#footer-notification').empty();
          
          for (var i = 0; i < data.notification.length; i++) {

            if(parseInt(data.notification[i].id) == params) {
              var html = `<div class="notification-item clearfix" id="unread">
              <div class="heading open">
                <a href="#" id="cs" class="text-danger pull-left toggle-more-details">
                  <div class="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
                  <img width="30" height="30" data-src-retina="` + path + `assets/img/profiles/contact-center.png" data-src="` + path + `assets/img/profiles/contact-center.png" alt="" src="` + path + `assets/img/profiles/contact-center.png">
                </div>
                  <span class="bold">` + data.notification[i].group_code + `</span>
                  <span class="fs-12 m-l-10">` + data.notification[i].progress_fullname + `</span>
                </a>
                <div class="pull-right">
                  <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
                    <div><i class="fa fa-angle-left"></i>
                    </div>
                  </div>
                  <span class=" time">` + data.notification[i].progress_created_on + `</span>
                </div>
                <div class="more-details">
                  <div class="more-details-inner">
                    <h5 class="semi-bold fs-16">“` + data.notification[i].progress_message + `.”</h5>
                    <p class="small hint-text">
                      Commented on incidents <b><a onClick="is_read(` + data.notification[i].id + `)" href="` + baseurl + `incident/progress/` + data.notification[i].incident_id + `">` + data.notification[i].description + `.
                    </a></b></p>
                  </div>
                </div>
              </div>
              ` + statment_read(data.notification[i].is_read) + `
              
                <a href="#" class="mark" onClick="is_read(` + data.notification[i].id + `)"></a>
              </div>
            </div>`
            } else {
              if (data.notification[i].is_read == '1') {
                var html = `<div class="notification-item clearfix" id="unread">
                <div class="heading">
                  <a href="#" id="cs" class="text-danger pull-left toggle-more-details">
                    <div class="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
                    <img width="30" height="30" data-src-retina="` + path + `assets/img/profiles/contact-center.png" data-src="` + path + `assets/img/profiles/contact-center.png" alt="" src="` + path + `assets/img/profiles/contact-center.png">
                  </div>
                    <span class="bold">` + data.notification[i].group_code + `</span>
                    <span class="fs-12 m-l-10">` + data.notification[i].progress_fullname + `</span>
                  </a>
                  <div class="pull-right">
                    <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
                      <div><i class="fa fa-angle-left"></i>
                      </div>
                    </div>
                    <span class=" time">` + data.notification[i].progress_created_on + `</span>
                  </div>
                  <div class="more-details">
                    <div class="more-details-inner">
                      <h5 class="semi-bold fs-16">“` + data.notification[i].progress_message + `.”</h5>
                      <p class="small hint-text">
                        Commented on incidents <b><a onClick="is_read(` + data.notification[i].id + `)" href="` + baseurl + `incident/progress/` + data.notification[i].incident_id + `">` + data.notification[i].description + `.
                      </a></b></p>
                    </div>
                  </div>
                </div>
                ` + statment_read(data.notification[i].is_read) + `
                
                  <a href="#" class="mark" onClick="is_read(` + data.notification[i].id + `)"></a>
                </div>
              </div>`
              } else {
                  var html = `<div class="notification-item unread clearfix" id="unread">
                  <div class="heading" onClick="is_read(` + data.notification[i].id + `)">
                    <a href="#" id="cs" class="text-danger pull-left toggle-more-details">
                      <div class="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
                      <img width="30" height="30" data-src-retina="` + path + `assets/img/profiles/contact-center.png" data-src="` + path + `assets/img/profiles/contact-center.png" alt="" src="` + path + `assets/img/profiles/contact-center.png">
                    </div>
                      <span class="bold">` + data.notification[i].group_code + `</span>
                      <span class="fs-12 m-l-10">` + data.notification[i].progress_fullname + `</span>
                    </a>
                    <div class="pull-right">
                      <div class="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
                        <div><i class="fa fa-angle-left"></i>
                        </div>
                      </div>
                      <span class=" time">` + data.notification[i].progress_created_on + `</span>
                    </div>
                    <div class="more-details">
                      <div class="more-details-inner">
                        <h5 class="semi-bold fs-16">“` + data.notification[i].progress_message + `.”</h5>
                        <p class="small hint-text">
                          Commented on incidents <b><a onClick="is_read(` + data.notification[i].id + `)" href="` + baseurl + `incident/progress/` + data.notification[i].incident_id + `">` + data.notification[i].description + `.
                        </a></b></p>
                      </div>
                    </div>
                  </div>
                  ` + statment_read(data.notification[i].is_read) + `
                  
                    <a href="#" class="mark" onClick="is_read(` + data.notification[i].id + `)"></a>
                  </div>
                </div>`
              }
            }

            // if (data.notification[i].mention_to == groupcode) {
              if (data.notification[i].is_read == '1') {
                  $('#notification').append(html);
                  $('#footer-notification').html(`<a href="#" class="">notifications</a>
                  <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                    <i class="pg-refresh_new"></i>
                  </a>`);
              } else {
                  $('#notification').append(html);
                  $('#footer-notification').html(`<a href="#" class="">notifications</a>
                  <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                    <i class="pg-refresh_new"></i>
                  </a>`);
              }
            // } else {
            //   $('#footer-notification').html(`<a href="#" class="">none notifications</a>
            //   <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
            //     <i class="pg-refresh_new"></i>
            //   </a>`);
            // }
          }
          
        }

    })

}

function statment_read(params) {
    if (params == '1') {
        // notifikasi sudah terbaca
        return `<div class="option" data-toggle="tooltip" data-placement="left" title="mark as read">`
    } else {
        // notifikasi belum terbaca
        return `<div class="option">`
    }
}