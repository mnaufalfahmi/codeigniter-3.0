$(document).ready(function() {
    $.fn.dataTable.ext.errMode = 'none';
    activeClass('ticket', 'open-ticket')

    var table = $('#open_ticket').DataTable({
        dom: 'Bfrtpi Rl',
        stateSave: true,
        scrollX: true,
        // fixedColumns:   {
        //     rightColumns: 1
        // },
        buttons: [{
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
                className: 'shadow'
            },
            {
                text: '<i class="fa fa-plus-circle"></i> Define Incident',
                className: 'btn btn-primary btn-xs hidden',
                attr: {
                    id: 'add-incident',
                    'data-target': '#modalAddIncident',
                    'data-toggle': "modal"

                },
                init: function(api, node, config) {
                    $(node).removeClass('dt-button')
                }
            }
        ],
        columnDefs: [{
                searchable: false,
                orderable: false,
                targets: 0
            },
            {
                targets: 1,
                visible: false
            }
        ],
        order: [
            [
                7, "desc"
            ]
        ],
        // 
        // processing: true,
        // serverSide: true,
        // responsive:true,
        // select: {
        //     style: 'single'
        //     },
        lengthChange: false,
        pageLength: 15,
        ajax: baseurl + '/ticket/list_open_ticket',

        columns: [{
                "data": "id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                    console.log(data)
                }
            },
            { "data": "ticket_id" },
            { "data": "ismilling_id" },
            { "data": "created_on" },
            { "data": "class" },
            {
                "data": "description",
                "render": function(data) {
                    if (data) {
                        return `<p title="` + data + `">` + data + `</p>`
                    }
                }
            },
            { "data": "service_id" },
            { "data": "username" },
            { "data": "bobot" },
            {
                render: function(data, type, row) {
                    if (create == 't') {
                        // || row.username != session_username
                        if (row.username === null || row.username === '') {
                            return `<button id='define-` + row.ticket_id + `' class='btn btn-danger btn-xs ml-1 actionDefine'>Define</button>
                 <button id='attach-` + row.ticket_id + `' class='btn btn-default btn-xs ml-1 actionAttach'>Attach</button>
                 <button id='analyze-` + row.ticket_id + `' class='btn btn-warning btn-xs ml-1 actionAnalyze' onClick='actionAnalyze("` + row.ticket_id + `")'>Analyze</button>
                 <button id='history-"` + row.service_id + `"' class='btn btn-success btn-xs btn-xs ml-1' onClick='actionHistory("` + row.service_id + `")'>History</button>`
                        } else {
                            if (row.incident_id === '' || row.incident_id === null) {
                                if (row.username !== session_username) {
                                    return `<button data-toggle="modal" data-target="#modalpopupNotUser" class='btn btn-danger btn-xs ml-1'>Define</button>
                        <button data-toggle="modal" data-target="#modalpopupNotUser" class='btn btn-default btn-xs ml-1'>Attach</button>
                        <button data-toggle="modal" data-target="#modalpopupNotUser" class='btn btn-warning btn-xs ml-1'>Analyze</button>
                        <button id='history-"` + row.service_id + `"' class='btn btn-success btn-xs btn-xs ml-1' onClick='actionHistory("` + row.service_id + `")'>History</button>
                        <button data-toggle="modal" data-target="#modalpopupNotUser" class='btn btn-danger btn-xs ml-1 actionUnlock'>Unlock</button>`
                                } else {
                                    return `<button id='define-` + row.ticket_id + `' class='btn btn-danger btn-xs ml-1 actionDefine'>Define</button>
                        <button id='attach-` + row.ticket_id + `' class='btn btn-default btn-xs ml-1 actionAttach'>Attach</button>
                        <button id='analyze-` + row.ticket_id + `' class='btn btn-warning btn-xs ml-1 actionAnalyze' onClick='actionAnalyze("` + row.ticket_id + `")'>Analyze</button>
                        <button id='history-"` + row.service_id + `"' class='btn btn-success btn-xs btn-xs ml-1' onClick='actionHistory("` + row.service_id + `")'>History</button>
                        <button id='unlock-` + row.ticket_id + `' class='btn btn-danger btn-xs ml-1 actionUnlock'>Unlock</button>`
                                }
                            } else {
                                if (row.incident_id_p == null) {
                                    return `<button id='history-"` + row.service_id + `"' class='btn btn-success btn-xs btn-xs ml-1' onClick='actionHistory("` + row.service_id + `")'>History</button>
                        <button id='history-"` + row.service_id + `"' class='btn btn-complete btn-xs btn-xs ml-1' onClick='actionProgress("` + row.incident_id_close + `")'>Progress</button>`
                                } else {
                                    // usernamenya ada dan incidentnya ada
                                    return `<button id='history-"` + row.service_id + `"' class='btn btn-success btn-xs btn-xs ml-1' onClick='actionHistory("` + row.service_id + `")'>History</button>
                        <button id='history-"` + row.service_id + `"' class='btn btn-complete btn-xs btn-xs ml-1' onClick='actionProgress("` + row.incident_id_p + `")'>Progress</button>`
                                }

                            }
                        }
                    }
                }
            },
        ],
        initComplete: function() {
            if (create == 't') {
                $("#add-incident").removeClass("hidden");
            }
        },
    })

    table.on('order.dt search.dt', function() {
        table.column(0, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();


    // double click row data
    $('#open_ticket tbody').on('dblclick', 'tr', function() {
        var data = table.row(this).data();
        ticketDetail(data.ticket_id)
            // alert(data.username)
            // actionAnalyze(data.ticket_id)
        if (create == 't') {
            if (data.username === null || data.username === '') {
                // data tickets baru yang dari crm
                updateLockBy(data.ticket_id)
                $('.btn-summary').hide();
            } else {
                if (data.incident_id === '' || data.incident_id === null) {
                    if (data.username !== session_username) {
                        $('.btn-summary').hide();
                    } else {
                        // data incident_id di table tickets = 0 atau null
                        // updateLockBy(data.ticket_id)  
                        $('.btn-summary').hide();
                    }

                } else {
                    // data sudah ada di table incidents 
                    $('.btn-summary').show();
                }
            }
        }

    });


    // action define incidents
    $('#open_ticket tbody').on('click', '.actionDefine', function() {

        var data = table.row($(this).parents('tr')).data();
        if (data.username === null || data.username === '') {
            // data tickets baru yang dari crm
            updateLockBy(data.ticket_id)
        } else {
            if (data.incident_id === '' || data.incident_id === null) {
                if (data.username !== session_username) {

                } else {
                    // data incident_id di table tickets = 0 atau null
                    // updateLockBy(data.ticket_id) 
                }

            } else {
                // data sudah ada di table incidents 
            }
        }
        $('#modalAddIncident').modal('show')
        $('#define_attach_ticket tr:last').after('\
        <tr class="data">\
        <td><button type="button" id="remove" class="btn btn-xs btn-danger remove" data-service="' + data.service_id + '" data-create="' + data.created_on + '" data-ticket="' + data.ticket_id + '" data-desc="' + data.description + '"><i class="fa fa-minus"></i></button></td>\
        <td>' + data.service_id + '</td>\
        <td>' + data.created_on + '</td>\
        <td class="attach" style="display:none;">' + data.ticket_id + '</td>\
        <td class="description" style="display:none;">' + data.description + '</td>\
        <td class="ismilling_id" style="display:none;">' + data.ismilling_id + '</td>\
        <td class="created_on" style="display:none;">' + data.created_on + '</td>\
        </tr>');
        $('#definedescription').val(data.description)

    })

    // action attach incidents
    $('#open_ticket tbody').on('click', '.actionAttach', function() {

        var data = table.row($(this).parents('tr')).data();
        if (data.username === null || data.username === '') {
            // data tickets baru yang dari crm
            updateLockBy(data.ticket_id)
        } else {
            if (data.incident_id === '' || data.incident_id === null) {
                if (data.username !== session_username) {

                } else {
                    // data incident_id di table tickets = 0 atau null
                    // updateLockBy(data.ticket_id)  
                }

            } else {
                // data sudah ada di table incidents 
            }
        }
        $('#modalTicketAttach').modal('show')
        $('#listAttachTickets tr:last').after('\
        <tr class="data">\
        <td><button type="button" id="remove" class="btn btn-xs btn-danger remove" data-service="' + data.service_id + '" data-create="' + data.created_on + '" data-ticket="' + data.ticket_id + '" data-desc="' + data.description + '"><i class="fa fa-minus"></i></button></td>\
        <td>' + data.service_id + '</td>\
        <td>' + data.created_on + '</td>\
        <td class="description">' + data.description + '</td>\
        <td class="attach" style="display:none;">' + data.ticket_id + '</td>\
        </tr>');

        // add table incidents
        var incidents = $('#listIncident').DataTable({
            dom: 'Bfrtpi',
            stateSave: true,
            // scrollX: true,
            buttons: [],
            select: {
                style: 'single'
            },
            responsive: true,
            lengthChange: false,
            pageLength: 15,
            ajax: baseurl + 'ticket/incident',

            columns: [
                { "data": "start_time" },
                { "data": "creator" },
                { "data": "description" }
            ]
        })

        $('#listIncident tbody ').on('click', 'tr', function() {
            var data = incidents.row(this).data();

            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $("input[name = 'incident']").val(data.incident_id)
            }
        });
    });

    // action analyze
    $('#open_ticket tbody').on('click', '.actionAnalyze', function() {
        var data = table.row($(this).parents('tr')).data();
        if (data.username === null || data.username === '') {
            // data tickets baru yang dari crm
            updateLockBy(data.ticket_id)
        } else {
            if (data.incident_id === '' || data.incident_id === null) {
                if (data.username !== session_username) {

                } else {
                    // data incident_id di table tickets = 0 atau null
                    // updateLockBy(data.ticket_id)  
                }

            } else {
                // data sudah ada di table incidents 
            }
        }
    });

    // action unlock tickets
    $('#open_ticket tbody').on('click', '.actionUnlock', function() {

        var data = table.row($(this).parents('tr')).data();
        if (data.username === null || data.username === '') {
            // data tickets baru yang dari crm
            // updateLockBy(data.ticket_id)
        } else {
            if (data.incident_id === '' || data.incident_id === null) {
                if (data.username !== session_username) {

                } else {
                    // updateLockBy(data.ticket_id) 
                    actionUnlock(data.ticket_id)
                }

            } else {
                // data sudah ada di table incidents 
            }
        }
    });

})

// auto reload new data tickets
setInterval(function() {
    $('#open_ticket').DataTable().ajax.reload();
}, 60000);



// func lock_by tickets
function updateLockBy(id) {
    axios.get(baseurl + 'ticket/updateLockBy?ticket_id=' + id).then(function(response) {
            response = response.data
            $('#open_ticket').DataTable().ajax.reload();
        })
        .catch(function(error) {
            console.log(error)
        });
}

// func action unlock
function actionUnlock(ticket_id) {

    $('#modalpopupUnlock').modal('show')
    $('#yesUnlock').on('click', function() {
        axios.get(baseurl + 'ticket/updateUnLockBy?ticket_id=' + ticket_id).then(function(response) {
                response = response.data
                $('#modalpopupUnlock').modal('hide')
                $('#open_ticket').DataTable().ajax.reload();
            })
            .catch(function(error) {
                console.log(error)
            });
    })
    $('#noUnlock').on('click', function() {
        $('#modalpopupUnlock').modal('hide')
    })
}

// func tickets duoble click
function ticketDetail(id) {

    $("#modalTicketDetail").modal("show")
    axios.get(baseurl + 'ticket/get_ticket_detail?ticket_id=' + id).then(function(response) {
            response = response.data
            $('#open_ticket').DataTable().ajax.reload();
            $("#ticket_id").val(response.ticket_id);
            $("#ismilling_id").val(response.ismilling_id);
            $("#service_id").val(response.service_id);
            $("#open_time").val(response.create_ticket);
            $("#attach_time").val(response.attach_time);
            $("#description").val(response.desc_ticket);


            var frameElement = document.getElementById("frameIsmilling");
            frameElement.src = `http://10.14.22.211:85/fmsapi/getservice?sid=` + response.service_id;

            // summary incident
            $("#mod_row_incidentId").val(response.incident_id);
            $("#mod_row_creator").val(response.creator);
            $("#mod_row_notificationTrigger").val(response.notification_trigger);
            $("#mod_row_state").val(response.status);
            $("#mod_row_clasification").val(response.classification_name);
            $("#mod_row_rootCause").val(response.root_cause_name);
            $("#mod_row_rootCauseDetail").val(response.root_cause_detail_name);
            $("#mod_row_description").val(response.description);
            $("#mod_row_hirarchy").val(response.hierarchy);
            $("#mod_row_link").val(response.link);
            $("#mod_row_link_start_region").val(response.link_start_region);
            $("#mod_row_link_end_region").val(response.link_end_region);
            $("#mod_row_link_start_site").val(response.link_start_site);
            $("#mod_row_link_end_site").val(response.link_end_site);
            $("#mod_row_region").val(response.region_name);
            $("#mod_row_site").val(response.site);
            $("#mod_source_category").val(response.source_category);
            $("#mod_row_creationTime").val(response.creation_time);
            $("#mod_row_duration").val(fn_time(response.duration));
            $("#mod_row_stopLock").val(fn_time(response.total_stop_clock));
            $("#mod_row_ttr").val(response.ttr);

        })
        .catch(function(error) {
            console.log(error)
        });

    // func in tickets detail for Tickets Progress
    $(document).ready(function() {

        var table = $('#ticket_progress').DataTable({
            dom: 'Rl',
            destroy: true,
            buttons: [

            ],
            order: [
                [
                    0, "desc"
                ]
            ],
            responsive: true,
            lengthChange: false,
            pageLength: 5,
            ajax: {
                url: progress,
                cache: true,
                data: function(data) {
                    data.ticket_id = id;
                }
            },
            columns: [
                { "data": "owner" },
                { "data": "timestamp" },
                {
                    "data": "description_progress",
                    "render": function(data) {
                        if (data) {
                            return `<p title="` + data + `">` + data + `</p>`
                        }
                    }
                }
            ]
        });

        var table2 = $('#detail_incident_ticket').DataTable({
            dom: 'Rl',
            destroy: true,
            buttons: [

            ],
            order: [
                [
                    0, "asc"
                ]
            ],
            responsive: true,
            lengthChange: false,
            pageLength: 5,
            ajax: baseurl + 'incident/get_ticket_incident?id=495',
            columns: [{
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "service_id" },
                { "data": "ticket_id" },
                { "data": "open_time" },
                { "data": "description" },
                {
                    render: function(data, type, row, meta) {
                        return "<button type='button' class='btn btn-xs btn-warning rounded-0 ml-1 btn-ismiling' data-service_id=" + row.service_id + "><i class='fa fa-file'></i></button>" +
                            "<button type='button' class='btn btn-xs btn-complete rounded-0 ml-1 btn-progress' data-ticket_id='" + row.ticket_id + "'><i class='fa fa-share'></i></button>"
                    }
                }
            ]
        });
    })
}

// ticket progress
function dt_ticket_progress(ticket_id) {
    var table_ticket = $('#ticket_progress2').DataTable({
        dom: 'Rl',
        responsive: true,
        lengthChange: false,
        pageLength: 10,
        destroy: true,
        columnDefs: [{
                "targets": [0], // your case first column
                "className": "text-center",
            },
            {
                "targets": '_all',
                "createdCell": function(td, cellData, rowData, row, col) {
                    $(td).css('padding', '3px');
                }
            }
        ],
        ajax: baseurl + "ticket/get_ticket_progress?ticket_id=" + ticket_id,
        columns: [{
                "data": "ticket_id",
                render: function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "username" },
            { "data": "timestamp" },
            { "data": "description_progress" }
        ]
    });
}

// get crm by service id
$(document).on('click', '.btn-ismiling', function() {

    $('#modalCrm').modal('show');

    var service_id = $(this).attr('data-service_id');
    var frameElement = document.getElementById("frameIsmillingSummary");

    frameElement.src = `http://10.14.22.211:85/fmsapi/getservice?sid=` + service_id;

});

// get ticket progress
$(document).on('click', '.btn-progress', function() {

    var ticket_id = $(this).attr('data-ticket_id');
    dt_ticket_progress(ticket_id);

    $('#modalTicketProgress').modal('show');

});

// func action define
function actionDefine(id) {
    axios.get(baseurl + 'ticket/get_ticket_detail?ticket_id=' + id).then(function(response) {
            response = response.data
        })
        .catch(function(error) {
            console.log(error)
        });
}

// func action attach
function actionAttach(id) {
    $('#modalTicketAttach').modal('show')
}

// func action analyze
function actionAnalyze(id) {
    $('#modalTicketAnalyze').modal('show')
    axios.get(baseurl + 'ticket/get_ticket_detail?ticket_id=' + id).then(function(response) {
            response = response.data
            $("#description_analyze").val(response.desc_ticket);

            var frameElementAnalyze = document.getElementById("frameIsmilling-Analyze");
            frameElementAnalyze.src = `http://10.14.22.211:85/fmsapi/getservice?sid=` + response.service_id;

            $(document).ready(function() {

                var table = $('#analyze_possible').DataTable({
                    dom: 'Rl',
                    stateSave: true,
                    buttons: [

                    ],
                    responsive: true,
                    lengthChange: false,
                    pageLength: 15,
                    ajax: baseurl + 'ticket/analyze',
                    columns: [

                        //     { render: function ( data, type, row ) {
                        //         return "<i class='fa fa-search' aria-hidden='true'></i>"
                        //     }
                        // },
                        { "data": "classification_name" },
                        { "data": "root_cause_name" },
                        { "data": "description" },
                        {
                            "data": "percentase",
                            "render": function(data) {
                                if (data) {
                                    return `<p title="` + data + `">` + data + `%</p>`
                                }
                            }
                        },
                        {
                            render: function(data, type, row) {
                                return "<button type='button' class='btn btn-danger btn-xs ml-1 Defaultdefine'>Define</button>"
                            }
                        }
                    ]
                })

                // one click row (selected)
                $('#analyze_possible tbody').on('click', '.Defaultdefine', function() {

                    var data = table.row($(this).parents('tr')).data();
                    updateLockBy(data.ticket_id)
                    $('#modalAddIncident').modal('show')
                    $('#select_classification').val(data.classification_id).trigger('change');
                    $('#select_root_cause').val(data.root_cause_id).trigger('change');
                    $('#region1').val(data.region_code).trigger('change');
                    $('#link_start_region').val(data.region_code).trigger('change');
                })

            })

        })
        .catch(function(error) {
            console.log(error)
        });
}


// func action history
function actionHistory(service_id) {
    window.location.href = baseurl + 'ticket/close_ticket?service_id=' + service_id;
    var data = { service_id: service_id };
    $.ajax({
        url: baseurl + 'ticket/close_ticket',
        type: "POST",
        data: data,
        success: function(data) {}
    });

}

function actionProgress(id) {
    window.location.href = baseurl + 'incident/progress/' + id;
}


// func in Analyze Ticket part related andop
$(document).ready(function() {

    // var table = $('#related_andop').DataTable({
    //     dom: 'Rl',
    //     buttons: [

    //     ],
    //     responsive:true,
    //     lengthChange: false,
    //     pageLength:5,
    //     ajax: related_andop,
    //     columns: [

    //     { "data": "title" },
    //     { "data": "execution"},
    //     { "data": "downtime"},
    //     { "data": "location"},
    //     { "data": "creator"}

    //     ]
    // })
})

// func create timestamp for define incidents
function getDateTime() {
    var now = new Date();
    var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    if (month.toString().length == 1) {
        month = '0' + month;
    }
    if (day.toString().length == 1) {
        day = '0' + day;
    }
    if (hour.toString().length == 1) {
        hour = '0' + hour;
    }
    if (minute.toString().length == 1) {
        minute = '0' + minute;
    }
    if (second.toString().length == 1) {
        second = '0' + second;
    }
    var dateTime = day + '/' + month + '/' + year + ' ' + hour + ':' + minute + ':' + second;
    return dateTime;
}
$('#ok').on('click', function() {
        window.location.reload(true);
    })
    // example usage: realtime clock
setInterval(function() {
    currentTime = getDateTime();
    document.getElementById("timestamp").value = currentTime;
}, 1000);

$('.form_save').submit(function(e) {
    e.preventDefault();
    progressbar(true, 'progressbar_show');
    // return false;

    var arr_tiket = [];
    var arr_description = [];
    var arr_ismilling_id = [];
    var arr_created_on = [];

    // alert($('.form_save').find("select[name = 'region1']").val());
    // return false;
    $('#define_attach_ticket .attach').each(function() {
        var txt = $(this).text();
        arr_tiket.push(txt);
    });

    $('#define_attach_ticket .description').each(function() {
        var txt = $(this).text();
        arr_description.push(txt);
    });

    $('#define_attach_ticket .ismilling_id').each(function() {
        var txt = $(this).text();
        arr_ismilling_id.push(txt);
    });

    $('#define_attach_ticket .created_on').each(function() {
        var txt = $(this).text();
        arr_created_on.push(txt);
    });

    var data = {
        user_id: $('.form_save').find("input[name = 'user_id']").val(),
        timestamp: $('.form_save').find("input[name = 'timestamp']").val(),
        region1: $('.form_save').find("select[name = 'region1']").val(),
        site_name1: $('.form_save').find("select[name = 'site_name1']").val(),
        select_classification: $('.form_save').find("select[name = 'select_classification']").val(),
        select_root_cause: $('.form_save').find("select[name = 'select_root_cause']").val(),
        select_root_cause_detail: $('.form_save').find("select[name = 'select_root_cause_detail']").val(),
        network: $('.form_save').find("input[name = 'network']:checked").val(),
        priority: $('.form_save').find("input[name = 'priority']:checked").val(),
        description: $('.form_save').find("textarea[name = 'description']").val(),
        link: $('.form_save').find("select[name = 'link']").val(),
        brand1: $('.form_save').find("select[name = 'brand1']").val(),
        me1: $('.form_save').find("select[name = 'me1']").val(),
        deleted: $('.form_save').find("input[name = 'deleted']").val(),
        ticket_id: $('.form_save').find("input[name = 'ticket_id']").val(),
        creator: $('.form_save').find("input[name = 'creator']").val(),

        // tambah form input baru 19/08/2019
        link_start_region: $('.form_save').find("select[name = 'link_start_region']").val(),
        link_end_region: $('.form_save').find("select[name = 'link_end_region']").val(),
        link_start_site: $('.form_save').find("select[name = 'link_start_site']").val(),
        link_end_site: $('.form_save').find("select[name = 'link_end_site']").val(),
        source_category_input: $('.form_save').find("input[name = 'source_category_input']").val(),
        broken_fo: $('.form_save').find("input[name = 'broken_fo']").val(),

        arr_ismilling_id: arr_ismilling_id,
        arr_data: arr_tiket,
        arr_description: arr_description,
        arr_created_on: arr_created_on
    };

    $.ajax({
        url: baseurl + 'ticket/defineIncident',
        type: "POST",
        data: data,
        success: function(data) {
            removed();
            window.location.href = baseurl + 'incident/open_incident';
        },
        error: function(xhr, status, errorThrown) {
            progressbar(false, 'ajax_handling');
        }
    });
});

$('.form_attach').submit(function(e) {
    e.preventDefault();
    progressbar(true, 'progressbar_show');

    var arr_tiket = [];
    var arr_description = [];
    var incident_id = $("input[name = 'incident']").val();
    $('#listAttachTickets .attach').each(function() {
        var txt = $(this).text();
        arr_tiket.push(txt);
    });

    $('#listAttachTickets .description').each(function() {
        var txt = $(this).text();
        arr_description.push(txt);
    });


    var data = {
        ticket_id: $('.form_attach').find("input[name = 'ticket_id']").val(),
        arr_data_incident: $("input[name = 'incident']").val(),
        arr_data_ticket: arr_tiket,
        arr_data_description: arr_description
    };

    if (incident_id == '') {
        $('#modalpopupIncident').modal('show');
    } else {
        $.ajax({
            url: baseurl + 'ticket/updateIncidents',
            type: "POST",
            data: data,
            success: function(data) {
                alert('success')
                window.location.href = baseurl + 'incident/open_incident/';
            },
            error: function(xhr, status, errorThrown) {
                progressbar(false, 'ajax_handling');
            }
        });
    }

});

function hideRow(event) {
    $(event.target || event.srcElement).parents('tr').hide();
}

function removeRow(event) {
    $(event.target || event.srcElement).parents('tr').remove();
}

function showRow(event) {
    $(event.target || event.srcElement).parents('tr').show();
}



// func list table attach other tickets
$(document).ready(function() {
    $('#attachotherticket').on('click', function() {

        var table = $('#add_attach_ticket').DataTable({
            dom: 'Bfrtpi',
            select: {
                style: 'multi'
            },
            buttons: [

            ],
            // searching: false,
            // paging:   false,
            // ordering: false,
            // info:     false
            responsive: true,
            lengthChange: false,
            pageLength: 10,
            ajax: baseurl + 'ticket/list_attach',
            columns: [{
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                        console.log(data)
                    }
                },
                { "data": "created_on" },
                { "data": "description" },
                { "data": "service_id" },
                { "data": "ismilling_id" },
            ]
        })
        $('#add_attach_ticket tbody ').on('click', 'tr', function() {
            var data = table.row(this).data();

            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                // alert('no selected')
            } else {
                $('#define_attach_ticket tr:last').after('\
                <tr class="data">\
                <td><button type="button" id="remove" class="btn btn-xs btn-danger remove" data-service="' + data.service_id + '" data-create="' + data.created_on + '" data-ticket="' + data.ticket_id + '" data-desc="' + data.description + '"><i class="fa fa-minus"></i></button></td>\
                <td>' + data.service_id + '</td>\
                <td>' + data.created_on + '</td>\
                <td class="attach" style="display:none;">' + data.ticket_id + '</td>\
                <td class="description" style="display:none;">' + data.description + '</td>\
                <td class="ismilling_id" style="display:none;">' + data.ismilling_id + '</td>\
                <td class="created_on" style="display:none;">' + data.created_on + '</td>\
                </tr>');
            }

        });
        // $('#add_attach_ticket').DataTable().ajax.reload();
    })


    // func show and hide in classification of define incidents
    $('#select_classification').on('change', function() {
        if (this.value) {

            $('#region1').val(null).trigger('change');
            $('#link_start_region').val(null).trigger('change');
            $('#link_end_region').val(null).trigger('change');
            $('#site_name1').empty();
            $('#brand1').empty();
            $('#me1').empty();
            $('#link_start_site').empty();
            $('#link_end_site').empty();
            $('#select_root_cause_detail').select2('val', [0]);

        }

        if (this.value == '151' || this.value == '121' || this.value == '171' || this.value == '161' || this.value == '661' || this.value == '131' || this.value == '411' || this.value == '666') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOT")
            $('#source_category_input').val("FOT")
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }
        if (this.value == '251' || this.value == '241' || this.value == '141') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("PS")
            $('#source_category_input').val("PS")
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }
        if (this.value == '111') {
            $('#1').hide()
            $('#3').show()
            $('#11').show()
            $('#foc-show').show()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOC")
            $('#source_category_input').val("FOC")
            $('#div_link_upstream_isp').hide()
            $('#div_region').hide()
            $('#div_site_name').hide()
            $('#broken_fo').val('1')
        }
        if (this.value == '381' || this.value == '361' || this.value == '888') {
            $('#1').hide()
            $('#3').hide()
            $('#11').show()
            $('#foc-show').hide()
                // $('#foc-hide').hide()
            $('#source_category').hide()
            $('#div_link_upstream_isp').show()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }


        if (this.value == '999') {
            $('#1').hide()
            $('#3').hide()
            $('#11').hide()
            $('#foc-show').hide()
                // $('#foc-hide').show()
            $('#source_category').hide()
            $('#div_link_upstream_isp').hide()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }
        if (this.value == '221') {
            $('#1').hide()
            $('#3').show()
            $('#11').show()
            $('#foc-show').show()
                // $('#foc-hide').hide()
            $('#source_category').show()
            $('#source_category').text("FOC")
            $('#source_category_input').val("FOC")
            $('#div_link_upstream_isp').hide()
            $('#div_region').hide()
            $('#div_site_name').hide()
            $('#broken_fo').val('1')
        }
    })

    $('#link_upstream_isp').on('change', function() {

        if (this.value) {
            $('#region1').val(null).trigger('change');
            $('#region').val(null).trigger('change');
            $('#link_start_region').val(null).trigger('change');
            $('#link_end_region').val(null).trigger('change');
            $('#site_name1').empty();
            $('#site_name').empty();
            $('#brand').empty();
            $('#me').empty();
            $('#link_start_site').empty();
            $('#link_end_site').empty();
        }

        // kondisi jika pilih link upstream ISP bagian source category 
        if (this.value == '1') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("FOT")
            $('#foc-hide').show()
            $('#foc-show').hide()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }

        if (this.value == '2') {
            $('#1').hide()
            $('#3').show()
            $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("FOC")
            $('#foc-hide').hide()
            $('#foc-show').show()
            $('#div_region').hide()
            $('#div_site_name').hide()
            $('#broken_fo').val('1')
        }

        if (this.value == '3') {
            $('#1').show()
            $('#3').hide()
            $('#11').show()
            $('#source_category').hide()
            $('#source_category_input').val("PS")
            $('#foc-hide').show()
            $('#foc-show').hide()
            $('#div_region').show()
            $('#div_site_name').show()
            $('#broken_fo').val(null)
        }


    })

})

$(document).on('click', '.remove', function() {
    $(this).parents('tr').remove();
})

function removed() {
    // default define incident
    $('#define_attach_ticket .data').after().remove();
    $('#select_classification').select2('val', [0]);
    $('#select_root_cause').select2('val', [0]);
    $('#select_root_cause_detail').select2('val', [0]);
    $('#link').select2('val', [0]);
    $('#region1').select2('val', [0]);
    $('#site_name1').select2('val', [0]);
    $('#link_start_region').select2('val', [0]);
    $('#link_start_site').select2('val', [0]);
    $('#link_end_region').select2('val', [0]);
    $('#link_end_site').select2('val', [0]);
    $('#link_upstream_isp').select2('val', [0]);
    $('#brand1').select2('val', [0]);
    $('#me1').select2('val', [0]);
    $('#broken_fo').val(null);
    $('#definedescription').val(null);

    $('#div_region').hide();
    $('#div_site_name').show();
    $('#foc-show').hide();
    $('#11').show();
    $('#source_category').hide();
    $('#div_link_upstream_isp').hide();
    $('#link_upstream_isp').hide();
    $('#1').hide();
    $('#3').hide();
}

function removedAttach() {
    $('#listAttachTickets .data').after().remove();
}

// select2 chained
$(document).ready(function() {

    $("#select_root_cause").remoteChained({
        parents: "#select_classification",
        url: baseurl + "ticket/load_root_cause"
    });

    $("#select_root_cause_detail").remoteChained({
        parents: "#select_root_cause",
        url: baseurl + "ticket/load_root_cause_detail"
    });

    $("#site_name1").remoteChained({
        parents: "#region1",
        url: baseurl + "ticket/load_incident_regions1"
    });

    $("#link_start_site").remoteChained({
        parents: "#link_start_region",
        url: baseurl + "ticket/load_incident_regions_start"
    });

    $("#link_end_site").remoteChained({
        parents: "#link_end_region",
        url: baseurl + "ticket/load_incident_regions_end"
    });

    $("#brand1").remoteChained({
        parents: "#site_name1",
        url: baseurl + "ticket/load_brands"
    });

    $("#me1").remoteChained({
        parents: "#brand1",
        url: baseurl + "ticket/load_me"
    });
})

// func list table attach other tickets
$(document).ready(function() {
    $('#attachIncident').on('click', function() {
        var table = $('#addAttachTickets').DataTable({
            dom: 'Bfrtpi',
            buttons: [],
            select: {
                style: 'multi'
            },
            responsive: true,
            lengthChange: false,
            pageLength: 10,
            ajax: baseurl + 'ticket/list_attach',
            columns: [{
                    "data": "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                        console.log(data)
                    }
                },
                { "data": "created_on" },
                { "data": "description" },
                { "data": "service_id" }
            ]
        })

        $('#addAttachTickets tbody ').on('click', 'tr', function() {
            var data = table.row(this).data();

            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                // alert('sadis')
            } else {
                $('#listAttachTickets tr:last').after('\
            <tr class="data">\
            <td><button type="button" id="remove" class="btn btn-xs btn-danger remove" data-service="' + data.service_id + '" data-create="' + data.created_on + '" data-ticket="' + data.ticket_id + '" data-desc="' + data.description + '"><i class="fa fa-minus"></i></button></td>\
            <td>' + data.service_id + '</td>\
            <td>' + data.created_on + '</td>\
            <td class="description">' + data.description + '</td>\
            <td class="attach" style="display:none;">' + data.ticket_id + '</td>\
            </tr>');
            }

        });
    })
});


// select2 option
$(document).ready(function() {

    $('#select_classification').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#select_root_cause').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#select_root_cause_detail').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#link').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#site_name1').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#region1').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#link_start_region').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#link_start_site').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#link_end_region').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#link_end_site').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#link_upstream_isp').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#brand1').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('#me1').select2({
        allowClear: true,
        dropdownParent: $('#modalAddIncident')
    });

    $('.btn-summary').on('click', function() {
        $('#modalLogDetail').modal('show')

    })

})