$(document).ready(function(){
    $.fn.dataTable.ext.errMode = 'none';
    activeClass('ticket', 'close-ticket')
    date()

    var table = $('#close_ticket').DataTable({
        dom: '<"toolbar"> Bfrtpi Rl',
        stateSave: true,
        scrollX: true,
        cache: false,
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="fa fa-table"></i>',
            }
        ],
        responsive:true,
        lengthChange: false,
        pageLength:15,
        columnDefs: [ {
            searchable: false,
            orderable: false,
            targets: 0
        } ],
        initComplete: function () {
            if (create == 't') {
                $("#filter").removeClass("hidden");
            }
        },
    })

    $("div.toolbar").html('&nbsp;<button class="btn btn-primary" id="filter" data-target="#modalFilter" data-toggle="modal"><i class="fa fa-filter"></i></button>');

    $('#close_ticket tbody').on( 'dblclick', 'tr', function () {
        var data = table.row( this ).data();
        ticketDetail(data[1]);
    });

    // set to fix number 
    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    
if(service_id !== 0){
    var no = 1;
    $.ajax({
        url: baseurl + 'ticket/history?service_id='+service_id,
        type: 'GET',
        dataType: 'JSON',
        async: true,
        success: function(data){
            table.clear().draw();

            $.each(data, function(i,item){
                table.row.add( [
                    (i+no),
                    item.ticket_id,
                    item.created_on,
                    item.class,
                    item.description,
                    item.service_id
                ] ).draw( false );
            })

        }
    });
}

})

function ticketDetail(id)
{
    $("#modalTicketDetail").modal("show")
    $('.btn-summary').hide();
    axios.get(baseurl+'ticket/get_ticket_detail?ticket_id='+id).then(function (response) {
        response = response.data
        // $('#open_ticket').DataTable().ajax.reload();
        $("#ticket_id").val(response.ticket_id);
        $("#ismilling_id").val(response.ismilling_id);
        $("#service_id").val(response.service_id);
        $("#open_time").val(response.created_on);
        $("#attach_time").val(response.attach_time);
        $("#description").val(response.description);

        var frameElement = document.getElementById("frameIsmilling");
        frameElement.src = `http://10.14.22.211:85/fmsapi/getservice?sid=`+response.service_id;
    })
    .catch(function (error) {
        console.log(error)
    });

    // func in tickets detail for Tickets Progress
    $(document).ready(function(){

        var table2 = $('#ticket_progress').DataTable({
            dom: 'Rl',
            destroy: true,
            buttons: [

            ],
            order: [
                [
                    0, "desc"
                ]
            ],
            responsive:true,
            lengthChange: false,
            pageLength:5,
            ajax: 
            {
                url : baseurl +'ticket/get_ticket_progress',
                cache: true,
                data: function(data) {
                 data.ticket_id = id;
             }
         },
         columns: [
         { "data": "owner" },
         { "data": "timestamp"},
         { "data": "description_progress",
          "render" : function(data)
            {
                if (data)
                {
                    return `<p title="`+data+`">`+data+`</p>`
                }
            }
        }
         ]
     })
    })
}

function date()
{
    // config datepicker
    $('#datepicker-start').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#datepicker-end').datepicker({
        format: 'yyyy-mm-dd'
    });
}





$('.form_filter').submit(function(e){
    e.preventDefault();
    var t  = $('#close_ticket').DataTable();
    var no = 1;

    $.ajax({
        url: baseurl+'ticket/list_close_ticket',
        type: "POST",
        data: new FormData(this),
        processData: false,
        contentType: false,
        cache: false,
        async: false,
        dataType : 'JSON',
        success: function (data) {
            $('#modalFilter').modal('hide');
            t.clear().draw();

            $.each(data, function(i,item){
                t.row.add( [
                    (i+no),
                    item.created_on,
                    item.ticket_id,
                    item.ismilling_id,
                    item.service_id,
                    item.class,
                    item.description,
                    `<a href="`+baseurl + 'incident/progress/' + item.incidentid+`" target="_blank" class='btn btn-complete btn-xs btn-xs ml-1'>Progress</a>`
                ] ).draw( false );
            })
            
        }
    })
})


