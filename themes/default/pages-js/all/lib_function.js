// function for andop
function removed_form() {
    $('#andop_type').select2('val', [0]);
    $('.form_save').find("input[name = 'andop_title']").val(null);
    $('.form_save').find("textarea[name = 'andop_description']").val(null);
    $('.form_save').find("input[name = 'andop_duration_hours']").val(0);
    $('.form_save').find("input[name = 'andop_duration_minutes']").val(0);
    $('.form_save').find("input[name = 'andop_downtime_hours']").val(0);
    $('.form_save').find("input[name = 'andop_downtime_minutes']").val(0);
    $('#andop_location').select2('val', [0]);
    $('.form_save').find("input[name = 'andop_severity']").prop('checked', false);

    $('.fs-r').remove();
    $('#andop_name_field_support').val(null);
    $('#andop_mobile_field_support').val(null);

    $('.fa-r').remove();
    $('#andop_file_attachment').val(null);

    $('#affected_services .data').after().remove();
}

function number_null(params) {
    var data = (params == null || params == '') ? "" : params;
    return data;
}

function progressbar(params, error) {
    if (params == false && error == 'ajax_handling') {
        $('#modalprogressbar').modal('hide');
        $('#modalerror').modal('show');
    } else if (params == true && error == 'progressbar_show') {
        $('#modalprogressbar').modal('show');
    } else if (params == false && error == 'progressbar_hide') {
        $('#modalprogressbar').modal('hide');
    }
}

function statment_time_progress(params) {
    var date;
    if (params == null || params == '') {
        date = `00:00:00`
    } else {
        date = params;
    }
    return date;
}

function time_client()
{
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    return(date +' '+time)
}

function getTimezoneName() {
    const today = new Date();
    const short = today.toLocaleDateString(undefined);
    const full = today.toLocaleDateString(undefined, { timeZoneName: 'short' });
  
    // Trying to remove date from the string in a locale-agnostic way
    const shortIndex = full.indexOf(short);
    if (shortIndex >= 0) {
      const trimmed = full.substring(0, shortIndex) + full.substring(shortIndex + short.length);
      
      // by this time `trimmed` should be the timezone's name with some punctuation -
      // trim it from both sides
      return trimmed.replace(/^[\s,.\-:;]+|[\s,.\-:;]+$/g, '');
  
    } else {
      // in some magic case when short representation of date is not present in the long one, just return the long one as a fallback, since it should contain the timezone's name
      return full;
    }
  }

function date()
{
    // config datepicker
    $('#datepicker-start').datepicker({
        format: 'yyyy-mm-dd',
    });

    $('#datepicker-end').datepicker({
        format: 'yyyy-mm-dd'
    });
}

// function opacity modal if 2 modal
$(document).on('show.bs.modal', '.modal', function(event) {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

function allowAlphaNumericSpace(e) {
    var code = ('charCode' in e) ? e.charCode : e.keyCode;
    if (!(code == 95) && // underscore
      !(code > 47 && code < 58) && // numeric (0-9)
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      e.preventDefault();
    }
}

// $(function () {
//     $("input").keydown(function () {
//         // Save old value.
//         if (!$(this).val() || (parseInt($(this).val()) <= 11 && parseInt($(this).val()) >= 0))
//         $(this).data("old", $(this).val());
//     });
//     $("input").keyup(function () {
//         // Check correct, else revert back to old value.
//         if (!$(this).val() || (parseInt($(this).val()) <= 11 && parseInt($(this).val()) >= 0))
//         ;
//         else
//         $(this).val($(this).data("old"));
//     });
//     });